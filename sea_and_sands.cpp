#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator

using namespace std;
typedef long long LL;

int main(){
	int n,k;
	cin >> n >> k;
	if (k > (n%2==0?n*n/2:(n*n+1)/2)){
		cout << "NO" << endl;
		return 0;
	}
	cout << "YES" << endl;
	for (int i=0;i<n;i++){
		for (int j=0;j<n;j++){
			if (k && (i+j)%2==0){
				cout << 'L';
				k--;
			}else{
				cout << 'S';
			}
			
		}
		cout<< endl;
	}
	return 0;
}

