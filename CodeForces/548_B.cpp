#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int a[505][505],n,m,q;
int sc[505];
int main(){
	cin >>n >> m >> q;
	FOR (i,0,n){
		sc[i]=0;
		int t = 0;
		FOR (j,0,m){
			cin >> a[i][j];
			if (a[i][j]){
				t++;
				sc[i] = max(sc[i],t);
			}else{
				t = 0;
			}
		}
	}
	FOR (i,0,q){
		int x,y;
		cin >> x >> y;
		x--;y--;
		a[x][y] = 1 - a[x][y];
		// recalc
		int t = 0;
		sc[x]=0;
		FOR (j,0,m){
			if (a[x][j]){
				t++;
				sc[x] = max(sc[x],t);
			}else{
				t = 0;
			}
		}
		t = 0;
		FOR (j,0,n){
			t = max(t,sc[j]);
		}
		cout << t << endl;
	}
	return 0;
}

