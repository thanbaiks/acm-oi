#include <bits/stdc++.h>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int dp[2][1000007];
LL h[2],a[2],x[2],y[2],m;
ULL c[2],q[2];

bool try_(int i){
	int next;
	LL hh = h[i];
	dp[i][h[i]]=1;
	while (h[i]!=a[i]){
		next = (h[i]*x[i]+y[i])%m;
		if (dp[i][next]!=0){
			// entered loop
			return false;
		}
		dp[i][next] = dp[i][h[i]]+1;
		h[i] = next;
	}
	q[i]=dp[i][a[i]]-1;
	next = (h[i]*x[i]+y[i])%m;
	c[i] = 1;
	while (dp[i][next] == 0){
		c[i]++;
		next = (next*x[i]+y[i])%m;
	}
	c[i] += dp[i][a[i]]-dp[i][next];
	return true;
}
int main(){
	//freopen("input.txt","r",stdin);
	memset(dp,0,sizeof(dp));
	cin >> m;
	FOR (i,0,2){
		cin >> h[i] >> a[i] >> x[i] >> y[i];
	}
	if (!try_(0) || !try_(1)){
		cout << -1 << endl;
		return 0;
	}
	while (q[0]!=q[1]){
		if (q[0]<q[1])
			q[0]+=c[0];
		else
			q[1]+=c[1];
	}
	cout << q[0] << endl;
	return 0;
}

