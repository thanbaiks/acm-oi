#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
char kb[][20]={"qwertyuiop","asdfghjkl;","zxcvbnm,./"};
char leftof[256],rightof[256],dir,str[101],len;
void buildfix(){
	for (int i=0;i<3;i++){
		int len = strlen(kb[i]);
		for (int j=0;j<len-1;j++){
			leftof[kb[i][j+1]]=kb[i][j];
			rightof[kb[i][j]]=kb[i][j+1];
		}
	}
}
int main(){
	ifstream fin("A.txt");
	buildfix();
	inp >> dir >> str;
	len = strlen(str);
	for (int i=0;i<len;i++){
		if (dir=='R'){
			str[i]=leftof[str[i]];
		}else{
			str[i]=rightof[str[i]];
		}
	}
	cout << str << endl;
	return 0;
}
