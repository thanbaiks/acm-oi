#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define ZERO 0.00001
#define inp cin

using namespace std;
int n;
long moles[4][4];
int nummv;

double dist(long long x1,long long y1,long long x2, long long y2){
	return sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
}
bool equal(double d1,double d2){
	return (abs(d1-d2)<ZERO);
}
bool check(){
	// check for square
	double d[6];
	int k=0;
	for (int i=0;i<3;i++){
		for (int j=i+1;j<4;j++){
			d[k++]=dist(moles[i][0],moles[i][1],moles[j][0],moles[j][1]);
		}
	}
	sort(d,d+6);
	if (d[0]==0 || d[0]!=d[1] || d[0]!=d[2] || d[0]!=d[3]){
		return false;
	}
	if (d[0]==0)
		return false;
	if (!equal(d[0],d[1]) || !equal(d[0],d[2]) || !equal(d[0],d[3]) || !equal(d[4],d[5]))
		return false;
	if (!equal(d[4]/d[0],sqrt(2))){
		return false;
	}
	
	return true;
}

int move(int i){
	//i: id of the mole to move
	
	moles[i][0]-=moles[i][2];
	moles[i][1]-=moles[i][3];
	
	long tmp = moles[i][0];
	moles[i][0]=-moles[i][1];
	moles[i][1]=tmp;
	
	moles[i][0]+=moles[i][2];
	moles[i][1]+=moles[i][3];
}

void try_(int mi,int count){
	for (int i=0;i<4;i++){
		if (mi==3 && check()){
			nummv=min(nummv,count + i);
		}else if (mi<3){
			try_(mi+1,count+i);
		}
		move(mi);
	}
}
int main(){
	ifstream fin("C.txt");
	inp >> n;
	for (int ti=0;ti<n;ti++){
		// input
		for (int i=0;i<4;i++){
			for (int j=0;j<4;j++)
				inp >> moles[i][j];
		}
		nummv = 9999;
		try_(0,0);
		if (nummv==9999)
			nummv=-1;
		cout << nummv << endl;
	}
	return 0;
}
