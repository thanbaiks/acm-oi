#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp fin
#define MAXN 100002

using namespace std;

typedef unsigned long long ULL;
typedef unsigned long UL;
typedef pair<ULL,UL> Pillar;

ULL n,d,x,mx;
UL pre[MAXN],cnt[MAXN];
vector<Pillar> h;

int main(){
	ifstream fin("E.txt");
	inp >> n >> d;
	h.clear();
	for (UL i=0;i<n;i++){
		inp >> x;
		h.push_back(make_pair(x,i));
	}
	mx=0;
	memset(pre,0,sizeof(pre));
	memset(cnt,0,sizeof(cnt));
	// end of input
	
	sort(h.begin(),h.end());
	
	for (UL i=0;i<n;i++){
		cout << '(' << h[i].first << ';' << h[i].second << ") ";
	}
	return 0;
}
