#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#define inp cin

using namespace std;
long n,m;
long piles[100002],x,cur;
long p1[100002],p2[100002];
map<long,long> mp;
int main(){
	ifstream fin("b.txt");
	inp >> n;
	for (long i=1;i<=n;i++){
		inp >> x;
		piles[i]=piles[i-1]+x;
	}
	inp >> m;
	for (long i=0;i<m;i++){
		inp >> p1[i];
		p2[i]=p1[i];
	}
	sort(p1,p1+m);
	cur = 0,x = 0;
	while (cur<m){
		if (p1[cur]>piles[x]){
			x++;
		}else{
			mp[p1[cur]]=x;
			cur++;
		}
	}
	for (long i=0;i<m;i++){
		cout << mp[p2[i]] << endl;
	}
	return 0;
}
