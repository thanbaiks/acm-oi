#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <map>
#include <algorithm>
#define inp cin

using namespace std;
typedef long long LL;
LL ccu,cnt;
map<string,bool> mmap;
int main(){
	ifstream fin("5_A.TXT");
	ccu = cnt = 0;
	string s,ss;
	while (getline(inp,s)){
		if (s[0] == '+'){
			// add cmd
			ss = s.substr(1,s.length()-1);
			mmap[ss] = true;
			ccu++;
			//cout << "Added " << ss << endl;
		}else if (s[0]=='-'){
			// rmv cmt
			ss = s.substr(1,s.length()-1);
			mmap[ss] = false;
			ccu--;
			//cout << "Removed " << ss << endl;
		}else{
			// chat cmd
			int pos = s.find(':');
			cnt += (s.length() - pos - 1)*ccu;
			//cout << "Chat = " << cnt << endl;
		}
	}
	cout << cnt << endl;
	fin.close();
	return 0;
}

