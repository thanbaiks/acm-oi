#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
typedef long long LL;
LL a,m;
bool mark[100001];
int main(){
	inp >> a >> m;
	memset(mark,0,sizeof(mark));
	a%=m;
	mark[a]=true;
	while (true){
		a = (a*2)%m;
		if (a==0){
			cout << "Yes";
			break;
		}
		if (mark[a]){
			cout << "No";
			break;
		}
		mark[a]=true;
	}
	return 0;
}

