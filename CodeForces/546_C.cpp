#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;
int n,k1,k2,x,cnt;
vector<int> v1,v2;
int main(){
	//freopen("input.txt","r",stdin);
	cin >> n;
	cin >> k1;
	FOR(i,0,k1){
		cin >> x;
		v1.push_back(x);
	}
	cin >> k2;
	FOR(i,0,k2){
		cin >> x;
		v2.push_back(x);
	}
	cnt = 0;
	while (v1.size() && v2.size()){
		if (v1[0]<v2[0]){
			v2.push_back(v1[0]);
			v2.push_back(v2[0]);
			v1.erase(v1.begin());
			v2.erase(v2.begin());
		}else{
			v1.push_back(v2[0]);
			v1.push_back(v1[0]);
			v1.erase(v1.begin());
			v2.erase(v2.begin());
		}
		cnt++;
		if (cnt >= 1000000)
			break;;
	}
	if (cnt >= 1000000)
		cout << -1 << endl;
	else
		cout << cnt << ' ' << (v1.size()?1:2) << endl;
	return 0;
}

