#include <iostream>
#include <cstring>
using namespace std;

int main(){
	char	names[1000][33],tmp[33];
	long long	scores[1000];
	long long	max,n,num,i,j,k,winner;
	bool	f;
	cin >> n;cin.ignore();
	i = 0;num = 0; max = 0;winner = -1;
	for (i=0;i<n;i++){
		cin >> tmp >> k;
		cin.ignore();
		f = false;
		for (j = 0;j<num;j++){
			if (strcmp(tmp,names[j])==0){
				// found that name
				f = true;
				scores[j] += k;
				break;
			}
		}
		if (!f){
			scores[j] = k;
			strcpy(names[j],tmp);
			num++;
		}
		if (scores[j] > max){
			max = scores[j];
			winner = j;
		}
	}
	cout << names[winner];
}
