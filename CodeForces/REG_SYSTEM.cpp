#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
typedef long long LL;
struct Node{
	Node* sub[26];
	char val;
	LL cnt;
	Node(char c){
		val = c;
		memset(sub,0,sizeof(sub));
		cnt = 0;
	}
};
LL n;
string s;
int main(){
	ifstream fin("REG_SYSTEM.TXT");
	inp >> n;
	Node* root = new Node(0);
	for (LL j=0;j<n;j++){
		inp >> s;
		Node* cur = root;
		for (int i=0;i<s.length();i++){
			int ch = s[i]-'a';
			Node* next = cur->sub[ch];
			if (next == NULL){
				next = new Node(ch);
				cur->sub[ch]=next;
			}
			cur=next;
			if (i==s.length()-1){
				if (cur->cnt==0){
					// available
					cur->cnt = 1;
					cout << "OK";
				}else{
					// already have
					cout << s << cur->cnt++;
				}
			}
		}
		if (j<n-1)
			cout << endl;
	}
	return 0;
}

