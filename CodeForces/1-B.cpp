#include <iostream>
#include <cstring>
#define		MAX_LENGTH 100
using namespace std;

int main(){
	long n,c,r,i,j;
	char s[MAX_LENGTH],ss[MAX_LENGTH],temp;
	
	while (cin >> s){
		n--;
		for (i = 0;i<strlen(s) && s[i]>='A'&&s[i]<='Z';i++);
		for (;i<strlen(s)&&s[i]>='0'&&s[i]<='9';i++);
		r=c=0;
		if (i==strlen(s)){
			// method 1
			for (i=0;i<strlen(s);i++)
				if (s[i]>='A'&&s[i]<='Z'){
					c*=26;
					c+=s[i]-'A'+1;
				}else{
					r*=10;
					r+=s[i]-'0';
				}
			cout << "R" << r << "C" << c<<endl;
		}else{
			for (i=1;s[i]>='0' && s[i]<='9';i++){
				r*=10;
				r+=s[i]-'0';
			}
			i++;
			for (;i<strlen(s);i++){
				c*=10;
				c+=s[i]-'0';
			}
			i = 0;
			while (c>0){
				temp = 'A'+ (c-1)%26;
				c = (c-1)/26;
				ss[i++]=temp;
			}
			i--;
			while (i>=0)
				cout << ss[i--];
			cout << r << endl;
		}
		
	}
	return 0;
}
