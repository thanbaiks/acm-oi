#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <stack>
#include <algorithm>
#define inp cin

using namespace std;
typedef long long LL;

string s;
stack<pair<LL,LL> > st;
LL mx,mxc;
int main(){
	ifstream fin("5_C.txt");
	inp >> s;
	mx = mxc = 0;
	LL len = s.length();
	LL j=0;
	for (LL i=0;i<len;i++){
		char c = s[i];
		if (c==')'){
			if (st.empty()){
				// rong
				j=0;
				continue;
			}
			else{
				pair<LL,LL> p = st.top();
				st.pop();
				j = i-p.first+1+p.second;
				if (j>mx){
					mx = j;
					mxc = 1;
				}else if (j==mx)
					mxc++;
			}
		}else{
			pair<LL,LL> p = make_pair(i,j);
			st.push(p);
			j=0;
		}
	}
	if (!mx)
		mxc = 1;
	cout << mx << ' ' << mxc;
	fin.close();
	return 0;
}

