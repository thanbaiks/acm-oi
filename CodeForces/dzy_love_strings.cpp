#include <iostream>
#include <cstring>

using namespace std;
int main(){
	char	s[1000];
	long long i,k,w[26],max;
	long long ret;
	cin >> s >> k;
	max = 0;
	for (i=0;i<26;i++){
		cin >> w[i];
		if (w[i]>max)max=w[i];
	}
	ret = 0;
	for (i=0;i<strlen(s);i++)
		ret += w[s[i]-'a']*(i+1);
	while (k--)
		ret += max*++i;
	cout << ret;
	return 0;
}
