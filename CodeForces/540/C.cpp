#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <algorithm>
#define MAXSIZE 501
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;

char a[MAXSIZE][MAXSIZE];
bool finish(int r,int c){
	for (int i=-1;i<=1;i++)
		for (int j=-1;j<=1;j++)
			if (abs(i)+abs(j)==1 &&  r+i>=0 && c+j >= 0 && a[r+i][c+j]=='.')
				return true;
	return false;
}
bool dfs(int r,int c, int rr, int cc){
	if (r==rr && c == cc)
		return true;
	if (r < 0 || c < 0 || a[r][c] != '.')
		return false;
	a[r][c] = 'X';
	for (int i=-1;i<=1;i++)
		for (int j=-1;j<=1;j++)
			if (abs(i)+abs(j)==1 && dfs(r+i,c+j,rr,cc)){
				return true;
			}
	return false;
}
int main(){
	int n,m,r1,c1,r2,c2;
	freopen("C.TXT","r",stdin);
	scanf("%d %d",&n,&m);
	gets(a[0]);
	for (int i=0;i<n;i++){
		gets(a[i]);
	}
	scanf("%d%d%d%d",&r1,&c1,&r2,&c2);
	r1--;c1--;r2--;c2--;
	a[r1][c1] = '.';
	if (dfs(r1,c1,r2,c2) && (a[r2][c2]=='X' || finish(r2,c2)))
		printf("YES");
	else
		printf("NO");
	puts("");
	/*
	for (int i=0;i<n;i++)
		puts(a[i]);
	*/
}


