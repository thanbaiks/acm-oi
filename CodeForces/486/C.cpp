#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin
using namespace std;
typedef long long LL;
int n,p,r,l;
LL tot;
int main(){
	ifstream fin("C.TXT");
	inp >> n >> p;
	char s[n+1];
	inp >> s;
	p--;
	p = min(p,n-1-p);
	for (r=n/2-1;r>=0;r--){
		if (s[r]!=s[n-1-r])
			break;
	}
	for (l=0;l<n/2;l++){
		if (s[l]!=s[n-1-l])
			break;
	}
	if (r<0){
		// complety equal
		cout << 0 << endl;
		return 0;
	}
	tot = 0;
	// find sortest path
	if (abs(p-l)<abs(p-r)){
		// from ltr
		tot += abs(p-l);
		for (p=l;p<=r;p++){
			tot++;
			int x = abs(s[p]-s[n-1-p]);
			tot += min(x,26-x);
		}
	}else{
		// otherwise
		tot += abs(p-r);
		for (p=r;p>=l;p--){
			tot++;
			int x = abs(s[p]-s[n-1-p]);
			tot += min(x,26-x);
		}
	}
	tot--;
	cout << tot << endl;
	return 0;
}

