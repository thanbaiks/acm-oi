#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin
using namespace std;
typedef long long LL;

int mat[101][101],m,n,numcol,numrow;

int main(){
	ifstream fin("B.txt");
	inp >> m >> n;
	memset(mat,0,sizeof(mat));
	for (int i=1;i<=m;i++){
		for (int j=1;j<=n;j++){
			inp >> mat[i][j];
			if (mat[i][j]){
				mat[i][0]++;
				mat[0][j]++;
			}
		}
	}
	bool f = true;
	// clear table
	for (int i=1;i<=m;i++){
		for (int j=1;j<=n;j++){
			if (mat[i][0]==n || mat[0][j]==m)
				mat[i][j]=0;
			if (mat[i][j]==1)
				f = false;
				break;
		}
	}
	if (!f){
		cout << "NO";
	}else{
		cout << "YES" << endl;
		for (int i=1;i<=m;i++){
			for (int j=1;j<=n;j++){
				if (mat[i][0]==n && mat[0][j]==m)
					cout << 1;
				else
					cout << 0;
				cout << ' ';
			}
			cout << endl;
		}
	}
	return 0;
}

