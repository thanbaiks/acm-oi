#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define MAXN 100001
using namespace std;
typedef long long LL;

int n,cnt;
LL x[MAXN],h[MAXN];
char s[MAXN];
int main(){
	//freopen("545_C.txt","r",stdin);
	cin >> n;
	for (int i=0;i<n;i++){
		cin >> x[i] >> h[i];
		s[i]=0;
	}
	cnt = 0;
	// do tat ca cac cay ve ben trai
	for (int i=0;i<n;i++){
		if (i==0 || x[i]-x[i-1]>h[i]){
			cnt++;
			s[i] = 'L';
		}
	}
	// do tat cac cac cay ve ben phai
	if (s[n-1]==0){
		cnt++;
	}
	s[n-1] = 'R';
	
	for (int i=n-2;i>=0;i--){
		if (s[i]==0){
			// i dang dung
			// thu do i sang ben phai
			if (x[i+1]-x[i]>h[i]){
				// do duoc
				cnt++;
				s[i] = 'R';
				if (s[i+1]=='L'&&x[i+1]-x[i]<=h[i]+h[i+1]){
					// dung cay ben phai len
					cnt--;
					s[i+1] = 0;
				}
			}
		}
	}
	cout << cnt << endl;
	return 0;
}

