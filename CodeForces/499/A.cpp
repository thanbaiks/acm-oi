#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
int n,x;
int a[100];
int main(){
	scanf("%d%d",&n,&x);
	for (int i=0;i<n;i++){
		scanf("%d%d",a+2*i,a+2*i+1);
	}
	sort(a,a+2*n);
	int j = 1;
	int tot = 0;
	for (int i=0;i<n;i++){
		int k = (a[2*i]-j)/x;
		j+=k*x;
		tot += a[2*i+1]-j+1;
		j = a[2*i+1]+1;
	}
	printf("%d",tot);
}


