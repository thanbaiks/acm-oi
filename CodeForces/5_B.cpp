#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
typedef long long LL;
vector<string> vt;
string s;
LL len;
bool b;
int main(){
	ifstream fin("5_B.TXT");
	len = 0;
	while (getline(inp,s)){
		len = max(len,(LL)s.length());
		vt.push_back(s);
	}
	for (int i=0;i<len+2;i++)
		cout << '*';
	cout << endl;
	b = false;// move to left
	for (LL i=0;i<vt.size();i++){
		int k = vt[i].length();
		int l = len-k;
		if (l%2==0){
			l /= 2;
		}else{
			l /= 2;
			if (b)
				l++;
			b = !b;
		}
		cout << '*';
		for (int j=0;j<l;j++)
			cout << ' ';
		cout << vt[i];
		for (int j=0;j<len-l-k;j++)
			cout << ' ';
		cout << '*' << endl;
	}
	for (int i=0;i<len+2;i++)
		cout << '*';
	fin.close();
	return 0;
}

