#include <iostream>
#include <cmath>
#define max(a,b) ((a)>(b)?(a):(b))
using namespace std;
int main(){
	char c[2][3];
	cin >> c[0];
	cin >> c[1];
	cout << max(abs(c[0][0]-c[1][0]),abs(c[0][1]-c[1][1])) << endl;
	while (c[0][0]!=c[1][0] || c[0][1] != c[1][1]){
		if (c[0][0] < c[1][0]){
			cout << "R";
			c[0][0]++;
		}else if (c[0][0] > c[1][0]){
			cout << "L";
			c[0][0]--;
		}
		if (c[0][1] < c[1][1]){
			cout << "U";
			c[0][1]++;
		}else if (c[0][1] > c[1][1]){
			cout << "D";
			c[0][1]--;
		}
		cout << endl;
	}
}

