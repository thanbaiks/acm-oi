#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
typedef long long LL;
LL n,xmin,xmax,ymin,ymax,x,y;

int main(){
	ifstream fin("R276_D2_B.TXT");
	inp >> n;
	inp >> x >> y;
	xmax = xmin = x;
	ymax = ymin = y;
	for (LL i=1;i<n;i++){
		inp >> x >> y;
		xmin = min(xmin,x);
		xmax = max(xmax,x);
		ymin = min(ymin,y);
		ymax = max(ymax,y);
	}
	x = max((xmax-xmin),(ymax-ymin));
	cout << x*x;
	fin.close();
	return 0;
}

