#include <stdio.h>
#include "stdlib.h"
#include <string>
#include <iostream>

using namespace std;


void print(char board[3][3]);
char didwin(char board[3][3]);
void AImove(char board[3][3], int *rows, int *columns);
int MinMax(char board[3][3], int player);
int main(void)
{
    int i, i2, player, rows=0,columns=0;
    char board[3][3], tempboard[3][3], winner;
    string name;
    printf("The computer will now play tic-tac-tow with you.\n");
    printf("The computer will use \"O\" and you will use \"X\".\n");
    printf("You will go first. To begin, please enter your name: ");
    getline(cin,name);
    while(true)
    {
        for(i=0;i<3;i++)
        {
            for(i2=0;i2<3;i2++)
            {
                board[i][i2]='\0';
            }
        }
        print(board);
        for(i=0;i<9&&winner==0;i++)
        {
            printf("\n");
            player=i%2+1;
            if(player==1)
            {
                cout << "\n" << name << ", what row is your square in?\t";
                rows=GetInteger()-1;
                printf("\n%s, what column is your square in?\t", name);
                columns=GetInteger()-1;
            }
            else if(player==2)
            {
                printf("\nThe computer will move now.\n");
                for(i=0;i<3;i++)
                {
                    for(i2=0;i2<3;i2++)
                    {
                        tempboard[i][i2]=board[i][i2];
                    }
                }
                AImove(tempboard, &rows, &columns);
            }
            if(rows<0||rows>3||columns<0||columns>3||board[rows][columns]=='X'||board[rows][columns]=='O')
            {
                if(player==1)
                {
                    printf("The space is already taken or out of bounds, please try again");
                }
                i--;
            }
            else
            {
                if(player==1) board[rows][columns]='X';
                else board[rows][columns]='O';
                print(board);
            }
            winner = didwin(board);
        }
        if(winner!='\0')
        {
            if(winner=='X') printf("\n\nYou won!");
            else printf("\n\nThe computer won! Try harder next time.");
            break;

        }
        else
        {
            printf("No winner this round. Try again.");
            break;
        }
    }
}

void print(char board[3][3])
{
    int i,i2;
    printf("\n");
    for(i=0;i<3;i++)
    {
        for(i2=0;i2<3;i2++)
        {
            if(board[i][i2]=='\0')
            {
                printf("   ");
            }
            else
            {
                printf(" %c ", board[i][i2]);
            }
            if(i2<2) printf("|");
        }
        if(i<2)
        {
            printf("\n-----------\n");
        }
    }
}

char didwin(char board[3][3])
{
    int i,i2;
    char temp;
    char winner = '\0';

    for(i=0;i<3;i++)
    {
        temp=board[i][0];
        for(i2=0;i2<3;i2++)
        {
            if(board[i][i2]!=temp)
            {
                temp='\0';
            }
        }
        if(temp!='\0')
        {
            winner=temp;
        }
    }

    for(i=0;i<3;i++)
    {
        temp=board[0][i];
        for(i2=0;i2<3;i2++)
        {
            if(board[i2][i]!=temp)
            {
                temp='\0';
            }
        }
        if(temp!='\0')
        {
            winner=temp;
        }
    }

    temp=board[0][0];
    for(i=0;i<3;i++)
    {
        if(board[i][i]!=temp)
        {
            temp='\0';
        }
    }
    if(temp!='\0')
    {
        winner=temp;
    }

    temp=board[0][2];
    for(i=0;i<3;i++)
    {
        if(board[i][2-i]!= temp)
        {
            temp='\0';
        }
    }
    if(temp!='\0')
    {
        winner=temp;
    }
    return winner;
}

void AImove(char tempboard[3][3], int *rows,  int *columns)
{
    int points=-1, temppoints, i, i2;

    for(i=0;i<3;i++)
    {
        for(i2=0;i2<3;i2++)
        {
            if(tempboard[i][i2]=='\0')
            {
                tempboard[i][i2]='O';
                temppoints=MinMax(tempboard, 1);
                if(temppoints>points)
                {
                    points=temppoints;
                    *rows=i;
                    *columns=i2;
                }
                tempboard[i][i2]='\0';
            }
        }
    }
}

int MinMax(char tempboard[3][3], int player)
{
    int winner, points, temppoints, i, i2;
    winner=didwin(tempboard);
    if(winner=='X') return(0);
    else if (winner=='O') return(999);
    else
    {
        for(i=0;i<3;i++)
        {
            for(i2=0;i2<3;i2++)
            {
                if(tempboard[i][i2]='\0')
                {
                    if (player==1)
                    {
                        tempboard[i][i2]='X';
                    }
                    else
                    {
                        tempboard[i][i2]='O';
                    }
                    if (player==1)
                    {
                        temppoints=MinMax(tempboard, 2);
                    }
                    else
                    {
                        temppoints=MinMax(tempboard, 1);
                    }
                    if(temppoints>points)
                    {
                        points=temppoints;
                    }
                    tempboard[i][i2]='\0';
                }
            }
        }
    }
    return(points);
}
