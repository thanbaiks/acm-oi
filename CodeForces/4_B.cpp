#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
LL d,sum,mn[30],sumx,sumn,sum2,mx[30],sp[30];
int main(){
	ifstream fin("4_B.txt");
	inp >> d >> sum;
	sumn=sumx=sum2=0;
	for (LL i=0;i<d;i++){
		inp >> mn[i] >> mx[i];
		sumn += mn[i];
		sumx += mx[i];
		sp[i]=mn[i];
		sum2 += mn[i];
	}
	if (sum>=sumn && sum<=sumx){
		cout << "YES" << endl;
		int i = 0;
		while (sum2<sum){
			if(sp[i]<mx[i])
				sp[i]++,sum2++;
			else
				i++;
		}
		for (int i=0;i<d;i++)
			cout << sp[i] << ' ';
   		cout << endl;
	}
	else
		cout << "NO";
	fin.close();
	return 0;
}

