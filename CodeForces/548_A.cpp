#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

string s;
int k;
bool pal(int p){
	int i = p*(s.length()/k);
	int j = (p+1)*(s.length()/k)-1;
	while (i<j){
		if (s[i]!=s[j])
			return false;
		i++;j--;
	}
	return true;
}
int main(){
	cin >> s;
	cin >> k;
	if (s.length()%k!=0){
		cout << "NO";
		return 0;
	}
	FOR (i,0,k){
		if (!pal(i)){
			cout << "NO";
			return 0;
		}
	}
	cout << "YES";
	return 0;
}

