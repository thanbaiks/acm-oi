#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int main(){
	int n,x;
	vector<int> v;
	cin >> n;
	for (int i=0;i<n;i++){
		bool f = false;
		for (int j=0;j<n;j++){
			cin >> x;
			if (x==3 || x==1){
				f = true;
			}
		}
		if (!f){
			v.push_back(i+1);
		}
	}
	cout << v.size() << endl;
	for (int i=0;i<v.size();i++){
		cout << v[i] << ' ';
	}
	if (!v.empty())
		cout << endl;
	return 0;
}

