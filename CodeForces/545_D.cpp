#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator

using namespace std;
typedef long long LL;

int n,x,cnt;
vector<int> v;
LL l;
int main(){
	cin >> n;
	v.clear();
	for (int i=0;i<n;i++){
		cin >> x;
		v.push_back(x);
	}
	sort(v.begin(),v.end());
	l = 0; cnt = 0;
	for (int i=0;i<n;i++){
		if (l<=v[i]){
			cnt++;
			l+=v[i];
		}
	}
	cout << cnt;
	return 0;
}

