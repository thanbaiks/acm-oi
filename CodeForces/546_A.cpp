#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;
int k,n,w;
LL cost;
int main(){
	cin >> k >> n >> w;
	cost = k*w*(w+1)/2;
	if (cost > n){
		cout << cost - n << endl;
	}else{
		cout << 0 << endl;
	}
	return 0;
}

