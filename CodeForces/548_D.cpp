#include <stdio.h>

#define N 200000
int a[N+1] = {0};
int max[N] = {0};

int r = 0;
void f(int l)
{
    int i, j;

    i = r;
    do {
        while (a[++r] == a[i]);
        if (a[r] > a[i])
            f(r);
    } while (a[r] >= a[i]);

    for (j = r-l-1; j >= 0 && max[j] < a[i]; j--)
        max[j] = a[i];

    if (a[r] && (!l || a[l-1] < a[r]))
        f(l);
}

int main(int argc, char *argv[])
{
    int n;
    int i;

    scanf("%d", &n);
    for (i = 0; i < n; i++)
        scanf("%d", &a[i]);

    f(0);
    for (i = 0; i < n; i++)
        printf(i ? " %d" : "%d", max[i]);
    puts("");

    return 0;
}
