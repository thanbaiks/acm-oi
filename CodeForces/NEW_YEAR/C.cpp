#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin
#define MAXN 300001
using namespace std;
typedef long long LL;
LL n,x;
vector<pair <long,long> > b;
bool cmp(const pair<long,long> &p1,const pair<long,long> &p2){
	return p1.second < p2.second;
}
int main(){
	ifstream fin("C.TXT");
	inp >> n;
	for (int i=0;i<n;i++){
		inp >> x;
		b.push_back(make_pair(i+1,x));
	}
	sort(b.begin(),b.end(),cmp);
	x = b[0].second;
	for (int i=1;i<n;i++){
		x = max(x+1,(LL)b[i].second);
		b[i].second = x;
	}
	sort(b.begin(),b.end());
	for (int i=0;i<n;i++){
		cout << b[i].second << ' ';
	}
	fin.close();
	return 0;
}

