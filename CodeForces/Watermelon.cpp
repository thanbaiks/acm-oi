#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
typedef unsigned long long ULL;
int main(){
	int n;
	inp >> n;
	if (n>2 && n%2==0)
		cout << "YES";
	else
		cout << "NO";
	return 0;
}

