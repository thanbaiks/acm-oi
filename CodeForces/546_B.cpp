#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)
#define MAXN 3001
using namespace std;
typedef long long LL;
int n,x,cnt;
bool arr[2*MAXN];
vector<int> vi;
int main(){
	//freopen("input.txt","r",stdin);
	cin >> n;
	memset(arr,0,sizeof(arr));
	FOR (i,0,n){
		cin >> x;
		if (arr[x]){
			vi.push_back(x);
		}else{
			arr[x] = true;
		}
	}
	sort(vi.begin(),vi.end());
	int j=0;
	cnt = 0;
	FOR(i,0,vi.size()){
		j = max(vi[i],j);
		while (arr[j]){
			j++;
		}
		arr[j]=true;
		cnt+=j-vi[i];
	}
	cout << cnt << endl;
	return 0;
}

