#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
typedef long long LL;
LL n,m,l,r,x,xx,y;
int main(){
	ifstream fin("R276_D2_C.TXT");
	inp >> n;
	x = 1;
	for (int i=0;i<60;i++)
		x *= 2;
	xx = x;
	for (int i=0;i<n;i++){
		inp >> l >> r;
		x = xx;
		m = 0;
		if (l==r){
			cout << l << endl;
			continue;
		}
		// find first differ bit
		while (x > 0){
			if ((r&x)==(l&x)){
				// all two have same bit, fill bit
				m |= l&x;
				x /= 2;
			}else{
				//cout << (r&x) << ' ' << (l&x) << endl;
				break;
			}
		}
		y = x/2;
		// check if r is perfect
		bool f = true;
		while (y>0)
			if ((r&y)==0){
				f=false;
				break;
			}else
				y/=2;
		if (f){
			//its perfect
			while (x>0){
				m |= r&x;
				x/=2;
			}
		}else{
			x/=2;
			while (x>0){
				m |= x;
				x /=2;
			}
		}
		cout << m << endl;
	}
	fin.close();
	return 0;
}

