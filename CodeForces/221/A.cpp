#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;

typedef unsigned long long ULL;

ULL lft,rgt;
long long cmp;
string str;
int main(){
	ifstream fin("A.txt");
	inp >> str;
	ULL mid = str.find('^');
	lft = rgt = 0;
	for (long long i=mid-1, len = 1; i >= 0;i--,len++){
		if (str[i]=='=')
			continue;
		lft+=len*(str[i]-'0');
	}
	for (long long i = mid+1,len=1;i<str.length();i++,len++){
		if (str[i]=='=')
			continue;
		rgt+=len*(str[i]-'0');
	}
	long long cmp = lft-rgt;
	//cout << cmp << endl;
	if (cmp==0)
		cout << "balance"<< endl;
	else if(cmp<0)
		cout << "right"<< endl;
	else if (cmp>0)
		cout << "left"<< endl;
	return 0;
}
