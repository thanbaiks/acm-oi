#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp fin

using namespace std;
typedef unsigned long long ULL;

ULL n,m;
char mat[101][101];

int main(){
	ifstream fin("B.txt");
	inp >> n >> m;
	memset(mat,0,sizeof(mat));
	ULL a,b,c;
	for (ULL i=0;i<m;i++){
		inp >> a >> b >> c;
		mat[a][b]=c;
	}
	for (int k=1;k<=n;k++){
		for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			if (mat[i][k] && mat[k][j]){
				if (mat[i][k]>=mat[k][j]){
					mat[i][j]+=mat[k][j];
					mat[i][k]-=mat[k][j];
					mat[k][j]=0;
				}else{
					mat[i][k]+=mat[k][j]-mat[i][k];
					mat[k][j]-=mat[i][k];
				}
			}
	}
	ULL sum = 0;
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			sum+=mat[i][j];
		}
	}
	cout << sum << endl;
	return 0;
}
