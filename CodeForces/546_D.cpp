#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)
#define MAXN 5000017
using namespace std;
typedef long long LL;

int t[MAXN];
int p[MAXN];
vector<int> primes;
LL dp[MAXN];

void build(){
	// prime build
	memset(p,0,sizeof(p));
	memset(dp,0,sizeof(dp));
	memset(t,0,sizeof(t));
	FOR (i,2,MAXN){
		if (!p[i]){
			primes.push_back(i);
			p[i]=i;
		}
		for (int j=0;j<primes.size() && primes[j] <= p[i] &&  i*primes[j]<MAXN;j++)
			p[i*primes[j]]=primes[j];
	}
	int k = 0;
	// build table
	FOR (i,2,MAXN){
		while (primes[k] < i && k < primes.size()-1){
			// find next prime number
			k++;
		}
		if (i==primes[k]){
			// i is prime
			t[i]=1;
			dp[i]=dp[i-1]+1;
			continue;
		}
		FOR (j,0,k+1){
			if (i%primes[j]==0){
				t[i] = t[primes[j]]+t[i/primes[j]];
				dp[i] = dp[i-1]+t[i];
				break;
			}
		}
		if (!t[i]){
			cout << i << ' ' << primes[k]<< endl;
			break;
		}
	}
}
int main(){
	int n,a,b;
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	build();
	cin >> n;
	FOR (i,0,n){
		cin >> a >> b;
		cout << dp[a] - dp[b] << endl;
	}
	return 0;
}

