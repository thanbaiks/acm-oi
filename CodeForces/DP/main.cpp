#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
typedef unsigned long long ULL;
long m,n,arr[100],mn;

int main(){
    ifstream fin("337-A.inp");
    inp >> n >> m;
    for (int i=0;i<m;i++){
        inp >> arr[i];
    }
    sort(arr,arr+m);
    mn = 999999;
    for (int i=n-1;i<m;i++){
        mn = min(mn,arr[i]-arr[i+1-n]);
    }
    cout << mn;
	return 0;
}
