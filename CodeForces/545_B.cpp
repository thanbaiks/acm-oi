#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator

using namespace std;
typedef long long LL;

int main(){
	string s[2];
	int j;
	cin >> s[0] >> s[1];
	j = 0;
	
	for (int i=0;i<s[0].length();i++){
		if (s[0][i]!=s[1][i]){
			if (j)
				s[0][i] = s[1][i];
			j = (j+1)%2;
		}
	}
	if (!j){
		cout << s[0];
	}else{
		cout << "impossible";
	}
	return 0;
}

