#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
typedef unsigned long long ULL;
struct Env{
	int w,h,id,dp,pre;
};

vector<Env> v;
int n,w,h,mx,cur;
Env card;
bool operator< (const Env &a,const Env &b){
	return a.w < b.w || (a.w == b.w && a.h < b.h);
}
void print(int cid){
	if (cid<0)
		return;
	print(v[cid].pre);
	cout << v[cid].id << ' ';
}
int main(){
	ifstream fin("4_D.txt");
	inp >> n >> card.w >> card.h;
	for (int i=0;i<n;i++){
		Env e;
		inp >> e.w >> e.h;
		e.dp = 0;
		e.id = i+1;
		e.pre = -1;
		v.push_back(e);
	}
	mx = 0;
	cur = -1;
	sort(v.begin(),v.end());
	for (int i=0;i<n;i++){
		if (v[i].w>card.w && v[i].h>card.h)
			v[i].dp=1;// itself
		else
			continue;
		//cout << 'C' << i << "= " << v[i].w << ';' << v[i].h<< endl;
		for (int j=0;j<i;j++){
			if (v[j].w<v[i].w && v[j].h<v[i].h && v[j].dp>0 && v[j].dp+1 > v[i].dp){
				v[i].dp = v[j].dp+1;
				v[i].pre=j;
			}
		}
		if (mx<v[i].dp)
			mx = v[i].dp,cur = i;
	}
	cout << mx << endl;
	if (mx>0){
		print(cur);
	}
	fin.close();
	return 0;
}

