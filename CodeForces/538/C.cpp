#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;

int main(){
	int m,n;
	cin >> n >> m;
	int cd,ch,d,h;
	bool imps = false;
	int mx = 0;
	// read first line
	cin >> cd >> ch;
	mx = ch+cd-1;
	for (int i=1;i<m;i++){
		cin >> d >> h;
		int numstep = abs(h - ch);
		if (numstep > d-cd){
			imps = true;
			break;
		}
		int freestep = d-cd-numstep;
		mx = max(mx,max(ch,h)+freestep/2);
		cd = d;
		ch = h;
	}
	if (imps)
		cout << "IMPOSSIBLE";
	else{
		mx = max(mx,ch+n-cd);
		cout << mx;
	}
}


