#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;

int main(){
	string s;
	string cf = "CODEFORCES";
	cin >> s;
	int x = 0;
	int del = s.length()-cf.length();
	if (del <= 0){
		cout << "NO";
		return 0;
	}
	for (int i=0;i<=cf.length();i++){
		string ss = s.substr(0,i) + s.substr(i+del,s.length()-i-del);
		if (ss==cf){
			cout << "YES";
			return 0;
		}
	}
	cout << "NO";
}


