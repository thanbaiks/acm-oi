#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;

int main(){
	string s;
	cin >> s;
	int mx = 0;
	int len = s.length();
	for (int i=0;i<s.length();i++){
		s[i]-='0';
		mx = max(mx,(int)s[i]);
	}
	cout << mx << " ";
	for (int i=0;i<mx;i++){
		int k = 0;
		for (int j=0;j<len;j++){
			k *= 10;
			if (s[j]){
				s[j]--;
				k+=1;
			}
		}
		cout << k << " ";
	}
}


