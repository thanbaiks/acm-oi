#include <iostream>
#include <fstream>

typedef unsigned long long ULL;

using namespace std;

ULL fibo[100],test,x;

void genFibo(){
	fibo[1]=fibo[2]=1;
	for (int i=3;i<=91;i++)
		fibo[i]=fibo[i-1]+fibo[i-2];
}
int main(){
	genFibo();
	cin >> test;
	for (ULL ti=0;ti<test;ti++){
		cin >> x;
		cout << fibo[x+1] << endl;
	}
	return 0;
}
