#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;
struct Triple{
	int a,b,c;
};
vector<Triple> x[100];
LL cnt;
void build(){
	memset(x,0,sizeof(x));
	// base 50
	FOR (i,1,10) FOR (j,1,10) FOR (k,1,10){
		Triple t = {i,j,k};
		x[i+j-k+50].push_back(t);
	};
}
vector<Triple> get(int i){
	if (i<-9||i>18)
		return vector<Triple>();
	return x[i+50];
}
int main(){
	freopen("rs.txt","w",stdout);
	build();
	cnt = 0;
	vector<Triple> t;
	for (int c=1;c<10;c++){
		for (int b=c;b<10;b+=c){
			for (int e=1;e<10;e++)
				for (int g=1;g<10;g++){
					for (int h=1;h<10;h++){
						for (int i=1;i<10;i++){
							if (h*g%i==0){
								// chia het
								t = get(87-13*b*c-h*g/i-12*e);
								FOR (ii,0,t.size()){
									printf("%d %d %d %d %d %d %d %d %d\n",
										t[ii].a,
										b,
										c,
										t[ii].b,
										e,
										t[ii].c,
										g,
										h,
										i
										);
								}
								cnt += t.size();
							}
						}
					}
				}
		}
	}
	cout << "So dap an: " << cnt << endl;
	return 0;
}

