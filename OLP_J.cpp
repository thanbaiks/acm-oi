#include <iostream>
#include <fstream>
#include <cstring>
#define inp cin
using namespace std;
int n;
char s[10001];
int count[26];
long num[2];

long numof(int n,int x){
	long res = 0;
	for (int i=x;i<=n;i+=x){
		int j = i;
		while (j%x==0)
			res++,j/=x;
	}
	return res;
}

int main(){
	ifstream fin("OLP_J.inp");
	inp >> n;
	for (int ti=0;ti<n;ti++){
		inp >> s;
		//cout << "s="<<s <<endl;
		memset(count,0,sizeof(count));
		num[0]=num[1]=0;
		int len = strlen(s);
		for (int i=0;i<len;i++){
			count[s[i]-'A']++;
		}
		num[0] += numof(len,2);
		num[1] += numof(len,5);
		for (int i=0;i<26;i++){
			num[0]-=numof(count[i],2);
			num[1]-=numof(count[i],5);
		}
		cout << min(num[0],num[1]) << endl;
	}
	return 0;
}
