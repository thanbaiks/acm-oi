#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <map>
#include <algorithm>
#define _i map<ULL,int>::iterator

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n;
map<ULL,int> m;
int main(){
	ULL tmp;
	freopen("P153PROA.TXT","r",stdin);
	scanf("%d",&n);
	for (int i=0;i<n;i++){
		scanf("%lld",&tmp);
		m[tmp]++;
	}
	for (_i i = m.begin();i != m.end();++i){
		printf("%d %d\n",i->first,i->second);
	}
}

