#include <iostream>
#include <fstream>
#define ui64 unsigned long long

using namespace std;
ui64 menhgia[]={500000,200000,100000,50000,20000,10000,5000,2000,1000,500,200},s,soto,x,buf;
int test,i;
int main(){
	cin >> test;
	for (int ti=1;ti<=test;ti++){
		cin >> s;
		soto = 0;
		i=0;
		buf=0;
		x = s/100%10;
		if ((x == 1 || x == 3) && s > 1000){
			// vay 1000
			s-=1000;
			buf=1000;
		}
		
		while (i < 9 && s > 0){
			if (s>=menhgia[i]){
				x = s/menhgia[i];
				s-=x * menhgia[i];
				soto+=x;
				//cout << "Picked >> "<< x << " of " << menhgia[i] << endl;
			}else
				i++;
		}
		s+=buf;
		if (s/100>10){
			soto++;
			s-=500;
		}
		switch(s){
			case 900:
				soto+=3;
				s=0;
				break;
			case 800:
				soto+=4;
				s=0;
				break;
			case 700:
				soto+=2;
				s=0;
				break;
			case 600:
				soto+=3;
				s=0;
				break;
			case 500:
				soto+=1;
				s=0;
				break;
			case 400:
				soto+=2;
				s=0;
				break;
			case 200:
				soto+=1;
				s=0;
				break;
		}
		// output
		if (s > 0){
			// con du
			cout << "Case #" << ti <<": " << "NO" << endl;
		}else{
			cout << "Case #" << ti <<": " << soto << endl;
		}
	}
	return 0;
}
