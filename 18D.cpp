#include <iostream>
#include <fstream>
#include <cstring>

#define inp fin
#define MAX 15000001

using namespace std;
bool money[MAX];
int test,n;
unsigned long m,t;

int main(){
	ifstream fin("18D.inp");
	inp >> test;
	for (int ti=1;ti<=test;ti++){
		// input
		inp >> n >> m;
		memset(money,0,sizeof(money));
		int mx=0;
		for (int i=0;i<n;i++){
			inp >> t;
			for (int j=mx;j>0;j--){
				if (money[j]){
					money[j+t]=true;
				}
			}
			money[t]=true;
			mx+=t;
			if (mx > MAX)mx = MAX;
		}
		// output
		if (m<=mx){
			while (!money[m])m++;
			cout << "Case #" << ti << ": " << m << endl;
		}else{
			cout << "Case #" << ti << ": " << -1 << endl;
		}
	}
	return 0;
}
