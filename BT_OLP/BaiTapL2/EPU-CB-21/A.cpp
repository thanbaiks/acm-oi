#include <cstdio>
#include <queue>

using namespace std;

typedef long long ll;

inline int abs(const int x)
{
    if (x>0) return x;
    return -x;
}
int calc(ll s){
    int a = s % 10, b, r = 0;
    s/=10;
    for (int i = 0; i < 9; i++) 
    {
        b = s % 10;
        r = r + abs(a-b);
        a = b;
        s /= 10;    
    }
    while(r>=10) r = r%10 + r/10;
    return r;    
}
void dotest(const int num)
{
    int n, m; scanf("%d %d", &n, &m);      
    priority_queue<ll> list[10]; 
    
    ll s; int t;
    for (int i = 0; i < n; i++) {
        scanf("%lld", &s);
        t = calc(s);
        list[t].push(s);
    }
    
    printf("Case #%d:\n", num);
    for (int i = 0; i < m; i++){
        scanf("%d", &t);
        if (list[t].empty()) puts("-1");
        else {
             printf("%010lld\n", list[t].top());
             list[t].pop(); 
        }
    }
}
int main(int argc, char* argv[])
{
    int t; scanf("%d",&t);     
    for (int i = 1 ; i <= t; i++) dotest(i);
    return 0;
}
