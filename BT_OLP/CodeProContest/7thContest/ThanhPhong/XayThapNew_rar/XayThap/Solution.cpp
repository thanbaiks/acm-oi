#include <iostream>
#include <stdio.h>

using namespace std;

int n;
long long h1, hn, H;

//h is h(n-1)
int Satisfy(long long h){
    int i;
    long long pre = hn, loc = h , t;
    H = 0;
    for(i=3 ; i<=n ; i++) {      
            t = loc;
            loc = pre + loc;
            pre = t;
            H = H + loc;
    }
    H = H + hn + h;
    if(loc==h1) return 0;
    else if(loc<h1) return -1;
    else return 1;
}

//Binary Search
void Find(long long left , long long right, long long k){//init with Find(1 , h1, (1+hn)/2);
     //cout<<left << " "<<right<<endl;
     int t;
     if( right == left+1){
         t = Satisfy(left);
         if(t==0) return;
         t = Satisfy(right);
         if(t==0) return;
         cout<<"erorr!\n";
         return;
     }
     t = Satisfy(k);
     if(t==0) return;
     if(t>0)  Find(left, k , (left+k)/2);
     else Find(k, right, (k+right)/2);
}

/*
//Liner Search
void Find(){
     long long k;
     int t;
     for(k=1 ; k<=h1 ; k++){
            t = Satisfy(k);
            if(t==0) return;  
     }
}*/

int main(){
   FILE *fin = fopen("Large_In.txt" , "r");
   //FILE *fin = fopen("Small_In.txt" , "r");
   FILE *fout = fopen("Large_Out.txt" , "w");
   //FILE *fout = fopen("Small_Out.txt" , "w");
   int T;
   fscanf(fin, "%d", &T);
   for(int i=1 ; i<=T ; i++){
           fscanf(fin, "%d %u %u", &n, &h1 , &hn);
           Find(1 , h1 , (1+h1)/2); //Binary search
           //Find();  //Liner Search
           fprintf(fout, "Case #%d: %u", i , H);
           if(i<T) fprintf(fout, "\n");
   }
   cout<<"Finish!";
   fclose(fin);
   fclose(fout);
   return 0;
}
