#include <iostream>
#define max 10000001
using namespace std;

int a[max];
int main(){
   int  T, i, j, k, N, S;
   long long r;
   FILE *fin = fopen("Small_In.txt", "r");
   FILE *fout = fopen("Small_Out.txt", "w");
   fscanf(fin, "%d", &T);
   for(i=0 ; i<T ; i++){
           r = 0;
           fscanf(fin, "%d", &N);
           fscanf(fin, "%d", &S);
           for(k=0 ; k<N ; k++) fscanf(fin, "%d", &a[k]);
           for(k=0 ; k<N ; k++)
               for(j=k+1; j<N ; j++) if(a[k]+a[j]==S) r++;
           fprintf(fout, "Case #%d: %d", i+1, r);
           if(i<T-1) fprintf(fout, "\n");
   }
   fclose(fin);
   fclose(fout);
   return 0;
}
