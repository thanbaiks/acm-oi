#include <iostream>
#define max 2000005
#define offset 1000000
using namespace std;

int a[max];
int main(){
   int  T, i, j, k, N, S;
   long long r;
   FILE *fin = fopen("In.txt", "r");
   FILE *fout = fopen("Out.txt", "w");
   fscanf(fin, "%d", &T);
   for(i=0 ; i<T ; i++){
           r = 0;
           for(k=0 ; k<max ; k++) a[k] = 0;
           fscanf(fin, "%d", &N);
           fscanf(fin, "%d", &S);
           S += 2*offset;
           for(k=0 ; k<N ; k++){
                   int num;
                   fscanf(fin, "%d", &num);
                   a[num+offset]++;
           }
           for(k=0 ; k<max ; k++){
                   if(S == 2*k) r+=a[k]*(a[k]-1);
                   else if((S-k)<max) r+=a[k]*a[S-k];
           }
           r /= 2;
           fprintf(fout, "Case #%d: %d", i+1, r);
           if(i<T-1) fprintf(fout, "\n");
   }
   fclose(fin);
   fclose(fout);
   return 0;
}
