#include <iostream>
#define Num 2601
#define oo 100000000
using namespace std;

int G[Num][Num];
int d[Num];
int check[Num];
int MAX, L1, L2, N;
float r1a, r2a, r1b, r2b;

int Map(int L1, int L2){
    return L1*(MAX+1)+L2;
}

void init(){
     int i, j, D1, D2;
     N = (MAX+1)*(MAX+1);
     for(i=0; i<N ; i++)
           for(j=0 ; j<N; j++) G[i][j] = 0;
     for(i=0 ; i<N ; i++){
           d[i] = oo;
           check[i] = 1;
     }
     
     d[Map(L1, L2)] = 0;
     
     for(i=0; i<=MAX ; i++)
           for(j=0 ; j<=MAX; j++){
                   //tiem thuoc A
                   D1 = (int)(r1a * (float)i);
                   D2 = (int)(r2a * (float)j);
                   if( D1<=MAX && D2<=MAX) G[Map(i, j)][Map(D1, D2)] = 1;
                   //tiem thuoc B
                   D1 = (int)(r1b * (float)i);
                   D2 = (int)(r2b * (float)j);
                   if( D1<=MAX && D2<=MAX) G[Map(i, j)][Map(D1, D2)] = 1;
           }
     
}

void Dijkstra(){
     int i, u, min;
     while(true){
          min = oo;
          for(i=0 ; i<N ; i++) if(check[i] && min>d[i]){
                u = i;
                min = d[i];
          }
          if(min == oo) return;
          check[u] = 0;
          for(i=0 ; i<N ; i++) if(G[u][i]){
                if(d[i] > d[u]+G[u][i]) 
                      d[i] = d[u]+G[u][i];
          }
     }
}

int main(){
   int  T, i;
   
   FILE *fin = fopen("In.txt", "r");
   FILE *fout = fopen("Out.txt", "w");
   fscanf(fin, "%d", &T);
   for(i=0 ; i<T ; i++){
                      
           fscanf(fin, "%d", &MAX);
           fscanf(fin, "%d %d", &L1, &L2);
           fscanf(fin, "%f %f %f %f", &r1a, &r2a, &r1b, &r2b);
           
           init();
           Dijkstra();
           
           if(d[0] == oo) d[0]=-1;
           fprintf(fout, "Case #%d: %d", i+1, d[0]);
           if(i<T-1) fprintf(fout, "\n");
   }
   fclose(fin);
   fclose(fout);
   return 0;
}
