#include <iostream>
#include <queue>
using namespace std;

#define REP(i, n) for (int i = 0; i < n; i++)
#define FOR(i, a, b) for (int i = a; i <= b; i++)

int n, m, x, y;
int c[111][111];
int p[11];
bool visited[111][1030];
int v[1030];

void read_data() {
	cin >> n >> m >> x >> y;
	memset(c, 0, sizeof(c));
	FOR(i, 0, m - 1) {
		cin >> p[i];
		int k;
		cin >> k;
		REP(j, k) {
			int a, b;
			cin >> a >> b;
			c[a][b] |= (1 << i);
			c[b][a] |= (1 << i);
		}
	}
	REP(state, (1 << m)) {
		int sum = 0;
		REP(i, m) if ((state & (1 << i)) != 0) sum += p[i];
		v[state] = sum;
	}
}

int process() {
	int res = 1000000000;
	memset(visited, false, sizeof(visited));
	queue<int> q1;
	queue<int> q2;
	q1.push(x);
	q2.push(0);
	visited[x][0] = true;
	if (x == y) res = 0;
	while (!q1.empty()) {
		int i = q1.front(); q1.pop();
		int state = q2.front(); q2.pop();
		if (v[state] >= res) continue;
		FOR(j, 1, n) if ((c[i][j] & state) != 0 && !visited[j][state]) {
			q1.push(j);
			q2.push(state);
			visited[j][state] = true;
			if (j == y && v[state] < res) res = v[state];
		}
		FOR(j, 0, m - 1) if ((state & (1 << j)) == 0 && !visited[i][state + (1 << j)]) {
			q1.push(i);
			q2.push(state + (1 << j));
			visited[i][state + (1 << j)] = true;
			if (i == y && v[state + (1 << j)] < res) res = v[state + (1 << j)];
		}
	}
	int laststate;
	REP(state, (1 << m)) {
		if (visited[y][state] && v[state] == res) {
			int sum = 0;
			REP(i, m) if ((state & (1 << i)) != 0) sum ++;
			cerr << sum << endl;
		}
	}
	if (res == 1000000000) return -1;
	else return res;
}

int main() {
	freopen("small-4.in", "r", stdin);
	freopen("small-4.ans", "w", stdout);
	int test;
	cin >> test;
	for (int i = 1; i <= test; i++) {
		cout << "Case #" << i << ": ";
		//cerr << "Case #" << i << ": ";

		read_data();
		cout << process();

		if (i < test) cout << endl;
	}
	return 0;
}
