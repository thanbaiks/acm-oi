#include <iostream>
using namespace std;

FILE *fin = fopen("In.txt", "r");
FILE *fout = fopen("Out.txt", "w");

int p[100001];
int l[100001];
long long F[100001];//so cac so ko nguyen to cung nhau voi i

void init(){
     int i, j;
     for(i=0 ; i<100001 ; i++){
             p[i] = 1;
             l[i] = i;
     }
     p[0] = p[1] = 0;
     p[2] = 1;
     l[2] = 2;
     for(i=2 ; i<100001 ; i++) if(p[i])
         for(j=2*i ; j<100001 ; j+=i){
                   p[j] = 0;
                   l[j] = i;
         }
}

int ThuGiam(int k){
    int t=k, r=1;
    if(p[k] || k==1) return k;
    while(t%l[k]==0) t/=l[k];
    return l[k]*ThuGiam(t);
}

long long KoNguyenTo(int x, int N){     
     int a[20];
     int num=0, i, t, n, run, p;
     long long r = 0;          
     while(x>1){
           a[num] = l[x];
           x /= l[x];
           num++;
     }     
     for(i=1 ; i< (1<<num) ; i++){
             t = i;
             n = 0;
             run = 0;
             p = 1;
             while(t>0){
                   if(t%2){
                       p *= a[run];
                       n++;    
                   }
                   t/=2;
                   run++;
             }
             if(n%2) r += N/p;
             else r -= N/p;
     }
     return r;
}

long long com(int m, int n){
     int i, j, t;
     long long r;
     F[1] = 0;
     for(i=2 ; i<=m ; i++){
         if(p[i]) F[i] = n/i;
         else{
              t = ThuGiam(i);
              if(t<i)  F[i] = F[t];
              else{
                   F[i] = KoNguyenTo(i, n);
              }
         }         
     }
     r = 0;
     for(i=1 ; i<= m ; i++) r+=(n-F[i]);
     return r+2;
}

void print(long long r){
     if(r>0){
         print(r/10);
         int t = r%10;
         fprintf(fout, "%d", t);
     }
}

int main(){
   int  T, i, M, N;
   long long r;
   fscanf(fin, "%d", &T);
   init();
   for(i=0 ; i<T ; i++){
           fscanf(fin, "%d %d", &M, &N);
           r = com(M, N);
           fprintf(fout, "Case #%d: ", i+1);
           print(r);
           if(i<T-1) fprintf(fout, "\n");
   }
   fclose(fin);
   fclose(fout);
   return 0;
}
