#include <iostream>
#include <math.h>
using namespace std;

FILE *fin = fopen("In.txt", "r");
FILE *fout = fopen("Out.txt", "w");

long long com(long long a, long long b, long long r){ //a>b
    if(b*b>=2*r*r){
           a = (long long)(sqrt(2.0)*r);
           if((a+1)*(a+1)+a*a<=4*r*r) return a*(a+1);
           return a*a;
    }
    a = (int)(sqrt(4*r*r-b*b));    
    if((a+1)*(a+1)+b*b<=4*r)        return b*(a+1);
    return a*b;
}

void print(long long r){
     if(r>0){
         print(r/10);
         int t = r%10;
         fprintf(fout, "%d", t);
     }
}

int main(){
   int T, i, A, B, R;
   long long  a, b, r;
   long long kq;
   fscanf(fin, "%d", &T);
   for(i=0 ; i<T ; i++){
           fscanf(fin, "%d %d %d", &A, &B, &R);
           a = A; b = B; r = R;
           if(a*a+b*b<=4*r*r) kq = a*b;
           else if(a>b) kq = com(a, b, r);
           else kq = com(b, a, r);
           fprintf(fout, "Case #%d: ", i+1);
           print(kq);
           if(i<T-1) fprintf(fout, "\n");
   }
   fclose(fin);
   fclose(fout);
   return 0;
}
