#include <iostream.h>

int R[100][1001];
int a[100];

int main(){
	int T, M, N, k, i, j;
	int result;
	FILE *fin = fopen("in.txt", "r");
	FILE *fout = fopen("out.txt", "w");
	fscanf(fin, "%d", &T); 
	for(k=0 ; k<T ; k++){
		fscanf(fin, "%d %d", &N, &M);
		for(i=0 ; i<N ; i++) fscanf(fin, "%d", &a[i]);
		//dynamic programing
		for(i=0 ; i<N ; i++) R[i][0]=1;
		for(j=1; j<=M ; j++) R[0][j]=0;
		if(a[0]<=M) R[0][a[0]] = 1;
		for(i=1 ; i<N; i++)
			for(j=1; j<=M; j++){
				R[i][j] = R[i-1][j] ;
				if(j>=a[i]) R[i][j] += R[i-1][j-a[i]];
			}
		//trace
		if(R[N-1][M]==0 || M==0) result=0;
		else if( R[N-1][M]>1) result=-1;
		else{
			result=0;
			for(i=N-1 ; i>=0 ; i--){
				if(R[i][M]==1) {
					result++;
					M-=a[i];
				}
			}
		}
		fprintf(fout, "Case #%d: %d", k+1, result);
		if(k<T-1) fprintf(fout, "\n");
	}
	fclose(fin);
	fclose(fout);
}
