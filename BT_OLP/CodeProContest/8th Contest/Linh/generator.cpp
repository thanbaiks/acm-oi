#include <cstdio>
#include <cstdlib>
#include <iostream>
using namespace std;

double val = 1e12;

int main()
{
    freopen("c.i2","w",stdout);
    int T = 50;
    printf("%d\n", T);
    while (T--)
    {
        double slope = (double) rand() / RAND_MAX;
        double N = val * slope;
        printf("%.0lf\n", N);
    }
}
