#include <iostream>
using namespace std;

long long N;
int T;
long long p[14],p10[14];

long long calc(long long x)
{
    if (x <= 0) return 0;
    long long tmp = x;  int digit = 0;    
    while (tmp)
    {
        digit++;  tmp /= 10;        
    }
    long long ret = 0;
    for (int i = 1; i < digit; i++) ret += p[i];
    ret += 1LL * digit * (x - p10[digit] + 1);
    return ret;
}

int main()
{
//    freopen("c.i2","r",stdin);
//    freopen("c.o2","w",stdout);
    
    p[1] = 9;  long long num = 9;
    p10[1] = 1;
    for (int i = 2; i <= 12; i++)
    {
        num *= 1LL * 10;
        p[i] = 1LL * num * i;
        p10[i] = p10[i - 1] * 10;
    }
    scanf("%d", &T);
    for (int it = 1; it <= T; it++)
    {
        printf("Case #%d: ", it);
        cin >> N;                        
        long long low = 1,high = N,ans = -1;
        while (low <= high)
        {
            long long med = (low + high)/2;
            if (calc(med) < N) low = med + 1; else
            {
                ans = med;  high = med - 1;
            }
        }
        
        N -= calc(ans);
        while (N < 0)
        {
            N++;  ans /= 10;
        }
        cout << (ans % 10);
        if (it < T) printf("\n");
    }
}
