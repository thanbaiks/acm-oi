#include <iostream>
using namespace std;

int b[64][64];

void Init(){
        int i,j,k,x1,y1,x2,y2;
		for(i=0 ; i<64 ; i++) 
			for(j=0 ;j<64 ;j++){
				x1=i/8;
				y1= i%8;
				x2=j/8;
				y2=j%8;
				k = (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
				if(k==5) b[i][j]=1;
				else b[i][j]=0;
			}

     
}

void Floyd(){
     int i, j, k;
     for(i=0 ; i<64 ;i++)
			for(j=0; j<64 ;j++)
				if(b[j][i]>0) for(k=0; k<64 ;k++)
					if(b[i][k]>0)
						if(b[j][k]==0 ||  b[j][k] > b[j][i]+b[i][k]) 
							b[j][k] = b[j][i]+b[i][k];
}

int main(){
   int  T, i, r;
   char s1[3];
   char s2[3];
   FILE *fin = fopen("In.txt", "r");
   FILE *fout = fopen("Out.txt", "w");
   Init();
   Floyd();
   fscanf(fin, "%d", &T);
   for(i=0 ; i<T ; i++){
           fscanf(fin, "%s %s", &s1, &s2);
           if(s1[0]==s2[0] && s1[1]==s2[1]) r=0;
           else r = b[(s1[0]-'A')*8+s1[1]-'1'][(s2[0]-'A')*8+s2[1]-'1'];
           fprintf(fout, "Case #%d: %d", i+1, r);
           if(i<T-1) fprintf(fout, "\n");
   }
   fclose(fin);
   fclose(fout);
   return 0;
}
