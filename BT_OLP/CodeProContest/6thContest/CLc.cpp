#include <algorithm>
#include <bitset>
#include <cmath>
#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <deque>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stack>
#include <queue>
#include <vector>
#include <utility>
typedef long long ll;
using namespace std;

int m,n,u,v,T;

int calc()
{
    int low = 1,high = m,left = 1,right = n,ans = 0;
    while (1)
    {
        if (u == low) return ans + v - left + 1; else
        {
            ans += (right - left + 1);  low++;
        };
        if (v == right) return ans + u - low + 1; else
        {
            ans += (high - low + 1);  right--;
        };
        if (u == high) return ans + right - v + 1; else
        {
            ans += (right - left + 1);  high--;
        };
        if (v == left) return ans + high - u + 1; else
        {
            ans += (high - low + 1);  left++;
        };
    };
    return 0;
};

int main()
{
    freopen("c.i2","r",stdin);
    freopen("c.o2","w",stdout);
    scanf("%d", &T);
    for (int it = 1; it <= T; it++)
    {
        scanf("%d %d %d %d", &m, &n, &u, &v);
        printf("Case #%d: %d", it,calc());
        if (it < T) printf("\n");
    };
};
