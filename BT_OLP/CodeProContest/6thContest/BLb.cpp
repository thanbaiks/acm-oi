#include <algorithm>
#include <bitset>
#include <cmath>
#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <deque>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stack>
#include <queue>
#include <vector>
#include <utility>
typedef long long ll;
using namespace std;

int main()
{
    freopen("b.i2","r",stdin);
    freopen("b.o2","w",stdout);
    int T,n,k;
    scanf("%d", &T);
    for (int it = 1; it <= T; it++)
    {
        scanf("%d %d", &n, &k);
        int ret = 0;
        for (int i = 0; i < (1 << n); i++)
        {
            int pop = 0;  bool flag = false;
            for (int j = 0; j < n; j++) if (i & (1 << j))
            {
                pop++; if (pop >= k) flag = true;
            }
            else pop = 0;
            if (flag) ret++;
        };
        printf("Case #%d: %d", it, ret);        
        if (it < T) printf("\n");
    };
};
