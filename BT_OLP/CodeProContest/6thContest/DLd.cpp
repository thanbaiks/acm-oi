#include <algorithm>
#include <bitset>
#include <cmath>
#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <deque>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stack>
#include <queue>
#include <vector>
#include <utility>
#define INF 1e12
typedef long long ll;
using namespace std;

double d[352];
double f[352][352];
int T;
int n;
int x[352],y[352],X,Y,R;
bool chs[352];

double dist(int u,int v)
{
    double a = y[u] - y[v];
    double b = x[v] - x[u];
    double c = -(a * x[u] + b * y[u]);
    double dd = abs(a * X + b * Y + c) / sqrt(a * a + b * b);
    if (dd > 1.0 * R)
    {
        double tmp = sqrt( (x[u] - x[v]) * (x[u] - x[v]) + (y[u] - y[v]) * (y[u] - y[v]) );
        return tmp;
    }
    else return INF;
};

void Dijkstra()
{
    memset(chs,true,sizeof(chs));
    for (int i = 1; i <= n; i++) d[i] = INF;
    d[1] = 0.0;
    while (1)
    {
        int u = 0;  double best = INF;
        for (int i = 1; i <= n; i++) if (chs[i] && best > d[i])
        {
            best = d[i];  u = i;
        };
        if (u == n || !u) return;
        chs[u] = false;
        for (int i = 1; i <= n; i++)
          if (chs[i] && d[i] > d[u] + f[u][i]) d[i] = d[u] + f[u][i];
    };
};

int main()
{
    freopen("d.i2","r",stdin);
    freopen("d.o2","w",stdout);
    scanf("%d", &T);
    for (int it = 1; it <= T; it++)
    {
        printf("Case #%d: ", it);
        scanf("%d %d %d", &X, &Y, &R);
        scanf("%d", &n);
        for (int i = 1; i <= n; i++) scanf("%d %d", &x[i], &y[i]);
        
        for (int i = 1; i <= n; i++)
          for (int j = 1; j <= n; j++) f[i][j] = (i == j) ? 0.0 : dist(i,j);
        Dijkstra();
        if (d[n] >= INF) printf("-1"); 
        else
        {
            double tmp = trunc(d[n] * 100);
            printf("%.0lf", tmp);
        };
        if (it < T) printf("\n");
    };
};
