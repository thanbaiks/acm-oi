#include <iostream>
#include <string>
using namespace std;

string pt1, pt2, tong;
string res1, res2, res3;
int A[44], B[44], C[44];
bool used[10];
int match[111];
int n;


string daonguoc(string a) {
	string aa = "";
	for (int i = a.size() - 1; i >= 0; i--) aa += a[i];
	return aa;
}

string cong(string a, string b) {
	string aa, bb, cc;
	for (int i = a.size() - 1; i >= 0; i--) aa += a[i];
	for (int i = b.size() - 1; i >= 0; i--) bb += b[i];

	int nho = 0;
	cc = "";
	for (int i = 0; i < n; i++) {
		int aaa, bbb;
		aaa = aa[i] - '0';
		bbb = bb[i] - '0';
		int next = aaa + bbb + nho;
		cc =  cc + "0";
		cc[cc.size() - 1] += next % 10;
		nho = next / 10;
	}
	if (nho) return cc + "1";

	int u = 0;
	int v = cc.size() - 1;
	while (u < v) {
		swap(cc[u], cc[v]);
		u++;
		v--;
	}

//	while (cc.size() > 0 && cc[0] == 0) cc.erase(0, 1);
	return cc;
}

bool nhohon(string a, string b) {
	while (a.size() > 0 && a[0] == 0) a.erase(0, 1);
	while (b.size() > 0 && b[0] == 0) b.erase(0, 1);
	if (a.size() < b.size()) return true;
	if (a.size() > b.size()) return false;
	for (int i = 0; i < a.size(); i++) {
		if (a[i] < b[i]) return true;
		if (a[i] > b[i]) return false;
	}
	return false;
}

void update() {
	string p1, p2, p3;
	p1 = "";
	p2 = "";
	p3 = "";
	for (int i = 1; i <= n; i++) {
		char c = A[i];
		if (c > 10) c = '0' + match[A[i]]; else c = '0' + A[i];
		p1 = c + p1;
	}
	for (int i = 1; i <= n; i++) {
		char c = B[i];
		if (c > 10) c = '0' + match[B[i]]; else c = '0' + B[i];
		p2 = c + p2;
	}
	for (int i = 1; i <= n; i++) {
		char c = C[i];
		if (c > 10) c = '0' + match[C[i]]; else c = '0' + C[i];
		p3 = c + p3;
	}
	if (cong(p1, p2) != p3) return;

	if (res1 == "-1" || nhohon(p1, res1) || (p1 == res1 && nhohon(p2, res2))) {
		res1 = p1;
		res2 = p2;
		res3 = p3;
	}
}

void tinh(int X, int &l, int &r) {
	if (X < 10) {
		l = r = X;
		return;
	}
	if (match[X] == -1) {
		l = 0;
		r = 9;
	} else {
		l = r = match[X];
	}
}

void duyet(int pos, int nho) {
	if (pos > n) {
		update();
		return;
	}
	int a1, a2, b1, b2, c1, c2;
	bool editA = false;
	bool editB = false;
	bool editC = false;

	tinh(A[pos], a1, a2);
	for (int a0 = a1; a0 <= a2; a0++) {
		if (A[pos] > 10 && match[A[pos]] == -1 && used[a0]) continue;
		if (A[pos] > 10 && match[A[pos]] == -1) {
			match[A[pos]] = a0;
			editA = true;
			used[a0] = true;
		}

		tinh(B[pos], b1, b2);
		for (int b0 = b1; b0 <= b2; b0++) {
			if (B[pos] > 10 && match[B[pos]] == -1 && used[b0]) continue;
			if (B[pos] > 10 && match[B[pos]] == -1) {
				match[B[pos]] = b0;
				editB = true;
				used[b0] = true;
			}


			tinh(C[pos], c1, c2);
			for (int c0 = c1; c0 <= c2; c0++) {
				if (C[pos] > 10 && match[C[pos]] == -1 && used[c0]) continue;
				if (C[pos] > 10 && match[C[pos]] == -1) {
					match[C[pos]] = c0;
					editC = true;
					used[c0] = true;
				}
				if ((a0 + b0 + nho) % 10 == c0) {
					//cout << pos << " " << a0 << " " << b0 << " " << c0 << endl;
					duyet(pos + 1, (a0 + b0 + nho) / 10);
				}

				if (editC) {
					match[C[pos]] = -1;
					editC = false;
					used[c0] = false;
				}
			}

			if (editB) {
				match[B[pos]] = -1;
				editB = false;
				used[b0] = false;
			}
		}

		if (editA) {
			match[A[pos]] = -1;
			editA = false;
			used[a0] = false;
		}
	}

}

string process() {
	memset(A, 0, sizeof(A));
	memset(B, 0, sizeof(B));
	memset(C, 0, sizeof(C));
	n = pt1.size();
	if (n < pt2.size()) n = pt2.size();
	if (n < tong.size()) n = tong.size();
	for (int i = 0; i < pt1.size(); i++) A[pt1.size() - i] = ('A' <= pt1[i] && pt1[i] <= 'Z' ? pt1[i] : pt1[i] - '0');
	for (int i = 0; i < pt2.size(); i++) B[pt2.size() - i] = ('A' <= pt2[i] && pt2[i] <= 'Z' ? pt2[i] : pt2[i] - '0');
	for (int i = 0; i < tong.size(); i++) C[tong.size() - i] = ('A' <= tong[i] && tong[i] <= 'Z' ? tong[i] : tong[i] - '0');
	memset(used, false, sizeof(used));
	for (int i = 0; i < 111; i++) match[i] = -1;
	res1 = res2 = res3 = "-1";

	duyet(1, 0);

	if (res1 == "-1") return "no solution";
	else {
		while (res1.size() > 1 && res1[0] == '0') res1.erase(0, 1);
		while (res2.size() > 1 && res2[0] == '0') res2.erase(0, 1);
		while (res3.size() > 1 && res3[0] == '0') res3.erase(0, 1);
		return res1 + " + " + res2 + " = " + res3;
	}
}

int main() {
	freopen("large.in", "r", stdin);
	freopen("large.out", "w", stdout);
	int test;
	cin >> test;
	for (int i = 1; i <= test; i++) {
		//cerr << "Case #" << i << endl;
		string tmp;
		cin >> pt1 >> tmp;
		cin >> pt2 >> tmp >> tong;
		cout << "Case #" << i << ": " << process();
		if (i < test) cout << endl;
	}
	return 0;
}
