#include <iostream>
using namespace std;

int N, M;
int v[15];

int process(){
    int i, a, run, sum, min;
    min = 1000000000;
    for(i=1; i<(1<<N) ; i++){
             a = i;
             run = 0;        
             sum = 0;     
             while(a>0){
                   if(a%2)   sum+=v[run];
                   a /= 2;
                   run++;
             }             
             if(sum>=M && sum<min) min = sum;
     }
    return min;
}

int main(){
   int  T, i, j, S;
   FILE *fin = fopen("In.txt", "r");
   FILE *fout = fopen("Out.txt", "w");
   fscanf(fin, "%d", &T);
   for(i=0 ; i<T ; i++){
           fscanf(fin, "%d %d", &N, &M);
           S = 0;
           for(j=0 ; j<N ; j++){
                   fscanf(fin, "%d", &v[j]);
                   S+=v[j];
           }
           if(S<M) fprintf(fout, "Case #%d: -1", i+1);
           else{
               S = process();
               fprintf(fout, "Case #%d: %d", i+1, S); 
           }
           if(i<T-1) fprintf(fout, "\n");
   }
   fclose(fin);
   fclose(fout);
   return 0;
}
