#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>

using namespace std;

int t[101][2], p[101][2], a[101];
int n,i,j;
long dt;

void nhap()
{
    ifstream fi("fence.inp");
        fi >> n;
        for (i=1;i<=n;i++)
            fi >> t[i][1] >> t[i][2] >> p[i][1] >> p[i][2];
        for (i=1;i<=n;i++)
            a[i]=1;
    fi.close();
}

void QS(int l, int r)
{
    int ii,jj,x,tg;
    if (l==r) return;
    x = t[(l+r)/2][1];
    ii=l;
    jj=r;
    while (ii<=jj)
        {
            while (t[ii][1]<x) ii++;
            while (t[jj][1]>x) jj--;
            if (ii<=jj)
                {
                    tg=t[ii][1];t[ii][1]=t[jj][1];t[jj][1]=tg;
                    tg=p[ii][1];p[ii][1]=p[jj][1];p[jj][1]=tg;
                    tg=t[ii][2];t[ii][2]=t[jj][2];t[jj][2]=tg;
                    tg=p[ii][2];p[ii][2]=p[jj][2];p[jj][2]=tg;
                    ii++;
                    jj--;
                }
            if (jj>l) QS(l,jj);
            if (ii<r) QS(ii,r);
        }
}
void xuly()
{
    QS(1,n);
    dt=0;
    int x1,y1,x2,y2;
    for (i=1;i<=n;i++)
        if (a[i]==1)
            {
                a[i]=0;
                x1=t[i][1];
                y1=t[i][2];
                x2=p[i][1];
                y2=p[i][2];
                for (j=i+1;j<=n;j++)
                    {
                        if ((x1<=t[j][1]) && (x2>=t[j][1]) && (y1<=p[j][2]) && (y2>=p[j][2]))
                            {
                                y1=t[j][2];
                                x2=p[j][1];
                                a[j]=0;
                            }
                    }
                dt=dt+(x2-x1)*(y2-y1);
            }
}

void ghi()
{
    ofstream fo("fence.out");
        fo << dt;
    fo.close();
}

int main (int argc, char *argv[])
{
   nhap();
   xuly();
   ghi();
}
