#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>

using namespace std;

long n,i,j,tg;
bool kn;
long *a = new long[20001];


void nhap()
{
    ifstream fi("series.in9");
        fi >> n;
        for (i=1;i<=n;i++) 
            fi >> a[i];
    fi.close();
}

void QS(long l,long r)
{
    long ii,jj,x,t;
    if (l==r) return;
    x = a[(l+r)/2];
    ii = l;
    jj = r;
    while (ii<=jj)
        {
            while (a[ii]<x) ii++;
            while (a[jj]>x) jj--;
            if (ii <= jj)
                {
                    t=a[ii];
                    a[ii]=a[jj];
                    a[jj]=t;
                    ii++;
                    jj--;
                }   
            if (jj>l) QS(l,jj);
            if (r>ii) QS(ii,r);              
        } 
}

void xuly()
{
    int d;
    for (i=1;i<n;i++)
        for (j=i+1;j<=n;j++)
            if (a[i]>a[j])
            {
                tg=a[i];
                a[i]=a[j];
                a[j]=tg;          
            }
    //cout << 1;
    //QS(1,n);
    //cout << 2;
    d = a[n]-a[n-1];
    kn=true;
    for (i=2;i<=n-1;i++)
        {
            if ((a[i]+d) != (a[i+1]))
                {
                    kn=false;
                    break;
                }
        }
}

void ghi()
{
    ofstream fo("series.ou9");
    if (kn==false) fo << "-1";
    else
        for (i=1;i<=n;i++)
            fo << a[i] << "\n";
    fo.close();
}

int main (int argc, char *argv[])
{
    nhap();
    xuly();
    ghi();
}
