#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

using namespace std;

char c[101][4], cs[4];
int x[(int) 'Z'+1];
int n,i,j,d[401],dem;
bool b[101];

void nhap()
{
    ifstream fi("gold.in2");
        fi >> n;
        for (i=1;i<=n;i++)
            fi >> c[i][1] >> c[i][2] >> c[i][3] >> c[i][4];   
        fi >> cs[1] >> cs[2] >> cs[3] >> cs[4];
    fi.close();    
}

bool xet(char t)
{
    int i,j;
    if (x[(int) t]==2) return(true);
    else
    {
        bool kn;
        kn=false;
        for (i=1;i<=n;i++)
            if ((c[i][3]==t) || (c[i][4]==t))
            {
                kn=true;
                if ((xet(c[i][1])==true) && (xet(c[i][2])==true)) 
                {
                   if (b[i]==false) 
                   {
                        dem++;
                        d[dem]=i;
                        b[i]=true;
                    }
                   x[(int) t]=2;
                   return(true);
                }
                else return(false);
            }
        if (kn==false) return(false);
    }
}

void xuly()
{
    for (i=(int) 'A';i<=(int) 'Z';i++)
        x[i]=0;
    for (i=1;i<=4;i++) x[(int) cs[i]]=2;
    for (i=1;i<=400;i++) d[i] = 0;
    for (i=1;i<=n;i++) b[i]=false;
    dem=0;
}

void ghi()
{
    ofstream fo("gold.out");
        if (xet('V')==false) fo << "-1";
        else
        {
            fo << dem << "\n";
            for (i=1;i<=dem;i++) fo << d[i] << " ";
        }   
        
    fo.close();
}

int main (int argc, char *argv[])
{
    nhap();
    xuly();
    ghi();   
}
