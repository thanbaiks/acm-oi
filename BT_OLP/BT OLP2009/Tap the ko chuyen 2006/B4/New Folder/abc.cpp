#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

using namespace std;

long m,n,i,j;
long *d = new long[10001];
long *c = new long[10001];
bool kn;

void nhap()
{
    ifstream fi("a.inp");
        fi >> m >> n;
        for (i=1;i<=n;i++)
            fi >> d[i] >> c[i];
    fi.close();
}

void QS(long l, long r)
{
    long ii,jj,x,tg;
    if (l==r) return;
    x = d[(l+r)/2];
    ii=l;
    jj=r;
    while (ii<=jj)
        {
            while (d[ii]<x) ii++;
            while (d[jj]>x) jj--;
            if (ii<=jj)
                {
                    tg=d[ii];
                    d[ii]=d[jj];
                    d[jj]=tg;
                    tg=c[ii];
                    c[ii]=c[jj];
                    c[jj]=tg;
                    ii++;
                    jj--;
                }
            if (jj>l) QS(l,jj);
            if (ii<r) QS(ii,r);
        }
}

void xuly()
{
    QS(1,n);
}
void ghi()
{
    ofstream fo("a.out");
        for (i=1;i<=n;i++) 
            fo << d[i] << "\n";
    fo.close();
}
int main (int argc, char *argv[])
{
    nhap();
    xuly();
    ghi();
}
