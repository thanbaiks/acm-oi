#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator

using namespace std;
typedef long long LL;

int main(){
	int n;
	LL v[1001],t[1001];
	LL x;
	//freopen("super_jumping.txt","r",stdin);
	while (true){
		cin >> n;
		if (!n)
			break;
		memset(t,0,sizeof(t));
		x = 0;
		for (int i=0;i<n;i++){
			cin >> v[i];
		}
		for (int i=0;i<n;i++){
			t[i] = v[i];
			for (int j=0;j<i;j++){
				if (v[j]<v[i] && t[i]<v[i]+t[j]){
					t[i] = v[i]+t[j];
				}
			}
			x = max(x,t[i]);
		}
		cout << x << endl;
	}
	
	return 0;
}

