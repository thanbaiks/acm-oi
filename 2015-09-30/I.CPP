#include <bits/stdc++.h>
using namespace std;
typedef map <double,int> MDI;
typedef MDI::iterator MDIi;

int t,n,tw,tb;
MDI mapp;

int main() {
	//freopen("I.INP","r",stdin);
	cin >> t;
	while (t--){
		cin >> n;
		tw = tb = 0;
		int k; // count
		char c; // char
		mapp.erase(mapp.begin(),mapp.end());
		for (int i=0;i<n;i++){
			cin >> k >> c;
			while (k--){
				if (c=='W')
					tw++;
				else
					tb++;
				mapp[(float)tw/tb]++;
			}
		}
		cout << mapp[(float)tw/tb] << endl;
	}
}

