#include <bits/stdc++.h>
#define EPS 0.0001
using namespace std;

int gcd(int a,int b){
	return !b?a:gcd(b,a%b);
}
struct PS{
	int t,m;
	void o(){
		int x = gcd(t,m);
		t /= x;
		m /= x;
	}
};

struct Wheel {
	PS rot; // rotate
	int x,y,r;
	bool con; // connected
	bool ccw; // clockwise
	Wheel(){
		rot.t = 1;
		rot.m = 1;
	}
};

int t,n;
Wheel w[1001];

double sqr(double x){
	return x*x;
}

bool connect(Wheel a,Wheel b){
	return abs(a.r+b.r - sqrt(sqr(a.x-b.x)+sqr(a.y-b.y))) < EPS;
}

void dfs(int i){
	w[i].con = true;
	for (int j=0;j<n;j++){
		if (i!=j && !w[j].con && connect(w[i],w[j])){
			//cout << "Connected: " << i << ", " << j << endl;
			// calculate rate
			w[j].ccw = !w[i].ccw;
			w[j].rot = w[i].rot;
			w[j].rot.t *= w[i].r;
			w[j].rot.m *= w[j].r;
			w[j].rot.o();
			dfs(j);
		}
	}
}

int main() {
	//freopen("D.INP","r",stdin);
	cin >> t;
	while (t--){
		cin >> n;
		for (int i=0;i<n;i++){
			cin >> w[i].x >> w[i].y >> w[i].r;
		}
		dfs(0);
		for (int i=0;i<n;i++){
			if (!w[i].con){
				cout << "not moving" << endl;
				continue;
			}
			if (w[i].rot.m != 1)
				cout << w[i].rot.t << "/" << w[i].rot.m;
			else
				cout << w[i].rot.t;
			if (w[i].ccw)
				cout << " counterclockwise" << endl;
			else
				cout << " clockwise" << endl;
		}
	}
}

