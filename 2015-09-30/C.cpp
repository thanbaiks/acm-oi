#include <bits/stdc++.h>
using namespace std;
int t,n;
int main() {
	//freopen("C.INP","r",stdin);
	cin >> t;
	while (t--){
		cin >> n;
		n *= 2;
		bool f = false;
		for (int i=2;i<=sqrt(n);i++){
			if (n%i==0 && (n/i-i+1)%2==0){
				f = true;
				int a = (n/i-i+1)/2;
				int b = i + a - 1;
				cout << n/2 << " = " << a;
				for (int j=a+1;j<=b;j++)
					cout << " + " << j;
				cout << endl;
				break;
			}
		}
		if (!f)
			cout << "IMPOSSIBLE" << endl;
	}
}

