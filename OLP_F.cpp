#include <iostream>
#include <fstream>
#include <cstring>
#define inp fin
using namespace std;

char map[500][500];
bool cols[500],rows[500];// true mean odd
int test,m,n;
bool rmCols();
bool rmRows();

bool rmCols(){
	// return true if success
	int i = n-1;
	if (cols[i])
		return false;
	while (i>=0 && !cols[i]){// cot i la cot chan
		for (int j=0;j<m;j++){
			if (map[j][i]){
				rows[j]=!rows[j];
			}
		}
		i--;
	}
	int oldn = n;
	n=i+1;
	bool f = !rmRows();
	n=oldn;
	// restore
	i = n-1;
	while (i>=0 && !cols[i]){// cot i la cot chan
		for (int j=0;j<m;j++){
			if (map[j][i]){
				rows[j]=!rows[j];
			}
		}
		i--;
	}
	return f;
}
bool rmRows(){
	// return true if success
	int i = m-1;
	if (rows[i])
		return false;
	while (i>=0 && !rows[i]){// hang i la cot chan
		for (int j=0;j<n;j++){
			if (map[i][j]){
				cols[j]=!cols[j];
			}
		}
		i--;
	}
	int oldm = m;
	m=i+1;
	bool f = !rmCols();
	m=oldm;
	// restore rows
	i = m-1;
	while (i>=0 && !rows[i]){// hang i la cot chan
		for (int j=0;j<n;j++){
			if (map[i][j]){
				cols[j]=!cols[j];
			}
		}
		i--;
	}
	return f;
}
int main(){
	ifstream fin("OLP_F.inp");
	inp >> test;
	long long x;
	for (int ti=0;ti<test;ti++){
		inp >> m >> n;
		memset(cols,0,sizeof(cols));
		memset(rows,0,sizeof(rows));
		for (int i=0;i<m;i++){
			for (int j=0;j<n;j++){
				inp >> x;
				map[i][j]=x%2;
				if (map[i][j]){
					cols[j]=!cols[j];
					rows[i]=!rows[i];
				}
			}
		}
		// check for onehit move
		bool f = true;
		for (int i=0;i<m;i++)
			if (rows[i]){
				f=false;
				break;
			}
		if (f){
			cout << "TRUE" << endl;
			continue;
		}
		f = true;
		for (int i=0;i<n;i++)
			if (cols[i]){
				f=false;
				break;
			}
		if (f){
			cout << "TRUE" << endl;
			continue;
		}
		if (rmCols()||rmRows()){
			cout << "TRUE" << endl;
		}else{
			cout << "FALSE" << endl;
		}
		
	}
	return 0;
}
