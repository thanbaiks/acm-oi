#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#include <time.h>
#include <fstream>
#include <limits>
#include <iomanip>
#include <iterator>
#define INT_MAX 2147483647
#define INT_MIN -2147483648
#define pi acos(-1.0)
#define E 2.71828182845904523536
using namespace std;

map<int,bool> CanWin;
bool whoWins(long n, long p, bool Current)
{
    for (int i=2; i<=9; i++)
    {
        if (p*i >= n) return Current;

        if (CanWin.find(p*i) != CanWin.end())
        {
            if (CanWin[p*i]) return Current;
            continue;
        }
        CanWin[p*i] =  whoWins(n, p*i, (Current+1)%2) == Current;
        if (CanWin[p*i]) return Current;


    }

    return (Current+1)%2;
}
int main()
{

    long long int n;

    while(scanf("%lld",&n) == 1)
    {
        cout << ( (whoWins(n,1,0)) ? ("Ollie wins.") : ("Stan wins.")) << '\n';
        CanWin.clear();
    }
    return 0;
}
