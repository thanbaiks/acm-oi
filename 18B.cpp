#include <iostream>
#include <fstream>
#include <cstring>
#define inp cin
#define uint32 unsigned long

using namespace std;
int test,n;
uint32 A[10][10],mx;
bool mask[10];

uint32 max(uint32 a,uint32 b){
	return a>b?a:b;
}

void try_(int row,int sum){
	for (int i=0;i<n;i++){
		if (mask[i])
			continue;
		if (row==n-1){
			// last row
			mx=max(mx,sum+A[row][i]);
		}else{
			mask[i]=true;
			try_(row+1,sum+A[row][i]);
			mask[i]=false;
		}
	}
}
int main(){
	ifstream fin("18B.txt");
	inp >> test;
	for (int ti=1;ti<=test;ti++){
		// input
		inp >> n;
		for (int i=0;i<n;i++){
			for (int j=0;j<n;j++){
				inp >> A[i][j];
			}
			mask[i]=false;
		}
		// program
		try_(0,0);
		// output
		cout << "Case #" << ti << ": " << mx << endl;
	}
	return 0;
}
