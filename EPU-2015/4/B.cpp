#include <iostream>
#include <cmath>
#include <vector>
#include <cstring>
/**
 * (b-a+1)(a+b)   = 2N
 *    t  (t+2a-1) = 2N
 **/
#define MAXX 15000000
using namespace std;
typedef long long LL;
int tt,t,c;
vector<int> prm;
LL n;

void makeprime(){
	bool *prime = new bool[MAXX];
	prm.clear();
	memset(prime,1,MAXX*sizeof(bool));
	for (int i=2;i<MAXX;i++){
		if (prime[i]){
			prm.push_back(i);
			for (int j=i*2;j<MAXX;j+=i){
				prime[j] = false;
			}
		}
	}
	delete[] prime;
}

int main() {
	//makeprime();
	//cout << "ready: " << prm.size() << endl;
	cin >> t;
	for (tt=1;tt<=t;tt++){
		cin >> n;
		n *= 2;
		c = 0;
		for (int i=2;i<=round(sqrt(n));i++){
			if (n%i==0){
				int j = n/i-i+1;
				if (!(j%2))
					c++;
			}
		}
		cout << "Case " << tt << ": " << c << endl;
	}
}

