#include <iostream>
#include <cstring>
#define inp cin
#define out cout

using namespace std;

int m,n,c;
char a[102][102];
int step[102][102];

int deltaX(char c){
	switch (c){
		case 'N':
		case 'S':
			return 0;
		case 'W':
			return -1;
		case 'E':
			return 1;
	}
}

int deltaY(char c){
	switch (c){
		case 'W':
		case 'E':
			return 0;
		case 'N':
			return -1;
		case 'S':
			return 1;
	}
}

int main() {
	while (true){
		inp >> m >> n;
		if (m==0)
			break;
		inp >> c;
		memset(a,0,sizeof(a));
		for (int i=1;i<=m;i++)
			inp >> a[i]+1;
		memset(step,0,sizeof(step));
		int xx = c,yy = 1,count = 0;
		while (true){
			//cout << "(" << yy << ":" << xx << "), ";
			if (step[yy][xx] != 0){
				// Loop detected
				//cout << "Loop : " << step[yy][xx] << "; " << count << endl;
				out << step[yy][xx]-1 <<" step(s) before a loop of " << count-step[yy][xx]+1 << " step(s)" << endl;
				break;
			}else if (a[yy][xx] == 0){
				// Leaved map
				out << count << " step(s) to exit" << endl;
				break;
			}
			// Continue go
			step[yy][xx] = ++count;
			int dx = deltaX(a[yy][xx]);
			int dy = deltaY(a[yy][xx]);
			xx += dx;
			yy += dy;
		}
	}
}
