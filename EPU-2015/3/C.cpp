#include <iostream>
#include <string>

using namespace std;
string line;
unsigned long long chksum;

int main() {
	while (true){
		getline(cin,line);
		if (line == "#")
			break;
		chksum = 0;
		for (int i=0;i<line.length();i++){
			chksum += (line[i]==' '?0:line[i]-'A'+1) * (i+1);
		}
		cout << chksum << endl;
	}	
}

