#include <iostream>
#define MAXX 999999999

using namespace std;

int t,na,nb;
int a[105],b;

int diff(int a,int b){
	int c = 0;
	a ^= b;
	while (a!=0){
		if (a&1)
			c++;
		a >>= 1;
	}
	return c;
}

int main() {
	cin >> t;
	while (t--){
		cin >> na >> nb;
		for (int i=0;i<na;i++)
			cin >> a[i];
		while(nb--){
			cin >> b;
			int minf = MAXX, minv = MAXX;
			for (int i=0;i<na;i++){
				int d = diff(a[i],b);
				if (d < minf || (d==minf && a[i] < minv))
					minf = d,minv = a[i];
			}
			cout << minv << endl;
		}
	}
}

