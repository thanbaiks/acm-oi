#include <iostream>
#define MAXN 1005
using namespace std;

int t,tt,n,m,l,r,a[MAXN];
int max(int a,int b){
	return a>b?a:b;
}
int main() {
	cin >> t;
	for (int tt=0;tt<t;tt++){
		cin >> n;
		for (int i=0;i<n;i++)
			cin >> a[i];
		// queries
		cin >> n;
		for (int i=0;i<n;i++){
			cin >> l >> r;
			int mx = -1;
			for (int j=l-1;j<r;j++){
				mx = max(mx,a[j]);
			}
			cout << mx << endl;
		}
	}
}

