#include <iostream>
#include <cmath>
#include <vector>
using namespace std;
typedef long long LL;

int t,tt;
int main() {
	cin >> t;
	for (tt=0;tt<t;tt++){
		LL len = 0,total = 0,n;
		int i;
		cin >> n;
		for (i=1;i<1000000;i++){
			len += log10((double)i)+1;
			if (total + len >= n)
				break;
			total += len;
		}
		n -= total;
		for (i=1;;i++){
			int j = log10((double)i)+1;
			if (n-j<=0){
				vector<int> v;
				while (i > 0){
					v.push_back(i%10);
					i /= 10;
				}
				cout << v[j-n] << endl;
				break;
			}
			n-=j;
		}
	}
}

