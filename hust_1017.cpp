#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define VI vector<int>
#define DEBUG 1

using namespace std;
typedef long long LL;


/** VARIABLES **/
vector<pair<int,VI> > a;
VI rs;
int m,n,c,t;
int b[1002]; // begin of i width
bool row[1002];//

bool cmp(const pair<int,VI> &a,const pair<int,VI> &b){
	int l = min(a.second.size(),b.second.size());
	for (int i=0;i<l;i++){
		if (a.second[i]<b.second[i])
			return true;
		else if (a.second[i]>b.second[i])
			return false;
	}
	if (a.second.size()<b.second.size())
		return true;
	else if (a.second.size()>b.second.size())
		return false;
	return true;
}


bool try_(int r){
	for (int i=0;i<a[r].second.size();i++){
		if (row[a[r].second[i]])
			return false; // trung bit
	}
	// khong bi trung bit
	// them vao row
	for (int i=0;i<a[r].second.size();i++){
		row[a[r].second[i]] = true;
	}

	// tim bit 0 dau tien
	for (int i=1;i<=m;i++){
		if (!row[i]){
			// i la cot co bit 0
			int rr=b[i];
			// rr la bat dau cua dong co begin(i)
			if (rr==-1)// cannot find a row have start with bit i
			{
				// rollback
				for (int i=0;i<a[r].second.size();i++){
					row[a[r].second[i]] = false;
				}
				return false;
			}
			do {
				if (try_(rr)){
					rs.push_back(a[r].first);
					return true;
				}
				rr++;
			} while (rr < n && a[rr].second[0]==i);
			// rollback
			for (int i=0;i<a[r].second.size();i++){
				row[a[r].second[i]] = false;
			}
			return false;
		}
	}
	rs.push_back(a[r].first);
	return true;
}

int main(){
	if (DEBUG){
		freopen("hust_1017_out.txt","w",stdout);
		freopen("hust_1017.txt","r",stdin);
	}
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++){
		scanf("%d",&c);
		VI vi;
		for (int j=0;j<c;j++){
			scanf("%d", &t);
			vi.push_back(t);
		}
		sort(vi.begin(),vi.end());
		a.push_back(make_pair(i,vi));
	}
	sort(a.begin(),a.end(),cmp);
	
	if (DEBUG){
		printf("after sort:\n");
		for (int i=0;i<n;i++){
			printf("%d: ",i);
			for (int j=0;j<a[i].second.size();j++){
				printf("%d ",a[i].second[j]);
			}
			printf("\n");
		}
		printf("\n");
	}
	
	for (int i=1;i<=m;i++){
		b[i] = -1;
	}
	for (int i=0;i<n;i++){
		if (b[a[i].second[0]] == -1)
			b[a[i].second[0]] = i;
	}
	
	if (DEBUG){
		printf("B=\n");
		for (int i=1;i<=m;i++)
			printf("%d ",b[i]);
		printf("\n\n");
	}
	
	memset(row,0,sizeof(row));
	for (int i=0;i<n && a[i].second[0]==1;i++){
		if (try_(i)){
			break;
		}
	}
	if (!rs.size()){
		printf("NO");
		return 0;
	}
	sort(rs.begin(),rs.end());
	printf("%d ",rs.size());
	for (int i=0;i<rs.size();i++){
		printf("%d ", rs[i]);
	}
	return 0;
}

