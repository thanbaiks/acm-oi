#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)
#define MAXN 10000001
using namespace std;
typedef long long LL;
int n,q,qq,x;
int a[MAXN];

int bs(int x){
	if (x < a[0] || x >= n){
		return -1;
	}
	int l=0,r=n-1,m;
	while (l<r){
		m = (l+r)/2;
		if (x < a[m]){
			r = m-1;
		}else if (x > a[m]){
			l = m+1;
		}else{
			return m;
		}
	}
	
}
int main(){
	freopen("input.txt","r",stdin);
	// == input
	int tt = 0;
	while (cin >> n >> q){
		cout << n << q;
		FOR(i,0,n){
			cin >> a[i];
		}
		sort(a,a+n);
		cout << "CASE# " << ++tt << ':' << endl;
		FOR (i,0,q){
			cin >> qq;
			x=bs(qq);
			if (x >= 0){
				cout << qq << " found at " << x + 1 << endl;
			}else{
				cout << qq << " not found"<< endl;
			}
		}
	}
	return 0;
}

