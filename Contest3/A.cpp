#include <bits/stdc++.h>
#define MAXX 100005
using namespace std;
int n,m,a[MAXX],sum;

int main() {
	cin >> n >> m;
	sum = 0;
	for (int i=0;i<m;i++){
		cin >> a[i];
		sum += a[i];
	}
	int res = 0;
	for (int i=0;i<m;i++){
		int k = round(1.0f*(n-m)*a[i]/sum);
		res = max(res,(int)ceil(1.0f*a[i]/(k+1)));
	}
	cout << res;
}

