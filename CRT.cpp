#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator

using namespace std;
typedef long long LL;
long long CRT(const vector< long long > &r,const vector< long long > &mods){
    long long M=1;
    for(int i=0; i<int(mods.size()); i++) M*=mods[i];
    vector< long long > m, s;
    for(int i=0; i<int(mods.size()); i++){
        m.push_back(M/mods[i]);
        long long temp=m[i]%mods[i];
        long long k=0;
        while(true){
            if((k*temp)%mods[i]==1) break;
            k++;
        }
        s.push_back(k);
    }
    long long ret=0;
    for(int i=0; i<int(s.size()); i++) {
        ret+=( (m[i]*s[i])%M *r[i] )%M;
        if(ret>=M) ret-=M;
    }
    return ret;
}
LL crt(const vector<LL> &r,const vector<LL> &m){
	/*
	LL M = 1;
	vector<LL> x,y;
	for (int i=0;i<m.size();i++) M*=m[i];
	for (int i=0;i<m.size();i++){
		y.push_back(M/m[i]);
		LL tmp = y[i]%m[i];
		LL k = 1;
		while (true){
			if ((k*tmp)%m[i]==1)
				break;
			k++;
		}
		x.push_back(k);
	}
	LL ret = 0;
	for (int i=0;i<m.size();i++){
		ret = (ret + (x[i]*y[i])%M*r[i])%M;
	}
	return ret;
	*/
	LL M = 1;
	vector<LL> x,y;
	for (int i=0;i<m.size();i++){
		M *= m[i];
	}
	for (int i=0;i<m.size();i++){
		y.push_back(M/m[i]);
		LL tmp = y[i]%m[i];
		LL k = 1;
		while ((k*tmp)%m[i] != 1){
			k++;
		}
		x.push_back(k);
	}
	LL ret = 0;
	for (int i=0;i<m.size();i++){
		ret = (ret+(x[i]*y[i])%M*r[i])%M;
	}
	return ret;
}
int main(){
	LL r[] = {2,3,1};
	LL m[] = {3,4,5};
	vector<LL> vr(r,r+3);
	vector<LL> vm(m,m+3);
	cout << CRT(vr,vm) << endl << crt(vr,vm);
	return 0;
}

