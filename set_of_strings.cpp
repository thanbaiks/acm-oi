#include <iostream>
#include <vector>
#include <cstring>
using namespace std;
int main(){
    string s;
    int n,c;
    bool a[128];
    vector<int> vi;
    while (cin >> n){
        cin >> s;
        memset(a,0,sizeof(a));
        vi.clear();
        c = 0;
        for (int i=0;i<s.length();i++){
            if (!a[s[i]]){
                c++;
                vi.push_back(i);
                a[s[i]] = true;
            }
        }
        if (c >= n){
            cout << "YES" << endl;
            for (int i=0;i<n;i++){
                if (i < n-1){
                    cout << s.substr(vi[i],vi[i+1]-vi[i]) << " ";
                }else{
                    cout << s.substr(vi[i],s.length()-vi[i]) << endl;
                }
            }
        }else{
            cout << "NO" << endl;
        }
    }
}
