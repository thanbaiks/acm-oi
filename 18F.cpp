#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#define inp	fin
#define INF 10000000000
using namespace std;

int test,n,cur;
double kc[1000][1000],pos[1000][2],d[1000],mn,sum;
bool picked[1000];

double distance(double x1,double y1,double x2,double y2){
	return sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
}

int main(){
	ifstream fin("18F.inp");
	inp >> test;
	for (int ti=1;ti<=test;ti++){
		// input
		inp >> n;
		for (int i=0;i<n;i++){
			inp >> pos[i][0] >> pos[i][1];
		}
		// calc distances
		for (int i=0;i<n-1;i++)
			for (int j=i+1;j<n;j++){
				kc[i][j]=kc[j][i]=distance(pos[i][0],pos[i][1],pos[j][0],pos[j][1]);
			}
		// reset distance
		for (int i=0;i<n;i++)
			d[i] = INF,picked[i]=false;
		sum=0;
		// add first point
		d[0]=0;
		picked[0]=true;
		for (int i=1;i<n;i++){
			d[i]=kc[0][i];
		}
		for (int l=1;l<n;l++){
			// repeat n-1 times
			// find smallest node
			mn = INF;
			cur = -1;
			for (int i=1;i<n;i++){
				if (d[i]<mn && !picked[i]){
					mn = d[i];
					cur = i;
				}
			}
			// add that node
			picked[cur]=true;
			sum+=mn;
			//cout << "Picked " << cur << endl;
			for(int i=1;i<n;i++)
				if (!picked[i])
					d[i]=min(d[i],kc[cur][i]);
		}
		// output
		cout << "Case #" << ti <<": " << fixed << setprecision(2) <<  sum << endl;
	}
}
