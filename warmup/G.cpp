#include <bits/stdc++.h>
#define MACK 18
#define ZACK 17

using namespace std;
int t;
bool f[100];
int a[10];

int main() {
	cin >> t;
	while (t--){
		memset(f,0,sizeof(f));
		for (int i=0;i<10;i++){
			cin >> a[i];
			cout << a[i] << ' ';
			f[a[i]]=true;
		}
		cout << endl;
		if (f[MACK])
			if (f[ZACK])
				cout << "both" << endl << endl;
			else
				cout << "mack" << endl << endl;
		else
			if (f[ZACK])
				cout << "zack" << endl << endl;
			else
				cout << "none" << endl << endl;
	}
}

