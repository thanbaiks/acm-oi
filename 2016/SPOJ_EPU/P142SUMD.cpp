#include <iostream>
#include <stdio.h>
#include <vector>
#include <cstring>
#define rep(i, n) for (int i=0;i<n;i++)
#define N 1000005
#define P 256
#define uint64 unsigned long long
using namespace std;
 
uint64 hashVal[N], hashPow[N], hashCur;
char s[N];
int n, lo, hi, len, flag, ans;
vector <int> pos;
 
uint64 getHash(int l, int r) {
    return (hashVal[r + 1] - hashVal[l]) * hashPow[n - r];
}
 
int main() {
    gets(s);
	n = strlen(s);
    hashPow[0] = 1; hashVal[0] = 0;
    for (int i = 1;i <= n;i++) {
        hashPow[i] = hashPow[i - 1] * P;
        hashVal[i] = hashVal[i - 1] + s[i - 1] * hashPow[i - 1];
    }
    for (int i = 0;i < n;i++)
        if (getHash(0, i) == getHash(n - i - 1, n - 1)) pos.push_back(i);
    lo = 0; hi = pos.size() - 1; ans = -1;
    while (lo <= hi) {
        int mid = (lo + hi) >> 1;
        len = pos[mid] + 1;
        hashCur = getHash(0, pos[mid]);
        flag = 0;
        for (int j = 1;j <= n - len - 1 && !flag;j++) {
            if (getHash(j, j + len - 1) == hashCur) flag = 1;
        }
        if (!flag) hi = mid - 1;
        else {
            ans = pos[mid];
            lo = mid + 1;
        }
    }
    if (ans == -1) puts("Just a legend");
    else {
        s[ans + 1] = '\0';
        puts(s);
    }
    return 0;
} 
