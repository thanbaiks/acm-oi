#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e6+5, MOD = 1000007;
LL A[N];

void init() {
	LL t = 0;
	_for (i,1,1e6+1) {
		t = (t + i*3-1)%MOD;
		A[i] = t;
	}
}

LL solve(int n) {
	return A[n];
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    
    int t,n;
    init();
    cin >> t;
    while (t--) {
    	cin >> n;
    	cout << solve(n) << '\n';
	}
}


