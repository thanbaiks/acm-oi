#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

void solve() {
	LL a,b,s,n,d;
	cin >> a >> b >> s;
	n = s*2/(a+b);
	d = (b-a)/(n-5);
	
	cout << n << '\n';
	a -= 2*d;
	_for (i,0,n) {
		cout << a << ' ';
		a += d;
	}
	cout << '\n';
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
	cin >> t;
    while (t--)
    	solve();
}


