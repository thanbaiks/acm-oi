#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e5+5;
int n,m,A[N];
map<int,int> MAP;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> n >> m;
	_for (i,0,n) cin >> A[i];
	
	int t = 0, i=0,j=0, res = 0;
	while (j < n) {
		if (MAP[A[j]] == 0) {
			t++;
			while (t > m) {
				MAP[A[i]]--;
				if (!MAP[A[i]]) t--;
				i++;
			}
		}
		MAP[A[j]]++;
		j++;
		res = max(res, j-i);
	}
	cout << res;
}
