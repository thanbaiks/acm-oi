#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	char s[105];
	cin >> s;
	int n = strlen(s);
	int k = sqrt(n);
	while (n%k!=0) k--;
	int c = n/k;
	swap(c,k);
	_for (i,0,n) {
		cout << s[i%k*c + i/k];
	}
}

