#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

vector<double> v;

void prepair() {
	double lim = 5.2L;
	double d = 0;
	int k = 2;
	while (d <= lim) {
		d += 1.0l/k;
		k++;
		v.push_back(d);
	}
}

void solve() {
	prepair();
	double d;
	while (cin >> d) {
		if (d == 0) return;
		cout << lower_bound(v.begin(), v.end(), d) - v.begin() + 1<< " card(s)\n";
	}
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);

	solve();
}

