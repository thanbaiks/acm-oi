#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int d[505];
int n,s;

void solve() {
	while (1) {
		cin >> s >> n;
		if (s==0) return;
		
		__(d);
		int u,v;
		_for(xxx,0,n) {
			cin >> u >> v;
			for (int i=s;i-u>=0;i--) {
				if (i-u==0 || d[i-u]) {
					d[i] = max(d[i],d[i-u]+v);
				}
			}
		}
		u = 0;
		_for(i,1,s+1) 
			if (d[i]>d[u]) u = i;
		cout << u << ' ' << d[u] << '\n';
	}
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


