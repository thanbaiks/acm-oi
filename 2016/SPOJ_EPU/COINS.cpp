#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

map<int,LL> m;

LL solve(LL a) {
	if (a<12) return a;
	if (m.count(a)) return m[a];
	return m[a] = solve(a/2)+solve(a/3)+solve(a/4);
}

void solve() {
	int a;
	while (cin >> a) {
		cout << solve(a) << '\n';
	}
}

void test() {
	_for (i,0,1000) {
		cout << i << '\t' << i/2+i/3+i/4 << '\t' << i/2 << '\t' << i/3 << '\t' << i/4 << '\n';
//		if (i >= i/2+i/3+i/4) cout << i << ' ';
	}
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	solve();
//	test();
}
