#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

void solve() {
	int n;
	cin >> n;
	if (n==1) {
		cout << 1;
		return;
	}
	bool b = false;
	for (int i=2;i<=sqrt(n);i++) {
		while (n%i==0) {
			n /= i;
			if (b) cout << '*';
			cout << i;
			b = true;
		}
	}
	if (n>1){
		if (b) cout << '*';
		cout << n;
	}
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}
