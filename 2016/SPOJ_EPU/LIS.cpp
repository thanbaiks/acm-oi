#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N=3e4+5;
int A[N],n;

struct S {
	int v,d;
};

bool cmp(const S &a, const int x) {
	return a.v<x;
}

void solve() {
	cin >> n;
	_for(i,0,n) {
		cin >> A[i];
	}
	vector<S> v;
	vector<S>::iterator it;
	int d;
	_for(i,0,n) {
		if (v.empty()) {
			v += (S){A[i],1};
		} else {
			it = lower_bound(_all(v), A[i], cmp)-1;
			if (it>=v.begin()) {
				d = it->d+1;
			} else {
				d = 1;
			}
			it++;
			if (it==v.end() || it->d > d) {
				v += (S){A[i], d};
			} else if (it->v>A[i]){
				it->v = A[i];
			}
		}
	}
	cout << v[v.size()-1].d;
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


