#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e5+5;
int n,m,ql,qr;
LL T[N*4], P[N*4];

void apply(int i, int l, int r) {
    if (P[i]) {
        int m = (l+r)>>1;
        T[i<<1] += P[i]*(m-l+1);
        P[i<<1] += P[i];
        
        T[i<<1|1] += P[i]*(r-m);
        P[i<<1|1] += P[i];
        P[i] = 0;
    }
}

LL get(int i, int l, int r) {
    if (l>qr || r<ql) return 0;
    if (l>=ql && r<=qr) return T[i];
    
    apply(i,l,r);
    int m = (l+r)>>1;
    return get(i<<1,l,m) + get(i<<1|1,m+1,r);
}

void update(int i, int l, int r, LL v) {
    if (l>qr || r<ql) return;
    if (l>=ql && r<=qr) {
        T[i] += v*(r-l+1);
        P[i] += v;
        return;
    }
    apply(i,l,r);
    int m = (l+r)>>1;
    update(i<<1,l,m,v);
    update(i<<1|1,m+1,r,v);
    T[i] = T[i<<1] + T[i<<1|1];
}

void solve() {
    cin >> n >> m;
    int op,v;
    __(T);
    __(P);
    _for(i,0,m) {
        cin >> op >> ql >> qr;
        if (op) {
            cout << get(1,1,n) << '\n';
        } else {
            cin >> v;
            update(1,1,n,v);
        }
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--)
        solve();
}


