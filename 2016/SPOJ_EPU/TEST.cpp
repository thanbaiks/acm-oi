#include <iostream>

using namespace std;

int a[100], c = 0;
const int LIM = 1000;

void gen(int k) {
	for (int i=4;i<10;i+=3) {
		int t = k*10 + i;
		if (k < LIM) {
			a[c++] = t;
			gen(t);
		}
	}
}

int main() {
	gen(0);
	int x;
	cin >> x;
	for (int i=0;i<c;i++) {
		if (x%a[i] == 0) {
			cout << "YES";
			return 0;
		}
	}
	cout << "NO";
}
