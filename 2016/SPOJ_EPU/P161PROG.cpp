#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	LL n, res = 1, t;
	cin >> n;
	_for (i,2,sqrt(n)+1) {
		if (n%i == 0) {
			res *= i;
			while (n%i == 0) n /= i;
		}
	}
	res *= n;
	cout << res;
	return 0;
}

