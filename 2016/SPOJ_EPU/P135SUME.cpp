#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int solve() {
	int a[8];
	_for(i,0,8) cin >> a[i];
	_for(i,2,8) if ((a[i]-a[i-1])*(a[i-1]-a[i-2]) < 0) {
		return 0;
	}
	return a[1]>a[0]?1:-1;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    int t = solve();
    if (t<0) {
    	cout << "descending";
	} else if (t>0) {
    	cout << "ascending";
	} else {
		cout << "mixed";
	}
}


