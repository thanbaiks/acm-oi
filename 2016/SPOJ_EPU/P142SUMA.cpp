#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

bool check(LL n) {
	n = 8*n+1;
	LL k = sqrt(n);
	while (k*k<n)k++;
	return k*k==n;
}

void solve() {
	LL n;
	cin >> n;
	for (LL i = 1;i*(i+1)<2*n;i++) {
		if (check(n-i*(i+1)/2)) {
			cout << "YES";
			return;
		}
	}
	cout << "NO";
}

int main(){
	#ifdef NGOBACH
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


