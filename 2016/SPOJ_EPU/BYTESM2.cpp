#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e2+4;
int n,m,A[N][N];

void solve() {
	cin >> n >> m;
	__(A);
	_for(i,1,n+1) {
		_for(j,1,m+1) {
			cin >> A[i][j];
			A[i][j] += max(max(A[i-1][j-1],A[i-1][j]),A[i-1][j+1]);
		}
	}
	int ans = 0;
	_for(i,1,m+1) ans = max(ans, A[n][i]);
	cout << ans << '\n';
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--)
    solve();
}


