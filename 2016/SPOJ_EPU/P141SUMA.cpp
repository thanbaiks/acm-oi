#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

struct Vector {
	LL x,y;
} u,v,v1,v2;

bool check(const Vector &a, const Vector &b) {
	if (a.x*b.x<0 || a.y*b.y<0) return false;
	if (a.y*b.y==0) {
		// Mot trong 2 bang khong
		if (a.y+b.y!=0) return false;
		else return a.x*b.x>0;
	}
	return a.x*b.y == a.y*b.x;
}

int A[3][2];

void solve() {
	_for(i,0,3) _for(j,0,2) cin >> A[i][j];
	u.x=A[1][0]-A[0][0]; u.y = A[1][1]-A[0][1];
	v.x=A[2][0]-A[1][0]; v.y = A[2][1]-A[1][1];
	int t;
	// Xoay theo chieu kim dong ho
	v1.y = -v.x; v1.x = v.y;
	v2.y = v.x; v2.x = -v.y;
	if (check(u,v)) {
		cout << "TOWARDS";
	} else if (check(u,v1)) {
		cout << "LEFT";
	} else if (check(u,v2)) {
		cout << "RIGHT";
	} else throw;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


