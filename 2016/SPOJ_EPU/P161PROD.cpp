#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int LEFT = 0, RIGHT = 1;
char cm[2][1000];

void init(){
	char s[3][100] = {
		"qwertyuiop",
		"asdfghjkl;",
		"zxcvbnm,./"
	};
	_for (i,0,3) {
		int len = strlen(s[i]);
		_for (j,0, len) {
			cm[LEFT][s[i][j]] = s[i][(len+j-1)%len];
			cm[RIGHT][s[i][j]] = s[i][(len+j+1)%len];
		}
	}
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	char c, s[1000];
	init();
	cin >> c >> s;
	if (c=='L') c = RIGHT; else c = LEFT;
	_for(i,0,strlen(s)) {
		cout << cm[c][s[i]];
	}
	return 0;
}

