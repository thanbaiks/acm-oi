#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

LL randll() {
	LL res = 0;
	_for (i,0,8) {
		res = (res<<8) | (LL(rand())&0xFF);
	}
	res &= LLONG_MAX;
	return res;
}

void solve() {
	srand(time(0));
	const int MOD = 1e9;
	const int COUNT = 10000;
	cout << COUNT << '\n';
	_for(i,0, COUNT) {
		cout << randll()%MOD << ' ' << randll()%MOD << '\n';
	}
}

int main(){
	#if NGOBACH
	freopen("input.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


