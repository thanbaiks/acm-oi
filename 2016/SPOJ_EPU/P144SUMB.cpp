#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> P;
int n;

void solve() {
    cin >> n;
    int x;
    map<int,P> m;
    
    _for(i,1,n+1) {
        cin >> x;
        if (!m.count(x)) {
            // First seen
            m[x] = make_pair(0,i);
            continue;
        }
        P &p = m[x];
        if (!p.first){
            // set acceleration
            p.first = i-p.second;
        } else if (p.first>0 && p.first != i-p.second) {
            p.first = -1;
        }
        p.second = i;
    }
    
    queue<P> que;
    _it(i,m) {
        if (i->second.first>=0) {
            que.push(make_pair(i->first, i->second.first));
        }
    }
    cout << que.size() << '\n';
    while (!que.empty()) {
        cout << que.front().first << ' ' << que.front().second << '\n'; que.pop();
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


