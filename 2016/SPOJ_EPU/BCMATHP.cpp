#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int a, b, e;
	ULL t, _ = 1;
	
	cin >> a >> b;
	e = a+1;
	while (e < 63) {
		t = ULL(1)<<e;
		while (t >= 10*_) _*=10;
		if (t/_ == b) {
			cout << e;
			return 0;
		}
		e++;
	}
	cout << 0;
}


