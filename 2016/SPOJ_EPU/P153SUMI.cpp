#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e5+3;
int a[N*2],d[N*2],n;
void solve() {
    cin >> n;
    _for(i,0,n) {
        cin >> a[i]; a[i+n] = a[i];
    }
    bool f = false;
    d[0] = 1;
    _for(i,1,n<<1) {
        if (a[i] >= a[i-1])
            d[i] = d[i-1]+1;
        else
            d[i] = 1;
        if (!f && d[i]==n) f = true;
    }
    if (!f) {
        cout << -1;
    } else {
        cout << d[n-1]%n;
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


