#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 5e4+5;
int n;
vector<int> v[N];
int dp[N][2];
bool visited[N];

bool dfs(int x) {
	if (visited[x]) return false;
	visited[x] = true;
	dp[x][0] = 0; dp[x][1] = 1;
	for (vector<int>::iterator i = v[x].begin(); i != v[x].end(); ++i) {
		int y = *i;
		if (dfs(y)){
			dp[x][0] += max(dp[y][1],dp[y][0]);
			dp[x][1] += dp[y][0];
		}
	}
	return true;
}


int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> n;
	int a,b;
	_for(i,0,n) {
		cin >> a >> b;
		v[a].push_back(b);
		v[b].push_back(a);
	}
	
	memset(dp,0,sizeof(dp)); memset(visited, 0, sizeof(visited));
	dfs(1);
	cout << max(dp[1][0], dp[1][1]);
}


