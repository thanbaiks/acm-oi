#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

void solve() {
	int n;
	while (1) {
		cin >> n;
		if (!n) return;
		LL res = 0;
		_for(i,1,n+1) {
			res += i*i;
		}
		cout << res << '\n';
	}
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);

	solve();
}

