#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}
typedef pair<int,int> P;

const int N = 105;
const int INF = 1e9;
int f[N],d[N][N],v[N],n,A[N],B[N][2];

int solve() {
    cin >> n;
    _for(i,2,n)
        cin >> A[i];
    _for(i,1,n+1) {
        cin >> B[i][0] >> B[i][1];
    }
    _for(i,1,n+1)_for(j,1,n+1) {
        d[i][j] = abs(B[i][0]-B[j][0])+abs(B[i][1]-B[j][1]);
    }
    _for(i,1,n+1) f[i] = INF, v[i] = false;
    
    priority_queue<P, vector<P>, greater<P> > pq;
    v[1] = true;
    _for(i,2,n+1) pq.push(make_pair(d[1][i],i));
    int ans = 0, rem = 0;
    while (1) {
        P p = pq.top();
        pq.pop();
        if (v[p.second]) continue;
        if (p.second==n) {
            return ans + max(0,p.first-rem);
        }
        v[p.second] = true;
        rem += 
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout << solve();
}


