#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef vector<LL> Bach;
const int BASE = 1000000000, BS = 9;

istream &operator>>(istream &is, Bach &b) {
	string s;
	cin >> s;
	if (s.empty()) return is;
	int n = s.size();
	b.clear();
	LL t = 0;
	for (int i=n-1;i>=0;i-=BS) {
		int j = max(0,i-BS+1);
		t = 0;
		_for(k,j,i+1) t = t*10+s[k]-'0';
		b += t;
	}
	return is;
}

ostream &operator<<(ostream &os, Bach b) {
	os << b.back();
	_fod(i,b.size()-2,-1) os << setw(BS) << setfill('0') << b[i];
	return os;
}

Bach operator+(Bach a, Bach b) {
	int n = max(a.size(), b.size());
	Bach res(n+2,0);
	_for(i,0,n) {
		if (i<a.size()) res[i] += a[i];
		if (i<b.size()) res[i] += b[i];
		res[i+1] += res[i]/BASE;
		res[i] %= BASE;
	}
	while (res.back()==0) res.pop_back();
	return res;
}

Bach operator-(Bach b, int t) {
	Bach res = b;
	res += 0LL;
	res[0] -= t;
	int i = 0;
	while (b[i]<0) {
		res[i] += BASE;
		res[i+1]--;
		i++;
	}
	while (res.back()==0) res.pop_back();
	return res;
}
bool operator<(const Bach b, int n) {
	return b.size()==1 && b[0]<n;
}

void solve() {
	Bach b;
	while (cin >> b) {
		if (b<2) {
			cout << b << endl;
		} else {
			cout << b+b-2<< endl;
		}
	}
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


