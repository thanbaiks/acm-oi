#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

LL T[1005][1005];

LL get(int n, int r) {
    if (n==1) return 1;
    else if (n==0) return 0;
    if (T[n][r]) return T[n][r];
    int k = n - int(sqrt(2*n+1)+0.5f);
    return T[n][r] = 2*get(k,r) + get(n-k, r - 1);
}
void solve() {
    int n;
    while (cin >> n && n) {
        cout << get(n, 4) << '\n';
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


