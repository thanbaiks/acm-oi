#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e5+5;
LL n,n2,a[N], k, res;
char mov[300005];

bool rot(int &x, int &y, int n) {
	if (y > n-1-x){
		x=n-1-x,y=n-1-y;
		return true;
	}
	return false;
}

LL calc(int x, int y, int n) {
	bool r = rot(x, y, n);
	LL ret = a[x+y];
	ret += ((x+y)&1)?x:y;
	return r?n2+1-ret:ret;
}

void move(int &x, int &y, char c) {
	switch (c) {
		case 'U': x--; break;
		case 'D': x++; break;
		case 'L': y--; break;
		case 'R': y++; break;
	}
}
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> n >> k >> mov;
	n2 = n*n;
	a[0] = 1;
	_for(i,1,n){
		a[i] = a[i-1] + i;
	}
	
	res = 1;
	int x=0,y=0;
	_for (i,0, k){
		move(x,y, mov[i]);
		LL t = calc(x,y,n);
//		cout << "x=" << x << "; y=" << y << "; t=" << t << endl;
		res += t;
	}
	cout << res << endl;
}


