#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

bool prime(int x) {
	int j = sqrt(x);
	for (int i = 2; i <= j; i++) {
		if (x % i == 0) {
			return false;
		}
	}
	return true;
}
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	LL x,n;
	cin >> n;
	while (n--) {
		cin >> x;
		LL t = sqrt(x);
		if (x == 1 || t*t != x || !prime(t)) {
			cout << "NO\n";
		} else {
			cout << "YES\n";
		}
	}
	return 0;
}
