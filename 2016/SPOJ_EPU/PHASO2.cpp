#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e6+5;
int A[N],B[N],c = 0;

void solve() {
	int t,m;
	cin >> t >> m;
	cout << t/m << '.';
	t %= m;
	if (t ==0) {
		cout << '0';
		return;
	}
	
	memset(A,0,sizeof(A));
	fill(B,B+N, -1);
	
	while (1) {
		if (B[t]>=0) {
			// Loop here
			int k = B[t];
 			_for(i,0,k) cout << A[i];
 			cout << '(';
			_for(i,k,c) cout << A[i];
			cout << ')';
			return;
		}
		B[t] = c;
		t = t*10;
		A[c] = t/m;
		t %= m;
		c++;
		if (!t){
			// Not looping
			_for(i,0,c) cout << A[i];
			return;
		}
	}
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

