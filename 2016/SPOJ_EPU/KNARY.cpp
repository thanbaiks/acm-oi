#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int k,n, A[100];

int pow(int a, int e) {
	int r = 1;
	while (e--) r *= a;
	return r;
}

void print(int at) {
	_for(i,1,k+1) {
		A[at] = i;
		if (at == n-1) {
			_for (i,0,n) cout << A[i];
			cout << '\n';
		} else {
			print(at+1);
		}
	}
}

void solve() {
    cin >> k >> n;
    cout << pow(k,n) << '\n';
    print(0);
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

