#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int n,A[105],D[1000005],m;

void solve() {
    cin >> n;
    _for(i,0,n) cin >> A[i];
    m = 0;
    _for(d,0,n) _for(e,0,n) _for(f,0,n) {
        if (!A[d]) continue;
        D[m++] = (A[e]+A[f])*A[d];
    }
    sort(D,D+m);
    int ans = 0;
    _for(a,0,n) _for(b,0,n) _for(c,0,n) {
        int k = A[a]*A[b]+A[c];
        ans += upper_bound(D,D+m,k)-lower_bound(D,D+m,k);
    }
    cout << ans;
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


