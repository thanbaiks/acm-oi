#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 105, INF = 1e7;
int n,m;
int d[N][N];
void solve() {
    cin >> n >> m;
    _for(i,1,n+1) _for(j,1,m+1) cin >> d[i][j];
    _for(i,0,n+2) d[i][0]=d[i][m+1] = INF;
    _for(i,0,m+2) d[0][i]=d[n+1][i] = INF;
    _for(i,2,n+1) _for(j,1,m+1) d[i][j] += min(d[i-1][j-1],min(d[i-1][j], d[i-1][j+1]));
    cout << *min_element(d[n]+1, d[n]+m);
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


