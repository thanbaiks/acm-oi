#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

void solve() {
	double a,b;
	cin >> a >> b;
	LL x = ceil(a), y = floor(b);
	cout << (x+y)*(y-x+1)/2;
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

