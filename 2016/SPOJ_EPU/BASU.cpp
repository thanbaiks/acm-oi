#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

char s[10][10];
const int D[] = {10,9,8,7,6};
void solve() {
    _for (i,0,7) {
    	cin >> s[i];
	}
	int sc = 0;
	_for(i,0,7)_for(j,0,7) {
		if (s[i][j]!='.') {
			int n = s[i][j] - '0', t = abs(i-3) + abs(j-3);
			if (t <= 4)
				sc += D[t]*n;
		}
	}
	cout << sc;
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

