#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

void solve() {
	int a[3];
	while (1) {
		cin >> a[0] >> a[1] >> a[2];
		if (!(a[0]|a[1]|a[2])) return;
//		sort(a,a+3);
		if (a[1]*2 == a[0]+a[2]) {
			cout << "AP " << 2*a[2]-a[1] << '\n';
		} else {
			cout << "GP " << a[2]*a[2]/a[1] << '\n';
		}
	}
	
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);

	solve();
}

