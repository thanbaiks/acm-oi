#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e4+5;
int n;
LL d[2][N],a[N];

void solve() {
	cin >> n;
	_for(i,1,n+1) {
		cin >> a[i];
	}
	d[0][0] = d[1][0] = 0;
	_for(i,1,n+1) {
		d[0][i] = max(d[0][i-1],d[1][i-1]);
		d[1][i] = d[0][i-1]+a[i];
	}
	cout << max(d[0][n], d[1][n]) << '\n';
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    _for(i,1,t+1) {
    	cout << "Case " << i << ": ";
    	solve();
	}
}


