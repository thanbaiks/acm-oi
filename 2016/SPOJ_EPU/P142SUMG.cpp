#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

char s[1005];
int n;
pair<int,char> d[30];

bool check(char c) {
	return c>='A' && c<='Z';
}

void solve() {
	do {
		cin.getline(s,1005);
	} while (!(n=strlen(s)));
	
	_for(i,0,26) d[i] = make_pair(0,'A'+i);
	_for(i,0,n) if (check(s[i])) d[s[i]-'A'].first++;
	sort(d,d+26, greater<pair<int,char> >());
//	cout << d[0].second << ' ' << d[0].first << endl;
	if (d[0].first == d[1].first) {
		cout << "NOT POSSIBLE\n";
	} else {
		int dis = (d[0].second - 'E'+26*26)%26;
		cout << dis  << ' ';
		_for(i,0,n) {
			if (check(s[i])) cout << char((s[i]-'A'-dis+26*26)%26+'A');
			else cout << s[i];
		}
		cout << '\n';
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    int T;
    cin >> T;
    while (T--)
    	solve();
}


