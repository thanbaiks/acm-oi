#include <iostream>
#include <limits>
#include <cstring>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(0);
	cin.tie(0);
	int n, a[10000], t;
	memset(a,0, sizeof(a));
	cin >> n;
	while (n--) {
		cin >> t;
		a[t] = 1;
	}
	_for (i,1,30005) {
		if (!a[i]) {
			cout << i;
			return 0;
		}
	}
	return 0;
}

