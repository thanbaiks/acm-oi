#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int MOD = 1e9+7;
const int N = 1e6+6;
int a,b,n;
LL gt[N];

void init() {
    gt[0] = 1;
    _for(i,1,N) gt[i] = (gt[i-1]*i)%MOD;
}

bool check(int t) {
    if (!t) return false;
    
    while (t) {
        if (t%10!=a && t%10 !=b) return false;
        t /= 10;
    }
    return true;
}

LL nCk2(LL n, LL k) {
    LL res = 1;
    _for (i,k+1,n+1) res *= i;
    _for (i,1,n-k+1) res /= i;
    return res;
}

LL pm(LL a, LL e, LL m) {
    if (m==1) return 0;
    if (!e) return 1;
    LL t = 1;
    while (e > 1) {
        if (e&1) t = t*a%m;
        a = a*a%m;
        e >>= 1;
    }
    return t*a%m;
}

LL nCk(LL n, LL k) {
    if (k==0 || k==n) return 1;
    LL tu = gt[n];
    LL mau = (gt[k]*gt[n-k])%MOD;
    return (tu * pm(mau,MOD-2,MOD)) % MOD;
}

void solve() {
    cin >> a >> b >> n;
    if (a==b) {
        cout << (check(a)?1:0);
        return;
    }
    
    int sum;
    LL ans = 0;
    _for(i,0,n+1) {
        sum = a*i+b*(n-i);
        if (check(sum))
            ans = (ans+nCk(n,i)) % MOD;
    }
    cout << ans;
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    init();
    solve();
}


