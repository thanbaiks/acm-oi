#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e4+5;
const int INF = 1e9;
char pat[N],s[N*100];
int d[N*2][27];
int f[27];

void solve() {
    cin >> pat >> s;
    int m=strlen(pat), n = m*2-1;
    _for(i,0,26) d[n][i] = INF, f[i] = INF;
    _for(i,0, strlen(pat)) {
        if (f[pat[i]-'a']==INF)
            f[pat[i]-'a'] = i;
    }
    while (--n>=0) {
        _for(i,0,26) d[n][i] = d[n+1][i]+1;
        d[n][pat[(n+1)%m]-'a'] = 1;
    }
    
    LL len = f[s[0]-'a'];
    if (len>=INF) {
        cout << -1;
        return;
    }
    
    _for(i,1,strlen(s)) {
        len += d[len%m][s[i]-'a'];
        if (len>=INF) {
            cout << -1;
            return;
        }
    }
    cout << len/m+1;
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


