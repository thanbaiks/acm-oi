#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> P;
const int N = 2e5+5;
int n,A[N],d[N];

void solve() {
    cin >> n;
    stack<P> s;
    int t;
    s.push(make_pair(-69,-1));
    _for(i,0,n) {
        cin >> A[i];
    }
    A[n++] = -1;
    
    _for(i,0,n) {
        t = A[i];
        while (t < s.top().first) {
            P p = s.top();
            s.pop();
            int len = i-1-s.top().second;
            d[len] = max(d[len], p.first);
        }
        if (t > s.top().first) {
            s.push(make_pair(t,i));
        } else {
            s.top().second = i;
        }
    }
    n--;
    _fod(i,n-1,0) d[i] = max(d[i],d[i+1]);
    _for(i,1,n+1) cout << d[i] << ' ';
}

void trau() {
    cin >> n;
    _for(i,0,n+1)
        cin >> A[i];
    _for(len,1,n) {
        int ans = 0;
        for (int l = 0;l+len-1<n; l++) {
            int mn = A[l];
            _for(i,l,l+len) mn = min(mn, A[i]);
            ans = max(ans, mn);
        }
        cout << ans << ' ';
    }
}

void gen() {
    freopen("input.txt","w",stdout);
    srand(time(0));
    int n = 100;
    cout << n << '\n';
    _for(i,0,n)
        cout << rand()%200 << ' ';
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
//    gen(); return 0;
    solve();
//    trau();
}


