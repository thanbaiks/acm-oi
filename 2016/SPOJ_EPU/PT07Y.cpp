#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e4+5;
vector<int> E[N];
int n,m, id[N], ticks;

bool dfs(int x, int par) {
	id[x] = ++ticks;
	vector<int> &e = E[x];
	_it(i, e) {
		if (*i==par) {
			continue;
		}
		if (id[*i]) return false;
		if (!dfs(*i,x)) return false;
	}
	return true;
}

void solve() {
	int u,v;
	cin >> n >> m;
	_for(i,0,m) {
		cin >> u >> v;
		E[u].push_back(v);E[v].push_back(u);
	}
	memset(id,0,sizeof(id));
	ticks = 0;
	if (!dfs(1, -1)) {
		// Loop
		cout << "NO";
		return;
	}
	_for (i,1,n+1) if (!id[i]) {
		cout << "NO";
		return;
	}
	cout << "YES";
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


