#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> P;
const int N = 2e5+5;
int n,A[N],d[N];

void solve() {
    while (cin >> n && n>0) {
        stack<P> s;
        s.push(make_pair(-69,-1));
        _for(i,0,n) cin >> A[i];
        A[n++] = -1;
        LL ans = 0;
        _for(i,0,n) {
            while (A[i] <= s.top().first) {
                P p = s.top();
                s.pop();
                LL len = i-1-s.top().second;
                ans = max(ans, len*p.first);
            }
            s.push(make_pair(A[i],i));
        }
        cout << ans << '\n';
    }
}
int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


