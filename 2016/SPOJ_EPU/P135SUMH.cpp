#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int A[105],B[105],AA[105],BB[105], n, cnt;

int query() {
	int ans = 0, r = cnt, a = 1, b = 100, t;
	memcpy(AA,A,sizeof(AA));memcpy(BB,B,sizeof(BB));
	while (r) {
		if (!AA[a])a++;
		else if (!BB[b])b--;
		else {
			ans = max(ans, a+b);
			t = min(AA[a], BB[b]);
			AA[a]-=t;
			BB[b]-=t;
			r -= t;
		}
	}
	return ans;
}

void solve() {
	int a,b;
	cnt = 0;
	cin >> n;
	memset(A,0,sizeof(A));
	memset(B,0,sizeof(B));
	
	_for (i,0,n) {
		cin >> a >> b;
		A[a]++;
		B[b]++;
		cnt++;
		cout << query() << '\n';
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


