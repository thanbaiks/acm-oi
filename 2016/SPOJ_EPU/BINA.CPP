#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

void solve() {
	stack<int> s;
	int n;
	cin >> n;
	if (!n) {
		cout << 0;
		return;
	}
	while (n) {
		s.push(n&1?1:0);
		n >>= 1;
	}
	
	while (!s.empty()) {
		cout << s.top();
		s.pop();
	}
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

