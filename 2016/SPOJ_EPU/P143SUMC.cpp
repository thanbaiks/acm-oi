#include <bits/stdc++.h>
 
#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))
 
using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}
 
const int N = 1e5+5;
int A[N];
char s[N];
LL d[N],f[N];
int n,m;

void solve() {
    cin >> n;
    _for(i,0,n) cin >> A[i];
    cin >> s;
    m = strlen(s);
    while (m>0 && s[m-1]=='0') m--;
    s[m] = 0;
    
    d[0] = f[0] = A[0];
    _for(i,1,m) d[i] = d[i-1] + A[i];
    _for(i,1,m)
        if (s[i]=='1') {
            f[i] = max(d[i-1], f[i-1]+A[i]);
        } else {
            f[i] = f[i-1];
        }
    cout << f[m-1];
}
 
int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}
 
