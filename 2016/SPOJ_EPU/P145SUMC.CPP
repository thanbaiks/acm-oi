#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}


void solve() {
    int a,b,l,r;
    vector<int> v;
    
    cin >> a >> b;
    a = __gcd(a,b);
    _for(i,1,sqrt(a)+1) if (a%i==0) {
        v += i;
        if (i*i < a) v += a/i;
    }
    sort(_all(v));
    cin >> b;
    
    vector<int>::iterator il,ir;
    while (b--) {
        cin >> l >> r;
        ir = upper_bound(_all(v),r);
        if (ir == v.begin() || *(ir-1)<l) {
            cout << -1 << '\n';
        } else {
            cout << *(ir-1) << '\n';
        }
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


