#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long ll;

ll a[100000], c = 0;
const ll LIM = 10000000000L;

void gen(ll k) {
	for (ll i=1;i<3;i++) {
		ll t = k*10 + i;
		if (t < LIM) {
			a[c++] = t;
			gen(t);
		}
	}
}

int main(){
//	freopen("input.txt","r",stdin);
	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	
	cin.tie(NULL);
	gen(0);sort(a,a+c);
	_for(i,0,100) {
		cout << i << ' ' << a[i] << endl;
	}
	return 0;
}


