#include <bits/stdc++.h>
using namespace std;
typedef long long LL;
LL calc(LL x) {
	if (x&1) {
		x >>= 1;
		return (x+1)*(4*x*x+7*x+2)/2;
	} else {
		x >>= 1;
		return x*(x+1)*(4*x+1)/2;
	}
}
int main(){
	int t;
	LL n;
	cin >> t;
	while (t--) {
		cin >> n;
		cout << calc(n) << '\n';
	}
}


