#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e7+7;
int A[N],n;

void update(int x) {
	while (x<N) {
		A[x]++;
		x += -x&x;
	}
}
int get(int x) {
	int res = 0;
	while (x) {
		res += A[x];
		x -= -x&x;
	}
	return res;
}

void solve() {
	int t;
	cin >> n;
	memset(A,0,sizeof(A));
	LL res = 0;
	_for (i,0,n) {
		cin >> t;
		res += i-get(t);
		update(t);
	}
	cout << res << '\n';
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--)
    	solve();
}


