#include <bits/stdc++.h>
#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
using namespace std;
void solve() {
	string s;
	int n;
	cin >> n;
	long long t;
	while (n--) {
		cin >> s >> t;
		if (!t) cout << 1 << '\n';
		else {
			t = (t-1)%4;
			int a = s[s.size()-1]-'0', b = a;
			_for(i,0,t) {
				b = (b*a)%10;
			}
			cout << b << '\n';
		}
	}
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


