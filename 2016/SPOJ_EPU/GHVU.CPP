#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

bool test1(int a[3][2]) {
	_for (i,1,3) {
		if (a[i][1] != a[i-1][1]) return false;
	}
	return a[0][0] + a[1][0] + a[2][0] == a[0][1];
}
bool test2(int a[3][2]) {
	int mx = 0, rem, t = 0;
	_for (i,0,3) {
		if (a[i][1] > mx) {
			mx = a[i][1];
			rem = a[i][1] - a[i][0];
		}
	}
	_for (i,0,3) {
		if (a[i][1] < mx) {
			if (a[i][0]==rem) t += a[i][1];
			else if (a[i][1]==rem) t += a[i][0];
			else return false;
		}
	}
	return t == mx;
}

void solve() {
    int a[3][2], s = 0;
    _for (i,0,3){
    	_for (j,0,2) cin >> a[i][j];
    	if (a[i][0] > a[i][1])
    		swap(a[i][0],a[i][1]);
    	s += a[i][0]*a[i][1];
	}
	int t = sqrt(s);
	if (t*t != s) {
		cout << "NO";
		return;
	}
    if (test1(a)) {
		cout << "YES";
		return;
	}
	if (test2(a)) {
		cout << "YES";
		return;
	}
	cout << "NO";
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

