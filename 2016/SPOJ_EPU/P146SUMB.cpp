#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

void solve() {
    int n;
    cin >> n;
    _for(i,0,2*n+1) {
        int k=abs(n-i),d = n-k;
        _for(j,0,k) cout << "  ";
        if (!d) {
            cout << "0\n";
        }else {
            _for(j,0,2*d+1) {
                cout << d - abs(d-j);
                if (j<2*d) cout << ' ';
            }
            cout << '\n';
        }
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


