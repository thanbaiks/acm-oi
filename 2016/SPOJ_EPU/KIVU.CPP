#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e5+5;
int A[2][N], n,m;

void solve() {
	cin >> n >> m;
	_for (i,0,n) cin >> A[0][i];
	_for (i,0,m) cin >> A[1][i];
	sort(A[0], A[0]+n);sort(A[1], A[1]+m);
	
	int cnt = 0,i=0,j=0;
	while (i<n & j<m) {
		if (A[0][i] > A[1][j]) {
			cnt++;i++;j++;
		} else i++;
	}
	cout << cnt;
}


int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

