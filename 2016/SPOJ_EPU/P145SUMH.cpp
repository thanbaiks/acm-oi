#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> P;
const int N = 1005;
const int INF = 1e4+5;

int n,m,h;
vector<int> E[N];
int d[N];

void solve() {
    cin >> n >> m >> h;
    _for(i,0,n) {
        d[i] = INF;
    }
    queue<P> q;
    int t,w;
    _for(i,0,m) {
        cin >> t;
        d[t] = 0;
        q.push(make_pair(t,0));
    }
    
    _for(i,0,h) {
        cin >> t >> w;
        E[t] += w;
        E[w] += t;
    }
    while (!q.empty()) {
        P p = q.front();
        q.pop();
        _it(i, E[p.first]) {
            if (d[*i] == INF) {
                d[*i] = p.second + 1;
                q.push(make_pair(*i, p.second+1));
            }
        }
    }
    int ans = 0;
    _for(i,1,n) if (d[ans]<d[i]) ans = i;
    cout << ans;
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}
