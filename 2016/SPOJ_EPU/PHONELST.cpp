#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 10005;
int n;
string s[N];
int t[N*10][11],m;

void solve() {
    cin >> n;
    _for(i,0,n)
        cin >> s[i];
    __(t); m=0;
    _for(i,0,n) {
        int root = 0;
        _for(j,0,s[i].length()) {
            int &next=t[root][s[i][j]-'0'];
            if (!next) {
                next = ++m;
            }
            root = next;
            if (t[root][10]) {
                cout << "NO\n";
                return;
            }
        }
        _for(i,0,10) if (t[root][i]){
            cout << "NO\n";
            return;
        }
        t[root][10] = 1;
    }
    cout << "YES\n";
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--)
        solve();
}


