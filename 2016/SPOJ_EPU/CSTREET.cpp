#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> P;
struct Edge {
    int v, d;
};
const int N = 1005;
const LL INF = 1LL<<60;

int p,n,m;
LL d[N];
bool v[N];
vector<Edge> E[N];

void solve() {
    cin >> p >> n >> m;
    _for(i,1,n+1) E[i].clear(),d[i] = INF, v[i] = false;
    {
        int u,v,d;
        _for(i,0,m) {
            cin >> u >> v >> d;
            E[u] += (Edge){v,d};
            E[v] += (Edge){u,d};
        }
    }
    priority_queue<P, vector<P>, greater<P> > pq;
    pq.push(make_pair(0, 1));
    d[1] = 0;
    while (!pq.empty()) {
        P p = pq.top(); pq.pop();
        if (v[p.second]) continue;
//        cout << "SELECT: " << p.second << "\n";
        v[p.second] = true;
        _it(i, E[p.second]) {
            if (!v[i->v] && d[i->v] > i->d) {
                d[i->v] = i->d;
                pq.push(make_pair(i->d, i->v));
            }
        }
    }
    int ans = 0;
    _for (i,1,n+1) ans += d[i];
    cout << ans*p << '\n';
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--)
        solve();
}


