#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e3+3;
int f[N*2],A[N];
char s[N*2][N];
int n;

void solve() {
	memset(f,0,sizeof(f));
	memset(s,0,sizeof(s));
	cin >> n;
	_for(i,0,n) cin >> A[i];
	
	int k = N,j=0, v=1;
	_for(i,0,n) {
		s[k][j++]=v; f[k]++;
		_for(z,1,A[i]) {
			k += v;
			s[k][j++] = v;
			f[k]++;
		}
		v = -v;
	}
	_for(i,2*N-1,-1) {
		if (f[i]) {
			int j = 0;
			while (f[i]) {
				if (s[i][j]==0) cout << ' ';
				else if (s[i][j] > 0) cout << '/', f[i]--;
				else cout << '\\', f[i]--;
				j++;
			}
			cout << '\n';
		}
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


