#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1005;
int A[N],P[N],H[N];
char s[N];

void solve() {
    int n,t,ans=0;
    cin >> s;
    n = strlen(s);
    H[0] = 0;
    _for(i,1,n+1)
        H[i] = H[i-1]*31+s[i-1];
    _for(i,1,n+1) {
        set<int> ss;
        _for(j,i,n+1) {
            t = H[j] - H[j-i]*P[i];
            if (!ss.count(t)) {
                ans++;
                ss.insert(t);
            }
        }
    }
    cout << ans << '\n';
}

void init() {
    P[0] = 1;
    _for(i,1,N) P[i] = P[i-1]*31;
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
    init();
    int t;
    cin >> t;
    while (t--)
        solve();
}
