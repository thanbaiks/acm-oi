#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> II;
typedef pair<int,II > III;

const int N = 1e5+5;
III A[N];
int n,d[N];

void solve() {
    cin >> n;
    _for(i,0,n) {
        cin >> A[i].first >> A[i].second.first;
        A[i].second.second = i;
    }
    
    sort(A,A+n);
    stack<II> st;
    __(d);
    _fod(i,n-1,-1) {
        int x = A[i].first, h = A[i].second.first, idx = A[i].second.second;
        
        if (st.empty()) {
            st.push(make_pair(x, 1));
            d[idx] = 1;
        } else {
            int cnt = 1;
            while (!st.empty() && st.top().first < x+h) {
                cnt += st.top().second;
                st.pop();
            }
            st.push(make_pair(x, cnt));
            d[idx] = cnt;
        }
    }
    _for(i,0,n) cout << d[i] << ' ';
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


