#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> P;
int n,m;
P A[100005];

void solve() {
	cin >> n >> m;
	_for (i,0,n) {
		cin >> A[i].first >> A[i].second;
	}
	sort(A,A+n);
	LL res = A[0].second;
	_for (i,1,n) {
		if (A[i].second > A[i-1].second) res += A[i].second-A[i-1].second;
	} 
	cout << res;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


