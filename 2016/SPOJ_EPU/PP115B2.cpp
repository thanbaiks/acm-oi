#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

void p(int i, char c) {
	cout << '*';
	while (i--) cout << c;
	cout << "*\n";
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int x;
	cin >> x;
	_for(i,0,x) p(x, '.');p(x, '*');p(x, '*');_for(i,0,x) p(x, '.');
	return 0;
}


