#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

vector<int> primes;
void init() {
	int lim = 100000;
	bool b[lim] = {false};
	for (int i=2;i<=sqrt(lim);i++) {
		if (!b[i]) {
			for (int j=i*i;j<lim;j+=i) if (!b[j]) b[j] = 1;
		}
	}
	for (int i=2;i<lim;i++) if (!b[i]) primes += i;
//	_for (i,0,50) cout << primes[i] << ' '; cout << '\n';
}

LL pow(LL a, LL e) {
	LL res = 1;
	while (e>1) {
		if (e&1) res *= a;
		a = a*a;
	}
	return res*a;
}

LL ans;
const LL MAX = 2e18;

void Try(int x, int n, int k, LL cur) {
	if (n==1) {
		ans = min(ans, cur);
		return;
	}
	
	_for(i,2,63) {
		if (cur > MAX/primes[x]) break;
		cur *= primes[x];
		if (n%i==0) {
			Try(x+1, n/i, i, cur);
		}
	}
}

LL solve(int n){
	ans = LLONG_MAX;
	Try(0, n, 63, 1);
	return ans;
}

void solve() {
	init();
	int n;
	cin >> n;
	cout << solve(n);
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


