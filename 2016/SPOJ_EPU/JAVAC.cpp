#include <bits/stdc++.h>
 
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
 
using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
 
 
string solve(string s) {
	bool java = true, cpp = true, error = false;
	int n = s.size();
	_for(i,0,n) {
		if (isupper(s[i])) cpp = false;
		else if (s[i] == '_') {
			java = false;
			if (i>0 && s[i-1] == '_') {
				error = true;
				break;
			}
		}
	}
	if (error || s[0] == '_' || s[n-1] == '_'  || isupper(s[0]) || (!java && !cpp)) return "Error!";
	
	string res;
	if (cpp) {
		bool up = false;
		_for(i,0,n) {
			if (s[i]=='_') {
				up = true;
			} else if (up){
				res += toupper(s[i]);
				up = false;
			} else {
				res += s[i];
			}
		}
	} else {
		_for(i,0,n) {
			if (isupper(s[i])) res += '_';
			res += tolower(s[i]);
		}
	}
	return res;
}
 
void solve() {
	string s;
	while (cin >> s) {
		cout << solve(s) << '\n';
	}
}
 
int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}
 
