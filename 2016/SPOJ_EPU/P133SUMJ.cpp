#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 105;
bool MAP[N][N];
int n;
LL res;

bool check(int x, int y) {
	return x >= 0 && x < N && y >= 0 && y < n;
}

void rutgon(int &x, int &y) {
	int t = __gcd(x,y);
	if (t < 0) t = -t;
	x /= t;
	y /= t;
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> n;
	memset(MAP,0, sizeof(MAP));
	char s[N];
	_for (i,0,n) {
		cin >> s;
		_for(j,0,n) MAP[i][j] = (s[j]!='.');
	}
	// == SOLVE
	res = 0;
	// Hang cot
	LL t[2][2*N];
	memset(t,0,sizeof(t));
	_for(i,0,n) {
		_for (j,0,n) {
			if (MAP[i][j])
				t[0][i]++;
			if (MAP[j][i])
				t[1][i]++;
		}
	}
	_for(i,0,n) 
		_for(j,0,2)
			if (t[j][i]>=3) 
				res += t[j][i]*(t[j][i]-1)*(t[j][i]-2)/6;
	_for (i,0,n-2) {
		_for (j,0,n) {
			if (MAP[i][j]) {
				_for (u,i+1,n-1) {
					_for (v,0,n) {
						if (j!=v && MAP[u][v]) {
							int x = u-i, y = v-j;
							rutgon(x,y);
							int xx = u+x, yy = v+y;
							while (check(xx,yy)) {
								if (MAP[xx][yy]) res++;
								xx += x;
								yy += y;
							}
						}
					}
				}
			}
		}
	}
	// == PRINT
	cout << res;
}


