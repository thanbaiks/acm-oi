#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int main(){
    freopen("input.txt","w",stdout);
    
    cout << "1\n1 100\n";
    int n = 7;
    cout << n << '\n';
    _for(i,0,n) {
        int x = rand()%100+1, y = rand()%10+1;
        cout << x << ' ' << y << '\n';
    }
}


