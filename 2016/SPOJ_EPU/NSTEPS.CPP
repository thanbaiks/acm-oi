#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

void solve() {
	int t,x,y;
	cin >> t;
	while (t--) {
		cin >> x >> y;
		if (y != x && y != x-2) {
			cout << "No Number\n";
			continue;
		}
		if (x==y && x==0) {
			cout << "0\n";
			continue;
		}
		
		LL res = 0;
		if (x==y){
			y--;
			res = y/2*4;
			if (y&1)
				res += 3;
		} else {
			res = y/2*4;
			if (y&1)
				res += 2;
			else
				res += 1;
		}
		cout << res + 1 << '\n';
	}
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);

	solve();
}

