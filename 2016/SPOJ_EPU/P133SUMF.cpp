#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int a[3];
	cin >> a[0] >> a[1] >> a[2];
	
	sort(a,a+3);
	if (a[2]-a[1] == a[1]-a[0]) {
		cout << a[2] * 2 - a[1];
	} else if ((a[2]-a[1])*2 == a[1]-a[0]) {
		cout << (a[0] + a[1]) / 2;
	} else {
		cout << (a[1] + a[2]) / 2;
	}
}

