#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 10005;
int d[256][256];

string S;

void init() {
    char s[3][20] = {
        "qwertyuiop",
        "asdfghjkl",
        "zxcvbnm"
    };
    __(d);
    _for(i,0,3) _for(j,0,strlen(s[i])) _for(u,0,3) _for(v,0,strlen(s[u]))
        d[s[i][j]][s[u][v]] = abs(u-i)+abs(v-j);
}

int dist(const string &a, const string &b) {
    int res = 0;
    _for(i,0,a.size()) {
        res += d[a[i]][b[i]];
    }
    return res;
}

void solve() {
    int n;
    vector<pair<int, string> > v;
    cin >> S >> n;
    string s;
    _for(i,0,n) {
        cin >> s;
        v += make_pair(dist(S,s), s);
    }
    sort(_all(v));
    _it(i,v) {
        cout << i->second << ' ' << i->first << '\n';
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    init();
    
    int t;
    cin >> t;
    while (t--)
        solve();
}


