#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

void make(char *s, int *lps, int n) {
	int len = 0, i = 1;
	lps[0] = 0;
	while (i<n) {
		if (s[len]==s[i]) {
			lps[i++] = ++len;
		} else {
			if (len) {
				len = lps[len-1];
			} else {
				lps[i++] = 0;
			}
		}
	}
}

string str;
void solve(int n) {
	char *s = new char[n+5];
	int *lps = new int[n+5];
	cin >> s;
	make(s, lps, n);
	
	cin >> str;
	int N = str.size();
	int i=1,j=0;
	while (i<N)
		if (str[i] == s[j]) {
			i++; j++;
			if (j==n) {
				cout << i-j << '\n';
				j = lps[j-1];
			}
		} else {
			if (j)
				j = lps[j-1];
			else
				i++;
		}
	delete[] s;
	cout << '\n';
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    while (cin >> t){
    	solve(t);
//    	return 0;
	}
}


