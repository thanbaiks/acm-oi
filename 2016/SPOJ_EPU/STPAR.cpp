#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e3+5;
int A[N],B[N];
int n,m;

void solve() {
	while (cin >> n) {
		if (!n) return;
		_for(i,0,n) cin >> A[i];
		int t = 0;
		m=0;
		_for(i,0,n) {
			if (A[i]==t+1) {
				t = A[i];
			} else if (m>0 && B[m-1] == t+1){
				t = B[--m];
				i--;
			} else B[m++] = A[i];
		}
		while (m>0 && B[m-1] == t+1) {
			t++;
			m--;
		}
		if (!m)  {
			cout << "yes\n";
		} else {
			cout << "no\n";
		}
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


