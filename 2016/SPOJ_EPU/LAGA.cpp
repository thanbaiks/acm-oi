#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;


int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int n,dp[1005][2],mod=100000000;
	cin >> n;
	dp[1][0] = 1;
	dp[1][1] = 2;
	_for (i,2,n+1) {
		dp[i][0] = (dp[i-1][0] + dp[i-1][1])%mod;
		dp[i][1] = (2*dp[i-1][0] + dp[i-1][1])%mod;
	}
	cout << dp[n][0] + dp[n][1];
}


