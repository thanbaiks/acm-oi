#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

bool e[11];
int a[11];
int n;

void print(int at) {
	_for(i,1,n+1) {
		if (!e[i]) {
			a[at] = i;
			e[i] = true;
			if (at < n-1) {
				print(at+1);
			} else {
				_for(i,0,n) cout << a[i]; cout << '\n';
			}
			e[i] = false;
		}
	}
}

int f(int x) {
	int t = 1;
	_for(i,1,x+1) t *= i;
	return t;
}

void solve() {
	cin >> n;
	memset(e,0,sizeof(e));
	cout << f(n) << '\n';
	print (0);
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

