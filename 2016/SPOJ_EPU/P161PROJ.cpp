#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e6+6;
int n, t, c, res = 0;
int a[N];

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> n;
	c = 0; // stack size
	while (n--) {
		cin >> t;
		if (!c) {
			// c = 0, stack empty, first case
			a[c++] = t;
		} else {
			int i = c-1; // i point to top of stack
			while (i>=0 && a[i]>t) {
				res = max(res, t ^ a[i]);
				i--;
			}
			if (i<0) {
				// oh, now it is empty
				a[0] = t;
				c = 1;
			} else {
				res = max(res, t ^ a[i]);
				a[i+1] = t;
				c = i+2;
			}
		}
	}
	cout << res;
}


