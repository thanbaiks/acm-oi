#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 105;
int n,m;
bool B[N][N];

void solve() {
    cin >> n >> m;
    memset(B,0,sizeof(B));
    int t;
    _for(i,0,n) {
    	_for (j,0, m) {
    		cin >> t;
    		B[i][j] = t==1;
		}
	}
	LL res = 0;
	_for (i,0,m) {
		int k = 0;
		_for (j,n-1,-1) {
			if (!B[j][i]) k++;
			else res += k;
		}
	}
	cout << res;
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

