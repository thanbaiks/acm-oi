#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef pair<int,int> P;

int main(){
//	freopen("input.txt","r",stdin);
	//freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	stack<P> s;
	int n;
	int t;
	LL res = 0;
	
	
	cin >> n;
	while (n--) {
		cin >> t;
		if (s.empty()) {
			// First entry
			s.push(make_pair(t, 1));
		} else {
			while (true) {
				if (s.top().first < t) {
					// Smaller
					// Pop out
					res += s.top().second;
					s.pop();
					if (s.empty()) {
						// Push in
						// This is new highest
						s.push(make_pair(t, 1));
						break;
					}
				} else if (s.top().first == t) {
					res += s.top().second;
					s.top().second++;
					if (s.size() > 1) res++;
					break;
				} else {
					res++;
					s.push(make_pair(t, 1));
					break;
				}
			}
		}
	}
	cout << res;
	return 0;
}

