#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}


const int N = 2e4+5;
int n,k;
LL A[N];

void solve() {
	cin >> n >> k;
	A[0] = 0;
	_for(i, 1, n+1)
		cin >> A[i];
	sort(A+1,A+1+n);
//	_for(i, 1, n+1) A[i] += A[i-1];
	
	LL ans = LLONG_MAX;
	for (int i=k;i<=n;i++) {
		ans = min(ans, A[i]-A[i-k+1]);
	}
	cout << ans << '\n';
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    int t;
    cin >> t;
    while (t--)
    	solve();
}


