#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e6+5, BASE = 1e3+5;
int n,f[BASE*2];

void solve() {
    cin >> n;
    __(f);
    int t;
    int ans = -1;
    _for(i,0,n) {
        cin >> t;
        t += BASE;
        f[t]++;
        if (f[t]*2>n) {
            ans = t;
        }
    }
    if (ans>0) {
        cout << "YES " << ans - BASE << '\n';
    } else {
        cout << "NO\n";
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--)
        solve();
}


