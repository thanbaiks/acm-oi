#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

void solve() {
    int n,T=0;
    LL x,y,m;
    cout << fixed << setprecision(2);
    while (cin >> n && n>0) {
        double sumX = 0, sumY = 0, sm = 0;
        _for(i,0,n) {
            cin >> x >> y >> m;
            sumX += x*m;
            sumY += y*m;
            sm += m;
        }
        cout << "Case " << ++T << ": " << sumX/sm << ' ' << sumY/sm << '\n';
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


