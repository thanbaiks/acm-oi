#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

vector<int> primes;

void init() {
	int N = 32000;
	bool *m = new bool[N];
	int lim = sqrt(N);
	memset(m, 1, sizeof(m[0])*N);
	
	for (int i=2;i<lim;i++) {
		if (m[i]) {
			for (int j=i*i;j<N;j+=i)  if (m[j]) m[j] = false;
		}
	}
	for (int i=2;i<N;i++) if (m[i]) primes.push_back(i);
}

const int N = 1e5+5;
bool sie[N];

void print(int a,int b) {
	memset(sie, 1,sizeof(sie));
	
	for (vector<int>::iterator i = primes.begin(); i != primes.end(); ++i) {
		int p = *i, k = a/p;
		if (k < 2) k = 2;
		for (k = k*p;k<=b;k+=p) {
			if (k>=a && sie[k-a]) sie[k-a] = false;
		}
	}
	
	_for (i,max(2,a),b+1) if (sie[i-a]) cout << i << '\n';
	cout << '\n';
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	init();
	
	int t,n,m;
	cin >> t;
	
	while (t--) {
		cin >> m >> n;
		print(m,n);
	}
}


