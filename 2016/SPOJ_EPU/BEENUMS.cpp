#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int A[20000], c;
void init() {
	int t = 1;
	c = 0;
	while (t<=1e9) {
		A[c] = t;
		t += 6 + c*6;
		c++;
	}
}

void solve() {
	int n;
	while (cin >> n) {
		if (n<0) return;
		if (binary_search(A,A+c,n)) {
			cout << "Y\n";
		} else {
			cout << "N\n";
		}
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    init();
    solve();
}


