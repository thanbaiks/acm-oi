#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int n,k,s,sum,h;
LL res;
bool B[22];

void Try(int x) {
	if (x>n) {
		// Check step
		if (sum==s  && h==k)
			res++;
	} else {
		sum += x; h++;
		Try(x+1);
		sum -= x; h--;
		Try(x+1);
	}
}

void solve() {
	while (1) {
		cin >> n >> k >> s;
		if ((n|k|s) == 0) return;
		res = h = sum = 0;
		Try(1);
		cout << res << '\n';
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


