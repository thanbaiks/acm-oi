#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1000005;
LL A[N], n, m;

void solve() {
	int T;
	cin >> T;
	_for(t,1,T+1) {
		cout << "Scenario #" << t << ":\n";
		cin >> n >> m;
		_for(i,0,m)
			cin >> A[i];
		sort(A, A+m, greater<LL>());
		LL sum = 0;
		_for (i,0,m) {
			sum += A[i];
			if (sum >= n) {
				cout << i+1 << "\n";
				break;
			}
		}
		if (sum < n) {
			cout << "impossible\n";
			continue;
		}
	}
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);

	solve();
	return 0;
}

