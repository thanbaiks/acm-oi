#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e5+5, M = 1e6+6;
int n, A[N],D[M],a,b;

int solve() {
	cin >> n;
	_for(i,0,n) cin >> A[i];
	cin >> a >> b;
	if (a==b) {
		return 0;
	}
	sort(A,A+n);
	memset(D, 0, sizeof(D));
	queue<int> q;
	q.push(a);
	while (!D[0] && !q.empty()) {
		a = q.front(); q.pop();
		int k = lower_bound(A,A+n, a) - A;
		_for(i,0,k) {
			if (a%A[i]==0) continue;
			int x = a - a%A[i];
			if (x>=b && !D[x-b]) {
				D[x-b] = D[a-b]+1;
				q.push(x);
			}
		}
		if (!D[a-1-b]) {
			D[a-1-b] = D[a-b]+1;
			q.push(a-1);
		}
	}
	return D[0];
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout << solve();
}


