#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	LL n,l,res;
	cin >> n;
	while (n--) {
		cin >> l;
		res = 0;
		for (LL t=5;t<=l;t*=5) res += l/t;
		cout << res << '\n';
	}
}


