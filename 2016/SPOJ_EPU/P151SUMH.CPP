#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))
#define PC __builtin_popcountll
using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

LL trau(LL l, LL r) {
    LL ans = l;
    for(l++;l<=r;l++) {
        if (__builtin_popcountll(l)>__builtin_popcountll(ans)) ans = l;
    }
    return ans;
}

LL calc(LL l, LL r) {
    if (!r) return 0;// r = 0
    LL ans = PC(r)>PC(l)?r:l, t = 1LL<<__lg(r), u = 0;
    while (t) {
        if (t&r){
            LL v = u|(t-1);
            if (v>=l && (PC(v)>PC(ans) || (PC(v)==PC(ans) && v < ans))) ans = v;
            u |= t;
        }
        t >>= 1;
    }
    return ans;
}

void solve() {
    LL l,r;
    cin >> l >> r;
//    cout << trau(l,r) << '\n';
    cout << calc(l,r) << '\n';
}
void test() {
    _for(i,0,10000) {
        LL l = rand(), r = rand();
        if (l>r) swap(l,r);
        if (calc(l,r) != trau(l,r)) {
            cout << l << ' ' << r << '\t' << calc(l,r) << ' ' << trau(l,r) << '\n';
            break;
        }
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
//    test(); return 0;
    int t;
    cin >> t;
    while (t--)
        solve();
}


