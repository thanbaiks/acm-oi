#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<LL,LL> P;
const int N = 505, M = 10005;
P A[N];
LL d[M];
int trace[M];

LL solve() {
	LL w,n;
	cin >> w >> n;
	w = n - w;
	cin >> n;
	_for(i,0,n) cin >> A[i].first >> A[i].second;
	__(d); __(trace);
	d[0] = 1;
	_for(j,1,w+1) {
		_for(i,0,n){
			LL p = A[i].first, t = A[i].second;
			if (j-t >=0 && d[j-t]>0 && (!d[j] || d[j]>d[j-t]+p)) {
				d[j] = d[j-t]+p;
				trace[j] = i;
			}
		}
	}
	cout << "Trace: \n";
	int t = w;
	while (t!=0) {
	    cout << A[trace[t]].first << ',' << A[trace[t]].second << ";   ";
	    t -= A[trace[t]].second;
    }
    cout << '\n';
	return d[w]-1;
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--) {
    	LL k = solve();
    	if (k<0) {
    		cout << "This is impossible.\n";
		} else {
			cout << "The minimum amount of money in the piggy-bank is " << k << ".\n";
		}
	}
}


