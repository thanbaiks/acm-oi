#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n;
bool f[50005];

void solve() {
	cin >> n;
	memset(f,0,sizeof(f));
	int t, res = 0;
	_for (i,0,n) {
		cin >> t;
		if (t >n || f[t])
			res++;
		else
			f[t] = 1;
	}
	cout << res;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


