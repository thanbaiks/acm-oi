#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 5e5+5;

struct Node {
	char s,t;
	int len;
} A[N];
int n;
int res = 0;

int d[256];
void _try(char c) {
	__(d);
	int l = 0;
	_for(i,0,n) {
		l = 0;
		if (d[A[i].s]) 
			l = d[A[i].s] + A[i].len;
		else if (A[i].s == c) 
			l = A[i].len;
		if (l && d[A[i].t] < l) {
			d[A[i].t] = l;
			if (A[i].t==c)
				res = max(res, l);
		}
	}
}

void solve() {
	cin >> n;
	char s[20];
	_for(i,0,n) {
		cin >> s;
		A[i].len = strlen(s);
		A[i].s = s[0];
		A[i].t = s[A[i].len-1];
	}
	for (char c = 'a'; c <= 'z'; c++)
		_try(c);
	cout << res;
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


