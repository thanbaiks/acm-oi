#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int n,c,m;
map<int,pair<int,int> > A;
struct Num {
	int x, cnt, fs;
	bool operator< (const Num &num) {
		return cnt>num.cnt || (cnt==num.cnt && fs < num.fs);
	}
} B[1005];
void solve() {
	int t;
	cin >> n >> c;
	_for (i,0,n) {
		cin >> t;
		if (A.count(t)==0) {
			A[t] = make_pair(1,i);
		} else {
			A[t].first++;
		}
	}
	m = 0;
	_it(i, A) {
		B[m].x = i->first;
		B[m].cnt = i->second.first;
		B[m].fs = i->second.second;
		m++;
	}
	sort(B,B+m);
	_for(i,0,m){
		Num &n = B[i];
		_for (i,0,n.cnt) cout << n.x << ' ';
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


