#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1082;
const int M[3][2] = {{3,2},{-5,-10},{-20,5}};
int dp[N][N];

int Try(int h,int a, int t) {
    if (h<=0||a<=0) return 0;
    if (dp[h][a]) return dp[h][a];
    int res = 0;
    _for(i,0,3) if (i!=t) res = max(res, Try(h+M[i][0],a+M[i][1],i));
    return dp[h][a] = res+1;
}

void solve() {
    int h,a;
    cin >> h >> a;
    __(dp);
    cout << Try(h,a,-1)-1 << '\n';
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--)
        solve();
}


