#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 2e3+4;
int n,m;
char f[N];
bool d[N][N];

bool dfs(int u, char c) {
    f[u] = c;
    _for(v,1,n+1) {
        if (d[u][v]) {
            if (!f[v]) {
                if (!dfs(v,-c)) return false;
            } else if (f[v]==c) return false;
        }
    }
    return true;
}

bool solve() {
    __(d); __(f);
    cin >> n >> m;
    int u,v;
    _for(i,0,m) {
        cin >> u >> v;
        d[u][v] = d[v][u] = 1;
    }
    _for(i,1,n+1) {
        if (!f[i]) {
            if (!dfs(i,1)) {
                return false;
            }
        }
    }
    return true;
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    _for(i,1,t+1){
        cout << "Scenario #" << i << ":\n";
        if (solve()) {
            cout << "No suspicious bugs found!\n";
        } else {
            cout << "Suspicious bugs found!\n";
        }
    }
}


