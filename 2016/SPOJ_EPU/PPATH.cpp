#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> P;

int a,b,d[4],t;
bool p[10005],v[10005];

void init() {
    int n = 10005;
    memset(p,1,sizeof(p));
    _for(i,2,sqrt(n)+1) {
        if (p[i])
            for (int j=i*i;j<n;j+=i)
                if (p[j])
                    p[j] = false;
    }
}

void solve() {
    cin >> a >> b;
    queue<P> q;
    __(v);
    q.push(make_pair(0,a));
    v[a] = 1;
    while (!q.empty()) {
        P next = q.front(); q.pop();
        if (next.second == b) {
            cout << next.first << '\n';
            return;
        }
        for (int i=1;i<10000;i*=10) {
            t = next.second;
            while ((t/i)%10 != (i==1000?1:0)) t -=  i;
            while (1) {
                if (p[t] && !v[t]) q.push(make_pair(next.first+1, t)), v[t] = true;
                if ((t/i)%10==9) break; else t += i;
            }
        }
    }
    cout << "Impossible\n";
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    init();
    int t;
    cin >> t;
    while (t--)
        solve();
}


