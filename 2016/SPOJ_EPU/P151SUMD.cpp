#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int convert(LL t) {
    LL res = 0;
    while (t) {
        res |= 1<<(t%10);
        t /= 10;
    }
    return res;
}

void solve() {
    int n,m;
    cin >> n;
    LL ans = 0, t;
    int d[1<<10]; __(d);
    _for(i,0,n) {
        cin >> t;
        m = convert(t);
        ans += d[m];
        _for(i,1,1<<10)
            if (i&m) d[i]++;
    }
    cout << ans;
}
void gen() {
    freopen("input.txt","w",stdout);
    int n = 1000000;
    cout << n << '\n';
    _for(i,0,n) {
        cout << rand() << '\n';
    }
}
int main(){
//    gen(); return 0;
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


