#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef pair<int,int> P;

const int N = 105;
int n,m;
bool fr[N];
int a,b,c;
queue<P> haters;
vector<int> dep[N];

// Tarjan variables
int id[N],low[N],scc[N],ticks,current_scc;
bool stacked[N];
stack<int> st;

void tarjan(int node) {
	id[node] = low[node] = ticks++;
	st.push(node);
	stacked[node] = true;
	vector<int>::iterator it;
	for (it = dep[node].begin();it != dep[node].end(); ++it) {
		int next = *it;
		if (id[next] < 0) {
			tarjan(next);
			low[node] = min(low[node], low[next]);
		} else if (stacked[next])
			low[node] = min(low[node], low[next]);
	}
	if (low[node]==id[node]) {
		// Get out of stack
		int v;
		do {
			v = st.top(); st.pop();
			stacked[v] = false;
			scc[v] = current_scc;
		} while (v != node);
		current_scc++;
	}
}

bool solve() {
	cin >> n >> m;
	memset(fr, 1,sizeof(fr));
	_for (i,0,N) {
		dep[i].clear();
	}
	
	// Read input
	_for (i,0,m) {
		cin >> a >> b >> c;
		if (c<0) {
			// Hater
			haters.push(make_pair(a,b));
		} else {
			dep[a].push_back(b);
		}
		fr[a] = false;
	}
	// Quick check for free vertext
	_for (i,1,n+1) if (fr[i]) return true;
	
	// Tarjan
	fill(id,id+N, -1);
	ticks = 0,current_scc = 0;
	while (!st.empty()) st.pop();
	memset(stacked,0,sizeof(stacked));
	
	_for (i,1,n+1) if (id[i] < 0) tarjan(i);
	// DEBUG
	cout << "SCC" << '\n';
	_for(i,1,n+1) cout << '(' << i << ';' << scc[i] << ')' << ' '; cout << '\n';
	bool died[N];
	memset(died, 0, sizeof(died));
	while (!haters.empty()) {
		P &next = haters.front(); haters.pop();
		if (scc[next.first] == scc[next.second]) {
			died[scc[next.first]] = true;
		}
	}
	_for (i,0,current_scc) {
		if (!died[i]) return true;
	}
	return false;
}

int main(){
	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int t;
	cin >> t;
	_for (i,0,t) cout << (solve()?"YES":"NO") << '\n';
}


