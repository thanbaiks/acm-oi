#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 505;

enum Type {
	INVALID,
	HUNTER,
	FREE,
	START,
	END
};

int n,m;
char M[N][N]; // Map
bool vis[N][N]; // visited?
int lev[N][N]; // Danger level

Type get(int x, int y) {
	if (x<=0 || x>n || y<=0 || y>m)
		return INVALID;
	switch (M[x-1][y-1]) {
		case '.':
			return FREE;
		case '+':
			return HUNTER;
		case 'V':
			return START;
		case 'J':
			return END;
	}
}

/**
 * Calculate danger level
 */
void bfs1() {
	queue<pair<int,int> > q;
	memset(lev,0,sizeof(lev));
	_for(i,1,n+1) {
		_for(j,1,m+1) {
			if (get(i,j)==HUNTER) {
				lev[i][j] = 1;
				q.push(make_pair(i,j));
			}
		}
	}
	while (!q.empty()) {
		int dx=1,dy=0,x=q.front().first,y=q.front().second,u,v;
		q.pop();
		
		_for(__,0,4) {
			int tmp = dx;dx = -dy, dy = tmp; // Rotate 90deg
			u = x+dx; v = y+dy;
			if (get(u,v) == INVALID || lev[u][v]) continue;
			lev[u][v] = lev[x][y]+1;
			q.push(make_pair(u,v));
		}
	}
	// DUMP
//	_for(i,1,n+1) {
//		_for(j,1,m+1) {
//			cout << lev[i][j] << ' ';
//		}
//		cout << '\n';
//	}
}

struct Que {
	int priority;
	int x,y;
};

bool operator< (const Que &a, const Que &b) {
	return a.priority < b.priority;
}

int bfs2() {
	int sx=-1,sy;
	memset(vis,0,sizeof(vis));
	priority_queue<Que> q;
	
	_for(i,1,n+1) {
		_for(j,1,m+1) 
			if (get(i,j) == START) {
				sx = i;
				sy = j;
				break;
			}
		if (sx>=0) break;
	}
	
	vis[sx][sy] = true;
	q.push((Que){lev[sx][sy], sx, sy});
	int res = INT_MAX;
	while (!q.empty()) {
		Que next = q.top(); q.pop();
		int sz = q.size();
		int x,y,dx=1,dy=0;
		res = min(res, next.priority);
		if (get(next.x, next.y) == END) {
			return res-1;
		}
		_for(__,0,4) {
			int tmp = dx;dx = -dy, dy = tmp; // Rotate 90deg
			
			x = next.x + dx; y = next.y + dy;
			if ((get(x,y)==FREE || get(x,y)==END) && !vis[x][y]) {
				vis[x][y] = true;
				q.push((Que){lev[x][y], x, y});
			}
		}
	}
	return 0;
}

void solve() {
	cin >> n >> m;
	_for (i,0,n) {
		cin >> M[i];
	}
	bfs1();
	cout << bfs2();
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


