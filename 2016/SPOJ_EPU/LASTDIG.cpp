#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

LL calc(LL a,LL b) {
	LL t = 1;
	a %= 10;
	while (b>1) {
		if (b&1)
			t = (t*a)%10;
		a = (a*a)%10;
		b >>= 1;
	}
	return (t*a)%10;
}

void solve() {
	LL n,a,b;
	cin >> n;
	while (n--) {
		cin >> a >> b;
		cout << (b?calc(a,b):1) << '\n';
	}
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);

	solve();
}

