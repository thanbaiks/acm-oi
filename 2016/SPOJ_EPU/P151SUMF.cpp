#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e5+5;
string s;
int m,d[N];

void solve() {
    cin >> s >> m;
    int t;
    _for(i,0,m) cin >> t, d[t-1]++;
    _for(i,1,(s.length()>>1)+1) d[i]+=d[i-1];
    int i=0,j=s.length()-1;
    while (i<j) {
        if (d[i]&1) swap(s[i],s[j]);
        i++;j--;
    }
    cout << s;
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


