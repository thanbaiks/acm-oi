#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int n,A[1005],m;
void solve() {
    cin >> n;
    _for(i,0,n) cin >> A[i];
    sort(A,A+n);
    int ans = INT_MAX,t,k;
    cin >> m;
    _for(i,0,m) {
        cin >> t;
        k = lower_bound(A,A+n,t)-A;
        if (k<n) ans = min(ans,abs(t-A[k]));
        if (k>0) ans = min(ans,abs(t-A[k-1]));
    }
    cout << ans << '\n';
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--)
        solve();
}


