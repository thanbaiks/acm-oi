#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

LL Pow[19];
typedef pair<LL,LL> P;
map<P,int> Map;

void init() {
	Pow[0] = 1;
	_for(i, 1,19) Pow[i] = Pow[i-1]*10;
}
void solve() {
	int n;
	cin >> n;
	Map.clear();
	LL a,b,c,d,e,f;
	_for (i,0,n) {
		cin >> a >> b >> c >> d >> e >> f;
		P p = make_pair(a*Pow[8]+b,c*Pow[12]+d*Pow[8]+e*Pow[4]+f);
		Map[p]++;
	}
	_it(i, Map) {
		a = i->first.first/Pow[8];
		b = i->first.first%Pow[8];
		c = i->first.second;
		f = c%Pow[4]; c /= Pow[4];
		e = c%Pow[4]; c /= Pow[4];
		d = c%Pow[4]; c /= Pow[4];
		
		cout << setw(2) << setfill('0') << a << ' ' << setw(8) << setfill('0') << b << ' ';
		cout << setw(4) << setfill('0') << c << ' ';
		cout << setw(4) << setfill('0') << d << ' ';
		cout << setw(4) << setfill('0') << e << ' ';
		cout << setw(4) << setfill('0') << f << ' ';
		cout << i->second << '\n';
	}
	cout << endl;
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    init();
    int t;
    cin >> t;
    while (t--)
    	solve();
}


