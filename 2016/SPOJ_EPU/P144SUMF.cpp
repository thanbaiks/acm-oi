#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e6+5;
int x;
char s[N];

void solve() {
    cin >> x >> s;
    int ans=0, sum = 0, n = strlen(s);
    deque<int> q;
    _for(i,0,n)
        q.push_back(s[i]=='M'?1:-1);
    while (!q.empty()) {
        if (abs(sum+q.front()) <= x) {
            ans++;
            sum += q.front();
            q.pop_front();
            continue;
        }
        int tmp = q.front();
        q.pop_front();
        if (abs(sum+q.front()) <= x) {
            ans++;
            sum += q.front();
            q.pop_front();
            q.push_front(tmp);
            continue;
        }
        break;
    }
    cout << ans;
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


