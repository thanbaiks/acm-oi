#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1005;

int A[N],n;
void solve() {
    cin >> n;
    int sum=0,ans=0;
    _for(i,1,n+1) 
        cin >> A[i], sum += A[i];
    _for(i,1,n+1) _for(j,i,n+1) {
        int t = sum;
        _for(k,i,j+1) t = t-A[k]+(1-A[k]);
        ans = max(ans,t);
    }
    cout << ans;
}
int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


