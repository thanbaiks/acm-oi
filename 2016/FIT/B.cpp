#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

char s[100];
LL n;

LL hvi(LL x) {
	LL t = 1;
	while (x) t *= x, x--;
	return t;
}

LL hvi(char *s, char *e) {
	if (e-s==1) return 1;
	char tmp[100];
	int len=0;
	for (char *t = s; t<e; t++) tmp[len++] = *t;
	tmp[len] = 0;
	sort(tmp, tmp+len);
	LL t = hvi(len);
	LL k = 1;
	_for(i,1, len) {
		if (tmp[i]==tmp[i-1]) {
			k++;
			t /= k;
		} else {
			k = 1;
		}
	}
//	cerr << "HVI " << tmp << " " << t << endl;
	return t;
}

void sswap(char *s, char *e) {
	char c = *s;
	sort(s,e);
	char *p = upper_bound(s,e,c);
	swap(*s,*p);
	sort(s+1,e);
}

void solve(char *s, char *e, LL n) {
	LL t = hvi(s+1, e);
	while (n >= t) {
		n -= t;
		sswap(s,e);
		t = hvi(s+1, e);
	}
	if (n==0) return;
	solve(s+1,e,n);
}

void solve() {
	cin >> n >> s;
	int len = strlen(s);
	sort(s,s + len);
	if (hvi(s,s+len) < n) {
		cout << -1;
		return;
	}
	solve(s, s+len, n-1);
	cout << s << endl;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


