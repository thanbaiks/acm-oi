#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int solve(int n) {
	int A[5];
	A[0]=A[1]=0;
	A[2] = 1;
	if (n <= 1) {
		return n;
	}
	while (A[2]<n) {
		A[3]=A[0]+A[1]+A[2];
		_for(i,0,3) A[i]=A[i+1];
	}
	return A[2];
}

void solve() {
	int n;
	while (cin >> n) cout << solve(n) << '\n';
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


