#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}
int line = 0;

string tmp;
bool next(string &s) {
	if (tmp != "") {
		s = tmp, tmp = "";
		return true;
	}
	return cin >> s;
}

void push_back(string s) {
	tmp = s;
}

LL read() {
	LL res = 1, cur=-1;
	bool hasElse;
	string s;
	while (next(s)) {
		line++;
		if (s == "IF") {
			cur = 0;
			hasElse = false;
			cur += read();
		} else if (s == "ELSE") {
			if (cur<0) {
				push_back(s);
				return res;
			}
			hasElse = true;
			cur += read();
		} else if (s == "END_IF") {
			if (cur<0) {
				push_back(s);
				return res;
			} else if (!hasElse) {
				// Khong co ELSE
				cur++;
			}
			res *= cur;
			cur = -1;
		} else if (s == "ENDPROGRAM")
			return res;
	}
	return res;
}


void solve() {
	cout << read();
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


