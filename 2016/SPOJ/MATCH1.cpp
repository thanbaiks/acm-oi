#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 105;
int m,n;
vector<int> E[N];
int A[N],V[N],t;

bool dfs(int u) {
	_it(i, E[u]) {
		if (V[*i]==t) continue;
		V[*i] = t;
		if (!A[*i] || dfs(A[*i])) {
			A[*i] = u;
			return true;
		}
	}
	return false;
}

void solve() {
	cin >> m >> n;
	int u,v;
	while (cin >> u >> v) {
		E[u] += v;
	}
	int res = 0;
	__(A);__(V);
	t = 0;
	_for(i,1,m+1) {
		t++;
		res += dfs(i);
	}
	cout << res << '\n';
	_for(i,1,n+1) {
		if (A[i]) cout << A[i] << ' ' << i << '\n';
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

