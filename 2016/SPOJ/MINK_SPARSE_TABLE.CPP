#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}


const int N = 17000, LOGN = 14;
int n,K;
int A[LOGN+1][N];

void solve() {
    cin >> n >> K;
    _for(i,0,n) cin >> A[0][i];
    
    for (int k=1;(1<<k)<=n; k++)
    for (int j=0;j+(1<<k)-1<n;j++) {
        A[k][j] = min(A[k-1][j], A[k-1][j+(1<<(k-1))]);
    }
    
    int t = __lg(K);
    _for(i,0,n-K+1) {
        cout << min(A[t][i],A[t][i+K-(1<<t)]) << ' ';
    }
    cout << '\n';
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--)
        solve();
}


