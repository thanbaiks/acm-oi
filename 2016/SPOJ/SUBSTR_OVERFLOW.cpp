#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e6+5;
char A[N],B[N];
int hash[N];

void solve() {
    cin >> A >> B;
    int n = strlen(A), m = strlen(B);
    int p=1,h=0;
    _for(i,0,m) {
        p *= 31;
        h = h*31+B[i]-'a';
    }
    _for(i,1,n+1) {
        hash[i] = hash[i-1]*31+A[i-1]-'a';
        if (i-m>=0 && hash[i]-hash[i-m]*p == h) cout << i-m+1 << ' ';
    }
    
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


