#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}
typedef pair<int,int> P;
int n,c;
P A[100005];
void solve() {
	cin >> n;
	c = 0;
	LL res = 0;
	P p;
	_for(i,0,n) {
		cin >> p.first;
		p.second = i;
		if (c>0) {
			while (c>0 && A[c-1].first <= p.first) {
				if (c>1) {
					int k = min(p.first,A[c-2].first);
					res += LL(p.second - A[c-2].second-1)*(k-A[c-1].first);
				}
				c--;
			}
		}
		A[c++] = p;
	}
	cout << res;
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


