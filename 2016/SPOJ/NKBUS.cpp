#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 2e5+5;
int A[N],n,m,c;

void solve() {
	cin >> n >> m;
	int t,k,sum=0;
	c = 0;
	_for(i,0,n) {
		cin >> t >> k;
		while (k--) {
			cin >> A[c];
			A[c] -= sum;
			c++;
		}
		sum += t;
	}
	sort(A,A+c);
	if (c <= m) {
		// thua hoac du cho ngoi
		cout << A[c-1] + sum;
	} else {
		cout << A[m-1] + sum;
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


