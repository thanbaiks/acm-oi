#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 50005;
int n,m;
int T[4*N],P[4*N],L,R;


void apply(int idx) {
    if (P[idx]) {
        T[idx<<1] += P[idx];
        T[idx<<1|1] += P[idx];        
        P[idx<<1] += P[idx];
        P[idx<<1|1] += P[idx];
        P[idx] = 0;
    }
}

void update(int idx, int l, int r, int val) {
    if (l>R || r<L) return;
    if (l>=L && r<=R) {
        P[idx] += val;
        T[idx] += val;
        return;
    }
    apply(idx);
    int mid = (l+r)>>1;
    update(idx<<1,l,mid,val);
    update(idx<<1|1,mid+1,r,val);
    T[idx] = max(T[idx<<1],T[idx<<1|1]);
}

int query(int idx, int l, int r) {
    if (l>R || r<L) return 0;
    if (l>=L && r<=R) return T[idx];
    apply(idx);
    int mid = (l+r)>>1;
    return max(query(idx<<1,l,mid),query(idx<<1|1,mid+1,r));
}

void solve() {
    cin >> n >> m;
    
    int x;
    while (m--) {
        cin >> x >> L >> R;
        if (!x) {
            cin >> x;
            update(1,1,n,x);
        } else {
            cout << query(1,1,n) << '\n';
        }
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


