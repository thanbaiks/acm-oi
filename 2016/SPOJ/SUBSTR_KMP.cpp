#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}


const int N = 1e6+5;
char a[N],b[N];
int lps[N];

void make(char *s, int *lps, int n) {
	int len = 0, i = 1;
	lps[0] = 0;
	while (i<n) if (s[len]==s[i]) lps[i++] = ++len; else if (len) len = lps[len-1]; else lps[i++] = 0;
}

void solve() {
    cin >> a >> b;
    int n = strlen(a), m = strlen(b);
    make(b,lps,m);
    
	int i=0,j=0;
	while (i<n)
		if (a[i] == b[j]) {
			i++; j++;
			if (j==m) {
				cout << i-j+1 << ' ';
				j = lps[j-1];
			}
		} else if (j)
			j = lps[j-1];
		else
			i++;
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


