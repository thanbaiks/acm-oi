#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int SZ = 2;
struct Mat {
	LL data[SZ][SZ];
	Mat(bool b = false) {
		__(data);
		if (b) {
			_for(i,0,SZ) data[i][i] = 1;
		}
	}
	void reset() {
		__(data);
	}
};
const Mat I(true);
LL MOD;

LL mm(LL a, LL b, LL m) {
    if (!a || !b || m==1) return 0;
    LL t = 0;
    while (b>1) {
        if (b&1) t = (t+a)%m;
        a = (a+a)%m;
        b >>= 1;
    }
    return (t+a)%m;
}

Mat operator* (const Mat &a, const Mat &b) {
	Mat res;
	_for (i,0,SZ) _for(j,0,SZ) _for(k,0,SZ)
		res.data[i][j] = (res.data[i][j] + mm(a.data[i][k],b.data[k][j],MOD)) % MOD;
	return res;
}

Mat operator^ (const Mat &a, LL e) {
	Mat res = I, x = a;
	while (e>1) {
		if (e&1) res = res*x;
		x = x*x;
		e >>= 1;
	}
	return res*x;
}

void solve() {
	int T;
	LL a,b,k;
	cin >> T;
	while (T--) {
		cin >> a >> b >> MOD >> k;
		if (k==1) {
			cout << b%MOD << '\n';
		} else {
			Mat m,mul;
			m.data[0][0] = b; m.data[0][1] = 1;
			mul.data[0][0]=a;mul.data[1][0]=b;mul.data[1][1]=1;
			mul = mul^(k-1);
			m = m*mul;
			cout << m.data[0][0] << '\n';
		}
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


