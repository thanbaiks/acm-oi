#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}
typedef pair<int,int> P;
const int N = 1e4+5;
P A[N],D[N]; // D[endtime - total time]
int n,m,c;

bool cmp(const P &a, const P &b) {
	return a.second<b.second||a.second==b.second&&a.first<b.first;
}
bool cmp2(const P &a, const int b) {
	return a.first < b;
}
void solve() {
	cin >> n;
	_for(i,0,n) cin >> A[i].first >> A[i].second;
	sort(A,A+n,cmp);
	c = 0;
	m = 0;
	_for(i,0,n) {
		P &p = A[i];
		if (!c) {
			// D is empty
			D[c++] = make_pair(p.second, p.second-p.first);
			m = p.second-p.first;
		} else {
			int t = lower_bound(D,D+c,p.first,cmp2)-D, d;
			if (t==c || D[t].first > p.first) {
				// Greater than every one before
				// Or not found any D_i has end time equal p.first
				t--;
			}
			if (t<0) {
				// Not connect to any
				d = p.second-p.first;
			} else {
				d = p.second-p.first + D[t].second;
			}
			if (D[c-1].second < d) {
				if (D[c-1].first == p.second) {
					D[c-1].second = d;
				} else {
					D[c++] = make_pair(p.second, d);
				}
				m = max(m,d);
			}
		}
	}
	cout << m;
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


