#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 5e4+5;
int A[N],B[17][N],n,m,q;

void solve() {
    cin >> n >> m;
    int u,v,k;
    _for(i,0,m) {
        cin >> u >> v >> k;
        A[u] += k;
        A[v+1] -= k;
    }
    _for(i,1,n+1) {
        B[0][i] = B[0][i-1] + A[i];
    }
    
    for (int i=1;(1<<i)<=n;i++)
    for (int j=1;j+(1<<i)-1 <=n; j++)
        B[i][j] = max(B[i-1][j],B[i-1][j+(1<<(i-1))]);
//    _for(i,1,n+1) cout << B[0][i] << ' '; cout << endl;
    cin >> q;
    while (q--) {
        cin >> u >> v;
        k = __lg(v-u+1);
        cout << max(B[k][u],B[k][v-(1<<k)+1]) << '\n';
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


