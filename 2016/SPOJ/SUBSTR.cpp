#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e6+5;
const int BASE = 33;

char a[N],b[N];
int Hash[N];

void solve() {
    cin.getline(a,N);
    cin.getline(b,N);
    int n = strlen(a), m = strlen(b);
    
    int k=0, Pow=1;
    _for(i,0,m)
        k = k*BASE+b[i], Pow = Pow*BASE;
        
    Hash[0] = 0;
    _for(i,1,n+1) {
        Hash[i] = Hash[i-1]*BASE+a[i-1];
        if (i-m>=0 && (Hash[i]-Hash[i-m]*Pow + MOD*MOD)%MOD == k)
            cout << i-m+1 << ' ';
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


