#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 2e6+10;
int d[N];

void solve() {
    int n;
    cin >> n;
    __(d);
    int car = 0;
    _for(i,1,n*2) {
        d[i] = n-abs(i-n)+car;
        car = d[i]/10;
        d[i] %= 10;
    }
    int i = n*2;
    while (car) {
        d[i] = car%10;
        car /= 10;
        i++;
    }
    i--;
    while (i) cout << d[i--];
    cout << '\n';
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--)
        solve();
}
