#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N=103,INF=1e9;
int n,m,q;
int d[N][N],t[N][N];


void solve() {
	cin >> n >> m >> q;
	__(d); __(t);
	_for(i,1,n+1) _for(j,1,n+1) d[i][j] = INF;
	_for(i,1,n+1) d[i][i] = 0;
	
	int u,v,x;
	_for(i,0,m) {
		cin >> u >> v >> x;
		d[u][v] = d[v][u] = x;
	}
	
	_for(k,1,n+1) {
		_for(i,1,n+1) _for(j,1,n+1) if (d[i][j]>d[i][k]+d[k][j]) {
			d[i][j] = d[i][k]+d[k][j];
			t[i][j] = k;
		}
	}
	stack<int> st;
	_for(i,0,q) {
		cin >> x >> u >> v;
		if (x) {
			// print way
			st.push(v);
			while (1) {
				v = t[u][v];
				if (!v) break;
				st.push(v);
			}
			st.push(u);
			cout << st.size() << ' ';
			while (!st.empty()) cout << st.top() << ' ', st.pop();
			cout << '\n';
		} else {
			cout << d[u][v] << '\n';
		}
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


