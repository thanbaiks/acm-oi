#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e4+5;
int n,m;
vector<int> E[N];
int num[N],low[N],ticks,cau,khop;

void dfs(int u, int par) {
	num[u] = low[u] = ++ticks;
	int k = 0, h = 0;
	
	_it(i, E[u]) {
		int v = *i;
		if (v==par) continue;
		if (!num[v]) {
			dfs(v,u);
			low[u] = min(low[u], low[v]);
			if (low[v]>=num[u]) h++;
			if (low[v]>num[u]) k++;
		} else {
			low[u] = min(low[u], num[v]);
		}
	}
	if (par<0) {
		// is root
		khop += k>1;
	} else {
		khop += h>0;
	}
	cau += k;
}

void solve() {
	cin >> n >> m;
	int u,v;
	_for(i,0,m) {
		cin >> u >> v;
		E[u] += v;
		E[v] += u;
	}
	__(num), __(low);
	ticks = cau = khop = 0;
	_for(i,1,n+1)
		if (!num[i]) dfs(i, -1);
    cout << khop << ' ' << cau;
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


