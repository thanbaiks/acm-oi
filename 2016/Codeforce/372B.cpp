#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int cnt[256];

bool solve() {
	char s[50005];
	cin >> s;
	int n = strlen(s), m = 0, qc=0;// question count
	if (n < 26)
		return false;
	memset(cnt,0,sizeof(cnt));
	_for(i,0,26) {
		if (s[i]=='?') {
			qc++;
		} else if (!cnt[s[i]]) {
			cnt[s[i]]++;
			m++;
		} else {
			if (cnt[s[i]]==1) m--;
			cnt[s[i]]++;
		}
	}
	int i = 26;
	while (1) {
		if (m+qc==26) {
			// Found
			// Fill char
			_for(j,i-26,i) {
				if (s[j]=='?')
					_for(k,'A','Z'+1) {
						if (!cnt[k]) {
							s[j] = char(k);
							cnt[k]++;
							break;
						}
					}
			}
			_for(j,0,i-26) {
				if (s[j]=='?') s[j] = 'A';
			}
			_for(j,i,n) {
				if (s[j]=='?') s[j] = 'A';
			}
			cout << s;
			return true;
		}
		if (i>=n) return false;
		// ADD s[i]
		if (s[i]=='?') qc++;
		else if (cnt[s[i]]==0) cnt[s[i]]++,m++;
		else if (cnt[s[i]]==1) cnt[s[i]]++,m--;
		else cnt[s[i]]++;
		// Remove s[i-26]
		int j = i-26;
		if (s[j]=='?') qc--;
		else if (cnt[s[j]]==2) cnt[s[j]]--,m++;
		else if (cnt[s[j]]==1) cnt[s[j]]--,m--;
		else cnt[s[j]]--;
		i++;
	}
	return false;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    if (!solve()) cout << -1;
}


