#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 300000;
int D[N];

int b2i(char *s){
	int len = strlen(s), res = 0;
	_for(i,0,len) {
		res = (res<<1)+s[i]-'0';
	}
	return res;
}
int d2i(char *s) {
	int len = strlen(s), res = 0;
	_for(i,0,len) {
		res = (res<<1)+(s[i]-'0')%2;
	}
	return res;
}
void solve() {
	memset(D,0,sizeof(D));
	
	int n;
	cin >> n;
	_for(i,0,n) {
		char q,s[200];
		cin >> q >> s;
		int k;
		switch (q) {
			case '+':
				k = d2i(s);
				D[k]++;
				break;
			case '-':
				k = d2i(s);
				D[k]--;
				break;
			case '?':
				k = b2i(s);
				cout << D[k] << '\n';
				break;
		}
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


