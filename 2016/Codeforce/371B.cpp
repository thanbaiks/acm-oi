#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> P;
const int N = 1e5+5;
int A[N],n;

bool solve() {
	int n;
	cin >> n;
	_for(i,0,n) {
		cin >> A[i];
	}
	sort(A,A+n);
	n = unique(A,A+n)-A;
	
	if (n>3) return false;
	else if (n==1)
		return true;
	else if (n==2)
		return true;
	else if (n==3)
		return A[1]*2==A[0]+A[2];
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    if (solve()) {
    	cout << "YES";
	} else {
		cout << "NO";
	}
}


