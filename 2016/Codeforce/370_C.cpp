#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
typedef long long ll;
using namespace std;
int main()
{
	ios::sync_with_stdio(false);cin.tie(0);
#ifdef _DEBUG
#endif
	int x, y; cin >> x >> y;
	if (x > y) swap(x, y);
	int a[3]{ x,x,x };
	int cnt = 0;
	while (true)
	{
		if (a[0] == a[1] && a[1] == a[2] && a[2] == y)
			break;
		a[0] = min(a[1] + a[2] - 1,y);
		sort(a, a + 3);
		cnt++;
	}
	cout << cnt << endl;
	return 0;
}
