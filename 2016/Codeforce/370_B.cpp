#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int A[4];
void solve() {
	char s[100005];
	cin >> s;
	A[0]=A[1]=A[2]=A[3];
	_for(i,0, strlen(s)) {
		int t = -1;
		switch (s[i]) {
			case 'U':
				t = 0;
				break;
			case 'D':
				t = 2;
				break;
			case 'L':
				t = 1;
				break;
			case 'R':
				t = 3;
				break;
		}
		A[t]++;
	}
	if ((A[0]+A[2] + A[1]+A[3])&1) {
		cout << -1;
		return;
	}
	int t1 = abs(A[0]-A[2]), t2 = abs(A[1]-A[3]);
	cout << min(t1,t2)+abs(t1-t2)/2;
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

