#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

LL sqr(LL k) {
	return k*k;
}

LL find(LL m, LL lv) {
	LL t = lv+1, k = 1;
	while ((sqr(t*k)-m)%lv) k++;
	return (sqr(t*k)-m)/lv;
}

void solve() {
	LL n,m=2,lv=1;
	cin >> n;
	LL k;
	_for(i,0,n) {
		k = find(m,lv);
		cout << k << '\n';
		m = sqrt(m+k*lv);
		lv++;
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


