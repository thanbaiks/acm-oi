#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int n;
char s[3][100005];

int calc(char *s, char *d, int n) {
	int rb=0,br=0;
	_for(i,0,n) {
		if (s[i]==d[i]) continue;
		else if (s[i]=='r') rb++;
		else br++;
	}
	return max(rb,br);
}

void solve() {
	cin >> n >> s[0];
	_for(i,0,n) {
		s[1][i] = i&1?'r':'b';
		s[2][i] = i&1?'b':'r';
	}
	s[1][n] = s[2][n] = 0;
	int res = min(calc(s[0],s[1], n), calc(s[0],s[2], n));
	cout << res;
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


