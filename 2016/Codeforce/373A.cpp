#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int n, a[100];
void solve() {
	cin >> n;
	_for(i,0,n) cin >> a[i];
	if (a[n-1]==0) {
		cout << "UP";
		return;
	} else if (a[n-1]==15) {
		cout << "DOWN";
		return;
	}
	
	if (n==1) {
		cout << -1;
		return;
	}
	cout << (a[n-1]-a[n-2]>0?"UP":"DOWN");
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


