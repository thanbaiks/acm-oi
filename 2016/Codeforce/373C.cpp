#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

char s[200005];
int t,n;
void solve() {
	cin >> n >> t >> s;
	char *pos = strchr(s,'.');
	if (!pos) {
		// Not have decimal
		cout << s;
		return;
	}
	
	char *c, *end = s+n;
	for (c = pos+1; c<end && *c<'5'; c++);
	if (c==end) {
		cout << s;
		return;
	}
	bool dot = false, carry = false;
	while (c>=s) {
		if (*c=='.') {
			dot=true;
			c--;
			continue;
		}
		
		if (carry) {
			carry = false;
			(*c)++;
			if (*c>'9') {
				*c -= 10;
				carry = true;
			}
		}
		
		if (!dot && t>0 && *c>='5') {
			*c=0;
			t--;
			carry = 1;
		}
		c--;
		
	}
	if (carry) {
		n = strlen(s);
		cout << 1;
		_for(i,0,n-1) cout << 0;
	} else {
		n = strlen(s);
		if (s[n-1]=='.') s[n-1] = 0;
		cout << s;
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


