#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

char S[256][100] = {
    "@",
    "8",
    "(",
    "|)",
    "3",
    "#",
    "6",
    "[-]",
    "|",
    "_|",
    "|<",
    "1",
    "[]\\/[]",
    "[]\\[]",
    "0",
    "|D",
    "(,)",
    "|Z",
    "$",
    "']['",
    "|_|",
    "\\/",
    "\\/\\/",
    "}{",
    "`/",
    "2"
};


char s[10005];
void solve() {
    cin.getline(s,10005);
    int n = strlen(s);
    _for(i,0,n) {
        if (isalpha(s[i])) {
            cout << S[tolower(s[i])-'a'];
        } else {
            cout << s[i];
        }
    }
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


