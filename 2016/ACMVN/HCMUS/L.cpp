#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}
typedef pair<int,int> P;

const int N = 1e5+5, INF = 1e9;
int n,m,x,y,ans;
int d[2][N];
int low[N],num[N],ticks;
vector<P> E[N];

void dijkstra(int *d, int u) {
    bool *f = new bool[N];
    memset(f,0,N*sizeof(bool));
    _for(i,1,n+1) d[i] = INF;
    d[u] = 0;
    priority_queue<P,vector<P>, greater<P> > pq;
    pq.push(make_pair(0,u));
    while (!pq.empty()) {
        P p = pq.top(); pq.pop();
        if (f[p.second]) continue;
        f[p.second] = true;
        _it(it, E[p.second]) {
            if (f[it->first]) continue;
            if (d[it->first] > d[p.second] + it->second) {
                d[it->first] = d[p.second] + it->second;
                pq.push(make_pair(d[it->first], it->first));
            }
        }
    }
    delete f;
}

void tarjan(int u, int par) {
	num[u] = low[u] = ++ticks;
	
	_it(i, E[u]) {
		int v = i->first;
		if (v==par) continue;
		if (!num[v]) {
			tarjan(v,u);
			low[u] = min(low[u], low[v]);
    		if (low[v]>num[u]) {
    		    ans = min(ans,
    		      min(
    		          max(d[0][u],d[1][v]),
    		          max(d[0][v],d[1][u])
                  )
                );
            }
		} else {
			low[u] = min(low[u], num[v]);
		}
	}
}

void solve() {
    {
        cin >> n >> m;
        int u,v,d;
        _for(i,0,m) {
            cin >> u >> v >> d;
            E[u] += make_pair(v,d);
            E[v] += make_pair(u,d);
        }
        cin >> x >> y;
    }
    
    {
        dijkstra(d[0], x);
        dijkstra(d[1], y);
    }
    
    ticks = 1;
    ans = INF;
    _for(i,1,n+1) {
        if (!num[i]) {
            tarjan(i,-1);
        }
    }
    cout << ans;
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
} 
