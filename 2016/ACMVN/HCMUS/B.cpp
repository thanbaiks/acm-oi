#include <bits/stdc++.h>
 
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
 
using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef pair<int,int> P;
 
const int N = 1005;
vector<int> E[N];
int n,m,_[2],d[N];
bool f[2][N];
 
bool dfs(int u) {
	_it(it, E[u]) {
		int v = *it;
		if (d[v]) {
			if (d[v]==d[u]) return false;
		} else {
			d[v] = -d[u];
			_[d[v]<0]++;
			if (!dfs(v)) return false;
		}
	}
	return true;
}
 
 
void solve() {
	memset(f,0,sizeof(f));
	memset(d,0,sizeof(d));
    {
    	cin >> n >> m;
    	_for(i,1,n+1) E[i].clear();
    	int u,v;
    	_for(i,0,m){
    		cin >> u >> v;
    		E[u].push_back(v);
    		E[v].push_back(u);
    	}
    }
    
	queue<P> q;
	_for(i,1,n+1) {
		if (d[i]) continue;
		d[i] = 1;
		_[0] = 1;
        _[1] = 0;
		if (!dfs(i)) {
			cout << "IMPOSSIBLE\n";
			return;
		} else {
			q.push(make_pair(_[0], _[1]));
		}
	}
	f[0][0] = f[1][0] = 1;
	while (!q.empty()) {
		P p = q.front(); q.pop();
		for (int i=(n>>1);i>0;i--) {
			if (i-p.first >=0 && f[0][i-p.first])
				f[0][i] = 1;
			if (i-p.second >=0 && f[1][i-p.second])
				f[1][i] = 1;
		}
		for (int i=1;i<=(n>>1);i++) f[0][i] = f[1][i] = (f[0][i]||f[1][i]);
	}
	
	int i = n >> 1;
	while (!f[0][i]) i--;
	cout << n-i << '\n';
}


void sinh() {
	freopen("input.txt","w",stdout);
	int n = 1000, m = 505;
	cout << 1 << endl << endl;
	cout << n << ' ' << m << endl;
	_for(i,2,2+m) {
	    cout << 1 << ' ' << i << '\n';
	}
}
 
int main(){
//    sinh(); return 0;
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    int t;
    cin >> t;
    while (t--)
    	solve();
}
