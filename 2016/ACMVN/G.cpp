#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;


const int N = 1e3+5;
int A[N][N], n , m;

void solve() {
	cin >> m >> n;
	memset(A,0,sizeof(A));
	_for(i,1,m+1) {
		_for (j,1,n+1) {
			cin >> A[i][j];
			A[i][j] += A[i][j-1] + A[i-1][j]-A[i-1][j-1];
		}
	}
	LL res = LLONG_MAX;
	LL B[4];
	_for(i,1,m+1) {
		_for (j,1,n+1) {
			B[0] = A[i][j];
			B[1] = A[m][j]-B[0];
			B[2] = A[i][n]-B[0];
			B[3] = A[m][n]-B[0]-B[1]-B[2];
			LL u=B[0],v=B[0];
			_for(i,1,4) u = max(u,B[i]), v = min(v,B[i]);
			res = min(res, u-v);
		}
	}
	cout << res << '\n';
}

int main(){
	#if NGOBACH
    freopen("input_G.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--)
    	solve();
}
