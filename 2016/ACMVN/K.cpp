#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

char OP[][10] = {
	"INC", // 0
	"DOUBLE", // 1
	"TRIPLE", // 2
	"SQR",		//3 
	"SET"		// 4
};

struct Op {
	int op,val;
	LL apply(LL x) {
		switch (op) {
			case 0:
				return x+1;
			case 1:
				return x*2;
			case 2:
				return x*3;
			case 3:
				return x*x;
			case 4:
				return val;
		}
	}
} A[12];

set<LL> Set;
int n;

void Try(int i, int j, LL val) {
	if (i<n) {
		Try(i+1,j,A[i].apply(val));
	}
	if (j<n) {
		Try(i,j+1,A[j].apply(val));
	}
	if (i==j && j==n) {
//		cout << val << endl;
		Set.insert(val);
	}
}

void solve() {
	cin >> n;
	char s[20];
	_for(i,0,n) {
		cin >> s;
		int j = 0;
		while (strcmp(OP[j],s) != 0)
			j++;
		A[i].op = j;
		if (j==4) cin >> A[i].val;
	}
	
	Set.clear();
	Try(0,0,0);
	cout << Set.size() << '\n';
}

int main(){
    #if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--)
    	solve();
}


