#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 5e4+5, M = 250005;

struct Edge {
	int u,v,l;
} E[M];

int par[N];

bool cmp(const Edge &a, const Edge &b) {
	return a.l < b.l;
}
int get(int i) {
	if (i==par[i]) return i;
	return par[i] = get(par[i]);
}
void join(int u, int v) {
	u = get(u); v = get(v);
	par[v] = u;
}
void solve() {
	int n,m,l;
	cin >> n >> m >> l;
	
	LL sum = 0;
	_for(i,1,n+1) par[i] = i;
	_for (i,0,m) {
		cin >> E[i].u >> E[i].v >> E[i].l;
		if (i<l) sum += E[i].l;
	}
	sort(E, E+m, cmp);
	LL ssum = 0;
	_for(i,0,m) {
		if (get(E[i].u) != get(E[i].v)) {
			ssum += E[i].l;
			join(E[i].u, E[i].v);
		}
	}
	if (ssum > sum) {
		cout << "no\n";
		return;
	}
	_for(i,2,n+1) {
		if (get(i-1) != get(i)) {
			cout << "no\n";
			return;
		}
	}
	cout << "yes\n";
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--)
    	solve();
}


