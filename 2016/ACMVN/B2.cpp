#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

void solve() {
	set<string> s,s2;
	string ss;
	cin >> ss;
	int n = ss.size(), ans = 0;
	_for(i,0,n) _for (j,i,n) {
		string t1 = ss.substr(i,j-i+1), t2 = t1;
		reverse(_all(t2));
		if (t1==t2 && !s.count(t1)) {
			s.insert(t1);
			ans++;
		}
	}
	cout << ans << '\n';
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    
    int t;
    cin >> t;
    while (t--)
    	solve();
}


