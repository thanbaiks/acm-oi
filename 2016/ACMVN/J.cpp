#include<bits/stdc++.h>
#define For(i,a,b) for(int i=a,c=a>b?-1:1;i!=b;i+=c)
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
int A[40]={0,1,2,3,4,5,6,7,8,9,10,10,10,10,15,20,20,20,25,25,50,75,100,125,150,190,200,250,300,350,500,500,750,1000,1250,1500,2000,2500,3000,5000};
void init(){
    ULL sum=0;
    for(int i=0;i<40;i++){
        A[i]*=1000;
        sum+=A[i];
    }
//  cout<<sum;
}
void solve(){
    ULL p;
    cin>>p;
    ULL sum=0;
    int i=0;
    while(sum<=p&&i<41){
        sum+=A[i];
        i++;
    }
    cout<<i-1<<endl;
}
int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(NULL);
    init();
    int t;
    cin>>t;
    while(t--)
        solve();
    return 0;
}
