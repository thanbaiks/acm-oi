#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> P;

const int N = 1005;

struct Ed {
	int u,z;
};

vector<Ed> A[N];
int n,m,s,t,res;
int D[N];
bool f[N];

void solve() {
	cin >> n >> m >> s >> t;
	
	int u,v,z;
	_for(i,0,m) {
		cin >> u >> v >> z;
		A[u] += (Ed){v,z};
		A[v] += (Ed){u,z};
	}
	memset(D,0,sizeof(D));
	memset(f,0,sizeof(f));
	priority_queue<P> q;
	q.push(make_pair(INT_MAX, s));
	D[s] = INT_MAX;
	while (!q.empty()) {
		P p = q.top();
		q.pop();
		if (f[p.second]) continue; // already visited
		f[p.second] = true;
		if (p.second == t) {
			cout << p.first;
			return;
		}
		_it(it, A[p.second]) {
			int t = min(D[p.second], it->z);
			if (!f[it->u] && t > D[it->u]) {
				D[it->u] = t;
				q.push(make_pair(t, it->u));
			}
		}
	}
	cout << -1;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


