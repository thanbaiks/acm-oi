#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}
typedef pair<int,int> P;
const int N = 205;
int n;
P A[N];

bool cmp(const P &a, const P &b) {
	return a.second<b.second;
}

void solve() {
	cin >> n;
	_for (i,0,n) {
		cin >> A[i].first >> A[i].second;
	}
	sort(A,A+n, cmp);
	int i=0,j=0,c=0;
	while (1) {
		int endAt = A[i].second;
		while (j<n && A[j].first <= endAt) j++;
		i=j;
		c++;
		if (i>=n) break;
	}
	cout << c;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


