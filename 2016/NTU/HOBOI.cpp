#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int n,m;
char s[105];
void solve() {
	cin >> m >> n;
	LL res = 0, t = 0;
	_for(i,0,m) {
		cin >> s;
		_for(i,0,n) {
			if (s[i]=='.') res += t;
			else {
				t = 2-t;
				res++;
			}
		}
	}
	cout << res/2;
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


