#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int n,k;
bool b[100];
int a[100];

void recur(int pos, int rem) {
	if (!rem) {
		// Print
		_for (i,0,k) {
			cout << a[i] << ' ';
		}
		cout << '\n';
	} else {
		_for(i,1,n+1) {
			if (!b[i]) {
				b[i] = true;
				a[pos]=i;
				recur(pos+1, rem-1);
				b[i] = false;
			}
		}
	}
}

void solve() {
	cin >> n >> k;
	memset(b,0,sizeof(b));
	recur(0,k);
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


