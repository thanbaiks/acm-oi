#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e5+5;
int A[N], n, k;

void solve() {
	int t, b = 0;
	cin >> n >> k;
	fill(A, A+n+1, -1);
	LL res = 0;
	A[0] = 0;
	_for (i,1,n+1) {
		cin >> t;
		b = (b+t)%k;
		if (A[b]<0) A[b] = i;
		else res = max(res, LL(i)-A[b]);
	}
	cout << res;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


