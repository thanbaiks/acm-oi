#include <bits/stdc++.h>

#define finp "a.inp"
#define fout ""//a.out"
#define FOR(i,a,b) for(int i=a;i<=b;i++)
#define FORD(i,a,b) for(int i=a;i>=b;i--)
#define maxN 100005
#define mp make_pair
#define pb push_back
#define X first
#define Y second

using namespace std;

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef vector<int> vi;

char s[20]; int d=0;

long long gcd(long long x, long long y){
    while (y!=0){
        x%=y; swap(x,y);
    }
    return x;
}

void enter(){
    //getline(cin,sa,'.');
    cin >> s;
    int i;
    long long u,a=0,b=0,ts,len=strlen(s),ms=0,base=1,ok=0;
    for (i=0;i<len;i++){
        if (s[i]=='(') break;
        if (s[i]=='.') ok=1;
        else{
            a=a*10+s[i]-'0';
            if (ok) base*=10;
        }
    }
    FOR(j,i+1,len-2)
        {b=b*10+s[j]-'0'; ms=ms*10+9;}
    if (b>0){ ts=b+ms*a; ms*=base;}
    else {ts=a; ms=base;}
    u=gcd(ts,ms);
    ts/=u; ms/=u;
    cout << ts << '/' << ms << endl;
}

void process(){

}

int main(){
    //freopen(finp,"r",stdin);
    //freopen(fout,"w",stdout);
    int tests;

    
        enter();
        process();
    
    return 0;
}
  
