#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

inline int abs(int x) {
	return x<0?-x:x;
}
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int n;
	cin >> n;
	_for (i,0,2*n+1) {
		_for (j,0,2*n+1) {
			cout << ((abs(n-i)+abs(n-j))%3==0? 'b' : 'w');
		}
		cout << '\n';
	}
}


