#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const double PI = acos(-1),eps = 1e-6;

int n,R,r;

void solve() {
	cin >> n >> R >> r;
	if (n<=2) {
		cout << (R>r*n?"YES":"NO");
		return;
	}
	double x = double(r)/sin((2*PI)/n) + r;
	if (x<=R+eps)
		cout << "YES";
	else
		cout << "NO";
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}
