#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 4e5+5;
int n,m,num[N],low[N],ticks,res;
vector<int> E[N];

void dfs(int i,int p) {
	num[i] = low[i] = ++ticks;
	_it(it,E[i]) {
		if (*it == p) continue;
		if (!num[*it]) {
			dfs(*it, i);
			low[i] = min(low[i], low[*it]);
		} else {
			low[i] = min(low[i], num[*it]);
		}
	}
	if (low[i]>=num[i])
		res++;
}

void solve() {
	int u,v;
	cin >> n;
	_for (i,0,n-1) {
		cin >> u >> v;
		E[u] += v;
		E[v] += u;
	}
	cin >> m;
	_for (i,1,m+1) {
		cin >> u >> v;
		E[u] += n+i;E[n+i] += u;
		E[v] += n+i;E[n+i] += v;
	}
	
	__(num); __(low); res = 0;
	dfs(1,0);
	cout << res-1;
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


