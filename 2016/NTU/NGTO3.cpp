#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e6+6;

int BIT[MAX];
void add(int x) {
	while (x < MAX) {
		BIT[x]++;
		x += -x&x;
	}
}

int get(int x) {
	int ret = 0;
	while (x) {
		ret += BIT[x];
		x -= -x&x;
	}
	return ret;
}

void sieve() {
	add(1);add(2);
	bool *m = new bool[MAX];
	int i, lim = sqrt(MAX);
	memset(m,1,sizeof(m[0])*MAX);
	for (i=3;i<=lim;i+=2) {
		if (m[i]) {
			add(i);
			for (int j=i*i;j<MAX;j+=2*i) {
				if (m[j]) m[j] = false;
			}
		}
	}
	for (;i<MAX;i+=2) if (m[i]) add(i);
	delete[] m;
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	memset(BIT,0,sizeof(BIT));
	sieve();
	int T,n;
	
	cin >> T;
	_for(t,0,T) {
		cin >> n;
		cout << "Case #" << t+1 << ": " << get(n) << '\n';
	}
}
