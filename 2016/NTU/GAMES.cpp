#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e5+5;
int A[N], T[N*4], L[N*4];

void init(int i, int l, int r) {
	if (l == r) {
		T[i] = A[l];
	} else {
		int m = (l+r)>>1;
		init(i*2+1, l, m);
		init(i*2+2, m+1, r);
		T[i] = max(T[i*2+1], T[i*2+2]);
	}
}

void apply(int i,int l,int r) {
	if (L[i]) {
		T[i] = L[i];
		if (l<r) {
			L[i*2+1] = L[i*2+2] = L[i];
		}
		L[i] = 0;
	}
}
void update(int i, int l, int r, int ql, int qr, int val) {
	if (l>r || l>qr || r<ql)
		return;
	apply(i,l,r);
	if (ql<=l && r<=qr) {
		T[i] = L[i] = val;
		return;
	}
	int m = (l+r)>>1;
	update(2*i+1, l, m,ql,qr,val);
	update(2*i+2, m+1, r,ql,qr,val);
	T[i] = max(T[i*2+1], T[i*2+2]);
}

int get(int i, int l, int r, int ql, int qr) {
	if (l>r || l>qr || r<ql) {
		return 0;
	}
	apply(i,l,r);
	if (l>=ql && r <= qr) {
		return T[i];
	}
	int m = (l+r)>>1;
	return max(get(i*2+1,l,m,ql,qr), get(i*2+2,m+1,r,ql,qr));
}

int n,m,t,u,k;
void solve() {
	cin >> n >> m;
	_for(i,0,n) cin >> A[i];
	memset(T,0,sizeof(T));
	memset(L,0,sizeof(L));
	init(0,0,n-1);
	_for (i,0,m) {
		cin >> t >> u;
		k = get(0,0,n-1,t-1,t-2+u)+1;
		cout << k << '\n';
		update(0,0,n-1,t-1,t-2+u, k);
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


