#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N=1e5+5;
vector<int> V[N];
bool vis[N];

int n,k,c=0;
void dfs(int x) {
//	cerr << "dfs: " << x << endl;
	vis[x] = 1;
	c++;
	_it(it, V[x]) {
		int t = *it;
		if (!vis[t]) {
			dfs(t);
		}
	}
}

void solve() {
	cin >> n >> k;
	int m,t;
	_for(i,1,n+1) {
		cin >> m;
		_for(j,0,m) {
			cin >> t;
			V[i] += t;
//			V[t] += i;
		}
	}
	memset(vis,0,sizeof(vis));
	dfs(k);
	cout << c << '\n';
	_for(i,1,n+1) if (vis[i]) cout << i << ' ';
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


