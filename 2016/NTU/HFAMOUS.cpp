#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e5+5;
int n,m,k;
vector<int> E[N];
int d[N];
bool vis[N];

void dfs(int u, par) {
	vis[u] = 1;
	if (E[u].size() < k) return;
	
	d[u] = 1;
	_it(i, E[u]) {
		int v = *i;
		if (par==v) continue;
		if (!vis[v]) dfs(v,u);
		if (d[i]>=k) d[u]++;
	}
}

void solve() {
	cin >> n >> m >> k;
	int u,v;
	_for(i,0,m) {
		cin >> u >> v;
		E[u] += v;
		E[v] += u;
	}
	__(d); __(vis);
	_for(i,1,n+1) if (!vis[i]) dfs(i,0);
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


