#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

struct PS{
	LL t,m;
	PS() {
		t = 0;
		m = 1;
	}
	PS(int x) {
		t = 0;
		m = x;
	}
	void rg() {
		LL tmp = __gcd(t,m);
		t /= tmp;
		m /= tmp;
	}
};
LL pow10(int n) {
	int ret = 1;
	_for(i,0,n) ret *= 10;
	return ret;
}
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	char s[100];
	cin >> s;
	char *point = strchr(s,'.'), *open = strchr(s,'('), *close = strchr(s,')'), *end = s+strlen(s);
	PS p;
	if (point == NULL) {
		// TRAP
		cout << s << "/1";
		return 0;
	}
	for (char *i = s; i < point;i++) {
		p.t = p.t*10 + (*i-'0');
	}
	for (char *i = point+1;i<(open?open:end);i++) {
		p.t = p.t*10 + (*i-'0');
		p.m *= 10;
	}
	p.rg();
	if (open) {
		PS p2(0);
		for (char *i = open+1;i<close;i++) {
			p2.t = p2.t*10 + (*i-'0');
			p2.m = p2.m*10 + 9;
		}
		for (char *i = point+1;i<open;i++) {
			p2.m *= 10;
		}
		p2.rg();
		p.t = p.t*p2.m + p2.t*p.m;
		p.m *= p2.m;
		p.rg();
	}
	cout << p.t << '/' << p.m;
}


