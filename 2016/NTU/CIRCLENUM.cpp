#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

LL count(LL n) {
	if (n<10) return n;
	LL res = 9; // [1,9]
	vector<int> v;
	while (n) {
		v += int(n%10);
		n /= 10;
	}
	reverse(_all(v));
	
	LL k = 1;
	_for(i,0,v.size()-2) {
		res += 9*k;
		k*=10;
	}
	res += (v[0]-1)*k;
	k = 0;
	_for(i,1,v.size()-1) k = k*10 + v[i];
//	k--;
	if (k>0) {
		res += k;
	}
	if (v[v.size()-1]>=v[0]) res++;
	return res;
}

LL a,b;
void solve() {
	cin >> a >> b;
	cout << count(b)-count(a-1);
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

 
