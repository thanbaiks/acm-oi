#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int n,t,res,len;
char s[105],c;

int _try(int u, int v) {
	if (u<0 || v>=len) return 0;
	if (s[u] != s[v]) return 0;
	char c = s[u];
	int res = 2;
	while (u>0 && s[u-1]==c) u--,res++;
	while (s[v+1]==c) v++,res++;
	if (res <= 2) return false;
	return res + _try(u-1,v+1);
}

int solve() {
	cin >> n >> s >> t >> c;
	t--;
	len = strlen(s);
	res = 0;
	int u=t,v=t-1;
	while (u>0&&s[u-1]==c)u--;
	while (s[v+1]==c)v++;
	if (v-u+2<3) {
		return 0;
	}
	res = v-u+2;
	res += _try(u-1,v+1);
	return res;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout << solve();
}


