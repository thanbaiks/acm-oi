#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int n, A[15][15];

void solve() {
	cin >> n;
	_for (i,0,n) {
		_for(j,0,n)
			cin >> A[i][j];
	}
	_for(k,0,n) {
		bool m = false;
		stack<int> s;	
		_for (i,n-1,-1) {
			if (!A[i][k]) continue;
			if (s.empty() || m || s.top() != A[i][k]) {
				s.push(A[i][k]);
				m = false;
			} else{
				s.pop();
				s.push(A[i][k]*2);
				m = true;
			}
		}
		_for(i,0,n) {
			A[i][k] = 0;
		}
		while (!s.empty()) {
			A[n-s.size()][k] = s.top();
			s.pop();
		}
	}
	_for (i,0,n) {
		_for(j,0,n)
			cout << A[i][j] << ' ';
		cout << '\n';
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


