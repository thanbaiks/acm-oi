#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

bool test(int n) {
	int c = -1, st[12];
	while (n>0) {
		st[++c] = n%10;
		n /=10;
	}
	
	for (int i=0;i<c;i++,c--) {
		if (st[i] != st[c]) return false;
	}
	return true;
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int n,t;
	cin >> n;
	_for(i,0,n) {
		cin >> t;
		if (test(t)) cout << t << ' ';
	}

}

