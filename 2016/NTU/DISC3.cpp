#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e5+5;
int T[2*N],pos[N];

void update(int x, int v) {
	while (x<2*N) {
		T[x] += v;
		x += -x&x;
	}
}
int get(int x) {
	int res=0;
	while (x) res += T[x], x -= -x&x;
	return res;
}

int n,m;
void solve() {
	__(T);
	cin >> n >> m;
	_for(i,1,n+1) {
		update(N+i,1);
		pos[i] = N+i;
	}
	int k = N-1;
	while (m--) {
		cin >> n;
		cout << get(pos[n]-1) << ' ';
		update(pos[n],-1);
		update(k,1);
		pos[n]=k;
		k--;
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


