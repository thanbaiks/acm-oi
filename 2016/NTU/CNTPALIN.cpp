#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

void xl(LL a, LL &out, LL &b) {
	int v[28],c=0;
	while (a) v[c++] = a%10, a/=10;
	c--;
	out = 0;
	b = 0;
	int mid = c/2+1;
	_for(i,0,mid)
		out = out*10+v[c-i];
	mid = c+1-mid;
	b = out;
	_for(i,mid-1,-1)
		b = b*10+v[c-i];
}


LL sol(LL a, LL b) {
	LL res = 0,lo,hi,x=1,y,z,u,v;
	while (x*10<=a)x*=10;
	lo = a, hi = min(x*10-1,b);
	while (1) {
		xl(lo,y,u);
		xl(hi,z,v);
		res += z-y;
		if (u<lo)res--;
		if (v<=hi)res++;
		
		if (b<=hi) break;
		lo = hi+1;
		x *= 10;
		hi = min(x*10-1,b);
	}
	return res;
}

void solve() {
	int t; LL a,b;
	cin >> t;
	while (t--) {
		cin >> a >> b;
		if (a>b) swap(a,b);
		cout << sol(a,b) << '\n';
//		return; //TODO: rem
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}
