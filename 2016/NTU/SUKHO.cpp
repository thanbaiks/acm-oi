#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e4+4;
LL A[MAX];

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int t,n,m,k;
	cin >> n >> m >> k;
	memset(A,0,sizeof(A));
	while (n--) {
		cin >> t;
		for (int x = MAX-1;x-t>0;x--)
			if (A[x-t]) A[x] = min(1000LL, A[x] + A[x-t]);
		A[t] = min(A[t]+1,1000LL);
		
	}
	if (A[m] >= k) cout << "ENOUGH"; else cout << A[m];
}


