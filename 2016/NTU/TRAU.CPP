#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int N,M;

bool test(int d, int &n, int &g) {
	n = 3*M-N-14*d;
	if (n<=0 || n%8) return false;
	n /= 8;
	g = N-d-n;
	return g > 0;
}

void solve() {
	int n,g;
	cin >> N >> M;
	_for(d,1,M/3+1) {
		if (test(d,n,g)) {
			cout << d << ' ' << n << ' ' << g;
			return;
		}
	}
	cout << -1;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


