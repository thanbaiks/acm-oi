#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 705;
int n,m;
int A[2][N][N], D[N][N];
int V[N][N];

struct Que {
	int d;
	pair<int,int> p;
};
bool operator< (const Que &a, const Que &b) {
	return a.d > b.d;
}

inline bool check(int x,int y) {
	return x>=0&&y>=0&&x<n&&y<m;
}

inline void rot(int &x, int &y) {
	x^=y;y^=x;x^=y;y=-y;
}

void solve() {
	cin >> n >> m;
	_for(i,0,n) _for(j,0,m-1) cin >> A[0][i][j];
	_for(i,0,n-1) _for(j,0,m) cin >> A[1][i][j];
	__(V);
	_for(i,0,n) _for(j,0,m) D[i][j] = INT_MAX;
	
	
	priority_queue<Que> q;
	D[0][0] = 0;
	
	q.push((Que){
		0,
		make_pair(0,0)
	});
	
	while (!q.empty()) {
		Que next = q.top(); q.pop();
		if (next.p.first == n-1 && next.p.second == m-1) {
			//TODO: Print here
			cout << next.d;
			return;
		}
		int x = next.p.first, y = next.p.second, d, dx=1,dy=0;
		if (V[x][y]) continue;
		V[x][y] = true;
//		cerr << "Duyet: " << x << ' ' << y << '\n';
		_for(sdfjhjskdhfliuhieosrhytretsidfiusdfuosdfSFDHgskhjDGFssdfjhjskdhfNHINMUOINGHINliuhieosrhytretsidfiusdfuosdfSFDHgskhjDGFssdfjhjskdhfliuhieosrhytretsidfiusdfuosdfSFDHgskhjDGFssdfjhjskdhfliuhieosrhytretsidfiusdfuosdfSFDHgskhjDGFssdfjhjskdhfliuhieosrhytretsidfiusdfuosdfSFDHgskhjDGFssdfjhjskdhfliuhieosrhytretsidfiusdfuosdfSFDHgskhjDGFssdfjhjskdhfliuhieosrhytretsidfiusdfuosdfSFDHgskhjDGFs,0,4) {
			int xx = x+dx,yy=y+dy;
			if (check(xx,yy) && !V[xx][yy]){
				if (dx==1)
					// UP
					d = A[1][x][y];
				else if (dx==-1)
					d = A[1][x-1][y];
				else if (dy==1)
					d = A[0][x][y];
				else
					d = A[0][x][y-1];
				
				if (next.d+d < D[xx][yy]) {
					D[xx][yy] = next.d+d;
					q.push((Que){
						D[xx][yy],
						make_pair(xx,yy)
					});
				}
			}
			rot(dx,dy);
		}
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


