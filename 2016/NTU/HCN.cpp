#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int abs(int t) {
	return t<0?-t:t;
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int n,A[1000][2],dp[1000][2];
	cin >> n;
	_for (i,0,n) cin >> A[i][0] >> A[i][1];
	dp[0][0] = A[0][0];
	dp[0][1] = A[0][1];
	_for (i,1,n) {
		dp[i][0] = A[i][0] + max(dp[i-1][0]+abs(A[i][1]-A[i-1][1]),dp[i-1][1]+abs(A[i][1]-A[i-1][0]));
		dp[i][1] = A[i][1] + max(dp[i-1][0]+abs(A[i][0]-A[i-1][1]),dp[i-1][1]+abs(A[i][0]-A[i-1][0]));
	}
	cout << max(dp[n-1][0],dp[n-1][1]);
}


