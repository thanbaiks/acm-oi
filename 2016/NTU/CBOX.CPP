#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}
namespace BIT {
	const int N = 1e6+5;
	int T[N];
	void init() {
		memset(T,0,sizeof(T));
	}
	void add(int x, int v) {
		x++;
		while (x<N) {
			T[x]++;
			x += -x&x;
		}
	}
	int get(int x) {
		x++;
		int res = 0;
		while (x) {
			res += T[x];
			x -= -x&x;
		}
		return res;
	}
}
const int N = 1e6+5;
int A[N],B[N], C[N], n,m,q;

void solve() {
	BIT::init();
	cin >> n >> m;
	_for(i,0,m) {
		cin >> A[i] >> B[i];
	}
	sort(A,A+m); sort(B, B+m);
	int u(0),v(0),d=0;
	_for(i,1,n+1) {
		while (u<n && A[u]==i) {
			d++;
			u++;
		}
		BIT::add(d,1);
		while (v<n && B[v]==i) {
			d--;
			v++;
		}
	}
	cin >> q;
	while (q--) {
		cin >> u;
		cout << n-BIT::get(u-1) << '\n';
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


