#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e5+5;
bool f[N];
int cnt;

void dfs(int n) {
//	if (f[n]) return;
	f[n] = true;
	cnt++;
	int k,t;
	_for (k,2,sqrt(n)+1) {
		if (n%k==0) {
			t = (k-1)*(n/k+1);
			if (!f[t]) dfs(t);
		}
	}
}

void solve() {
	int n;
	cin >> n;
	__(f);
	f[0] = 1;
	cnt=0;
	dfs(n);
	cout << cnt << '\n';
	_for(i,0,n) if (f[i]) cout << i << ' ';
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


