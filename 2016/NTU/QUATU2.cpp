#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> P;

int D[10][10];
const int R[4][2] = {{-2,-2}, {-2,2},{2,-2},{2,2}};
bool check(int x, int y) {
	return x>0&&x<9&&y>0&&y<9;
}

void solve() {
	int x,y,tx,ty;
	cin >> y >> x >> ty >> tx;
	memset(D,0,sizeof(D));
	queue<P> Q;
	
	D[x][y] = 1;
	Q.push(make_pair(x,y));
	while (!Q.empty()) {
		P p = Q.front();
		Q.pop();
		_for(i,0,4) {
			int xx = p.first + R[i][0], yy = p.second + R[i][1];
			if (check(xx,yy) && !D[xx][yy]) {
				Q.push(make_pair(xx,yy));
				D[xx][yy] = D[p.first][p.second]+1;
			}
		}
	}
	if (!D[tx][ty]) {
		cout << -1;
	} else {
		cout << D[tx][ty]-1;
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


