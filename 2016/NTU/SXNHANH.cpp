#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

template <typename T>
T *qs(T *s, T *e) {
	T &pivot = *(e-1);
	T *i = s, *j = s;
	while (i<e-1) {
		if (*i <= pivot) {
			swap(*i,*j);
			++j;
		}
		++i;
	}																																																						
	swap(pivot, *j);
	return j;
}
void solve() {
	int n, A[22];
	cin >> n;
	_for(i,0,n) cin >> A[i];
	int *i = qs(A,A+n), *j = A;
	while (j<i) {
		cout << *(j++) << ' ';
	}
	cout << "[" << *i << "] ";
	i++;
	while (i<A+n) {
		cout << *(i++) << ' ';
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


