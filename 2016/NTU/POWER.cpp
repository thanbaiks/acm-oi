#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

LL powmod(LL a, LL e, LL m) {
	LL t = 1;
	while (e>1) {
		if (e&1) t = (t*a)%m;
		a = (a*a)%m;
		e >>= 1;
	}
	return (t*a)%m;
}


void solve() {
	LL n, t=1,i=2,k;
	cin >> n;
	k = n;
	while (i*i<=n) {
		if (n%i==0) {
			t*=i;
			while (n%i==0) n/=i;
		}
		i++;
	}
	if (n>1) t *= n;
	
	_for(i,1,1000000) {
		if (!powmod(t*i,t*i,k)) {
			cout << t*i;
			return;
		}
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


