#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e5+5;
int n,m, pre[N], next[N];

void solve() {
	cin >> n >> m;
	_for(i,1,n+1) {
		pre[i]=i-1;next[i]=i+1;
	}
	pre[1]=n;next[n]=1;
	
	int top = 1, t;
	while (m--) {
		cin >> t;
		if (t!=top) {
			pre[next[t]] = pre[t];
			next[pre[t]] = next[t];
			next[t] = top;
			pre[t] = pre[top];
			next[pre[top]] = t;
			pre[top] = t;
			top = t;
		}
		cout << pre[top] << ' ';
	}
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


