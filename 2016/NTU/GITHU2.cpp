#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

LL count(LL n) {
	LL x = 5, res = 0;
	while (n>=x) {
		res += n/x;
		x *= 5;
	}
	return res;
}
LL bsearch(LL n) {
	LL lo = 0, hi = LLONG_MAX,mid;
	while (hi-lo>1) {
		mid = (hi+lo)>>1;
		if (count(mid) >=n) {
			hi = mid;
		} else {
			lo = mid;
		}
	}
	return hi;
}
void solve() {
	int n; LL m;
	cin >> n;
	while (n--) {
		cin >> m;
		cout << bsearch(m) << '\n';
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


