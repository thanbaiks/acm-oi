#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const LL MOD = 1e6;

void solve() {
    LL n;
    cin >> n;
    if (n==1) {
    	cout << 1;
	} else {
		LL res = 0, rem = n, i = 1;
		
		do {
			LL t = (n/i - n/(i+1));
			res += n/i;
			rem--;
			if (rem){
				res += i*t;
				rem -= t;
			}
			res = res % MOD;
			
			i++;
		} while (rem);
		cout << res;
	}
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

