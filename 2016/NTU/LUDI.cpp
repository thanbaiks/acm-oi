#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef pair<int,int> P;

const int N = 1e3+5;
P a[1000];
int n,t;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> n;
	_for(i,0,n) {
		cin >> a[i].first >> a[i].second;
	}
	sort(a,a+n);
	t = 0;
	int k = a[0].second;
	_for(i,1,n) {
		if (a[i].first > k) {
			t++;
			k = a[i].second;
		} else {
			k = min(k,a[i].second);
		}
	}
	cout << t+1;
}


