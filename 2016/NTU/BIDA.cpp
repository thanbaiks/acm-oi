#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

LL calc(LL a, LL k, LL n) {
	n--;
	a += k;
	bool f = (a/n)&1;
	a %= n;
	if (!f) {
		return a;
	} else {
		return n-a;
	}
}

void solve() {
	LL n,m,x,y,k;
	cin >> n >> m >> y >> x >> k;
	x = calc(x-1,k,m);y = calc(y-1,k,n);
	cout << y+1 << ' ' << x+1;
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


