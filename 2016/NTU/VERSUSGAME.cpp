#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int 	A[] = {5+10,4+5+10,4+1,3,1,10},
			B[] = {2,3,2,1,1,1};
const int N=1e6+5;
LL d[N];

void init() {
	int k;
	__(d);
	_for(i,0,N) {
		if (!i || d[i]) {
			_for(j,0,6) {
				k = i+A[j];
				if (k<N && (!d[k] || d[k]>d[i]+B[j])) {
					d[k] = d[i]+B[j];
				}
			}
		}
	}
}
void solve() {
	int n,t;
	cin >> n;
	while (n--) {
		cin >> t;
		cout << d[t] << '\n';
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    init();
    solve();
}


