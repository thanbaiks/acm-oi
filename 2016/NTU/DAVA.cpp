#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e3+5;
LL A[N][N], B[N][N];
int n,k;
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> n >> k;
	_for(i,1,n+1)
		_for(j,1,n+1) cin >> A[i][j];
	
	memset(B,0,sizeof(B));
	_for (i,1,n+1) _for (j,1,n+1) {
		B[i][j] = B[i-1][j]+B[i][j-1]-B[i-1][j-1]+A[i][j];
	}
	LL res = 0;
	_for (i,0,n+1-k) _for (j,0,n+1-k) {
		res = max(res, B[i+k][j+k]-B[i+k][j]-B[i][j+k]+B[i][j]);
	}
	cout << res;
}


