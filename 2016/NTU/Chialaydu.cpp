#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}


int n,k;
int A[int(1e5+5)];

void solve() {
	cin >> n >> k;
	int res = 0;
	LL t;
	memset(A,0,sizeof(A));
	_for (i,0,n) {
		cin >> t;
		t %= k;
		if (!A[t]) {
			res++;
			A[t] = true;
		}
	}
	cout << res;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


