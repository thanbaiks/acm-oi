#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e3+5;
int n,m;
int TL[N][N],TR[N][N],BL[N][N],BR[N][N];
char S[N][N];

bool check(int x,int y) {
	return x>0&&y>0&&x<=n&&y<=m&&S[x-1][y-1]=='1';
}

void dump(LL mat[N][N]) {
	_for(i,1,n+1) {
		_for (j,1,m+1) {
			cout << mat[i][j] << ' ';
		}
		cout << '\n';
	}
	cout << '\n';
}
void solve() {
	cin >> n >> m;
	_for(i,0,n) {
		cin >> S[i];
	}
	
	memset(TL,0,sizeof(TL));
	memset(TR,0,sizeof(TR));
	memset(BL,0,sizeof(BL));
	memset(BR,0,sizeof(BR));
	
	// Top down
	_for(i,1,n+1) 
		_for (j,1,m+1){
			if (check(i,j)) {
				TL[i][j] = 1;
				if (check(i,j-1)) TL[i][j] += 1 + TL[i-1][j-1];
				TR[i][j] = 1;
				if (check(i-1,j)) TR[i][j] += 1 + TR[i-1][j+1];
			}
		}
	_for(i,n,0) 
		_for (j,1,m+1){
			if (check(i,j)) {
				BL[i][j] = 1;
				if (check(i+1,j)) BL[i][j] += 1 + BL[i+1][j-1];
				BR[i][j] = 1;
				if (check(i,j+1)) BR[i][j] += 1 + BR[i+1][j+1];
			}
		}
	LL mx = 0;
	_for(i,1,n+1) _for(j,1,m+1) {
		mx = max(mx, 0LL + TL[i][j]+TR[i][j]+BL[i][j]+BR[i][j]-3);
	}
	
//	dump(TL);dump(TR);dump(BL);dump(BR);
	
	cout << mx;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


