#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N=1e5+5;
int n,k;
LL A[N];

void solve() {
	cin >> n >> k;
	A[0]=0;
	_for(i,1,n+1) cin >> A[i], A[i]+=A[i-1];
	
	int u,v,m,l,r;
	LL sum,res;
	while (k--) {
		cin >> u >> v; l = u; r = v;
		sum = A[r]-A[l-1];
		while (l<r-1){
			m = (l+r)>>1;
			if ((A[m]-A[u-1])*2 <= sum)
				l = m;
			else
				r = m;
		}
		res = min(
			abs((A[l]-A[u-1]) -  (A[v]-A[l])),
			abs((A[r]-A[u-1]) -  (A[v]-A[r]))
			);
		cout << res << '\n';
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


