#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

void draw(int pad, int n) {
	n--;
	_for (i,0,n+1) {
		_for (j,0,pad) cout << '.';
		
		if (!i) {
			// First line
			_for(j,0,n) cout << '.'; cout << '#'; _for(j,0,n) cout << '.';
		} else {
			_for(j,0,n-i) cout << '.';
			cout << '#';
			_for(j,0,2*i-1) cout << 'x';
			cout << '#';
			_for(j,0,n-i) cout << '.';
		}
		
		_for (j,0,pad) cout << '.';
		cout << '\n';
	}
}

void solve() {
	int n;
	cin >> n;
	draw(2,n);
	draw(1,n+1);
	draw(0,n+2);
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


