#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int n,m;
bool A[50005];

int solve() {
	int t;
	cin >> m >> n;
	memset(A,0,sizeof(A));
	A[0] = 1;
	_for (i,0,n) {
		cin >> t;
		for (int x = m;x-t>=0;x--) {
			if (A[x-t] && !A[x]) {
				A[x] = true;
				if (x == m) return m;
			}
		}
	}
	while (!A[m])m--;
	return m;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
	
    cout << solve();
}


