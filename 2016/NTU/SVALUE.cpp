#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const LL MOD=1e9+7;
const int N=1e5+5;
int n,A[N];

void solve() {
	cin >> n;
	_for(i,0,n) cin >> A[i];
	sort(A,A+n);
	LL x=A[0],res=0,sum=A[0];
	_for(i,1,n) {
		res = (res + LL(A[i])*i-sum)%MOD;
		res = (res+MOD*MOD)%MOD;
		sum = (sum + A[i])%MOD;
		x = max(x, LL(A[i]));
	}
	res = (res*x)%MOD;
	cout << res;
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


