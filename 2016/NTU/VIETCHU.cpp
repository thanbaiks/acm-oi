#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const char S[11][20] = {"khong","mot","hai","ba","bon","nam","sau","bay","tam","chin"};


void doc(int t, queue<string> &s, bool b) {
	t %= 1000;
	if (b&&!t) return;
	if (!b && t<10) {
		s.push(S[t]);
		return;
	}
	int x=t/100,y=(t/10)%10,z=t%10;
	
	if (x || b) {
		s.push(S[x]);
		s.push("tram");
		if (!y && !z) return;
		if (!y && z) {
			s.push("le");
			s.push(S[z]);
			return;
		}
	}
	
	if (y) {
		if (y>1)
			s.push(S[y]);
		s.push("muoi");
	}
	if (z==5)
		s.push("lam");
	else if (z)
		s.push(S[z]);
}

void solve() {
	int t;
	cin >> t;
	queue<string> q;
	if (t>=1000) {
		doc(t/1000, q, false);
		q.push("nghin");
	}
	doc(t,q,t>=1000);
	
	while (!q.empty()) {
		cout << q.front(); q.pop();
		if (!q.empty()) cout << ' ';
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


