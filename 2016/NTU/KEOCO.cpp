#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int d[50005],a[105],n,sum;

void solve() {
	cin >> n;
	_for(i,0,n) cin >> a[i], sum += a[i];
	
	__(d);
	int s = sum/2, t = n/2, res = 0;
	_for(i,0,n) {
		for (int j=s;j-a[i]>=0;j--) {
			if (j-a[i]==0 || d[j-a[i]]) {
				d[j] = d[j-a[i]]+1;
				if (d[j]==t || d[j]==n-t) res = max(res,j);
			}
		}
	}
	cout << res << ' ' << sum-res;
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


