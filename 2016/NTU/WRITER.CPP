#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

string S[10] = {"_0",".,?!1","abc2","def3","ghi4","jkl5","mno6","pqrs7","tuv8","wxyz9"};
int _[256];

void init() {
	_for(i,0,10) {
		_it(it,S[i]) {
			_[*it] = i;
		}
	}
}

int solve() {
	string s;
	cin >> s;
	int k = 1;
	int res = 0;
	_it(i,s) {
		char c = *i;
		if (k != _[c])  {
			res++;
			k = _[c];
		}
		res += S[k].find(c)+1;
	}
	return res;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
	
	int T;
	init();
	cin >> T;
	while (T--) {
    	cout << solve() << endl;
	}
}


