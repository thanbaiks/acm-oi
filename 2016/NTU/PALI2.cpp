#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 2005;
int n;
char s[N];
bool f[N][N];
int trace[N],d[N];

void solve() {
	cin >> n >> s;
	_for (i,0,n)
		f[i][i] = 1;
	_for(i,1,n) {
		int j = i-1;
		if (s[i]==s[j]) f[j][i] = 1;
		j--;
		while (j>=0) {
			if (s[i]==s[j]&&f[j+1][i-1]) f[j][i]=1;
			j--;
		}
	}
	// _for(i,0,n) d[i] = i+1, trace[i]=i-1;
	d[0] = 1;
	trace[0] = 0;
	_for(i,1,n) {
		d[i] = d[i-1]+1;
		trace[i]=i;
		if (f[0][i]) {
			// Perfect
			d[i] = 1;
			trace[i] = 0;
		} else {
			_for (j,1,i) {
				if (f[j][i] && (d[i]>d[j-1]+1)) {
					d[i] = d[j-1]+1;
					trace[i]=j;
				}
			}
		}
	}
	stack<pair<int,int> > st;
	int u = n-1;
	while (u>=0) {
		st.push(make_pair(trace[u],u));
		u = trace[u]-1;
	}
	cout << st.size() << '\n';
	while (!st.empty()) {
		pair<int,int> p = st.top(); st.pop();
		_for(i,p.first, p.second+1) cout << s[i]; cout << '\n';
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


