#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 5e4+2;
int T[N][2],A[1005],n,nc=1;

void push(int n) {
    int cur = 0;
    _fod(i,29,-1) {
        int x = (n>>i)&1;
        if (!T[cur][x]) {
            T[cur][x] = nc++;
        }
        cur = T[cur][x];
    }
}
int get(int n) {
    int cur = 0,res = 0;
    _fod(i,29,-1) {
        int x = 1-((n>>i)&1);
        if (T[cur][x]) {
            res = (res<<1)|1;
            cur = T[cur][x];
        } else {
            cur = T[cur][1-x];
        }
    }
    return res;
}
void solve() {
    cin >> n;
    _for(i,0,n) cin >> A[i];
    __(T);
    _for(i,0,n) push(A[i]);
    int ans = 0;
    _for(i,0,n) _for(j,0,n) {
        ans = max(ans, get(A[i]^A[j]));
    }
    cout << ans;
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


