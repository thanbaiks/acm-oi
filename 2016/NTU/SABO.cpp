#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,k,A[1005];
int MOD = 1e6;
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> n >> k;
	A[0] = 1;
	_for (i,1,n+1) {
		A[i] = A[i-1];
		if (i-k-1>0) {
			A[i] += A[i-k-1];
		} else {
			A[i]++;
		}
		A[i]%=MOD;
	}
	cout << A[n];
}


