#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}
typedef pair<int,bool> P;

const int N = 2e5+5;
P A[N];
int n, d[N][2];

void solve() {
	cin >> n;
	_for(i,0,n) cin >> A[i].first, A[i].second = true;
	_for(i,0,n) cin >> A[n+i].first, A[n+i].second = false;
	n *= 2;
	sort(A,A+n);
	d[0][0] = d[0][1] = 0;
	
	_for(i,1,n) {
		d[i][0] = max(d[i-1][0], d[i-1][1]);
		
		if (A[i].second != A[i-1].second) {
			// Diff
			d[i][1] = d[i-1][0]+1;
		} else {
			// Same
			d[i][1]=0;
		}
	}
	cout << max(d[n-1][0], d[n-1][1]);
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


