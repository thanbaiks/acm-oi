#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	
	int a,b;
	cin >> a >> b;
	int q=a/b, r=a%b;
	if (!q) {
		cout << 10;
	} else {
		if (!r) {
			cout << q*10;
		} else if (2*r <= b) {
			cout << q*10+5;
		} else {
			cout << q*10+10;
		}
	}
}


