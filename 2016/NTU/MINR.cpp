#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e5+5;
int T[N*4],A[N],n,m;

void build(int x, int l, int r) {
	if (l==r) {
		T[x] = A[l];
	} else {
		int mid = (l+r)>>1;
		build(x<<1,l,mid);
		build(x<<1|1,mid+1,r);
		T[x] = min(T[x<<1], T[x<<1|1]);
	}
}

int get(int x,int l,int r, int ql, int qr) {
	if (ql>r || qr<l) return INT_MAX;
	if (l>=ql&&r<=qr) return T[x];
	int mid = (l+r)>>1;
	return min(get(x<<1,l,mid,ql,qr), get(x<<1|1,mid+1,r,ql,qr));
}

void solve() {
	cin >> n >> m;
	_for(i,1,n+1) cin >> A[i];
	__(T);
	build(1,1,n);
	int l,r;
	while (m--) {
		cin >> l >> r;
		cout << get(1,1,n,l,r) << '\n';
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


