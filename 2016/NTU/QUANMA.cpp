#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> P;

const int N = 1e3+5;
const int MOVES[8][2] = {{-2,1},{2,-1},{-2,-1},{2,1},{1,2},{1,-2},{-1,2},{-1,-2}};

int D[N][N];
int n,m,x,y,u,v;

bool check(int x,int y) {
	return x>0&&y>0&&x<=n&&y<=m;
}

void solve() {
	cin >> n >> m >> x >> y >> u >> v;
	memset(D,0,sizeof(D));
	D[x][y] = 1;
	queue<P> q;
	q.push(make_pair(x,y));
	while (!q.empty()) {
		x = q.front().first, y = q.front().second; q.pop();
		_for (i,0,8) {
			int xx = x+MOVES[i][0], yy = y+MOVES[i][1];
			if (check(xx,yy) && !D[xx][yy]) {
				D[xx][yy] = D[x][y]+1;
				q.push(make_pair(xx,yy));
			}
		}
	}
	if (!D[u][v]) {
		cout << -1;
	} else {
		cout << D[u][v]-1;
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


