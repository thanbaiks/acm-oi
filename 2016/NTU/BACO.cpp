#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int a,b,x,y,c;
int m[10][10];

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> a >> b >> x >> y;
	
	const int X[4][2] = {{1,-1},{1,1}, {-1,-1}, {-1,1}};
	
	memset(m,0,sizeof(m));
	for (int i=1;i<9;i++) {
		m[a][i] = m[i][b] = 1;
	}
	_for (i,0,4) {
		const int *t = X[i];
//		cout << t[0] << ' ' << t[1] << ' ';
		for (int xx = x+t[0], yy = y + t[1]; xx>0 && xx<9 && yy>0 && yy<9; xx += t[0], yy += t[1])
			m[xx][yy] = 1;
	}
	m[x][y] = 1;
	for (int i=1;i<9;i++) {
		for (int j=1;j<9;j++)
			c += m[i][j];
	}
	cout << c;
}


