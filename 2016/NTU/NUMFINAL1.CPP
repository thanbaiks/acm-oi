#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

char s[10000005];
int solve() {
	cin >> s;
	int a = s[strlen(s)-1]-'0';
	cin >> s;
	int n = strlen(s), b = s[n-1]-'0';
	if (n>1) b += 10*(s[n-2]-'0');
	if (n>2 && !b) b = 100;
	if (!b) {
		return 1;
	} else if (!a) {
		return 0;
	} else {
		b = (b-1)%4+1;
		int t = 1;
		while (b) t = t*a, b--;
		return t%10;
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout << solve();
}


