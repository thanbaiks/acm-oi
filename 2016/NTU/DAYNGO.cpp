#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

bool test(int *a, int n) {
	int d = 0;
	_for(i,0,n) {
		if (a[i])d++; else d--;
		if (d<0) return false;
	}
	return d==0;
}

void solve() {
	int n, A[22], m=0;
	cin >> n;
	_for(i,1<<n,0) {
		_for(j,0,n){
			A[j] = (i>>(n-1-j))&1;
		}
		if (test(A,n)) {
			_for(i,0,n) cout << char(A[i]?'(':')');
			cout << '\n';
			m++;
		}
	}
	cout << m;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
//    ios::sync_with_stdio(0);
//    cin.tie(0);
    solve();
}


