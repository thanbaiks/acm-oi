#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N=1e5+5;
int st[N*2],pos[N];

void solve() {
	int n,m,t=N,k;
	cin >> n >> m;
	__(st);
	_for(i,1,n+1) {
		st[N+i]=i;
		pos[i]=N+i;
	}
	_for(i,0,m) {
		cin >> k;
		st[pos[k]]=0;
		pos[k]=t;
		st[pos[k]]=k;
		t--;
	}
	while (n) {
		if (st[t]) cout << st[t] << ' ', n--;
		t++;
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


