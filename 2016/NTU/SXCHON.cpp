#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int A[333], n;
void solve() {
	cin >> n;
	_for (i,0,n) cin >> A[i];
	_for (i,0,n-1) {
		int k = i+1;
		_for (j,i+1,n) {
			if (A[j] < A[k]) {
				k = j;
			}
		}
		bool sw = false;
		if (A[i]>A[k]) {
			sw = true;
			swap(A[i], A[k]);
		}
		_for (j,0,n) {
			if (j==i || (sw && j==k)) {
				cout << '[' << A[j] << "] ";
			} else {
				cout << A[j] << ' ';
			}
		}
		cout << '\n';
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


