/**
 * Sparse table implement
 *
 **/

#include <cstdio>
#include <algorithm>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e5+5, LOGN = 16;
int n,m,A[N], T[N][LOGN+1];

void build() {
	_for(i,1,n+1) T[i][0] = A[i];
	
	for (int k=1;(1<<k)<=n;k++){
		for (int i=1;i+(1<<k)-1<=n;i++) {
			T[i][k] = min(T[i][k-1], T[i+(1<<(k-1))][k-1]);
		}
	}
}

int get(int l, int r) {
	int k = __lg(r-l+1);
	return min(T[l][k], T[r-(1<<k)+1][k]);
}

void solve() {
	scanf("%d%d", &n, &m);
	_for(i,1,n+1) scanf("%d", A+i);
	build();
	int l,r;
	while (m--) {
		scanf("%d%d", &l, &r);
		printf("%d\n", get(l,r));
	}
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    solve();
}


