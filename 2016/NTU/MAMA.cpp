#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> P;
const int N = 1e5+5;
const LL MOD = 1e9;
P A[N];
int n;

LL BIT1[N], BIT2[N];

void update(LL *BIT, int x, LL val) {
	while (x) {
		BIT[x] += val;
		x -= -x&x;
	}
}

LL get(LL *BIT, int x) {
	LL res = 0;
	while (x<N) {
		res += BIT[x];
		x += -x&x;
	}
	return res;
}

void solve() {
	cin >> n;
	_for (i,0,n) {
		cin >> A[i].first;
		A[i].second = i+1;
	}
	sort(A,A+n);
	LL res = 0;
	__(BIT1); __(BIT2);
	_for(i,0,n) {
		int u = A[i].first, v = A[i].second;
		res = (res+get(BIT1, v)*u - get(BIT2, v))%MOD;
		update(BIT1,v, 1);
		update(BIT2,v, u);
	}
	cout << res;
}

int main(){
	#ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


