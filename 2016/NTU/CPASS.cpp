#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}


const int N = 1e6+6;
const LL BASE=1e9+7;
LL Pow[N], Hash[N];
int n;
char s[N];

inline LL get(int l, int r) {
	return (Hash[r]-Hash[l-1]*Pow[r-l+1]%BASE + BASE<<1)%BASE;
}

void solve() {
	cin >> s;
	n = strlen(s);
	Pow[0] = 1; Hash[0] = 0;
	_for(i,1,n+1) {
		Pow[i] = (Pow[i-1]<<5) % BASE;
		Hash[i] = ((Hash[i-1]<<5) + s[i-1]) % BASE;
	}
	
	_fod (i,n-1,0) {
		if (get(1,i) == get(n-i+1,n)) {
			LL t = get(1,i);
			for (int j=2;j+i-1<n;j++) {
				if (get(j,j+i-1)==t) {
					s[j+i-1] = 0;
					cout << s+j-1;
					return;
				}
			}
		}
	}
	
	cout << "Just a joke";
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


