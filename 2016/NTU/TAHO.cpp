#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> P;

void solve() {
	int h,c;
	cin >> h >> c;
	stack<P> s;
	while (h&&c&&h+c>2){
		if (h>c){
			s.push(make_pair(2,1));
			h-=2;
			c-=1;
		} else {
			s.push(make_pair(1,2));
			h-=1;
			c-=2;
		}
	}
	cout << s.size() << endl;
	while (!s.empty()) {
		cout << s.top().first << ' ' << s.top().second << '\n';
		s.pop();
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


