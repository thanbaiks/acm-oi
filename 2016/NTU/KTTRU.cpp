#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

char s[100005];
int len;

void solve() {
	int q;
	LL a,b;
	cin >> s >> q;
	len = strlen(s);
	_for(i,0,q) {
		cin >> a >> b;
		a--;b--;
		if (s[a%len] == s[b%len]) {
			cout << "Yes\n";
		} else {
			cout << "No\n";
		}
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


