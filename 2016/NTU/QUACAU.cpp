#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int A[101],n;
void solve() {
	cin >> n;
	_for (i,0,n) {
		cin >> A[i];
	}
	sort(A,A+n);
	
	int cost = 0;
	while (n>3) {
		int i1 = A[n-1]+A[n-2]+2*A[0]; // Tung nguoi duoc dua qua boi A[0]
		int i2 = A[0]+A[1]*2+A[n-1]; // A[0] + A[1] Qua, A[0] mang ve, A[n-1] va A[n-2] qua, A[1] ve
		if (i2<i1) {
			n -= 2;
			cost += i2;
		} else {
			cost += A[0]+A[n-1];
			n --;
		}
	}
	if (n==3) {
		cost += A[0] + A[1] + A[2];
	} else if (n==2) {
		cost += max(A[0], A[1]);
	} else {
		cost += A[0];
	}
	cout << cost;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


