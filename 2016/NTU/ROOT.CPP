#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int A[500], cnt;

void init() {
	int LIM = 3200;
	bool b[LIM];
	memset(b,1,sizeof(b));
	cnt = 0;
	_for (i,2,sqrt(LIM)+1) {
		if (b[i]) {
			for (int j=i*i;j<LIM;j+=i) {
				if (b[j]) b[j] = false;
			}
		}
	}
	_for(i,2,LIM) if (b[i]) A[cnt++] = i;
}

void solve() {
	init();
	int a,b,c,n;
	cin >> n;
	while (n--) {
		cin >> b;
		a = 1;
		_for(i,0,cnt) {
			c = A[i]*A[i];
			if (c > b) break;
			while (b%c==0) {
				a *= A[i];
				b /= c;
			}
		}
		cout << a << ' ' << b << '\n';
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


