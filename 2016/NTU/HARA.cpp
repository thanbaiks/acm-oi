#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e7+9;
int A[N], n;

LL solve(int b, int e) {
	while (b<e && A[b]==0) b++;
	if (b==e) return 0;
	while(A[e-1]==0) e--;
	
	int mi = INT_MAX;
	_for (i,b,e) {
		mi = min(mi,A[i]);
	}
	_for (i,b,e) {
		A[i]-= mi;
	}
	LL res = 1;
	int j = b;
	_for (i,b+1,e) {
		if (A[i]==0 && A[i-1]>0) {
			res += solve(j,i);
			j = i+1;
		}
	}
	if (A[e-1]>0) {
		res += solve(j,e);
	}
	return res;
}

LL solve() {
	LL res = 0;
	cin >> n;
	_for(i,0,n) cin >> A[i];
	res = solve(0,n);
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout << solve();
}


