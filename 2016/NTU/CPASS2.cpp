#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const int N = 1e6+6;
char s[N];
int f[N],n;

void solve() {
	cin >> s;
	n = strlen(s);
	f[0] = 0;
	int i=1,j=0,k=0;
	while (i<n){
		if (s[i]==s[j]) {
			f[i++] = ++j;
		} else {
			if (j)
				j = f[j-1];
			else
				f[i++] = 0;
		}
	}
	_for(i,1,n-1) k = max(k,f[i]);
	int t = f[n-1];
	while (t) {
		if (t<=k) {
			s[t] = 0;
			cout << s;
			return;
		}
		t = f[t-1];
	}
	cout << "Just a legend";
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


