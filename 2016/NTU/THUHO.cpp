#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e3+5;
int A[N],n,m;

void solve() {
	cin >> n >> m;
	_for(i,1,n+1) {
		cin >> A[i];
	}
	A[0] = A[n+1] = 0;
	int res = 0, k, t;
	_for (i,1,n+2) {
		k = m;
		if (A[i-1]) {
			t = min(k,A[i-1]);
			res += t;
			k -= t;
		}
		if (k) {
			t = min(k,A[i]);
			res += t;
			A[i] -= t;
		}
	}
	cout << res;
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


