#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

bool test(LL n, LL k) {
	return k*(k+1)/2 < n;
}

LL find(LL n) {
	LL lo=0, hi = sqrt(LLONG_MAX), mi;
	while (lo < hi-1) {
		mi = (lo+hi)>>1;
		if (test(n,mi)) {
			lo = mi;
		} else {
			hi = mi;
		}
	}
	return lo*(lo+1)/2;
}

void solve() {
	LL t,n;
	cin >> t;
	while (t--) {
		cin >> n;
		LL k = find(n);
		cout << (n-k)%10 << '\n';
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


