#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int n,h;
bool used[22];
LL ans = -1;

struct Rb {
	int h,w,p; // height - weight - power
} A[22];


void _(int ch, LL cp) { // cur height - cur power
	if (ch >= h) {
		// found
		ans = max(ans, LL(cp));
		return;
	}
	_for (i,0,n) {
		if (!used[i]) {
			// try i
			used[i] = true;
			_(ch + A[i].h, min(cp-A[i].w, LL(A[i].p)));
			used[i] = false;
		}
	}
}

void solve() {
	cin >> n >> h;
	memset(used, 0, sizeof(used));
	_for (i,0,n) {
		cin >> A[i].h >> A[i].w >> A[i].p;
	}
	_(0, LLONG_MAX);
	// check ans
	if (ans<0) {
		cout << "H is too tall";
	} else {
		cout << ans;
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


