#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int tries(ULL a, ULL b) {
	if (a==b-2) {
		return 1;
	} else if (a>=b-1) {
		return 0;
	} else {
		ULL mid = (a+b)/2;
		return 1 + tries(a, mid);
	}
}

void solve() {
	ULL n,m,t;
	int cnt = 0;
	cin >> n >> m;
	ULL a = 1, b = m+1;
	cnt = tries(a,b);
	if (cnt <= 63) {
		cout << cnt;
	} else {
		cout << "More than 63 trials needed.";
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


