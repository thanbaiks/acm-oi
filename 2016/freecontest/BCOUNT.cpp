#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e5+5;
int n,q, BIT[3][N];

void add(int *a, int x) {
	while (x<N) {
		a[x]++;
		x += -x&x;
	}
}

int get(int *a, int x) {
	int res = 0;
	while (x) {
		res += a[x];
		x -= -x&x;
	}
	return res;
}

void solve() {
    cin >> n >> q;
    memset(BIT,0,sizeof(BIT));
    int t, u, v;
    _for(i,1,n+1) {
    	cin >> t;
    	add(BIT[t-1],i);
	}
	while (q--) {
		cin >> u >> v;
		_for(i,0,3) {
			cout << get(BIT[i], v) - get(BIT[i], u-1) << ' ';
		}
		cout << '\n';
	}
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

