#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef pair<int,int> P;

const int N = 102;
const int W[4][2] = {{1,0},{-1,0},{0,1},{0,-1}};

int n,m;
vector<P> ct[N][N];
bool sang[N][N],vis[N][N],ready[N][N];
LL res = 1;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> n >> m;
	while (m--) {
		int a,b,c,d;
		cin >> a >> b >> c >> d;
		ct[a][b].push_back(make_pair(c,d));
	}
	
	memset(sang, 0, sizeof(sang));
	memset(vis, 0, sizeof(vis));
	memset(ready, 0, sizeof(ready));
	
	sang[1][1] = true;
	ready[1][1] = true;
	
	queue<P> q;
	q.push(make_pair(1,1));
	while (!q.empty()) {
		P p = q.front();
		q.pop();
		if (vis[p.first][p.second]) continue; // ??
		vis[p.first][p.second] = 1;
		// danh dau 4 o xung quanh da san sang
		_for(i,0,4) {
			int x = p.first+W[i][0], y = p.second+W[i][1];
			ready[x][y] = 1;
			if (!vis[x][y] && sang[x][y]) {
				q.push(make_pair(x,y));
			}
		}
		// bat den o hien tai
		for (int i=0;i<ct[p.first][p.second].size();i++) {
			P t = ct[p.first][p.second][i];
			int x = t.first, y = t.second;
			if (!sang[x][y])
				res++;
			sang[x][y] = 1;
			if (!vis[x][y] && ready[x][y]) {
				q.push(make_pair(x,y));
			}
		}
	}
	cout << res;
}


