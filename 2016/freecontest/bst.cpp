#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}



const int N = 100005;
int n,r;
int B[N], BIT[2*N];

void add(int x, int v) {
	while (x<N*2) {
		BIT[x] += v;
		x += -x&x;
	}
}

int get(int x) {
	int res = 0;
	while (x) {
		res += BIT[x];
		x -= -x&x;
	}
	return res;
}

void solve() {
	cin >> n >> r;
	memset(BIT,0,sizeof(BIT));
	_for (i,1,n+1) {
		B[i] = N+i;
		add(N+i, 1);
	}
	int j=N, t;
	while (r--) {
		cin >> t;
		cout << get(B[t]-1) << ' ';
		add(B[t],-1);
		add(j,1);
		B[t] = j;
		j--;
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


