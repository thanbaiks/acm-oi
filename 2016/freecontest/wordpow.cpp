#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,m;
char B[1005][1005];

//LL MOD = 1e9+9;

struct Node {
	Node* child[26];
	bool isEnd, vis;
	int rem;
	
	Node() {
		_for(i,0,26) child[i] = NULL;
		isEnd = false;
		vis = false;
	}
	
	~Node() {
		_for(i,0,26) if (child[i] != NULL) delete child[i];
	}
	
	void reset() {
		vis = false;
		rem = 0;
		_for(i,0,26) if (child[i] != NULL) {
			rem++;
			child[i]->reset();
		}
	}
	
	void push(char *s) {
		char c = *s;
		
		if (c == 0) {
			isEnd = true;
			return;
		}
		
		if (child[c-'a'] == NULL) {
			Node *n = new Node();
			child[c-'a'] = n;
		}
		child[c-'a']->push(s+1);
	}
	
	void check(char c) {
		if (!c || !rem) return;
		_for (i,0,26) if (child[i] && child[i]->vis && child[i]->rem){
			child[i]->check(c);
			if (!child[i]->rem) rem--;
		}
		if (child[c-'a'] && !child[c-'a']->vis){
			child[c-'a']->vis = true;
		}
	}
	
	int get() {
		int res = 0;
		if (isEnd) res ++;
		_for (i,0,26) if (child[i] && child[i]->vis) res += child[i]->get();
		return res;
	}
} root;

void normalize(char *s) {
	int len = strlen(s);
	_for(i,0,len) {
		s[i] = tolower(s[i]);
	}
}

int check(int k) {
	root.reset();
//	root.vis = true;
	_for(i,0,strlen(B[k]))
		root.check(B[k][i]);
	return root.get();
}

void solve() {
	char s[32];
	cin >> n >> m;
	_for (i,0,n) {
		cin >> B[i];
		normalize(B[i]);
	}
	_for (i,0,m) {
		cin >> s;
		normalize(s);
		_for (i,0,n);
		root.push(s);
	}
	_for (i,0,n) {
		cout << check(i) << '\n';
	}
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

