#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 262144+5;
int a[MAX], nxt[MAX], pre[MAX];
int n,t;

int main(){
	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int res = 0, i=1;
	cin >> n;
	_for (i,1,n+1) {
		cin >> a[i];
		nxt[i]=i+1;
		pre[i]=i-1;
		res = max(res, a[i]);
	}
	while (nxt[i] <= n) {
		if (a[i]==a[nxt[i]]) {
			res = max(res,a[i]+1);
			a[i]++;
			nxt[i]=nxt[nxt[i]];
			pre[nxt[i]] = i;
			if (pre[i]) i = pre[i];
		} else {
			i = nxt[i];
		}
	}
	cout << res;
}


