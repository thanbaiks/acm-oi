#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

int n,h;
int ans = -1;

struct Rb {
	int h,w,p; // height - weight - power
	bool used;
	vector<int> v;
} A[22];

bool byW (const Rb &a, const Rb &b) {
	return a.w < b.w;
}

void init() {
	sort(A, A+n, byW);
	_for(i,0,n) {
		_for (j,0,n) {
			if (i==j) continue;
			if (A[i].p >= A[j].w)
				A[i].v += j;
		}
	}
}
void dfs(int i,int hei, int pow) {
	Rb &r = A[i];
	// Check height
	hei += r.h;
	pow = min(pow-r.w, r.p);
	
	if (hei >= h) {
		ans = max(ans, pow);
		return;
	}
	if (r.v.empty()) return;
	
	r.used = true;
	vector<int>::iterator it = upper_bound(r.v.begin(), r.v.end(), pow);
	it--;
	while (it >= r.v.begin()) {
		dfs(*it, hei, pow);
		it--;
	}
	r.used = false;
}

void solve() {
	cin >> n >> h;
	_for (i,0,n) {
		cin >> A[i].h >> A[i].w >> A[i].p;
	}
	init();
	_for(i,0,n)
		dfs(i,0,INT_MAX);
	
	// check ans
	if (ans<0) {
		cout << "H is too tall";
	} else {
		cout << ans;
	}
}

int main(){
	#if NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


