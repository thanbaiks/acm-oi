#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

vector<int> v;

void init() {
	v.push_back(1);
	int lim = 1e9+9;
	int a=1,b=1,c;
	while (1) {
		c = a+b;
		if (c >= lim) {
			break;
		}
		v.push_back(c);
		a = b, b = c;
	}
}

void solve() {
	int t,n;
	cin >> t;
	vector<int>::iterator i1;
	while (t--) {
		cin >> n;
		if (!n) {
			cout << "YES\n";
			continue;
		}
		i1 = lower_bound(_all(v), sqrt(n));
		bool f = false;
		if (i1 != v.end())
			while (i1 >= v.begin()) {
				int i = *i1;
				if (n%i!=0) {
					i1--;
					continue;
				}
				i = n/i;
				if (binary_search(_all(v), i)) {
					f = true;
					break;
				} else {
					i1--;
					continue;
				}
			}
		if (f)
			cout << "YES\n";
		else
			cout << "NO\n";
	}
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    init();
    solve();
}

