#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

const LL MOD = 10000LL;

LL randll() {
	LL res = 0;
	_for (i,0,7) {
		res = (res<<8) | LL(rand())&0xFF;
	}
	return res;
}

void solve() {
	cout << 20 << ' ' << 1000 << '\n';
	_for(i,0,20) {
		cout << 1 << ' ' << 1 << ' '  << 100 << '\n';
	}
}

int main(){
	#if NGOBACH
//    freopen("input.txt","r",stdin);
  freopen("input.txt","w",stdout);
	#endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


