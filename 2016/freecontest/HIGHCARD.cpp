#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e5+5;
int m[MAX], n;

int main(){
	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	memset(m, 0, sizeof(m));
	cin >> n;
	int t;
	_for (i,0,n) {
		cin >> t;
		m[t] = 1;
	}
	LL res = 0;t = 0;
	_for(i,1,2*n+1) {
		if (m[i]) {
			// Goten has i card
			t++;
		}else {
			if (t>0) {
				res++;
			}
			t--;
			if (t<0) t = 0;
		}
	}
	cout << res;
}


