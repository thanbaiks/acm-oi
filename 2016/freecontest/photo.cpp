#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef pair<int,int> P;

const int N = 1e3+5;
P A[1005];
int n,k;

void solve() {
    cin >> n >> k;
    _for (i,0,k){
    	cin >> A[i].first >> A[i].second;
    	if (A[i].first > A[i].second) swap(A[i].first, A[i].second);
	}
    sort(A,A+k);
    int m = -1;
    int cnt = 1;
    _for(i,0,k) {
    	if (m < 0) {
    		// Blank
    		m = A[i].second;
		} else if (A[i].first >= m){
			// New photo
			m = A[i].second;
			cnt++;
		} else {
			m = min(m, A[i].second);
		}
	}
	cout << cnt + 1;
}

int main(){
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    ios::sync_with_stdio(0);
    cin.tie(0);
    solve();
}

