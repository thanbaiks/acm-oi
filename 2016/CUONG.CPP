#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))
#define AUTO(u,v) typeof (v) u = v

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

typedef pair<int,int> P; // Val - Pos
const int N = 1e6+5;
int A[N],D[N],n,c;
P f[N];

bool cmp(const P &a, const P &b) {
    return a.first < b.first;
}

void solve() {
    cin >> n;
    _for(i,0,n) cin >> A[i];
    c = N;
    _fod(i,n-1,-1) {
        if (c==N) {
            f[--c] = make_pair(A[i], i);
            D[i] = -1;
        } else {
            P *p = upper_bound(f+c,f+N, make_pair(A[i]-1, 0), cmp) - 1;
            if (p < f+c) {
                D[i] = -1;
                f[--c] = make_pair(A[i], i);
            } else {
                D[i] = p->second - i - 1;
            }
        }
    }
    _for(i,0,n) cout << D[i] << ' ';
}

int main(){
    #ifdef NGOBACH
    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


