#include <iostream>
#include <cstring>
#include <algorithm>

using namespace std;

int main(){
	char s[100],ss[100], _min[100];
	cin >> s;
	int len = strlen(s);
	memset(_min, 'z', sizeof(_min));
	ss[len] = _min[len] = 0;
	for (int i = 1;i <= len-2;i++) {
		copy(s,s+i, ss);
		reverse(ss, ss+i);
		for (int j=1;j <= len-i-1;j++){
			copy(s+i,s+i+j, ss+i);
			reverse(ss+i, ss+i+j);
			copy(s+i+j,s+len, ss+i+j);
			reverse(ss+i+j, ss+len);
			if (strcmp(ss, _min) < 0) {
				memcpy(_min, ss, sizeof(ss));
			}
		}
	}
	cout << _min;
}
