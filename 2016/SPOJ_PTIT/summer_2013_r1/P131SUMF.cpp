#include <iostream>
#include <limits>
#include <cstring>
#include <set>
 
using namespace std;
typedef long long ll;
const int N = 1003;
int r,c;
ll hash[N];
char str[N][N];
 
int solve() {
	set<ll> s;
	pair<set<ll>::iterator,bool> ret;
	bool suc;
	for (int i=r-1;i>=0;i--) {
		s.clear();
		suc = true;
		for (int j=0;j<c;j++) {
			hash[j] = (hash[j] * 33 + str[i][j]-'a') % 100000007;
			ret = s.insert(hash[j]);
			if (!ret.second){
				suc = false;
				break;
			}
		}
		if (suc) return i;
	}
	return 0;
}
 
int main() {
	cin >> r >> c;gets(str[0]);
	for (int i=0;i<r;i++) gets(str[i]);
	memset(hash,0,sizeof(hash));
	fill(hash,hash+c, 5381);
	/** CALC **/
	cout << solve();
} 
