#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	vector<string> vs;
	vector<string>::iterator it;
	string s;
	int n;
	
	cin >> n;
	while (n--) {
		cin >> s;
		if (s == "cd") {
			string t;
			cin >> t;
			if (t[0] == '/') {
				// truncate path
				vs.clear();
				t = t.substr(1,t.size()-1);
//				cout << "Paht: " << t << endl;
			}
			char buff[1000];
			int j = 0, len = t.size();
			for (int i=0;i<=len;i++) {
				if (i == len || t[i] == '/') {
					// flush
					buff[j] = 0;
					string tmp = string(buff);
					if (tmp == "..") {
						vs.pop_back();
					} else if(tmp.size()) {
						vs.push_back(tmp);
					}
					j = 0;
				} else {
					buff[j++] = t[i];
				}
			}
		}else if (s == "pwd") {
			cout << '/';
			for (it=vs.begin();it!=vs.end();++it)
				cout << *it << '/';
			cout << endl;
		}
	}
}


