#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int main(){
//	freopen("A.txt","r",stdin);
	int m[5],w[5],h[2],x[5] = {500,1000,1500,2000,2500};
	_for(i,0,5) cin >> m[i];
	_for(i,0,5) cin >> w[i];
	cin >> h[0] >> h[1];
	LL res = 0;
	_for (i,0,5){
		res += max(x[i]*3/10,x[i]-m[i]*x[i]/250-50*w[i]);
	}
	cout << res + h[0]*100-h[1]*50;
    return 0;
}

