#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
const int MAX = 1e5+5;
LL n,a[MAX],res=1;

void try_(int s){
	LL c = 1;
	_for(i,1,n){
		if ((a[i]-a[i-1])*s>=0){
			c++;
			res=max(res,c);
		}else{
			c=1;
		}
	}
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n;
    _for(i,0,n){
    	cin >> a[i];
	}
	try_(1);
	try_(-1);
	cout << res;
    return 0;
}

