#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator

using namespace std;
typedef pair<double,double> PDD;

int t,m,n;
vector<PDD> v;
double es,p,q;

bool cmp (const PDD &a,const PDD &b){
	//return a.first < b.first || a.first == b.first && (1-a.first-a.second) > (1-b.first-b.second);
	return a.first/a.second > b.first/b.second;
}
void try_(int i,double prob,int m){
	es += prob*v[i].first;
	if (v[i].first + v[i].second < 1.0 && i < v.size()-1){
		try_(i+1,prob*(1.0-v[i].first-v[i].second),m);
	}
	if (m>0 && v[i].second>0){
		try_(i+1,prob*(v[i].second),m-1);
	}
}
int main(){
	freopen("passage.txt","r",stdin);
	cin >> t;
	for (int tt=0;tt<t;tt++){
		cin >> n >> m;
		v.clear();
		es = 0;
		for (int i=0;i<n;i++){
			cin >> p >> q;
			v.push_back(make_pair(p,q));
		}
		sort(v.begin(),v.end(),cmp);
		/*
		for (int i=0;i<n;i++){
			cout << v[i].first << ' ' << v[i].second << endl;
		}
		*/
		try_(0,1,m);
		cout << es << endl;
	}
	return 0;
}

