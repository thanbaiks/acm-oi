#include <iostream>
#include <fstream>
#include <cstring>

#define inp fin
#define MAX 100000000
#define ui32 unsigned long

using namespace std;
int test,n;
ui32 m,t,v[15],mx;
void try_(int i,ui32 sum){
	ui32 sum2 = sum + v[i];
	if (sum2 > m && sum2 < mx){
		mx = sum2;
	}
	if (i < n-1){
		if (sum<m)
			try_(i+1,sum);
		if (sum2<m)
			try_(i+1,sum2);
	}
}
int main(){
	ifstream fin("18D.inp");
	inp >> test;
	for (int ti=1;ti<=test;ti++){
		// input
		inp >> n >> m;
		for (int i=0;i<n;i++){
			inp >> v[i];
		}
		mx=MAX;
		try_(0,0);
		// output
		if (mx<MAX){
			cout << "Case #" << ti << ": " << mx << endl;
		}else{
			cout << "Case #" << ti << ": " << -1 << endl;
		}
	}
	return 0;
}
