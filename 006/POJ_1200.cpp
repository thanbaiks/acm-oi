#include <iostream>
#include <fstream>
#include <cstring>
#define  MAX_SETS 16000000
#define inp fin

using namespace std;
typedef unsigned long long ULL;

bool mk[MAX_SETS];
char val[256];
ULL n,nc,test;
char str[1000000];

char getCharVal(char c){
	for (int i=0;i<nc;i++){
		if (val[i]==c){
			return i;
		}else if(val[i]==0){
			val[i]=c;
			return i;
		}
	}
}

int main(){
	ifstream fin("B.txt");
	while(inp >> n >> nc){
		inp >> str;
		memset(mk,0,sizeof(mk));
		memset(val,0,sizeof(val));
		ULL len = strlen(str);
		if (n>len || !nc){
			cout << 0 << endl << endl;
			continue;
		}
		ULL count = 0;
		ULL tmp = 0;
		ULL base = 1;
		for (int i=1;i<n;i++){
			base *= nc;
		}
		// build first set
		for (int i=0;i<n;i++){
			tmp = tmp*nc + getCharVal(str[i]);
		}
		mk[tmp]=true;
		count++;
		for (int i=n;i<len;i++){
			tmp = (tmp - base*getCharVal(str[i-n]))*nc+getCharVal(str[i]);
			if (!mk[tmp]){
				count++;
				mk[tmp]=true;
			}
		}
		cout << count << endl << endl;
	}
	return 0;
}
