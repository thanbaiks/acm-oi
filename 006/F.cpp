#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
int n;
char mat[102][102];
long long arr[102],mx,x;

long long maxSub()
{
   int max_so_far = 0, max_ending_here = 0;
   int i;
   for(i = 0; i < n; i++)
   {
     max_ending_here = max_ending_here + arr[i];
     if(max_ending_here < 0)
        max_ending_here = 0;
     if(max_so_far < max_ending_here)
        max_so_far = max_ending_here;
    }
    return max_so_far;
} 

int main(){
	ifstream fin("f.txt");
	inp >> n;
	for (int i=0;i<n;i++){
		for (int j=0;j<n;j++)
			inp >> x,mat[i][j]=x;
	}
	// dump mat
	
	for (int st=0;st<n;st++){
		memset(arr,0,sizeof(arr));
		for (int line=st;line<n;line++){
			for (int i=0;i<n;i++){
				arr[i]+=mat[line][i];
			}
			mx = max(mx,maxSub());
		}
	}
	cout << mx;
	return 0;
}
