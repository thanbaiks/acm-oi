#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#define  MAX_SETS 16000000
#define inp cin

using namespace std;
typedef unsigned long long ULL;

bool mk[MAX_SETS];
char val[257];
ULL n,nc,test;
string str;


int main(){
	ifstream fin("B.txt");
	inp >> test;
	for (ULL ti=0;ti<test;ti++){
		inp >> n >> nc;
		inp.ignore();
		getline(inp,str);
		memset(mk,0,sizeof(mk));
		memset(val,0,sizeof(val));
		ULL len = str.length();
		if (n>len){
			cout << 0;
		}else{
			ULL count = 0;
			ULL tmp = 0;
			// build character value
			for (int i=0;i<len;i++)
				if (!val[str[i]]){
					val[str[i]]=count++;
					if (count == nc)
						break;
				}
			count = 0;
			for (int i=0;i<=len-n;i++){
				// get hash
				tmp=0;
				for (int j=0;j<n;j++){
					tmp = tmp*nc + val[str[i+j]];
				}
				if (!mk[tmp]){
					mk[tmp]=true;
					count++;
				}
			}
			cout << count;
		}
		if (ti<test-1)
			cout << endl;
		else
			cout << endl << endl;
	}
	return 0;
}
