#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

#define inp cin
using namespace std;
typedef unsigned long long ULL;
ULL n,w,h,s,t;
bool map[500][500];
int arr[500];
int main(){
	ifstream fin("A.txt");
	while (true){
		inp >> n;
		if (!n)
			break;
		inp >> w >> h;// size of the map
		memset(map,0,sizeof(map));
		memset(arr,0,sizeof(arr));
		int x,y;
		for (ULL i=0;i<n;i++){
			inp >> x >> y;
			map[x-1][y-1]=true;
		}
		inp >> t >> s;//height and width of my field
		// prog
		ULL mx = 0;
		for (int i=0;i<h;i++){// hang
			ULL sum = 0;
			for (int j=0;j<w;j++){ // cot
				if(map[j][i])
					arr[j]++;
				if (i>=s && map[j][i-s]){
					arr[j]--;
				}
				sum += arr[j];
				if (j>=t){
					sum -= arr[j-t];
				}
				mx = max(mx,sum);
			}
		}
		cout << mx << endl;
	}
	return 0;
}
