#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
#include <algorithm>
#define inp fin

using namespace std;
int n,x,mat_count;
bool mat[201][201];
int arr[201];

int main(){
	ifstream fin("G.txt");
	inp >> n;
	memset(mat,0,sizeof(mat));
	memset(arr,0,sizeof(arr));
	for (int i=1;i<=n;i++){
		mat[i][i]=true;
		while (true) {
			inp >> x;
			if (!x)
				break;
			mat[i][x]=true;// i -> x
		}
	}
	mat_count = 0;
	for (int i=1;i<=n;i++){
		if (!arr[i]){
			arr[i]=++mat_count;
		}
		for (int j=i+i;j<=n;j++){
			if (mat[i][j]||mat[j][i])
				arr[j]=arr[i];
		}
	}
	// floyd
	for (int k=1;k<=n;k++){
		for (int i=1;i<=n;i++){
			for (int j=1;j<=n;j++){
				if (mat[i][k]&&mat[k][j])
					mat[i][j]=true;
			}
		}
	}
	cout << mat_count << endl;
	return 0;
}
