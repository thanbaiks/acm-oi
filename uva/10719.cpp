#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;

LL k,n,arr[100001];
string s;
int main(){
	//freopen("input.txt","r",stdin);
	//freopen("output.txt","w",stdout);
	while (cin >> k){
		cin.ignore();
		getline(cin,s);
		FOR (i,0,s.length()){
			//cout << s[i] << ": " << (int)s[i] << endl;
			if (s[i]==-106)
				s[i] = 45;
		}
		stringstream ss(s);
		n = 0;
		while (ss >> arr[n])
			++n;
		arr[n]=arr[n+1]=0;
		FOR (i,0,n){
			arr[i+1]+=arr[i]*k;
		}
		cout << "q(x): ";
		FOR (i,0,n-1){
			cout << arr[i];
			if (i<n-2)
				cout << ' ';
		}
		cout << endl << "r = " << arr[n-1] << endl << endl;
	}
	return 0;
}

