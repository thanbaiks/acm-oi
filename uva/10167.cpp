#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;
int n;
int a,b;
vector<double> vd;

int main(){
	//freopen("input.txt","r",stdin);
	
	
	while (cin >> n && n){
		FOR (i,0,2*n){
			cin >> a >> b;
			double d = atan2(b,a)*180.0/M_PI;
			if (d<0)
				d+= 360;
			vd.push_back(d);
		}
		sort(vd.begin(),vd.end());
		FOR(i,0,n){
			if (vd[i+n]-vd[i]>180){
				// print result
				double d = vd[i+n]+1;
				d = tan(d*M_PI/180);
				int a,b;
				if (d>1){
					a = 500;
					b = 500/d;
				}else{
					b = 500;
					a = 500*d;
				}
				cout << a << ' ' << b << endl;
				break;
			}
		}
	}
	return 0;
}

