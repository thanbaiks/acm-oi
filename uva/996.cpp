#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int i=a;i<b;i++)

using namespace std;
typedef long long LL;
LL a,b;
int main(){
	freopen("input.txt","r",stdin);
	while (cin >> a >> b){
		cout << abs(b - a) << endl;
	}
	return 0;
}

