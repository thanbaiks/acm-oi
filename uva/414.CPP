#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;
int n,x,sum,minn;
int main(){
	freopen("input.txt","r",stdin);
	char c;
	while (cin >> n && n > 0){
		cin.ignore();
		sum=0;
		minn = 25;
		FOR (i,0,n){
			x=0;
			FOR (j,0,25){
				c = cin.get();
				if (c==' ')
					x++;
			}
			cin.ignore();
			sum+=x;
			minn = min(minn,x);
		}
		sum -= minn*n;
		cout << sum << endl;
	}
	return 0;
}

