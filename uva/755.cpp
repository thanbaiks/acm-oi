#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <iomanip>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)
#define MAXN 10000007
using namespace std;
typedef long long LL;
int t,n;
string s;
int a[MAXN];
int charmap[128];

void pre(){
	fill(charmap,charmap+128,-1);
	char s[][5] = {"0","1","ABC2","DEF3","GHI4","JKL5","MNO6","PRS7","TUV8","WXY9"};
	FOR (i,0,10)
	FOR (j,0,strlen(s[i]))
		charmap[s[i][j]]=i;
}
int parse(string s){
	int rs = 0;
	FOR(i,0,s.length()){
		if (charmap[s[i]]>=0){
			rs = rs*10+charmap[s[i]];
		}
	}
	return rs;
}
int main(){
	//freopen("input.txt","r",stdin);
	cin >> t;
	pre();
	
	FOR (tt,0,t){
		cin >> n;
		memset(a,0,sizeof(a));
		FOR (i,0,n){
			cin>>s;
			a[parse(s)]++;
		}
		bool f = false;
		cout << setfill('0') ;
		FOR (i,0,MAXN){
			if (a[i]>1){
				cout << setw(3)<<i/10000 << '-'  << setw(4)<<i %10000 << ' ' <<a[i] << endl;
				f = true;
			}
		}
		if (!f)
			cout << "No duplicates." << endl;
		if (tt < t-1)
			cout << endl;
	}
	return 0;
}

