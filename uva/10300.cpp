#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;
int test,nf,a,b,c,rs;

int main(){
	//freopen("input.txt","r",stdin);
	cin >> test;
	FOR (tt,0,test){
		cin >> nf;
		rs = 0;
		FOR (i,0,nf){
			cin >> a >> b >> c;
			rs += a*c;
		}
		cout << rs << endl;
	}
	return 0;
}

