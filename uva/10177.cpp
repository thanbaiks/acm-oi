#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;
int n;
LL a[3],b[3];
LL ex(LL a,LL b){
	LL rs = 1;
	while (b--)
		rs *= a;
	return rs;
}
LL sqr(LL x){
	return x*x;
}

LL calc(LL size,LL dim){
	LL rs = 0;
	FOR (i,0,size){
		rs += ex(size-i,dim);
	}
	return rs;
}

int main(){
	while (cin >> n){
		FOR (i,0,3){
			a[i] = calc(n,i+2);
			b[i] = ex(n*(n+1)/2,i+2)-a[i];
		}
		FOR (i,0,3){
			cout << a[i] << ' ' << b[i];
			if (i<2)
				cout << ' ';
		}
		cout << endl;
	}
	return 0;
}

