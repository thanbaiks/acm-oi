#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)
#define MAX 1000001

using namespace std;
typedef long long LL;
vector<int> primes;
int F[MAX];
int p,q;

void pre(){
	memset(F,0,sizeof(F));
	FOR(i,2,MAX){
		if (!F[i]){
			primes.push_back(i);
			F[i]=i;
		}
		for (int j=0;j<primes.size() && primes[j]<=F[i] && i*primes[j] < MAX;j++){
			F[i*primes[j]]=primes[j];
		}
	}
}
int gcd(int a,int b){
	return b?a:gcd(b,a%b);
}
int test(int x){
	if (x==1)
		return 1;
	int t = 0;
	vector<int> v;
	for (int i=0;i<primes.size() && x>1;){
		if (x%primes[i]==0){
			// chia het
			if (t != primes[i]){
				v.push_back(1);
			}else{
				v[v.size()-1]++;
			}
			t = primes[i];
			x/=primes[i];
		}else
			i++;
	}
	if (x > 1){
		cout << "Error , remain : " << x <<endl;
	}
	while (v.size() > 2){
		x = gcd(v[0],v[1]);
		v.erase(v.begin());
		v.erase(v.begin());
		v.push_back(x);
	}
	return v[0];
}
int main(){
	pre();
	//freopen("input.txt","r",stdin);
	while (cin >> p >> q && p && q){
		if (p==1){
			cout << "0 1" << endl;
			continue;
		}
		int t = test(p);
		if (q != 1){
			t = min(t,test(q));
		}
		//cout << t << endl;
		int n = round(pow(p,1.0/t))-1;
		//cout << t << ' ' << n << endl;
		//cout << test(p) << test(q) << endl;
		// idle mouse
		LL tt = 1;
		LL sum = 0;
		FOR (i,0,t){
			sum+=tt;
			tt*=n;
		}
		LL sumH = 0;
		tt = 1;
		while (p > 0){
			sumH += tt * p;
			p /= n+1;
			tt *= n;
		}
		cout << sum << ' ' << sumH << endl;
	}
	return 0;
}

