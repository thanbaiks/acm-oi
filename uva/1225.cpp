#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)
#define MAXN 10001
using namespace std;
typedef long long LL;

LL cnt[MAXN][10];
int n,t;

void proc(int i){
	int x = i;
	FOR (j,0,10){
		cnt[i][j]+=cnt[i-1][j];
	}
	while (x>0){
		cnt[i][x%10]++;
		x /= 10;
	}
	
}
int main(){
	//freopen("input.txt","r",stdin);
	memset(cnt,0,sizeof(cnt));
	FOR (i,1,10001){
		proc(i);
	}
	cin >> n;
	FOR(i,0,n){
		cin >> t;
		FOR (j,0,10){
			cout << cnt[t][j];
			if (j<9) cout << ' ';
		}
		cout << endl;
	}
	return 0;
}

