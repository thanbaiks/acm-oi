#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;
typedef pair<int,int> II;
typedef vector<II> VII;

int n,a,b;
II p[8];
double dt[9][9],minn;
bool mask[8];
double sqr(double x){
	return x*x;
}
double dis(int i,int j){
	return sqrt(sqr(p[i].first-p[j].first)+sqr(p[i].second-p[j].second));
}
double try_()
int main(){
	freopen("input.txt","r",stdin);
	while (scanf("%d",&n) && n > 0){
		FOR (i,0,n){
			scanf("%d%d",&a,&b);
			p[i] = make_pair(a,b);
		}
		memset(dt,0,sizeof(dt));
		FOR (i,0,n){
			FOR (j,0,n){
				if (i==j)
					continue;
				dt[i][j] = dt[j][i] = dis(i,j);
			}
		}
		memset(mask,0,sizeof(mask));
		minn = 9999999999;
		FOR (i,0,8){
			minn = min(try_(i),min);
			queue<int> q;
		}
		cout << min << endl;
	}
	return 0;
}

