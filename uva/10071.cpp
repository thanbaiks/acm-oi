#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;

int v,t;
int main(){
	while (cin >> v >> t){
		cout << 2 * v * t << endl;
	}
	return 0;
}

