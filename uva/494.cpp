#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;
char s[1000];
int main(){
	//freopen("input.txt","r",stdin);
	while (cin.getline(s,1000)){
		bool counting = false;
		int count = 0;
		FOR(i,0,strlen(s)){
			if (tolower(s[i]) >= 'a' && tolower(s[i]) <= 'z'){
				counting = true;
			}else if (counting){
				count++;
				counting = false;
			}
		}
		if (counting)
			count++;
		cout << count << endl;
	}
	return 0;
}

