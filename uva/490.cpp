#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)

using namespace std;
typedef long long LL;
vector<string> vs;
string s;
int n;
char charAt(int r,int c){
	if (r < 0 || r >= vs.size())
		return ' ';
		
	if (c < 0 || c >= vs[r].length()){
		return ' ';
	}
	return vs[r][c];
}

int main(){
	//freopen("input.txt","r",stdin);
	while (getline(cin,s)){
		vs.push_back(s);
		n = max(n,(int)s.length());
	}
	FOR (i,0,n){
		FOR (j,0,vs.size()){
			cout << charAt(vs.size()-j-1,i);
		}
		cout << endl;
	}
	return 0;
}

