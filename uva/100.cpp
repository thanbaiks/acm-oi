#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)
#define MAXN 1000001
using namespace std;
typedef long long LL;
LL a[MAXN*10];
int calc(int i){
	if (a[i])
		return i;
	if (i==1)
		return 1;
	return calc(i%2==0?i/2:3*i+1);
}
int main(){
	memset(a,0,sizeof(a));
	FOR (i,0,MAXN-1){
		if (!a[i]){
			a[i] = calc(i);
		}
	}
	return 0;
}

