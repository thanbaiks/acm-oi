#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define MOD 1000000007
#define ll long long
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;

ll modPow(ll a, ll x, ll p) {
	// return a^x mod p
	ll res = 1;
	while(x>0) {
	if(x & 1) res = (res*a) % p;
	a=(a*a)%p;
	x >>= 1;
	}

	return res;
}

ll modInverse(ll a, ll p) {
	// return modular multiplicative of: a mod p, assuming p is prime
	return modPow(a, p-2, p);
}

ll modBinomial(ll n, ll k, ll p) {
	// return C(n,k) mod p, assuming p is prime
	ll numerator = 1; // n*(n-1)* ... * (n-k+1)
	int i;
	for(i=0;i<k;i++) numerator = (numerator*(n-i))%p;

	ll denominator = 1; // k!
	for(i=1;i<=k;i++) denominator = (denominator*i) %p;

	ll res = modInverse(denominator,p);
	res = (res*numerator)%p;
	return res;
}

LL f(float x){
	if (x < 0)
		return 0;
	else if (x < 1)
		return 1;
	return f(x-1)+ f(x-3.14);
}
ULL f2(float x){
	if (x < 0)
		return 0;
	else if (x < 1)
		return 1;
	ULL cnt = 0;
	int m,n;
	m = x / 3.14;
	do {
		n = (int)(x-(m*3.14));
		cnt = (cnt + modBinomial(n+m,n,MOD))%MOD;
		m--;
		
	} while (m >= 0);
}

int main(){
	float x;
	int n;
	cin >> n;
	for (int i=0;i<n;i++){
		cin >> x;
		cout << f2(x);
	}
	return 0;
}

