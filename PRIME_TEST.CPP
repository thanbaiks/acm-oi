#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

LL power(LL x,LL e, LL m){
	LL tmp = 1;
	while (e>1){
		if (e%2) tmp= (tmp*x)%m;
		x = (x*x)%m;
		e >>= 1;
	}
	return (tmp*x)%m;
}

const int rab[6] = {3,5,7,11,13,17};
bool test(LL n){
	if (n==2) return true;
	if (n<2||n%2==0)
		return false;
	int m=n-1,s=0;
	while (m%2==0)
		s++,m/=2;
	_for(i,0,6) {
		int k = rab[i];
		if (n==k) return true;
		LL b = power(k,m,n);
		if (b==1) continue;
		bool f = false;
		_for(j,0,s){
			if (b==n-1){
				f = true;
				break;
			}
			b = (b*b)%n;
		}
		if (!f)
			return false;
	}
	return true;
}

LL n;
int main(){
//	freopen("input.txt","r",stdin);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n;
//    cout << power(2,n,1e9+7) << endl;
    if (test(n))
    	cout << n << " is prime!";
    else
    	cout << n << " is not prime!";
    return 0;
}

