#include <iostream>
using namespace std;
typedef long long LL;
LL n,x,t,y;

int main() {
	cin >> n;
	while (n--){
		cin >> x;
		int y = 1;
		while (x>=10){
			if (x%10>=5)
				x=x/10+1;
			else
				x=x/10;
			y*=10;
		}
		cout << x*y;
		if (n)
			cout << endl;
	}
}

