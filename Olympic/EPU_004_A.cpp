#include <iostream>
#include <cstring>
#include <fstream>
#include <vector>
#define ULL unsigned long long
#define inp cin
using namespace std;
ULL n,l,a,b;
int kq;
bool map[200][200];
char color[200];
vector<int> vi;
void bfs(){
	int size = vi.size(); // lay so luong
	if (size==0)
		return;
	char current = color[vi[0]];// o mau` hien tai
	//cout << "Current color of " << vi[0] << " is " << (int)current << endl;
	char next = current%2+1;
	for (int i=0;i<size;i++){
		int x = vi[i];
		for (int j=0;j<n;j++)// duyet tat ca cac canh ke ben x
			if (x != j && map[x][j]){
				// canh j ke ben x
				// so sanh mau cua x va j
				if (color[j]==0){
					// chua co mau
					color[j]=next;
					//cout << "assign color " << (int)next << " for " << j << " current: " << (int)current << endl;
					vi.push_back(j);
				}else if (color[j]==current){
					// co loi, ko the to mau
					kq = -1;
					//cout << "Node " << x << ":" << (int)color[x]<< " & " << j  << ":" << (int)color[j]<< " conflick" << endl;
					return;
				}
			}
	}
	vi.erase(vi.begin(),vi.begin()+size);
	bfs();
}
int main(){
	ifstream fin("EPU_004_A.inp");
	while (true){
		inp >> n >> l;
		if (!n)
			break;
		memset(map,0,sizeof(map));
		memset(color,0,sizeof(color));
		for (int i=0;i<l;i++){
			inp >> a >> b;
			map[a][b]=map[b][a]=true;
		}
		vi.clear();
		vi.push_back(0);
		color[0]=1;
		kq=0;
		bfs();
		if (kq==-1)
			cout << "NOT BICOLORABLE." << endl;
		else
			cout << "BICOLORABLE." << endl;
	}
	return 0;
}
