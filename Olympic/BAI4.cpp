#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <map>
#include <algorithm>
#define inp fin
#define oup fout

using namespace std;
typedef unsigned long long ULL;
map<string,ULL> us;
int n;
ULL mx;

long toTime(char time[7]){
	long res = 0;
	res += (time[0]-'0')*10+time[1]-'0';
	res = res*60 + (time[2]-'0')*10+time[3]-'0';
	res = res*60 + (time[4]-'0')*10+time[5]-'0';
	return res;
}
long timespan(char time1[7],char time2[7]){
	return toTime(time2)-toTime(time1);
}

int main(){
	char from[11],to[11],time1[7],time2[7];
	ifstream fin("XYZ.INP");
	ofstream fout("XYZ.OUT");
	inp >> n;
	mx=0;
	for (int i=0;i<n;i++){
		inp >> from >> to >> time1 >> time2;
		long len = timespan(time1,time2);
		us[from]+=2*len;
		us[to]+=len;
		mx=max(mx,max(us[from],us[to]));
	}
	oup << mx;
	fin.close();
	fout.close();
	return 0;
}
