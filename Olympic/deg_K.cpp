#include <iostream>
using namespace std;
long deg(int k){
	long ret = 1;
	for(int i=2;i<k;i++){
		if (k%i==0)ret++;
	}
	return ret;
}
int main(){
	int test;
	long n,m;
	cin >> test;
	for (int t=0;t<test;t++){
		cin >> n;
		m = deg(n);
		while (m!=deg(++n));
		cout << "Case #" << t+1 << ": " << n << endl; 
	}
	return 0;
}
