#include <iostream>
using namespace std;
int sum(long l){
	int temp = 0;
	while (l > 0){
		temp += l%10;
		l/=10;
	}
	return temp;
}

int main(){
	int n,i,count[3];
	long l;
	for (i = 0;i<3;i++)
		count[i] = 0;
	cin >> n;
	
	while (n--){
		cin >> l;
		i = sum(l);
		count[i%3]++;
	}
	n = count[0]/2;
	n += count[1] < count[2]?count[1]:count[2];
	cout << n;
}
