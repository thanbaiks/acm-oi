#include <iostream>
#include <fstream>
#include <algorithm>
#define inp cin
#define ULL unsigned long long
#define INF 1000000

using namespace std;


struct Db{
	double d;
	int id;
};

ULL test,n,s,t,sum;
bool selected[1000];
Db d[1000];


inline bool operator< (const Db &a,const Db &b){ return a.d > b.d;}
int main(){
	ifstream fin("EPU_002_B.inp");
	inp >> test;
	for (ULL ti=1;ti<=test;ti++){
		// input
		inp >> n;
		sum=0;
		for (int i=0;i<n;i++){
			inp >> t>> s;
			Db db = {s/t,i};
			d[i] = db;
		}
		// output
		sort(d,d+n);
		for (int i=0;i<n;i++){
			// output
			cout << d[i].id + 1;
			if (i==n-1)
				cout << endl;
			else
				cout << " ";
		}
	}
}
