#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;

void sort_descend(long a[],int indices[],int n){
	long tmp;
	for (int i=0;i<n-1;i++)
		for (int j=n-1;j>i;j--)
			if (a[i]<a[j])tmp=a[i],a[i]=a[j],a[j]=tmp,tmp=indices[i],indices[i]=indices[j],indices[j]=tmp;
}

int main(){		
	int test,n;
	long	a[1000],b[1000],c[1000];
	unsigned long long max;
	int 	cs[3][1000];
	cin >> test;
	for (int t=0;t<test;t++){
		cin >> n;
		for (int i=0;i<n;i++){
			cin >> a[i] >> b[i] >> c[i];
			cs[0][i]=cs[1][i]=cs[2][i]=i;
		}
		sort_descend(a,cs[0],n);sort_descend(b,cs[1],n);sort_descend(c,cs[2],n);
		max=0;
		for (int i1=0;i1<3;i1++)
		for (int i2=0;i2<3;i2++)
		for (int i3=0;i3<3;i3++)
			if (cs[0][i1]!=cs[1][i2] && cs[0][i1]!=cs[2][i3] && cs[1][i2]!=cs[2][i3] && (a[i1]+b[i2]+c[i3] > max))
				max = a[i1]+b[i2]+c[i3];
		cout << "Case #" << t+1 << ": " << max << endl;
	}
	return 0;
}

