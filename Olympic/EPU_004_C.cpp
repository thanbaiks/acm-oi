#include <iostream>
#define ULL unsigned long long
using namespace std;
ULL n,sum;
int main(){
	while (cin >> n){
		sum = n%2==0?(n+1)*(n/2):((n+1)/2)*n;
		cout << sum << endl << endl;
	}
	return 0;
}
