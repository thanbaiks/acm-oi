#include <iostream>
#include <cstring>
using namespace std;
int main(){
	int deg[10000],test,t;
	long n,m,mi,odd,i,a,b;
	bool f;
	cin >> test;
	for (t=0;t<test;t++){
		cin >> n >> m;
		memset(deg,0,sizeof(deg));
		f=true;
		odd=0;
		for (int mi=0;mi<m;mi++){
			cin >> a >> b;
			deg[a-1]++;
			deg[b-1]++;
		}
		for (i=0;i<n;i++)
			if (deg[i]<2){
				f=false;
				break;
			}else if (deg[i]%2!=0){
				odd++;
			}
		
		if (f && odd%2==0)
			cout << "Case #" << t+1 << ": YES" << endl;
		else
			cout << "Case #" << t+1 << ": NO" << endl;
	}
	return 0;
}
