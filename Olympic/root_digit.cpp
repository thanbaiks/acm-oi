#include <iostream>
#include <cstring>
using namespace std;
int tongchuso(int n){
	int tmp=0;
	while (n!=0)
		tmp+=n%10,n/=10;
	if (tmp>=10)
		return tongchuso(tmp);
	return tmp;
}
int main(){
	int test,ti,i;
	long sum;
	char s[10000][101];
	cin >> test;
	for (ti=0;ti<test;ti++)
		cin >> s[ti];
	for (ti=0;ti<test;ti++){
		sum=0;
		for (i=0;i<strlen(s[ti]);i++){
			sum+=s[ti][i]-'0';
		}
		sum = tongchuso(sum);
		cout << "Case #" << ti+1 <<": " << sum << endl;
	}
	return 0;
}
