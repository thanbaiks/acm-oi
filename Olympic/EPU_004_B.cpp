#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>
#define inp fin
#define MAX 99999999999
#define ULL unsigned long long
using namespace std;
struct Hole{
	long long x,y,z,r;
};

ULL n,count;
Hole holes[203];
long double distances[203][203];
long double d[203];
bool selected[203];

long long sqr(long long x){
	return x*x;
}
long double calc(Hole &a,Hole &b){
	long double tmp = sqrt(sqr(a.x-b.x)+sqr(a.y-b.y)+sqr(a.z-b.z));
	tmp-=a.r+b.r;
	if (tmp > 0){
		return tmp;
	}else{
		return 0;
	}
}
int main(){
	ifstream fin("EPU_004_B.inp");
	count = 1;
	while (true){
		inp >> n;
		if (n==-1)
			break;
		for (int i=1;i<=n;i++){
			inp >> holes[i].x >> holes[i].y >> holes[i].z >> holes[i].r;
		}
		inp >> holes[0].x >> holes[0].y >> holes[0].z;
		holes[0].r = 0;
		n+=2;
		inp >> holes[n-1].x >> holes[n-1].y >> holes[n-1].z;
		holes[n-1].r = 0;
		memset(selected,0,sizeof(selected));
		//cout << "Last point " << holes[n-1].x << " " << holes[n-1].y << ' ' << holes[n-1].y << ' ' << holes[n-1].r << endl;
		// calc distances
		for (int i=0;i<n-1;i++)
			for (int j=i;j<n;j++){
				distances[i][j]=distances[j][i]=calc(holes[i],holes[j]);
				//cout << "Time from " <<i << " to " << j << " is: " << distances[i][j] << endl;
			}
		// distra
		selected[0] = true;
		d[0]=0;
		for (int i=1;i<n;i++)
			d[i]=distances[0][i];
		for (int i=0;i<n-1;i++){
			long double mn = MAX;
			int sl=-1;
			for (int j=0;j<n;j++)
				if (!selected[j] && d[j]<mn)
					mn=d[j],sl=j;
			selected[sl]=true;
			for (int j=0;j<n;j++)
				if (!selected[j])
					d[j]=min(d[j],d[sl]+distances[sl][j]);
		}
		cout << "Cheese " << count++ << ": Travel time = " << round(d[n-1])*10 << " sec" << endl;
	}
	return 0;
}
