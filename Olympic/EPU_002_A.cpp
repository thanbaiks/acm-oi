#include <iostream>
#include <fstream>
#define inp cin
#define ULL unsigned long long
#define INF 20001
using namespace std;
int test;
ULL dist[500][500],d[500],roads[500],pre[500],m,n,a,b,w,t;
bool selected[500];
int main(){
	ifstream fin("EPU_002_A.inp");
	inp >> test;
	for (int ti=1;ti<=test;ti++){
		inp >> n >> m;
		// reset data
		for (int i=0;i<n;i++){
			for (int j=0;j<n;j++){
				dist[i][j]= INF;
			}
			dist[i][i] = 0;
			d[i]=INF;
			roads[i]=0;
			pre[i]=-1;
			selected[i]=false;
		}
		for (int i=0;i<m;i++){
			inp >> a >> b >> w;
			dist[a][b]=dist[b][a]=min(dist[a][b],w);
		}
		inp >> t;
		// my algorithm
		selected[t]=true;
		for(int i=0;i<n;i++){
			d[i]=dist[t][i];
			pre[i]=t;
		}
		
		for (int i=0;i<n-1;i++){
			// repeat n-1 times
			ULL mx = INF;
			int sl = -1;
			for (int j=0;j<n;j++)
				if (!selected[j] && d[j] < mx){
					mx = d[j];
					sl = j;
				}
			selected[sl]=true;
			//cout << "Select city " << sl << endl;
			for (int j=0;j<n;j++)
				if (d[j] > max(d[sl],dist[sl][j])){
					d[j] = max(d[sl],dist[sl][j]);
					pre[j] = sl;
				}
		}
		// output
		cout << "Case #" << ti << ": " << endl;
		for (int i=0;i<n;i++){
			if (d[i]<INF)
				cout << d[i] << endl;
			else
				cout <<"Impossible" << endl;
		}
	}
	return 0;
}
