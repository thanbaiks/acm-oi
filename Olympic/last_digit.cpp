#include <iostream>
#define uint64 unsigned long long
using namespace std;
int main(){
	int test;
	uint64 n,len,base,width;
	cin >> test;
	for (int ti=0;ti<test;ti++){
		cin >> n;
		width = 1;
		base = 1;
		while (base*9*width < n){
			// continue count
			n -= base*9*width;
			base*=10;
			width++;
		}
		//cout << "N:" << n << endl;
		n--;
		//cout << "width:" << width << " base:" << base << endl;
		base += n/width;
		n=width-(n%width)-1;
		while (n>0){
			n--;
			base/=10;
		}
		cout << "Case #" << ti+1 << ": " << base%10 << endl;
	}
	return 0;
}
