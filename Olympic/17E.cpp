#include <iostream>
#include <fstream>
#define uint unsigned int

#define inp cin
using namespace std;

uint n,test,A[100000],count[100000],mx;
int main(){
	ifstream fin("17E.inp");
	inp >> test;
	for (uint ti=1;ti<=test;ti++){
		inp >> n;
		mx=0;
		for (uint i = 1;i<=n;i++)
			inp >> A[i],count[i]=0;
		for (uint i = n;i>0;i--)
			if (i+A[i] <= n){
				// in ranger
				count[i] = count[i+A[i]]+1;
				if (count[i]>mx)mx=count[i];
			}
		cout << "Case #" << ti << ": " << mx << endl;
	}
	return 0;
}
