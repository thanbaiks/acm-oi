#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp fin
#define oup fout

using namespace std;
typedef unsigned long long ULL;
ULL m,n,t;

int main(){
	ifstream fin("SP.INP");
	ofstream fout("SP.OUT");
	inp >> m >> n >> t;
	ULL x = n / (m+1);
	ULL mod  = n-x*(m+1);
	oup << (x*m+mod)*t;
	fin.close();
	fout.close();
	return 0;
}
