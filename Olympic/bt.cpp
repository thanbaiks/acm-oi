#include 	<iostream>
#include 	<cstring>
#include 	<fstream>
#define		MAX_LENGTH 200
using namespace std;
char decode(char* s,char* result);
int main(){
	char s[MAX_LENGTH],ret[MAX_LENGTH];
	ifstream fin("maso.inp");
	fin.getline(s,MAX_LENGTH);
	fin.close();
	while (decode(s,ret)){
		strcpy(s,ret);
		cout << s << endl;
	}
	cout << "Final: " << s;
	ofstream fout("maso.out");
	fout << s;
	fout.close();
}

char decode(char* s,char* result){
	char temp[MAX_LENGTH];
	char lastChar,c,found = 0;
	int  pos=0,sumLength=0,len;
	if (strlen(s) % 2 != 0)
		return false;
	for (int i = 0;i<strlen(s);i+=2){
		len = s[i] - '0';
		c = s[i+1];
		if (c == lastChar){
			sumLength += len;
			if (sumLength > 9)
				return false;
		}else{
			sumLength = len;
			lastChar = c;
			for (int j=0;j<len;j++){
				temp[pos++] = c;
			}
		}
	}
	temp[pos] = 0;
	strcpy(result,temp);
	return true;
}
