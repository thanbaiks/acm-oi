#include <iostream>
#include <fstream>
#include <cstring>

#define inp cin

using namespace std;
struct Num{
	long sum;
	long pre;
};

int test,n,m,t,rs,mx;
int pre[1000],count[1000];

int main(){
	ifstream fin("nhat_tien.inp");
	inp >> test;
	for (int ti=0;ti<test;ti++){
		inp >> n >> m; // get data in
		rs = 0; // result
		mx = 0;
		// reset data
		for (int i = 0;i<1000;i++)
			pre[i] = -1,count[i] = 0;
		for (int i=0;i<n;i++){
			inp >> t;
			for (int j = mx;j>=1;j--){
				if (count[j] > 0){
					pre[j+t] = t;
					count[j+t]+=count[j];
				}
			}
			count[t]++;
			pre[t]=t;
			mx+=t;
			if (mx>m)
				mx = m;
		}
		// result
		cout << "Case #" << ti+1 << ": ";
		if (count[m] > 1){
			// more than one
			cout << -1 << endl;
		}else if (count[m] == 1){
			// found
			int i = 0;
			int j = m;
			while (pre[j]!=-1){
				i++;
				j -= pre[j];
			}
			cout << i << endl;
		}else{
			// not found
			cout << 0 << endl;
		}
	}
}
