#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp fin
#define oup fout

using namespace std;
typedef unsigned long long ULL;
ULL n,nodd,neven,res,x,sum;
ULL chap2(ULL n){
	n--;
	if (n%2==0){
		// n chan
		return (n+1)*n/2;
	}else{
		return n*(n/2)+n;
	}
}
int main(){
	ifstream fin("DEL.INP");
	ofstream fout("DEL.OUT");
	inp >> n;
	nodd=neven=sum=res=0;
	for (long i=0;i<n;i++){
		inp >> x;
		sum+=x;
		if (x%2)
			neven++;
		else
			nodd++;
	}
	if (sum%2==0){
		// chan
		oup << chap2(nodd)+chap2(neven);
	}else{
		oup << nodd*neven;
	}
	fin.close();
	fout.close();
	return 0;
}
