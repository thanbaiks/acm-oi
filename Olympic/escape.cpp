#include <iostream>
#include <fstream>
#include <vector>
#define inp cin

using namespace std;

struct Point{
	int x,y;
};
char map[555][555];
int test,m,n,size;


int mark(int x,int y,char ch,vector<Point> &container){
	if (map[x][y] == 'D' && ch=='*'){
		return -1;
	}
	if (map[x][y] == 'D' && ch=='S'){
		return 1;
	}
	// check for out of rangers
	if (x < 0 || x >= m || y < 0 || y >= n || map[x][y] != '.')
		return 0;
	map[x][y]=ch;
	Point myP = {x,y};
	container.push_back(myP);
	return 0;
}
int main(){
	fstream fin("escape.inp");
	inp >> test;
	for (int ti=0;ti<test;ti++){
		vector<Point> lut,step;
		int rs,count;
		inp >> m >> n;
		inp.ignore();
		
		// input data
		for (int i=0;i<m;i++){
			for (int j=0;j<n;j++){
				map[i][j] = inp.get();
				if (map[i][j]=='*'){
					Point myPoint = {i,j};
					lut.push_back(myPoint);
				}else if (map[i][j]=='S'){
					Point myPoint = {i,j};
					step.push_back(myPoint);
				}
			}
			inp.ignore();
		}
		// programs
		rs = 0;
		count = 0;
		while (true){
			count++;
			// loang lut
			size = lut.size();
			for (int i=0;i<size;i++){
				Point p = lut[i];
				if (mark(p.x-1,p.y,'*',lut)==-1){
					rs = -1;
					break;
				}
				if (mark(p.x+1,p.y,'*',lut)==-1){
					rs = -1;
					break;
				}
				if (mark(p.x,p.y-1,'*',lut)==-1){
					rs = -1;
					break;
				}
				if (mark(p.x,p.y+1,'*',lut)==-1){
					rs = -1;
					break;
				}
			}
			lut.erase(lut.begin(),lut.begin()+size);
			// loang step
			size = step.size();
			for (int i=0;i<size;i++){
				Point p = step[i];
				if (mark(p.x-1,p.y,'S',step)==1){
					rs = 1;
					break;
				}
				if (mark(p.x+1,p.y,'S',step)==1){
					rs = 1;
					break;
				}
				if (mark(p.x,p.y-1,'S',step)==1){
					rs = 1;
					break;
				}
				if (mark(p.x,p.y+1,'S',step)==1){
					rs = 1;
					break;
				}
			}
			step.erase(step.begin(),step.begin()+size);
			if ((lut.size()==0 && step.size()==0) || rs != 0)
				break;
		}
		// output
		cout << "Case #" << ti+1 << ": ";
		if (rs == 1)
			cout << count << endl;
		else
			cout << "DIE" << endl;
	}
	return 0;
}

