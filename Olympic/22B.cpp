#include <iostream>
#include <fstream>
#define LL long long
#define inp cin
using namespace std;

struct Brick{
	LL h1;
	LL h2;
};
Brick operator-(Brick &b1,Brick b2){
	Brick myb = {b1.h1-b2.h1,b1.h2-b2.h2};
	return myb;
}

Brick operator+(Brick &b1,Brick b2){
	Brick myb = {b1.h1+b2.h1,b1.h2+b2.h2};
	return myb;
}

int test,n,h1,hn,h2;
int main(){
	ifstream fin("22B.inp");
	inp >> test;
	for (int ti=1;ti<=test;ti++){
		// input
		inp >> n >> h1 >> hn;
		Brick b1 = {1,0};
		Brick b2 = {0,1};
		Brick b3 = {0,0};
		Brick total = {1,1};
		for (int i=2;i<n;i++){
			b3 = b1-b2;
			total = total+b3;
			b1 = b2;
			b2 = b3;
		}
		// calculate h2
		h2 = (hn - h1*b2.h1)/b2.h2;
		
		// output
		cout << "Case #" << ti <<": " <<total.h1*h1 + total.h2*h2 << endl;
	}
	return 0;
}
