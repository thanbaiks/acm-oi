#include <iostream>
#define UL unsigned long long

using namespace std;
UL sum(UL num,UL base){
	// tra ve tong cac chu so
	UL ret = 0;
	while (num!=0){
		ret += num%base;
		num/=base;
	}
	return ret;
}

UL gcd(UL u, UL v){
    // simple cases (termination)
    if (u == v)
        return u;
    if (u == 0)
        return v;
    if (v == 0)
        return u;
 
    // look for factors of 2
    if (~u & 1) // u is even
    {
        if (v & 1) // v is odd
            return gcd(u >> 1, v);
        else // both u and v are even
            return gcd(u >> 1, v >> 1) << 1;
    }
    if (~v & 1) // u is odd, v is even
        return gcd(u, v >> 1);
    // reduce larger argument
    if (u > v)
        return gcd((u - v) >> 1, v);
    return gcd((v - u) >> 1, u);
}

int main(){
	UL test,nums[50];
	UL fx,fy,s;
	cin >> test;
	for (UL ti=0;ti<test;ti++){
		cin >> nums[ti];
	}
	for (UL ti=0;ti<test;ti++){
		s = 0;// the sum
		for (UL i=2;i<nums[ti];i++){
			s += sum(nums[ti],i);
		}
		fx = s;
		fy = nums[ti]-2;
		s = gcd(fx,fy);
		cout << "Case #"<<ti+1 << ": "<< fx/s << "/" << fy/s << endl;
	}
	return 0;
}
