#include <iostream>
#include <fstream>
#include <cstring>

#define inp cin

using namespace std;
int test,n,kq,votes[100],mx,mxi;
char names[100][1000],tmp[1000];

int main(){
	ifstream fin("22A.inp");
	inp >> test;
	for (int ti=1;ti<=test;ti++){
		// input
		inp >> n;
		for (int i=0;i<n;i++){
			inp >> names[i];
		}
		memset(votes,0,sizeof(votes));
		// vote
		for (int i=0;i<n;i++){
			inp >> tmp;
			//cout <<  "Temp = "  << tmp << endl ;
			for (int j=0;j<n;j++)
				if (strcmp(tmp,names[j])==0 && i!=j)
					votes[j]++;
		}
		// find max
		kq = 1;
		mxi = -1;
		mx = -1;
		for (int i=0;i<n;i++){
			//cout << "Vote " << i << " = " << votes[i] << endl;
			if (votes[i]>mx){
				kq=0;
				mxi=i;
				mx=votes[i];
			}else if (votes[i]==mx){
				kq++;
			}
		}
		//  output
		cout << "Case #" << ti << ": ";
		if (kq==0){
			// yep!
			cout << names[mxi] << endl;
		}else{
			cout << 0 << endl;
		}
	}
	return 0;
}
