#include <iostream>
#include <fstream>
#define inp fin
#define ULL unsigned long long
using namespace std;
int test;
ULL kq,m,mm,n,nn;
int main(){
	ifstream fin("22C.inp");
	inp >> test;
	for (int ti=1;ti<=test;ti++){
		inp >> m >> n;
		// calc
		kq = 3;
		mm = m/2;// = 1
		nn = n/2;// = 1
		kq += (m+1)*(n+1) - (mm+1)*(nn+1);
		kq -= m-mm+n - nn;
		kq-=min(m-mm,n-nn);
		// output
		cout << "Case #" << ti << ": " << kq << endl;
	}
	return 0;
}
