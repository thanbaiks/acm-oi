#include <iostream>
#include <cmath>

using namespace std;

void swap(long long* a,long long* b){
	long long tmp = *a;
	*a=*b;
	*b=tmp;
}
int main(){
	int test,ti;
	long long n,k,i,j,sum,arr[100000];
	cin >> test;
	for (ti=0;ti<test;ti++){
		cin >> n >> k;
		for (i=0;i<n;i++){
			cin >> arr[i];
		}
	}
	for (ti=0;ti<test;ti++){
		// sort
		for (i=0;i<n-1;i++)
			for (j=n-1;j>i;j--)
				if (arr[j]<arr[j-1])swap(arr+j,arr+j-1);
		sum=0;
		i=0;
		while (arr[i]<0 && i<n){
			sum-=arr[i];
			i+=k;
		}
		i=n-1;
		while (arr[i]>0 && i>=0){
			sum+=arr[i];
			i-=k;
		}
		cout << "Case #" <<ti+1<< ": " << 2 * sum << endl;
	}
	return 0;
}
