#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;
typedef unsigned long long ULL;

void makeB(){
	ofstream out("DEL.INP");
	int n = 1000000;
	out << n << endl;
	unsigned long l;
	for (int i=0;i<n;i++){
		l=rand()%256;
		for (int j=0;j<4;j++)
			l=l*256+rand()%256;
		out << l%1000000000 << ' ';
	}
}
string toTime(long l){
	string str;
	long tmp = l/3600;
	if (tmp < 10)
		str+= '0';
	else
		str+= (char)(tmp/10+'0');
	str+= (char)(tmp%10+'0');
	tmp = l/60%60;
	if (tmp < 10)
		str+= '0';
	else
		str+= (char)(tmp/10+'0');
	str+= (char)(tmp%10+'0');
	tmp = l%60;
	if (tmp < 10)
		str+= '0';
	else
		str+= (char)(tmp/10+'0');
	str+= (char)(tmp%10+'0');
	return str;
}
void makeC(){
	ofstream out("XYZ.INP");
	int n = 30000;
	out << n << endl;
	unsigned long l;
	// random 30 phone numbers
	char s[600][11];
	for (int i=0;i<600;i++){
		for (int j=0;j<10;j++)
			s[i][j]=rand()%10+'0';
		s[i][10]=0;
	}
	
	for (int i=0;i<n;i++){
		int from = rand()%600;
		int to=0;
		do{
			to = rand()%600;
		}while(to==from);
		out << s[from] << ' ' << s[to] << ' ';
		//l = rand()%86400;
		l = rand()%300;
		out << toTime(l) << ' ';
		l = 80000 + rand()%6400;
		out << toTime(l) << endl;
	}
}
int main(){
	makeB();
	makeC();
	return 0;
}
