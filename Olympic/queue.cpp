#include <iostream>
#include <cstdio>
#include <fstream>
#include <iomanip>
#include <algorithm>
#define inp cin
#define out cout

using namespace std;

struct People{
	float height;
	unsigned int age;
};
bool operator< (const People &x,const People &y){
	return x.height < y.height || (x.height == y.height && x.age < y.age);
}
int main(){
	// declares
	long test,n,i;
	People p[100000];
	ifstream fin("queue.inp");
	ofstream fout("queue.out");
	// programs
	inp >> test;
	for (int ti=0;ti<test;ti++){
		inp >> n;
		for (i=0;i<n;i++){
			inp >> p[i].height >> p[i].age;
		}
		
		// sort
		sort(p,p+n);
		// print result
		out << "Case #" << ti+1 << ":" << endl;
		for (i=0;i<n;i++)
			out << fixed << setprecision(2) << p[i].height << " " << p[i].age << endl;
	}
	return 0;
}

