#include <bits/stdc++.h>
#define inp cin
#define out cout

using namespace std;

bool cmp(string par,string chld) {
	int i=0,j=0;
	while (i < par.length() && j < chld.length()) {
		if (par[i]==chld[j]) {
			i++;j++;
		} else {
			i++;
		}
	}
	return j >= chld.length();
}

int main() {
	ifstream fin("HCODE.INP");
	ofstream fout("HCODE.OUT");
	/** CODE BEGIN **/

	int n;
	string s[500];
	inp >> n;
	for (int i=0; i<n; i++) {
		inp >> s[i];
	}
	bool f = false;
	for (int i=0; i<n+1; i++) {
		for (int j=i+1; j<n; j++) {
			if (s[i].length() < s[j].length())
				f = cmp(s[j],s[i]);
			else if (s[i].length() > s[j].length())
				f = cmp(s[i],s[j]);
			else
				continue;
			if (f) {
				break;
			}
		}
		if (f) {
			out << 0 << endl << i+1;
			break;
		}
	}
	if (!f)
		out << 1;
	/** CODE END **/
	fin.close();
	fout.close();
}
