#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e5+5;
int n, a[MAX], b[MAX];
LL m,sum, res;
LL sqr(LL x){
	return x*x;
}
int main(){
//	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);
	cin >> m >> n;
	sum = 0;
	_for(i,0,n) {
		cin >> a[i];
		sum += a[i];
	}
	sort(a, a + n);
	memset(b,0, sizeof b);
	int i = 0; sum -= m; res = 0;
	
	while (true) {
		if (sum/(n-i) >= a[i]){
			sum -= a[i];
			res += sqr(a[i]);
			i++;
		} else {
			int j = n-i, k = sum % j, h = sum/j;
			res += sqr(h) * (j-k) + sqr(h+1) * k;
			break;
		}
	}
	cout << res;
}


