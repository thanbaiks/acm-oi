#include <iostream>
#include <fstream>
#define ULL unsigned long long
using namespace std;
int test;
ULL x;
bool try_(ULL nb,int base){
	int tmp = nb % base;
	while (nb%base==tmp)nb/=base;
	if (nb==0)
		return true;
	return false;
}
int main(){
	cin >> test;
	for (int ti=0;ti<test;ti++){
		cin >> x;
		ULL i;
		bool f = false;
		for (i=2;i<x;i++){
			if (try_(x,i)){
				f = true;
				break;
			}
		}
		if (f)
			cout << i << endl;
		else
			cout << x + 1 << endl;
	}
}
