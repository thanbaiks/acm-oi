#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;
typedef long long LL;

long randl(){
	long x = 0;
	for (int i=0;i<4;i++){
		x = (x<<8) + rand();
	}
	return x;
}
int main(){
	FILE * f = fopen("BCDINNER2.TXT","w");
	int n = 10,m=8;
	const int ranger = 10;
	fprintf(f,"%12d %12d\n",n,m);
	for (int i=0;i<n;i++){
		fprintf(f,"%12d %12d\n",randl()%ranger,randl()%ranger);
	}
	for (int i=0;i<m;i++){
		fprintf(f,"%12d %12d\n",randl()%ranger,randl()%ranger);
	}
	return 0;
}

