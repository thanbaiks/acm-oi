#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <algorithm>
#define BASE 1500
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;

int n;
int nam[1001][2],nu[1001][2],tot;

int main(){
	//freopen("BCPLES.TXT","r",stdin);
	scanf("%d",&n);
	memset(nam,0,sizeof(nam));
	memset(nu,0,sizeof(nu));
	int x;
	for (int i=0;i<n;i++){
		scanf("%d",&x);
		if (x>0)
			nam[x-BASE][0]++;
		else
			nam[-x-BASE][1]++;
	}
	for (int i=0;i<n;i++){
		scanf("%d",&x);
		if (x>0)
			nu[x-BASE][0]++;
		else
			nu[-x-BASE][1]++;
	}
	tot = 0;
	// nam thap - nu cao
	int c = 0,cc;
	for (int i=0;i<=1000;i++){
		cc = min(c,nu[i][1]);
		if (cc>0){
			c -= cc;
			tot += cc;
		}
		c += nam[i][0];
	}
	// nam cao - nu thap
	c = 0;
	for (int i=0;i<=1000;i++){
		cc = min(c,nam[i][1]);
		if (cc>0){
			c -= cc;
			tot += cc;
		}
		c += nu[i][0];
	}
	printf("%d",tot);
}


