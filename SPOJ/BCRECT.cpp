#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <map>
#include <vector>
#include <algorithm>
#define _i ::iterator
using namespace std;
typedef long long LL;
int n,x;
vector<int> primes;
map<int,int> uoca,uocb;
void make(){
	const int MAX = 32000;
	bool p[MAX];
	memset(p,true,sizeof(p));
	for (int i=2;i<MAX;i++){
		if (p[i]){
			primes.push_back(i);
			for (int j=i*i;j<MAX;j+=i)
				p[j]=false;
		}
	}
}
void solve(map<int,int> &m,int x){
	for (int i=0;i<primes.size()&&x>1;i++){
		if (x%primes[i]){
			int j = 0;
			while (x%primes[i])
				j++,x/=primes[i];
			m[i] = max(m[i],j);
		}
	}
	if (x>1)
		m[x]++;
}
int main(){
	freopen("BCRECT.TXT","r",stdin);
	make();
	scanf("%d",&n);
	for (int i=0;i<n;i++){
		scanf("%d",&x);
		solve(uoca,x);
	}
	scanf("%d",&n);
	for (int i=0;i<n;i++){
		scanf("%d",&x);
		solve(uocb,x);
	}
	// merge b & a
	for (map<int,int>_i i = uocb.begin();i!=uocb.end();++i){
		uoca[i->first]=max(uoca[i->first],i->second);
	}
	
	return 0;
}

