#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <queue>
#include <algorithm>

using namespace std;
typedef long long LL;
int r,c,br,bc;
char mat[102][102];
int move[4][2] = {{-1,0},{1,0},{0,-1},{0,1}};
queue<pair<int,pair<int,int> > > q;

int main(){
	//freopen("BCMUNCH.TXT","r",stdin);
	scanf("%d%d",&r,&c);getchar();
	memset(mat,0,sizeof(mat));
	for (int i=1;i<=r;i++){
		gets(mat[i]+1);
		for (int j=1;j<=c;j++)
			if (mat[i][j]=='C')
				q.push(make_pair(1,make_pair(i,j)));
			else if (mat[i][j]=='B')
				br = i,bc = j;
	}
	// bfs
	pair<int,pair<int,int> > cur;
	pair <int,int> pos;
	bool f = false;
	while (!q.empty() && !f){
		cur = q.front();q.pop();
		pos = cur.second;
		//printf("Pos %3d %3d\n",pos.first,pos.second);
		for (int i=0;i<4;i++){
			int rr = pos.first + move[i][0];
			int cc = pos.second + move[i][1];
			if (rr==br && cc==bc){
				printf("%d",cur.first);
				f = true;
				break;
			}
			if (mat[rr][cc]=='.')
				q.push(make_pair(cur.first+1,make_pair(rr,cc))),mat[rr][cc]='X';
		}
	}
	if (!f)
		printf("0");
	return 0;
}

