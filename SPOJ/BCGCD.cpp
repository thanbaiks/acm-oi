#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <algorithm>
#define inp cin
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
ULL x,y,z;
ULL gcd(ULL a,ULL b){
	return !b?a:gcd(b,a%b);
}

int main(){
	while (true){
		inp >> x >> y;
		if (!(x||y))
			break;
		z = gcd(x,y);
		cout << z << ' ' << x/z*y << endl;
	}
	return 0;
}


