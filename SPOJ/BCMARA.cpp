#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
typedef unsigned long long ULL;
struct Time{
	unsigned char h,m,s;
};

int n;
vector<Time> vt;

bool operator< (const Time &t1,const Time &t2){
	return (t1.h<t2.h||(t1.h==t2.h && (t1.m < t2.m || (t1.m==t2.m && t1.s < t2.s))));
}
int main(){
	ifstream fin("BCMARA.TXT");
	inp >> n;
	int h,m,s;
	for (int i=0;i<n;i++){
		inp >> h >> m >> s;
		Time time;
		time.h = h;
		time.m = m;
		time.s = s;
		vt.push_back(time);
	}
	sort(vt.begin(),vt.end());
	for (int i=0;i<n;i++){
		cout << (int)vt[i].h << ' ' << (int)vt[i].m << ' ' << (int)vt[i].s << endl;
	}
	fin.close();
	return 0;
}

