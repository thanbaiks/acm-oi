#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <vector>
#include <map>
#include <algorithm>
#define MAXN 10001

using namespace std;
typedef unsigned long long ULL;
typedef long long LL;

int p,x,z,y,c;
int a[10001];
map <int,vector<int> > b;
int main(){
	//freopen("BCBIN.TXT","r",stdin);
	scanf("%d",&p);
	memset(a,0,sizeof(a));
	c = 1;
	for (int ii=0;ii<p;ii++){
		scanf("%d%d%d",&x,&y,&z);
		if (z==1){
			// add
			//check group of a & b;
			if (!a[x] || !a[y]){
				if (a[x]){
					//y donot have in group
					b[a[x]].push_back(y);
					a[y]=a[x];
				}else if (a[y]){
					//x donot have in group
					b[a[y]].push_back(x);
					a[x]=a[y];
				}else{
					// both of two do not hava in any group
					a[x]=a[y]=c;c++;
					b[a[x]].push_back(x);b[a[y]].push_back(y);
				}
			}else{
				if (a[x]==a[y])
					continue;
				// connect two tree
				int t1,t2;
				if (b[a[x]].size()>b[a[y]].size()){
					t1 = a[x];t2=a[y];
				}else{
					t1 = a[y];t2=a[x];
				}
				for (int i=0;i<b[t2].size();i++){
					a[b[t2][i]]=t1;
					b[t1].push_back(b[t2][i]);
				}
				b[t2].clear();
			}
		}else{
			if (a[x]==a[y] && a[x]>0)
				printf("1\n");
			else
				printf("0\n");
		}
	}
}


