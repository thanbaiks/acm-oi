#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator

using namespace std;
typedef long long LL;
int n,k,x,a[51],b[51];
int main(){
	//freopen("QBSEQ.TXT","r",stdin);
	memset(a,0,sizeof(a));
	memset(b,0,sizeof(b));
	scanf("%d%d",&n,&k);
	/* input */
	for (int i=0;i<n;i++){
		scanf("%d",&x);
		x%=k;
		for (int i=0;i<k;i++){
			if (a[i])
				b[(i+x)%k]=a[i]+1;
		}
		for (int i=0;i<k;i++){
			if (b[i]>a[i])
				a[i]=b[i];
			b[i]=0;
		}
		if (a[x]==0)
			a[x]=1;
	}
	printf("%d",a[0]);
	return 0;
}

