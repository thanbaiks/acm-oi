#include <iostream>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <vector>
#define inp cin

using namespace std;
typedef unsigned long long ULL;
typedef long long LL;

LL x,i,j,k;
vector<pair<LL,LL> > vt;

int main(){
	inp >> x;
	vt.clear();
	i = 2;
	k = sqrt(x)+1;
	while (i<=k && x > 1){
		if (x%i==0){
			j = vt.size();
			if (j==0 || vt[j-1].first!=i){
				vt.push_back(make_pair(i,0));
			}else{
				j--;
			}
			while (x%i==0){
				vt[j].second++;
				x/=i;
			}
		}
		i++;
	}
	for (i=0;i<vt.size();i++){
		cout << vt[i].first << ' ' << vt[i].second << endl;
	}
	if (x>1){
		cout << x << " 1" << endl;
	}
	return 0;
}


