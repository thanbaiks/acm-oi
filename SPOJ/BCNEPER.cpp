#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
typedef unsigned long long ULL;

int test,x;
char s[100];
int main(){
	ifstream fin("BCNEPER.TXT");
	inp >> test;
	for (int ti=0;ti<test;ti++){
		inp >> x >> s;
		//cout << "Before:" << x << ' ' << s << endl;
		int len = strlen(s);
		int j;
		for (j=len-1;j>0;j--){
			if (s[j-1]<s[j]){
				// found j-1
				char c = s[j];
				int f = j;
				for (int k=j+1;k<len;k++)
					if (s[k]>s[j-1])
						c = min(s[k],c),f=k;
				// swap s[j-1] & s[k]
				c = s[j-1];
				s[j-1]=s[f];
				s[f]=c;
				sort(s+j,s+len);
				break;
			}
		}
		if (!j)
			cout << x << ' ' << "BIGGEST" << endl;
		else
			cout <<  x << ' '<< s << endl;
	}
	fin.close();
	return 0;
}

