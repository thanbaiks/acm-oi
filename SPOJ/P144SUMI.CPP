#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
typedef unsigned long long ULL;
long n,m,x;
inline long gcd(long a,long b){return !b?a:gcd(b,a%b);}
int main(){
	inp>>n>>m;
	x=gcd(n,m);
	cout<<m-x<<endl;
	return 0;
}
