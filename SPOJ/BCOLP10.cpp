#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <algorithm>
#define inp cin
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
LL a[256],sum;
string s;
int main(){
	ifstream fin("BCOLP10.TXT");
	memset(a,0,sizeof(a));
	inp >> s;
	for (int i=0;i<s.length();i++){
		a[s[i]]++;
	}
	inp >> s;
	for (int i=0;i<s.length();i++){
		a[s[i]]--;
	}
	sum=0;
	for (int i=0;i<256;i++)
		sum += abs(a[i]);
	cout << sum << endl;
	fin.close();
	return 0;
}


