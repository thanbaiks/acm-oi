#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstring>
#define inp cin
#define MAXN 100001
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
int a[MAXN],dp[MAXN],mx,n;

int main(){
	ifstream fin("BCLIQ.TXT");
	inp >> n;
	memset(dp,0,sizeof(dp));
	for (int i=0;i<n;i++){
		inp >> a[i];
		dp[i]=1;
		for (int j=0;j<i;j++){
			if (dp[j]+1>dp[i] && a[i]>a[j])
				dp[i]=dp[j]+1;
		}
		mx = max(mx,dp[i]);
	}
	cout << mx;
	fin.close();
	return 0;
}


