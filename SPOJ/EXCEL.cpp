#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
typedef long long LL;
typedef pair<LL,LL> PLL;
char s[100];
PLL format(string s){
	//cout << s << endl;
	LL c=0,r=0;
	int i;
	for (i=1;s[i]!='C';i++){
		r = r*10 + s[i]-'0';
	}
	i++;
	for (;i<s.length();i++){
		c = c*10 + s[i]-'0';
	}
	return make_pair(c,r);
}
int main(){
	ifstream fin("EXCEL.TXT");
	while (inp >> s){
		PLL p = format(s);
		if (p.first == 0 && p.second == 0)
			break;
		LL c = p.first;
		char temp;
		char i = 0;
		while (c>0){
			temp = 'A'+ (c-1)%26;
			c = (c-1)/26;
			s[i++]=temp;
		}
		for (i=i-1;i>=0;i--)
			cout << s[i];
		cout <<p.second << endl;
	}
	fin.close();
	return 0;
}

