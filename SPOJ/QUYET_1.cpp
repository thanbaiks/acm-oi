#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator

using namespace std;
typedef long long LL;
char s[1000000];
LL cnt[256];
vector<pair<LL,char> > v;
int main(){
	gets(s);
	memset(cnt,0,sizeof(cnt));
	for (LL i=0;i<strlen(s);i++){
		cnt[s[i]]++;
	}
	for (int i=0;i<256;i++){
		if (cnt[i])
			v.push_back(make_pair(cnt[i],i));
	}
	sort(v.begin(),v.end());
	for (int i=v.size()-1;i>=0;i--){
		printf("'%c'=(%.2f%%)",v[i].second,100.0*v[i].first/strlen(s));
		if (i>0)
			printf (", ");
	}
	return 0;
}

