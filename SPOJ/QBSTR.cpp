#include <iostream>
#include <cstring>
#define UI unsigned int
using namespace std;
UI max(UI a,UI b){
	return a>b?a:b;
}
UI max(UI a,UI b,UI c){
	return max(a,max(b,c));
}
int main(){
	char a[1000],b[1000];
	unsigned int map[700][700]={0},m,tmp;
	cin >> a >> b;
	m=0;
	for (int i=0;i<strlen(a);i++)
	for (int j=0;j<strlen(b);j++){
		tmp=map[i][j];
		if (a[i]==b[j])tmp++;
		m = max(tmp,map[i+1][j],map[i][j+1]);
		map[i+1][j+1]=m;
	}
	cout << m << endl;
	return 0;
}
