#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;
typedef long long LL;
int r,c,mat[101][101],cnt;
char s[101];
int main(){
	//freopen("BCGRASS.TXT","r",stdin);
	scanf("%d%d",&r,&c);
	getchar();
	memset(mat,0,sizeof(mat));
	for (int i=0;i<r;i++){
		gets(s);
		for (int j=0;j<c;j++)
			if (s[j]=='#'){
				mat[i][j]=1;
			}
	}
	cnt = 0;
	for (int i=0;i<r;i++){
		for (int j=0;j<c;j++)
			if (mat[i][j]){
				cnt++;
				if (mat[i][j+1])
					mat[i][j+1]=0;
				else
					mat[i+1][j]=0;
			}
	}
	printf("%d",cnt);
	return 0;
}

