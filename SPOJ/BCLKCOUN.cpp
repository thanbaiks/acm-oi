#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <queue>
#include <algorithm>

using namespace std;
typedef long long LL;
LL n,m,c;
char s[101][101];
queue<pair<int,int> > q;

void bfs(){
	while (!q.empty()){
		int i = q.front().first;
		int j = q.front().second;
		q.pop();
		for (int ii=i-1;ii<=i+1;ii++){
			for (int jj=j-1;jj<=j+1;jj++){
				if (ii<0||jj<0||ii>=n||jj>=m||s[ii][jj]!='W')
					continue;
				q.push(make_pair(ii,jj));
				s[ii][jj]='.';
			}
		}
	}
}
int main(){
	//freopen("BCLKCOUN.TXT","r",stdin);
	scanf("%d%d",&n,&m);
	getchar();
	for (int i=0;i<n;i++){
		gets(s[i]);
	}
	c = 0;
	for (int i=0;i<n;i++){
		for (int j=0;j<m;j++){
			if (s[i][j]=='W'){
				//printf("Found %5d %5d\n",i,j);
				s[i][j]='.';
				c++;
				q.push(make_pair(i,j));
				bfs();
			}
		}
	}
	printf("%lld",c);
	return 0;
}

