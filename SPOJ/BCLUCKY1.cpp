#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;
typedef long long LL;
LL x,c;
char s[30];
bool f;
int main(){
	scanf("%s",&s);
	c=0;
	for (int i=0;i<strlen(s);i++){
		if (s[i]=='4'||s[i]=='7')
			c++;
	}
	f = true;
	if (c==0)
		f = false;
	else
		while (c>0){
			if (c%10==4||c%10==7){
				c/=10;
			}else{
				f = false;
				break;
			}
		}
	if (f)
		printf("YES");
	else
		printf("NO");
	return 0;
}

