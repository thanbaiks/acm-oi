#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator

using namespace std;
typedef long long LL;
int randl(){
	int l = 0;
	for (int i=0;i<4;i++)
		l = (l<<8)+rand()%256;
	return l;
}
int main(){
	srand(1995);
	/* CONFIG */
	FILE * f = fopen("QBHEAP.TXT","w");
	const int n = 100000;
	const int LIMIT = 1000000000;
	/* PRINTS */
	for (int i=0;i<n;i++){
		if (rand()%100>50){
			fprintf(f,"-\n");
		}else{
			fprintf(f,"+%d\n",abs(randl())%LIMIT+1);
		}
	}
	return 0;
}

