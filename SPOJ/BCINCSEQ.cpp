#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <algorithm>
#define inp cin
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
LL n,a[100001],mx,cur;

int main(){
	ifstream fin("BCINCSEQ.TXT");
	inp >> n;
	for (int i=0;i<n;i++)
		inp >> a[i];
	mx = cur = 0;
	int j=0;
	for (j=0;j<n;j++){
		if (j>0 && a[j]>=a[j-1])
			cur++;
		else
			cur=1;
		mx=max(mx,cur);
	}
	cout << mx << endl;
	fin.close();
	return 0;
}


