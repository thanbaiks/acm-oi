#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <map>
#include <algorithm>
#define inp fin

using namespace std;
typedef long long LL;

LL n,a,b,k,test,step,x;
LL gcd(LL a, LL b){
	return !b?a:gcd(b,a%b);
}
int main(){
	ifstream fin("MROBOT.INP");
	
	/* INPUT */
	inp >> test;
	for (int ti=0;ti<test;ti++){
		inp >> n >> a >> b >> k;
		LL lcm = a*b/gcd(a,b);
		step = 0;// start at 0
		for (int i=0;i<=k;i++){// i number of move to right
			LL lft = (i*a-1)/lcm*lcm;
			x = i*a;
			if (x<n)
				step++;
			LL pos;
			for (int j=1;j<=k-i;j++){
				pos = x-j*b;
				if (pos<=lft){
					break;
				}else if (pos<min(n,lft+lcm)){
					step++;
					//cout << x-j*b << endl;
				}
			}
		}
		cout << n-step << endl;
	}
	fin.close();
	return 0;
}

