#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstring>
#define inp cin
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
bool mark[10];
char s[10];
LL n;
void dp(int depth){
	for (int i=1;i<=n;i++){
		if (!mark[i]){
			s[depth]='0'+i;
			mark[i]=true;
			if (depth==n-1){
				cout << s << endl;
			}else{
				dp(depth+1);
			}
			mark[i]=false;
		}
	}
}

int main(){
	memset(mark,0,sizeof(mark));
	memset(s,0,sizeof(s));
	inp >> n;
	dp(0);
	return 0;
}


