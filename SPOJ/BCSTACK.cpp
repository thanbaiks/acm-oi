#include <iostream>
#include <fstream>
#include <cstring>
#include <algorithm>
#include <map>
#include <stack>
#define inp cin
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
map<string,int> cmd;
stack<LL> st;
string s,ss;
LL x;
bool br = false;
int main(){
	ifstream fin("BCSTACK.TXT");
	cmd["init"]=1;
	cmd["push"]=2;
	cmd["pop"]=3;
	cmd["top"]=4;
	cmd["size"]=5;
	cmd["empty"]=6;
	cmd["end"]=7;
	
	
	while (!br && getline(inp,s)){
		LL i = s.find(' ');
		if (i!=string::npos){
			ss = s.substr(i+1,s.length()-i-1);
			s = s.substr(0,i);
			x = 0;
			for (i=0;i<ss.length();i++){
				x=x*10+ss[i]-'0';
			}
		}
		switch (cmd[s]){
			case 1:
				while (st.size()>0)
					st.pop();
				break;
			case 2:
				st.push(x);
				break;
			case 3:
				if (!st.empty())
					st.pop();
				break;
			case 4:
				cout << (st.size()>0?st.top():-1) << endl;
				break;
			case 5:
				cout << st.size() << endl;
				break;
			case 6:
				if (st.empty())
					cout << 1 << endl;
				else
					cout << 0 << endl;
				break;
			case 7:
				br = true;
			default:
				break;
		}
	}
	fin.close();
	return 0;
}


