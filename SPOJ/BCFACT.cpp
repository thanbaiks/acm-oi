#include <iostream>
#include <fstream>
#include <algorithm>
#define inp cin
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
ULL frac[20],n;
int main(){
	frac[1]=1;
	for (int i=2;i<=17;i++){
		frac[i]=frac[i-1]*i;
	}
	while (inp >> n && n > 0){
		cout << frac[n]<<endl;
	}
	return 0;
}


