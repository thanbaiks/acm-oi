#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
LL bit[20],sum,field,n,x;
int main(){
	//freopen("BCX3.TXT","r",stdin);
	memset(bit,0,sizeof(bit));
	scanf("%lld",&n);
	for (LL i=0;i<n;i++){
		scanf("%lld",&x);
		for (int j=0;j<20;j++){
			if (x&1)
				bit[j]++;
			x = x >> 1;
		}
	}
	sum = 0;
	field = 1;
	for (int i=0;i<20;i++){
		if (bit[i]>0 && bit[i]<n){
			sum += (bit[i]*(n-bit[i]))*field;
		}
		field = field << 1;
	}
	printf("%lld",sum);
}


