#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin
#define LIMIT 100001
using namespace std;
typedef unsigned long long ULL;
ULL test,n;
vector<long> primes;

void makePrime(){
	primes = vector<long>();
	bool prime[LIMIT];
	fill(prime,prime+LIMIT,true);
	for (ULL i=2;i<LIMIT;i++){
		if (prime[i]){
			primes.push_back(i);
			for (ULL j=i*i;j<LIMIT;j+=i){
				prime[j]=false;
			}
		}
	}
	//cout << "Size = " << primes[0] << ' ' << primes [1] << endl;
}
int main(){
	ifstream fin("BCPNA.TXT");
	inp >> test;
	makePrime();
	for (int ti=0;ti<test;ti++){
		inp >> n;
		ULL sum = 0;
		ULL cnt = 0;
		int i=0,j=0;
		while (true){
			if (i<primes.size() && primes[i]<=n && sum<n){
				// tiep tuc tang i
				sum+=primes[i++];
			}else if(j<primes.size() && sum > n){
				// tiep tuc tang j
				sum-=primes[j++];
			}else if (sum == n){
				// equal
				cnt++;
				if (i<primes.size()&& primes[i]<=n){
					sum+=primes[i++];
				}else{
					break;
				}
			}else{
				break;
			}
		}
		cout << cnt << endl;
	}
	fin.close();
	return 0;
}

