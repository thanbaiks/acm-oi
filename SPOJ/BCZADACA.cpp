#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <map>
#include <vector>
#include <algorithm>
#define DIV 1000000000
#define _i ::iterator
using namespace std;
typedef long long LL;
int n,x;
vector<int> primes;
map<int,int> uoca,uocb;
void make(){
	const int MAX = 32000;
	bool p[MAX];
	memset(p,true,sizeof(p));
	for (int i=2;i<MAX;i++){
		if (p[i]){
			primes.push_back(i);
			for (int j=i*i;j<MAX;j+=i)
				p[j]=false;
		}
	}
}
void solve(map<int,int> &m,int x){
	for (int i=0;i<primes.size()&&x>1;i++){
		while (x%primes[i]==0){
			m[primes[i]]++,x/=primes[i];
		}
	}
	if (x>1)
		m[x]++;
}
int main(){
	//freopen("BCZADACA.TXT","r",stdin);
	make();
	scanf("%d",&n);
	for (int i=0;i<n;i++){
		scanf("%d",&x);
		solve(uoca,x);
	}
	scanf("%d",&n);
	for (int i=0;i<n;i++){
		scanf("%d",&x);
		solve(uocb,x);
	}
	// merge b & a
	bool f=false;
	for (map<int,int>_i i = uoca.begin();i!=uoca.end();++i){
		i->second=min(uocb[i->first],i->second);
	}
	LL tmp = 1;
	for (map<int,int>_i i = uoca.begin();i!=uoca.end();++i){
		for (int j=0;j<i->second;j++){
			tmp*=i->first;
			if (tmp>=DIV)
				tmp%=DIV,f=true;
		}
	}
	if (f)
		printf("%09d",tmp);
	else
		printf("%d",tmp);
	return 0;
}

