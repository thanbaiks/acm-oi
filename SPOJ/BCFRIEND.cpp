#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <cmath>
#include <algorithm>
#define inp cin
#define MAX 100001
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
LL n,b,a[MAX],cnt;
vector<pair<LL,LL> > vt;
int main(){
	ifstream fin("BCFRIEND.TXT");
	inp >> n >> b;
	for (LL i=0;i<n;i++){
		inp >> a[i];
	}
	sort(a,a+n);
	for (int i=0;i<n;i++){
		if (vt.size()==0 || vt[vt.size()-1].first != a[i]){
			vt.push_back(make_pair(a[i],1));
		}else{
			vt[vt.size()-1].second++;
		}
	}
	int i = 0;int j = vt.size()-1;
	cnt = 0;
	while (i<=j){
		if (vt[i].first + vt[j].first == b){
			// a pair
			if (i==j){
				// equal
				cnt += vt[i].second*(vt[i].second-1)/2;
				break;
			}else{
				cnt += vt[i].second*vt[j].second;
				j--;
			}
		}else if(vt[i].first + vt[j].first < b){
			// increase i
			i++;
		}else{
			j--;
		}
	}
	cout << cnt << endl;
	fin.close();
	return 0;
}


