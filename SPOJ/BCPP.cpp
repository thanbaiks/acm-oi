#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <algorithm>
#define MAX 100001
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;

LL a,b,c;
LL tot[MAX];

int main(){
	scanf("%lld%lld",&a,&b);
	fill(tot,tot+MAX,1);
	
	for (int i=2;i<=b;i++){
		for (int j=i*2;j<=b;j+=i){
			tot[j]+=i;
		}
	}
	c = 0;
	for (int i=a;i<=b;i++){
		if (tot[i]>i)
			c++;
	}
	printf("%lld",c);
}


