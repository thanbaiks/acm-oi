#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define MAX 100000
using namespace std;
typedef long long LL;
int l[MAX],r[MAX],d[MAX],x,y,z;
LL n,c;
void dfs(int i){
	if (l[i]>0){
		d[l[i]]=d[i]+1;
		dfs(l[i]);
	}
	if (r[i]>0){
		d[r[i]]=d[i]+1;
		dfs(r[i]);
	}
}
int main(){
	//freopen("BCCOLDWA.TXT","r",stdin);
	scanf("%lld%lld",&n,&c);
	memset(l,0,sizeof(l));
	memset(r,0,sizeof(r));
	memset(d,0,sizeof(d));
	for (int i=0;i<c;i++){
		scanf("%d%d%d",&x,&y,&z);
		if (y>z){
			y+=z;
			z=y-z;
			y=y-z;
		}
		l[x]=y;
		r[x]=z;
	}
	d[1]=1;
	dfs(1);
	for (int i=1;i<=n;i++){
		printf("%d\n",d[i]);
	}
	return 0;
}

