#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <algorithm>
#define inp cin
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
LL n,x,k,sum;
int main(){
	inp >> n;
	for (LL ti=0;ti<n;ti++){
		inp >> x;
		sum=0;
		for (LL i=1;i*i<=x;i++){
			if (x%i==0){
				sum += i;
				if (x/i>i)
					sum += x/i;
			}
		}
		cout << sum << endl;
	}
	return 0;
}


