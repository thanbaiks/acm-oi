#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstring>
#define inp cin
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
LL n,x,y,k;
int main(){
	inp >> n;
	unsigned char s1[20],s2[20];
	for (LL ti=0;ti<n;ti++){
		inp >> x;
		y=x;
		k=0;
		if (x<0){
			cout << "NO" << endl;
			break;
		}
		while (y>0){
			k=k*10 + y%10;
			y/= 10;
		}
		if (k!=x)
			cout << "NO" << endl;
		else
			cout << "YES" << endl;
	}
	return 0;
}


