#include <iostream>
#include <fstream>
#include <algorithm>
#define inp cin
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;

LL n;
bool isprime(LL n){
	if (n<=3)
		return n > 1;
	if (n%2==0 || n%3==0)
		return false;
	for (LL i=5;i*i<=n;i+=6){
		if (n%i==0 || n%(i+2)==0)
			return false;
	}
	return true;
}
int main(){
	inp >> n;
	if (isprime(n)){
		cout << "YES";
	}else{
		cout << "NO";
	}
	return 0;
}

