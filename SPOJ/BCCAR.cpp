#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin

using namespace std;
typedef unsigned long long ULL;
int test,n,arr[21];
int main(){
	inp >> test;
	for (int ti=0;ti<test;ti++){
		inp >> n;
		for (int i=0;i<n;i++)
			inp >> arr[i];
		sort(arr,arr+n);
		cout << (arr[n-1]-arr[0])*2 << endl;
	}
	return 0;
}

