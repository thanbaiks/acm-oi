#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define ZERO 0.000001
using namespace std;
typedef long long LL;
typedef pair<int,int> Pos;
struct Node{
	double d;
	int c,s;
	static Node _(double _1,int _2,int _3){
		Node n;
		n.d=_1;n.c=_2;n.s=_3;
		return n;
	}
};
int n,m;
vector<Pos> a,b;
bool ma[1001],mb[1001];
vector<Node> d;
int x,y;

LL sqr(LL x){
	return x*x;
}

double dist(const Pos &x,const Pos &y){
	return sqrt(sqr(x.first-y.first)+sqr(x.second-y.second));
}
bool operator< (const Node &a,const Node &b){
	return (a.d < b.d || abs(a.d-b.d)<ZERO && (a.c < b.c || a.c==b.c && a.s<b.s));
}
int main(){
	freopen("BCDINNER2.TXT","r",stdin);
	scanf("%d%d",&n,&m);
	memset(ma,0,sizeof(ma));
	memset(mb,0,sizeof(mb));
	for (int i=0;i<n;i++){
		scanf("%d%d",&x,&y);
		a.push_back(make_pair(x,y));
	}
	for (int i=0;i<m;i++){
		scanf("%d%d",&x,&y);
		b.push_back(make_pair(x,y));
	}
	for (int i=0;i<n;i++){
		for (int j=0;j<m;j++){
			d.push_back(Node::_(dist(a[i],b[j]),i,j));
		}
	}
	sort(d.begin(),d.end());
	for (int i=0;i<d.size();i++){
		//printf("%14.2f %3d %3d\n",d[i].d,d[i].c,d[i].s);
		if (ma[d[i].c]||mb[d[i].s])
			continue;
		// add current cow and seat
		ma[d[i].c]=true;
		mb[d[i].s]=true;
	}
	if (m==n)
		printf("0");
	else
		for (int i=0;i<n;i++){
			if (!ma[i]){
				printf("%d\n",i+1);
			}
		}
	return 0;
}

