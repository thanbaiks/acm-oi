/*******************
 *  @Prog: Det
 *	@Author: BachNX
 *******************/
 

#include <iostream>
using namespace std;
const int MAX_SIZE = 20;
int arr[MAX_SIZE][MAX_SIZE];
bool mask[MAX_SIZE];
int size;

int det(int level);
void input();
void input_debug();

int main()
{
	input();
	// == calculate
	int result = det(0);
	cout << endl << "Ket qua la: " << result;
}

void input(){
	cout << ":::: Determinant Calculator" << endl;
	cout << "Kich thuoc ma tran: ";
	cin >> size;
	// == read matrix
	for (int i = 0; i < size;i++){
		cout << endl << "Dong thu " << i + 1 << endl;
		for (int j = 0; j < size; j++){
			cout << "Cot thu " << j + 1 << ": ";
			cin >> arr[i][j];
		}
	}
	
	for (int i = 0;i < size;i++){
		mask[i] = true;
	}
}

void input_debug(){
	size = 3;
	arr[0][0] = 3;
	arr[0][1] = 4;
	arr[0][2] = 5;
	arr[1][0] = 2;
	arr[1][1] = 4;
	arr[1][2] = 1;
	arr[2][0] = 4;
	arr[2][1] = 5;
	arr[2][2] = 9;
	for (int i = 0;i < size;i++){
		mask[i] = true;
	}
}

int det(int level){
	int result = 0;
	int j = 0;
	for (int i = 0;i < size;i++){
		if (!mask[i]){
			continue;
		}
		j++;
		int d = 1;
		if (j%2 == 0){
			d = -1;
		}
		if (level < size - 1){
			mask[i] = false;
			result += d * (arr[level][i] * det(level+1));
			mask[i] = true;
		} else {
			result += d * (arr[level][i]);
		}
	}
	return result;
}

