#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp fin
#define out fout
#define FIRST 10102010
using namespace std;
typedef unsigned long long ULL;
long n;
long cnt[8000];
ULL sum;
int main(){
	ifstream fin("PARK.INP");
	ofstream fout("PARK.OUT");
	
	inp >> n;
	sum=0;
	memset(cnt,0,sizeof(cnt));
	long x;
	for (long i=0;i<n;i++){
		inp >> x;
		cnt[x-FIRST]++;
	}
	for (long i=0;i<8000;i++){
		if (cnt[i])
			sum+=100;
		if (cnt[i]>5)
		sum+=cnt[i]-5;
	}
	out << sum;
	fin.close();
	fout.close();
	return 0;
}
