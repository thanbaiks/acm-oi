#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#define inp cin
#define MAX 9999999
using namespace std;
struct Point{
public:
	int x,y;
	static Point make(int a,int b){
		Point p = {a,b};
		return p;
	}
};

int test;
int R,C,T,G;
int MAP[1000][1000];
vector<Point> vp;
char move[4][2]={{1,0},{-1,0},{0,1},{0,-1}};
int level[4];
int calcLevel(int m, bool ngang){
	int level = 0;
	if (ngang){
		for (int i=0;i<R-1;i++){
			int tile = MAP[m][i];
			if (tile==-1){
				level=-1;
				break;
			}else if (tile>0){
				if (level==0)
					level=tile;
				else
					level=min(level,tile);
			}
		}
	}else{
		for (int i=0;i<C-1;i++){
			int tile = MAP[i][m];
			if (tile==-1){
				level=-1;
				break;
			}else if (tile>0){
				if (level==0)
					level=tile;
				else
					level=min(level,tile);
			}
		}
	}
	return level;
}
int main(){
	ifstream fin("OLP_C.inp");
	inp >> test;
	for (int ti=1;ti<=test;ti++){
		// input
		inp >> R >> C >> T >> G;
		memset(MAP,0,sizeof(MAP));
		memset(level,0,sizeof(level));
		int x,y;
		vp.clear();
		// bomb
		for (int i=0;i<T;i++){
			inp >> x >> y;
			x--;y--;
			MAP[x][y]=-1;
		}
		// linh
		for (int i=0;i<G;i++){
			inp >> x >> y;
			x--;y--;
			MAP[x][y] = 1;
			vp.push_back(Point::make(x,y));
		}
		
		// loang
		long size = vp.size();
		int antoan = 2;
		while (size>0){
			for (int i=0;i<size;i++)
				for (int j=0;j<4;j++){
					int xx = vp[i].x + move[j][0];
					int yy = vp[i].y + move[j][1];
					if (xx >= 0 && xx < R && yy >= 0 && yy < C && MAP[xx][yy]==0){
						MAP[xx][yy] = antoan;
						if (xx>0&&xx<R-1&&yy>0&&yy<C-1){
							vp.push_back(Point::make(xx,yy));
						}
					}
				}
			vp.erase(vp.begin(),vp.begin()+size);
			size = vp.size();
			antoan++;
		}
		
		// dump map
		/*
		for (int i=0;i<R;i++){
			for (int j=0;j<C;j++){
				cout << MAP[i][j] << " ";
			}
			cout << endl;
		}
		*/
		// calc level
		// top line
		Point st;Point end;
		level[0]=calcLevel(0,true);
		level[1]=calcLevel(C-1,false);
		level[2]=calcLevel(0,false);
		level[3]=calcLevel(R-1,true);
		if (level[0]<0||level[1]<0)
			level[0]=-1;
		else
			level[0]=min(level[0],level[1]);
		if (level[2]<0||level[3]<0)
			level[2]=-1;
		else
			level[2]=min(level[2],level[3]);
		if (level[0]<0&&level[2]<0)
			cout << -1 << endl;
		else if (level[0]*level[2]<0)
			cout << max(level[0],level[2]) << endl;
		else
			cout << min(level[0],level[2]) << endl;
	}
	return 0;
}
