#include <bits\stdc++.h>
using namespace std;

typedef pair<int,int> PII;

int n,test;
char mat[9][9]; // [0>>8]
vector<PII> d;
bool conf[5][5];
char cap[5][10];

int makeconf(int a,int b){
	conf[a][b] = true;
	conf[b][a] = true;
}
bool try_(int x){
	for (int i=1;i<=9;i++){
		// test all value of x
		if (cap[x][i]){
			// apply conflicts
			for (int j=0;j<5;j++){
				if (conf[x][j]){
					cap[j][i] = false;
				}
			}
			
			
			
			// unapply conflicts
			for (int j=0;j<5;j++){
				if (conf[x][j]){
					cap[j][i] = true;
				}
			}
		}
	}
}
int main(){
	freopen("input.txt","r",stdin);
	cin >> test;
	while (test--){
		char s[10];
		d.clear();
		memset(conf,0,sizeof(conf));
		memset(cap,1,sizeof(cap));
		
		for (int i=0;i<9;i++){
			cin >> s;
			for (int j=0;j<9;j++){
				mat[i][j] = s[j]-'0';
				if (!mat[i][j]){
					mat[i][j] = -d.size();
					d.push_back(make_pair(i,j));
				}
			}
		}
		
		for (int i=0;i<5;i++){
			int r = d[i].first, c = d[i].second;
			for (int j=0;j<9;j++){
				if (mat[r][j]>0)
					cap[i][mat[r][j]] = false;
				else
					makeconf(i,-mat[r][j]);
				if (mat[j][c]>0)
					cap[i][mat[j][c]] = false;
					makeconf(i,-mat[j][c]);
			}
			
			int top = max(r-2,0), bot = min(r+2,8);
			int left = max(c-2,0), right = min(c+2,8);
			for (int x=left;x<=right;x++){
				for (int y=top;y<=top;y++){
					if (mat[y][x]>0)
						cap[i][mat[y][x]] = false;
					else
						makeconf(i,-mat[y][x]);
				}
			}
		}
	}
	return 0;
}
