#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp cin
#define oup cout

using namespace std;
typedef unsigned long long ULL;
ULL n,m,k,test;

int main(){
	ifstream fin("CANARIUM.INP");
	ofstream fout("CANARIUM.OUT");
	
	inp >> test;
	for (int ti=0;ti<test;ti++){
		inp >> k;
		m = (sqrt(4*k-1)-1)/2;
		n = (2*k-m-1)/(2*m+1);
		while((2*k-m-1)%(2*m+1) && m > 0){
			m--;
			n = (2*k-m-1)/(2*m+1);
		}
		if (!m)
			oup << -1 << ' ' << -1 << endl;
		else
			oup << m << ' ' << n << endl;
	}
	fin.close();
	fout.close();
	return 0;
}
