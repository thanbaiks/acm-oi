#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp fin
#define oup fout

using namespace std;
typedef unsigned long long ULL;
ULL n,k,count;
char s[100000][201];
int calc(char* s1,char* s2){
	int l1 = strlen(s1)-1;
	int l2 = strlen(s2)-1;
	int i=0;
	while (i<=min(l1,l2)){
		if (s1[i]!=s2[i] || s1[l1-i]!=s2[l2-i])
			return i;
		i++;
	}
	return i-1;
}
int main(){
	ifstream fin("GENEMAP.INP");
	ofstream fout("GENEMAP.OUT");
	count=0;
	inp >> n >> k;
	for (ULL i=0;i<n;i++){
		inp >> s[i] >> endl;
	}
	for (int i=0;i<n-1;i++){
		for (int j=0;j<n;j++){
			if (calc(s[i],s[j])==k)
				count++;
		}
	}
	fin.close();
	fout.close();
	return 0;
}
