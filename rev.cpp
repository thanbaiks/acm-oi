#include <iostream>
#include <cstring>
using namespace std;
typedef unsigned long long ULL;
typedef long long LL;
char* rev(char *s){
	int l = strlen(s);
	char* ret = new char[l];
	for (int i=0;i<l;i++){
		ret[i] = s[l-1-i];
	}
	return ret;
}
int main(){
	char str[] = "Hello World!";
	cout << str << endl;
	cout << rev(str) << endl;
}


