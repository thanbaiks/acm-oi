#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

LL n,m;
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	cin.sync_with_stdio(0);
	cin.tie(0);

	cin >> n;
	m = 1;
	LL x;
	_for(i,2,sqrt(n)+1){
		if (n%i==0){
			x = n/i;
			if (i<n) m += i+x;
			else if (i==x) m += i;
		}
	}
	cout << (n==m?"YES":"NO");
}


