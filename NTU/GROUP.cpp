#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MOD = 1e9+7;
const int MAX = 1e3+5;

int n,t;
int a[MAX][MAX]; // {subgroup size} {group size}

void make(){
	memset(a,0,sizeof a);
	_for(i,2,1001){
		_for(j,1,1001){
			if (j%i==0){
				a[i][j] = 1;
			}
			for (int k=j;k>0;k-=i){
				a[i][j] = (a[i][j] + a[i-1][k]) % MOD;
			}
		}
	}
}

int main(){
//	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);
	make();
	cin >> t;
	while (t--){
		cin >> n;
		cout << a[n][n] << endl;
	}
}


