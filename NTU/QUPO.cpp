#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <string>

#define fori(i,__z) for (int i=0;i<(__z);i++)

#define HANG	1
#define COT		2

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

struct Range {
	int s,e;
	void set(int a,int b){
		s = a;
		e = b;
	}
	bool in(int x){
		return x >= s && x <= e;
	}
};

int qc[3][2],mod;
Range ra;
int main(){
	//freopen("input.txt","r",stdin);
	fori(i,3)
		scanf("%d%d",&qc[i][0],&qc[i][1]);
	if (qc[1][0]==qc[2][0]){
		// cung hang
		mod = HANG;
		if (qc[1][1] > qc[2][1]){
			if (qc[2][1]==0){	
				printf("-1");
				return 0;
			}
			ra.set(0,qc[2][1]-1);
		}else{
			if (qc[2][1]==8){	
				printf("-1");
				return 0;
			}
			ra.set(qc[2][1]+1,8);
		}
	}else if (qc[1][1] == qc[2][1]){
		mod = COT;
		if (qc[1][0] > qc[2][0]){
			if (qc[2][0]==0){	
				printf("-1");
				return 0;
			}
			ra.set(0,qc[2][0]-1);
		}else{
			if (qc[2][0]==9){	
				printf("-1");
				return 0;
			}
			ra.set(qc[2][0]+1,9);
		}
	}else{
		// DIE
		printf("-1");
		return 0;
	}
	if (mod == HANG){
		if (qc[0][0] == qc[1][0]){
			if (ra.in(qc[0][1])){
				printf("1");
			}else{
				printf("4");
			}
		}else{
			if (ra.in(qc[0][1])){
				printf("2");
			}else{
				printf("3");
			}
			
		}
	}else{
		if (qc[0][1] == qc[1][1]){
			if (ra.in(qc[0][0])){
				printf("1");
			}else{
				printf("4");
			}
		}else{
			if (ra.in(qc[0][0])){
				printf("2");
			}else{
				printf("3");
			}
			
		}
	}
	return 0;
}


