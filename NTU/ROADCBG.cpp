#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef pair<double,double> PDD;
typedef pair<double,int> PDI;

struct S {
	int a,b,v;
};

const int MAX = 1e5+5;
int n,m,x;
S a[MAX];
double ts[MAX],te[MAX],res[MAX];
PDI q[MAX];
vector<PDD> vt;

void setformat(){
	cout << fixed<< setprecision(6);
}

bool cmp(const double &a,const PDD &b){
	return a < b.first;
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> m >> x;
    int j = 0;
    _for(i,0,n){
    	cin >> a[j].a >> a[j].b >> a[j].v;
    	if (a[j].a>a[j].b){
    		a[j].v *= -1;
    		swap(a[j].a, a[j].b);
    	}
    	if ((a[j].v>0&&a[j].a<x) || (a[j].v<0&&a[j].b>x)) j++; // KEEP
	}
	n = j;
	_for(i,0,n){
		const S &s = a[i];
		if (s.v>0){
			if (x<=s.b){
				ts[i]=0;
			}else{
				ts[i]=double(x-s.b)/abs(s.v);
			}
			te[i] = double(x-s.a)/abs(s.v);
		}else{
			if (x>=s.a){
				ts[i]=0;
			} else {
				ts[i]=double(s.a-x)/abs(s.v);
			}
			te[i] = double(s.b-x)/abs(s.v);
		}
	}
	sort(ts,ts+n);sort(te,te+n);
	double d;
	int i=0,cnt=0;j=0;
	while (j<n){
		if (i==n||ts[i]>te[j]){
			cnt--;
			if (cnt==0){
				vt.push_back(make_pair(d,te[j]));
			}
			j++;
		}else if (ts[i]==te[j]){
			i++;j++;
		} else {
			if (!cnt){
				d = ts[i];
			}
			cnt++;i++;
		}
	}
	
	double t;
	_for(i,0,m){
		cin >> t;
		q[i] = make_pair(t,i);
	}
	sort(q,q+m);
	vector<PDD>::iterator it = vt.begin();
	vector<PDD>::iterator ie = vt.end();
	setformat();
	i = 0;
	while (i<m){
		if (it==ie || q[i].first<(*it).first){
			res[q[i].second] = q[i].first;
		}else if (q[i].first<(*it).second){
			res[q[i].second] = (*it).second;
		}else{
			it++;
			continue;
		}
		i++;
	}
	_for(i,0,m)
		cout << res[i] << endl;
    return 0;
}

