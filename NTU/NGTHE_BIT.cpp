#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e5+5;

int n,m;
int BIT[MAX];

void update(int x){
	while (x<MAX){
		BIT[x]++;
		x += x&-x;
	}
}

int get(int x){
	int res = 0;
	if (x<=0)
		return 0;
	while (x>0){
		res += BIT[x];
		x -= x&-x;
	}
	return res;
}

int main(){
//	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);
	cin >> n;
	memset(BIT,0,sizeof BIT);
	
	LL res = 0;
	_for(i,0,n){
		cin >> m;
		res += (i-get(m));
		update(m);
	}
	cout << res << endl;
}


