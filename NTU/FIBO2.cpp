#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MOD = 1e6+7;

struct Mat{
	/**
	 * a	b
	 *
	 * c	d
	 */
	LL a,b,c,d;
	Mat(int _a,int _b,int _c,int _d){
		a = _a;
		b = _b;
		c = _c;
		d = _d;
	}
	
	Mat operator*(const Mat &m) const {
		return Mat(
			(a*m.a+b*m.c)%MOD,
			(a*m.b+b*m.d)%MOD,
			(c*m.a+d*m.c)%MOD,
			(c*m.b+d*m.d)%MOD
		);
	}
};

Mat matpow(Mat m,LL n) {
	Mat t(1,0,0,1);
	while (n>1){
		if (n&1)
			t = t*m;
		m = m*m;
		n/=2;
	}
	return m*t;
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    LL n;
    cin >> n;
    if (n<=2){
    	cout << 1;
    	return 0;
	}
	
	Mat c(1,1,1,0);
	c = matpow(c,n-1);
	cout << c.a;
    return 0;
}

