#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)
#define MAXN 100005

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
int test,n,k,p,a[MAXN];

int main(){
//	freopen("input.txt","r",stdin);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> test;
	while (test--){
		cin >> n >> k >> p;
		fori(i,n) cin >> a[i];
		sort(a,a+n);
		bool kq = true;
		for (int i=0;i+k-1<n;i++){
			if (a[i+k-1]-a[i]<=2*p){
				kq = false;
				break;
			}
		}
		if (kq)
			cout << "YES" << endl;
		else
			cout << "NO" << endl;
	}
	return 0;
}


