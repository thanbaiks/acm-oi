#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)
#define MAXN 100005
#define INF 10000000000000000

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,m,qs,qe;
int arr[MAXN];
LL tree[MAXN*4];

int mid(int a,int b){
	return a+(b-a)/2;
}

LL maketree(int s,int e,int id){
	if (s==e){
		tree[id] = arr[s];
		return arr[s];
	}
	int m = mid(s,e);
	tree[id] = min(maketree(s,m,id*2+1),maketree(m+1,e,id*2+2));
	return tree[id];
}

LL tmin(int s,int e,int id){
	if (s >= qs && e <= qe)
		return tree[id];
	else if (s > qe || e < qs)
		return INF;
	int m = mid(s,e);
	return min(tmin(s,m,2*id+1),tmin(m+1,e,2*id+2));
}

int main(){
//	freopen("input.txt","r",stdin);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n >> m;
	fori(i,n){
		cin >> arr[i];
	}
	maketree(0,n-1,0);
	while (m--){
		cin >> qs >> qe; // query start & query end
		qs--;qe--;
		cout << tmin(0,n-1,0) << "\n";
	}
	return 0;
}


