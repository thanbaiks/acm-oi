#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 22;

int a[MAX],n;

int main(){
//	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);
	cin >> n;
	_for(i,0,n){
		cin >> a[i];
	}
	_for(i,1,n){
		int t = a[i];
		int *pos = upper_bound(a,a+i,a[i]);
		cout << t << ' ' << pos - a << endl;
		memmove(pos + 1,pos,(a+i-pos) * sizeof(a[0]));
		*pos = t;
	}
}


