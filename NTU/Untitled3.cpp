#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <string>

#define fori(i,__z) for (int i=0;i<(__z);i++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
int idx[128],n,k,rs=-1;
char str[100],tmp[100];

class Node {
	private:
		Node *ch[4]; // Array of 4 pointers
		int count; // Appear count
		int id = -1; // Last string id
	public:
		Node() {
			count = 0;
			fori(i,4)ch[i]=NULL;
		}
		void push(char *s,int id,int depth){
			if (*s==0)
				return;
			char c = *s;
			if (ch[idx[c]] == NULL)
				ch[idx[c]] = new Node();
			tmp[depth] = *s;tmp[depth+1] = 0;
			depth++;
			if (ch[idx[c]]->id != id){
				ch[idx[c]]->count++;
				ch[idx[c]]->id = id;
				if (ch[idx[c]]->count>=k && depth>rs){
					rs = depth;
				}
			}
			ch[idx[c]]->push(s+1,id,depth);
		}
		~Node() {
			for (int i=0;i<4;i++){
				if (ch[i] != NULL){
					delete ch[i];
				}
			}
		}
		
};


int main(){
//	freopen("input.txt","r",stdin);
	memset(idx,-1,sizeof idx);
	idx['A'] = 0;idx['X'] = 1;idx['T'] = 2;idx['G'] = 3;
	scanf("%d%d",&n,&k);
	Node *root = new Node();
	// Start job
	gets(str); // buffer
	fori(i,n){
		gets(str);
		fori(j,strlen(str)){
			root->push(str+j,i,0);
		}
	}
	// End job
	delete root;
	printf("%d",rs);
	return 0;
}


