#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 103;

int n,m,a[MAX][MAX],b[MAX][MAX],c[MAX][MAX];
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	cin.sync_with_stdio(0);
	cin.tie(0);

	cin >> n >> m;
	memset(b,0,sizeof b);
	memset(a,0,sizeof a);
	memset(c,0,sizeof c);
	_for(i,1,n+1) _for(j,1,m+1){
		cin >> a[i][j];
	}
	_for(i,1,n+1) _for(j,1,m+1) _for (k,j,m+1){
//		b[i][k]=max(max(b[i][k-1],b[i-1][k]),max(b[i][k],b[i-1][k-j]+a[i][j]));
		if (b[i][k] < b[i][k-1]){ // -> left
			b[i][k] = b[i][k-1];
			c[i][k] = c[i][k-1];
		}
		if (b[i][k] < b[i-1][k]){ // -> above
			b[i][k] = b[i-1][k];
			c[i][k] = 0;
		}
		if (b[i][k] < b[i-1][k-j] + a[i][j]){
			b[i][k] = b[i-1][k-j] + a[i][j];
			c[i][k] = j;
		}
	}
//	_for(i,1,n+1) {
//		_for(j,1,m+1){
//			cout << b[i][j] << ' ';
//		}
//		cout << '\n';
//	}
	cout << b[n][m] << endl;
	stack<int> s;
	int j = m;
	_for(i,n,0){
		s.push(c[i][j]);
		j -= c[i][j];
	}
	while (!s.empty()){
		cout << s.top() << '\n';
		s.pop();
	}
}


