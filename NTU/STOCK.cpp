#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,w,a[101],b[101];


int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> w;
    _for(i,0,n){
    	cin >> a[i];
		b[i]=a[i];
	}
    _for(i,1,n){
    	b[i]=min(b[i-1],b[i]);
	}
	LL res = 0;
	_for(i,0,n) res = max(res,(1LL*w/b[i])*(a[i]-b[i]));
	cout << res;
    return 0;
}

