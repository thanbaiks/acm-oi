#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e5+5;

int n,m;
int BIT[MAX];

void update(int x){
	while (x>0){
		BIT[x]++;
		x -= x&-x;
	}
}

int get(int x){
	int res = 0;
	while (x<MAX){
		res += BIT[x];
		x += x&-x;
	}
	return res;
}

int main(){
//	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);
	cin >> n;
	memset(BIT,0,sizeof BIT);
	
	LL res = 0;
	_for(i,0,n){
		cin >> m;
		res += get(m+1);
		update(m);
	}
	cout << res << endl;
}


