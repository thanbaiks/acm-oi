#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef pair<int,int> PII;
typedef map<int,vector<PII> > MAP;

const int MAX = 1e4+5;
int d[3][MAX]; // Tu nha(xe) - den truong (dibo) - den co quan (xe)
int n,m,k;
MAP tunha,dentruong,dencoquan;

void dij(int st,int d[],MAP &m){
	bool vis[MAX] = {false};
	d[st]=0;
	priority_queue<PII,vector<PII>, greater<PII> > q;
	q.push(make_pair(0,st));
	vector<PII>::iterator it;
	while (!q.empty()){
		st = q.top().second;
		q.pop();
		if (vis[st]) continue;
		vis[st]=true;
		for (it = m[st].begin();it!=m[st].end();++it){
			if (d[it->first] > d[st]+it->second){
				d[it->first] = d[st]+it->second;
				q.push(make_pair(d[it->first],it->first));
			}
		}
	}
}

int main(){
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> m >> k;
    _for(i,0,m){
    	int u,v,x,y;
    	cin >> u >> v >> x >> y;
    	dentruong[v].push_back(make_pair(u,x));
    	tunha[u].push_back(make_pair(v,y));
    	dencoquan[v].push_back(make_pair(u,y));
	}
	_for(i,0,3)_for(j,0,n+1) d[i][j]=1e9;
	dij(1,d[0],tunha);
	dij(k,d[1],dentruong);
	dij(n,d[2],dencoquan)
	int res = INT_MAX;
	_for(i,1,n+1){
		if (d[0][i]+d[1][i]<60){
			res = min(res,d[0][i]+d[2][i]);
		}
	}
	cout << res;
    return 0;
}
