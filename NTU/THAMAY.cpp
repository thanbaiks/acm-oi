#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,k,a[2002];

int main(){
//	freopen("input.txt","r",stdin);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n >> k;
	_for(i,0,n)cin >> a[i];
	sort(a,a+n);
	int t = n-1;
	LL res = 0;
	do {
		res += 2*(a[t]-1);
		t -= k;
	} while (t>=0);
	cout << res;
	return 0;
}

