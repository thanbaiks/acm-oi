#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef pair<int,int> PII;

struct S{
	int s,d,i;
	bool operator< (const S &b)const{
		return s < b.s || (s == b.s && d > b.d);
	}
};

int n,a,b;
S arr[100005];
map<int, int> _map;
queue<int> _q;

int main(){
	freopen("input.txt","r",stdin);
	scanf("%d%d%d",&n,&a,&b);
	int u,v;
	
	fori(i,n){
		scanf("%d%d",&arr[i].s,&arr[i].d);
		arr[i].i = i;
	}
	
	sort(arr,arr+n);
//	fori(i,n){
//		printf("%d - %d - %d\n",arr[i].s,arr[i].d,arr[i].i);
//	}
	
	//--
	int i=0,j=-1,jj,k=0;
	for(;i<n;i++){
		S s = arr[i];
		if (s.s > a)
			break;
		else if (s.d < a)
			continue;
		if (s.d > j)
			j = s.d, k = i;
	}
	if (j==-1){
		printf("-1\n");
		return 0;
	}
	_q.push(k); // pick first
	bool found = false;
	if (j>=b)
		found = true;
	jj = j;
	for (;i<n && !found;i++){
		S s = arr[i];
//		printf("- %d %d %d %d %d\n",s.s,s.d,j,jj,k);
		if (s.s > j){
			if (s.s > jj){
				printf("-1");
				return 0;
			}else{
				// Pick next
				_q.push(k);
				j = jj;
			}
		}
		if (s.d >= b){
			_q.push(i);
			found = true;
			break;
		}
		if (s.d > jj)
			jj = s.d, k = i;
	}
	if (found){
		printf("%d\n",_q.size());
		while (!_q.empty()){
			printf("%d ",arr[_q.front()].i+1);
			_q.pop();
		}
	}else{
		printf("-1");
	}
	return 0;
}


