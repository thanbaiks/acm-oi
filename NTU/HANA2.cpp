#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)
#define MAXN 205
using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MOD = 1e7;

int a[MAXN][MAXN];
int b[MAXN][MAXN];
int c[MAXN][MAXN];
int n,m;

int main(){
//	freopen("input.txt","r",stdin);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> n >> m;
	memset(a,0,sizeof a);
	memset(b,0,sizeof b);
	memset(c,0,sizeof c);
	fori(i,n)
	fori(j,m)
		cin >> a[i+1][j+1];
	
	// BASE 1 arrays
	b[1][1] = 1;
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			if (a[i][j]<=0)continue;
			b[i][j] = (b[i-1][j]+b[i][j-1])%MOD;
		}
	}
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			if (a[i][j]<=0)continue;
			
			if (a[i-1][j]>0 && a[i-1][j]!=a[i][j])
				c[i][j] += b[i-1][j];
			else
				c[i][j] += c[i-1][j];
				
			if (a[i][j-1]>0 && a[i][j-1]!=a[i][j])
				c[i][j] += b[i][j-1];
			else
				c[i][j] += c[i][j-1];
			c[i][j] %= MOD;
				
		}
	}
	cout << (c[n-1][m]+c[n][m-1]) % MOD;
	return 0;
}


