#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

bool b[256];
char s[105];
int cnt;

int main(){
//	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);
	cin.getline(s,105);
	memset(b,0,sizeof b);
	cnt = 0;
	_for(i,0,strlen(s)){
		if (!b[s[i]]){
			b[s[i]]=true;
			cnt++;
		}
	}
	cout << cnt << endl;
}


