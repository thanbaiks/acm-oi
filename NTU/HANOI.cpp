#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int a[4],n,k;
int step = 0;

int next(int a,int b){
	if (a+b == 1)
		return 2;
	else if (a+b==2)
		return 1;
	else
		return 0;
		
}
void dump(){
	fori(i,3)
		cout << a[i] << ' ';
}

bool trans(int src,int tar,int n){
	if (n>1){
		if (trans(src,next(src,tar),n-1))
			return true;// already found here
	}
	a[src]--;a[tar]++;
	step++;
//	cout << "Move " << src << " to " << tar << endl;
	if (step == k){
		dump();
		return true;
	}
	if (n>1){
		if (trans(next(src,tar),tar,n-1))
			return true;
	}
	return false;
}
int main(){
//	freopen("input.txt","r",stdin);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n >> k;
	memset(a,0,sizeof a);
	a[0] = n;
	trans(0,1,n);
	return 0;
}


