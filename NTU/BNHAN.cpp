#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 250001;
int n,m,k,a[MAX];

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> m >> k;
    
    int t=0;
    _for(i,1,n+1) _for(j,1,m+1)
    	a[t++]=i*j;
    sort(a,a+t);
    cout << a[k-1];
    return 0;
}

