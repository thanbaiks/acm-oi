#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e5+5;
LL n,a[MAX],b[4][MAX];


int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n;
    _for(i,0,3) b[i][0] = INT_MIN;
    _for(i,1,n+1){
    	cin >> a[i];
    	b[1][i] = max(b[1][i-1],a[i]);
	}
	_for(mul,2,4){
		b[mul][1] = INT_MIN;
		_for(i,2,n+1){
			b[mul][i]=max(b[mul][i-1],b[mul-1][i-1]+a[i]*mul);
		}
	}
//	_for(i,1,4){
//		_for (j,1,n+1)
//			cout << b[i][j] << ' ';
//		cout << endl;
//	}
	cout << b[3][n];
    return 0;
}

