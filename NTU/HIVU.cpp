#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

struct P{
	int x,y;
} p[4];
double d[6];

double sqr(double d){
	return d*d;
}

double dist(P a,P b){
	return sqrt(sqr(b.x-a.x)+sqr(b.y-a.y));
}
bool check(){
	fori(i,3)
		if (d[i]!=d[i+1])
			return false;
	if (d[4] != d[5])
		return false;
	return true;
}

int main(){
//	freopen("input.txt","r",stdin);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	fori(i,4){
		cin >> p[i].x >> p[i].y;
	}
	int t = 0;
	for (int i=0;i<4;i++){
		for (int j=i+1;j<4;j++)
			d[t++] = dist(p[i],p[j]);
	}
	sort(d,d+t);
	if (check())
		cout << "YES";
	else
		cout << "NO";
	return 0;
}


