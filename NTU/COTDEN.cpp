#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <string>

using namespace std;
typedef long long LL;

LL m,n;
int main(){
	scanf("%lld%lld",&m, &n);
	m = m%2==0?m/2:m/2+1;
	n = n%2==0?n/2:n/2+1;
	printf("%lld",m*n);
	return 0;
}

