#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n;
vector<pair<int,int> > v;
int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n;
	int k = 2;
	while (n>1){
		int c = 0;
		while (n%k==0)
			c++,n/=k;
		if (c)
			v.push_back(make_pair(k,c));
		k++;
	}
	fori(i,v.size())
		cout << v[i].first << ' ' << v[i].second << endl;
	return 0;
}


