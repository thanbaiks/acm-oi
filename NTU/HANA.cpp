#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,m,a[22][22];
char ac[10]; // accepts

struct Trace {
	int i,j;
} trace[22][22];

int main(){
//	freopen("input.txt","r",stdin);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> n >> m;
	fori(i,n){
		fori(j,m){
			cin >> a[i][j];
			if (a[i][j]>0)
				a[i][j] = 1<<(a[i][j]-1);
		}
	}
	memset(ac,1,sizeof(ac));
	for (int i=1<<2;;i=i>>1){
		ac[i] = false;
		if (!i)
			break;
	}
//	for (int i=0;i<=7;i++){
//		cout << i << '\t' << (int)ac[i] << '\n';
//	}
	fori(i,n){
		fori(j,m){
			if (a[i][j]<0 || i+j==0)
				continue;
			bool chk = false;
			if (i>0 && a[i-1][j]>=0){
//				cout << "D " << i << " " << j << "pick top" << endl;
				trace[i][j].i = i-1;
				trace[i][j].j = j;
				a[i][j] |= a[i-1][j];
				chk = true;
			}
			if (!ac[a[i][j]] && j>0 && a[i][j-1]>=0){
//				cout << "D " << i << " " << j << "pick left" << endl;
				trace[i][j].i = i;
				trace[i][j].j = j-1;
				a[i][j] |= a[i][j-1];
				chk = true;
			}
			if (!chk){
//				cout << "D " << i << " " << j << "chk = false" << endl;
				a[i][j]=-1;
			}
		}
	}
	
	
//	fori(i,n){
//		fori(j,m)
//			cout << a[i][j] << "\t";
//		cout << '\n';
//	}
	
	if (!ac[a[n-1][m-1]]){
		cout << -1 << endl;
	}else{
		stack<pair<int,int> > s;
		m--;n--;
		while (true){
			s.push(make_pair(n,m));
			if (m+n==0)
				break;
			Trace t = trace[n][m];
			n = t.i;
			m = t.j;
		}
		while (!s.empty()){
			cout << s.top().first + 1<< ' ' << s.top().second + 1 << endl;
			s.pop();
		}
	}
	return 0;
}


