#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;


int n,a,b;

int main(){
//	freopen("input.txt","r",stdin);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n >> a >> b;
	if (n%__gcd(a,b)){
		cout << -1;
		return 0;
	}
	if (a>b) swap(a,b);
	for (int i=n/b;i>=0;i--){
		if ((n-i*b)%a==0){
			cout << (i+(n-i*b)/a);
			return 0;
		}
	}
	cout << -1;
	return 0;
}

