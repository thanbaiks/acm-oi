#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

struct S {
	int a,b;
	void set(int _a,int _b){
		a = _a; b = _b;
	}
	bool operator < (const S &s) const {
		return b > s.b || (b == s.b && a > s.a);
	}
};

const int MAX = 105;

S lx[MAX];
int n,a = 0,b = 0;

int main(){
//	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);
	cin >> n;
	_for(i, 0, n)
		cin >> lx[i].a >> lx[i].b;
	sort(lx,lx+n);
	b = 1;
	_for (i,0,n){
		a += lx[i].a;
		b += lx[i].b;
		if (!--b)
			break;
	}
	cout << a << endl;
}


