#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,m,a[100005];

int main(){
//	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);
	
	cin >> n >> m;
	int j = 0;
	_for(i,0,n){
		cin >> a[i];
		while (a[i]-a[j] > m) j++;
		if (a[i]-a[j]==m){
			cout << a[j]  << ' ' <<  a[i] << endl;
			return 0;
		}
	}
	cout << -1 << endl;
}


