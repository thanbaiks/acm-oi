#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef pair<int,int> PII;

struct Q {
	int i,u,v;
	bool operator< (const Q &b) const{
		return i > b.i;
	}
};

const int dt[2][4] = {
	{0,0,1,-1},
	{1,-1,0,0}
};

int a[11][11][11][11],d[11][11],vis[11][11];
int n,m;

bool valid(int u,int v){
	return u>=0 && u<n && v>=0 && v<m;
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> m;
    int t;
	memset(a,0,sizeof a);
    _for (i,0,n) _for(j,0,m-1){
    	cin >> t;
    	a[i][j][i][j+1] = a[i][j+1][i][j] = t;
	}
    _for (i,0,n-1) _for(j,0,m){
    	cin >> t;
    	a[i][j][i+1][j] = a[i+1][j][i][j] = t;
	}
	
	// Dijstra
	priority_queue<Q,vector<Q> > q;
	q.push((Q){0,0,0});d[0][0] = 0;
	memset(d,0,sizeof d);
	memset(vis,0,sizeof vis);
	while (!q.empty()){
		Q cur = q.top();q.pop();
		if (vis[cur.u][cur.v]) // already visited
			continue;
		vis[cur.u][cur.v] = true;
		_for(i,0,4){
			int u = cur.u+dt[0][i], v = cur.v + dt[1][i]; // next
			if (valid(u,v) && (!d[u][v] || d[cur.u][cur.v]+a[cur.u][cur.v][u][v]<d[u][v])){
				d[u][v] = d[cur.u][cur.v]+a[cur.u][cur.v][u][v];
				q.push((Q){d[u][v],u,v});
			}
		}
	}
	cout << d[n-1][m-1] << endl;
	
    return 0;
}

