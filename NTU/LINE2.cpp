#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,sx,sy,dx,dy;
char a[11][11];
struct Trace{
	int x,y;
} t[11][11];

bool dfs(int y,int x){
	if (a[y][x])
		return false;
	if (y == dy && x == dx)
		return true;
	a[y][x] = 1;
//	cout << "DFS: " << y << ":" << x << endl;
	for (int i=-1;i<2;i++){
		for (int j=-1;j<2;j++){
			if (i*j!=0||i+j==0||a[y+i][x+j])
				continue;
			t[y+i][x+j].y = y;t[y+i][x+j].x = x;
			if (dfs(y+i,x+j))
				return true;
		}
	}
	return false;
}

int main(){
//	freopen("input.txt","r",stdin);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n >> sy >> sx >> dy >> dx;
	memset(a, 1,sizeof a);
	memset(t, 0,sizeof t);
	
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++){
		cin >> a[i][j];
		a[i][j] -= '0';
	}
	a[sy][sx] = 0; // for dfs
	
	if (dfs(sy,sx)) {
		stack<pair<int,int> > _s;
		
		while (1){
			_s.push(make_pair(dy,dx));
			if (dy==sy && dx==sx)
				break;
//			cout << dy << " " << dx << ' ' << sy << ' ' << sx << endl;
//			if (dy+dx==0)
//				break;
			Trace trace = t[dy][dx];
			dy = trace.y;
			dx = trace.x;
		}
		cout << _s.size() << endl;
		while (!_s.empty()){
			cout << _s.top().first << ' ' << _s.top().second << endl;
			_s.pop();
		}
	} else {
		cout << 0 << endl;
	}
	
	return 0;
}


