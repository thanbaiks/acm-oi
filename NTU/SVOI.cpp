#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e3+5;
int n,T,P;
struct S {
	int a,b,c,d;
} a[MAX];

int main(){
//	freopen("input.txt","r",stdin);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> T >> P;
    _for(i,0,n) cin >> a[i].a >> a[i].b >> a[i].c >> a[i].d;
	int res = 0;
    while (true){
    	bool f = false;
    	_for(i,0,n){
    		if (a[i].b<=P && a[i].a <=T){
    			res++;
    			T += a[i].c;
    			P += a[i].d;
    			f = true;
    			swap(a[i],a[n-1]);
    			n--;
    			break;
			}
		}
		if (!f)
			break;
	}
    cout << res;
    return 0;
}

