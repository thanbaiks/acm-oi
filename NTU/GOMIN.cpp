#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1005;
int n,a[MAX],t,m,k;

int main(){
//	freopen("input.txt","r",stdin);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> t;
    _for(i,0,n){
    	cin >> a[i];
	}
	cin >> k;
	m = a[k-1];
	sort(a,a+n);
	int res = a[n-1]-a[0];
	if (t < min(m-a[0],a[n-1]-m)){
		res += min(m-a[0],a[n-1]-m);
	}
	cout << res;
    return 0;
}

