#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,a[10];
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n;
	if (!n){
		cout << 10 << endl;
		return 0;
	}else if (n==1){
		cout << 1 << endl;
		return 0;
	}
	int i=9;
	memset(a,0,sizeof a);
	while (n>1 && i>1){
		if (n%i==0){
			a[i]++;
			n /= i;
		}else{
			i--;
		}
	}
	if (n>1){
		cout << -1;
	}else{
		_for(i,2,10){
			while (a[i]){
				a[i]--;
				cout << i;
			}
		}
	}
	return 0;
}

