#include <bits/stdc++.h>

#define fori(i,b) for (int i=0,_z=(b);i<(_z);i++)
#define ford(i,b) for (int i=(b);i>=0;i--)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

struct Point{
	int x,y;
} a[202];

int n,k,_min = 2e9+10;
char s[100005];

int dist(const Point &a,const Point b){
	return max(max(
		(abs(a.x-b.x)+1)/2,
		(abs(a.y-b.y)+1)/2
	)-1,0);
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> n >> k;
	fori(i,n) cin >> a[i].x >> a[i].y;
	// min dist
	for (int i=0;i<n;i++)
		for (int j=i+1;j<n;j++){
			_min = min(dist(a[i],a[j]),_min);
		}
//	cout << _min << endl;
	int cnt = 0;
	cin.getline(s,100005);
	cin.getline(s,100005);
	if (cnt>=_min){
		cout << 0 << endl;
		return 0;
	}
	fori(i,strlen(s)){
		cnt += s[i]-'0';
		if (cnt>=_min){
			cout << i+1 << endl;
			return 0;
		}
	}
	cout << -1 << endl;
	return 0;
}


