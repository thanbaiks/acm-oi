#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef pair<int,int> PII;


const int MAX = 1005;
int n,m,a[MAX][MAX],b[MAX][MAX];
bool vis[MAX][MAX];
int dif[2] = {1,2};
priority_queue<pair<int,PII> > q;

bool valid(int u,int v){
	return u>=0&&u<n&&v>=0&&v<m&&!vis[u][v];
}
void rot(int &u,int &v){
	int t = u;u=v;v=-t;
}
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> m;
    _for(i,0,n) _for(j,0,m)
    	cin >> a[i][j];
    memset(vis,0,sizeof vis);
    memset(b,0,sizeof b);
    
    
    q.push(make_pair(0,make_pair(0,0)));
    vis[0][0]=true;
    b[0][0]=1-a[0][0];
    while (!q.empty()) {
    	int u = q.top().second.first,v = q.top().second.second;q.pop();
//    	cout << u << ' ' << v << endl;
    	int du=1,dv=0;
    	_for(rep,0,4){
    		rot(du,dv);
	    	if (!valid(u+du,v+dv))
	    		continue;
	    	b[u+du][v+dv] = b[u][v] + dif[a[u][v]==a[u+du][v+dv]];
	    	vis[u+du][v+dv]=true;
    		q.push(make_pair(INT_MAX-b[u+du][v+dv],make_pair(u+du,v+dv)));
		}
	}
//    _for(i,0,n){
//		_for(j,0,m) {
//	    	cout << b[i][j] << ' ';
//	    }
//	    cout << endl;
//    }
	cout << b[n-1][m-1];
}

