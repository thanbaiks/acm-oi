#include<iostream>
#include<math.h>
#define ULL unsigned long long
using namespace std;

int main(){
	ULL n,t,b[32];
	for (int i=0;i<32;i++){
		b[i] = (1LL<<i)-1;
	}
	scanf("%llu",&t);
	while(t--){
		scanf("%llu",&n);
		if(n==0)
			cout<<1<<endl;
		else{
			cout<<b[log2(n)+1]-n<<endl;
		}
	}
}
