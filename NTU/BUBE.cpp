#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e5+5;
int n,k,a[MAX];
vector<int> v;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n >> k;
	_for (i,0,n){
		cin >> a[i];
	}
	sort(a,a+n);
//	_for(i,0,n) cout << a[i] << ' ';return 0;
	v.push_back(a[0]);
	_for(i,1,n){
		int ex = a[i]-k;
		vector<int>::iterator vi = upper_bound(v.begin(),v.end(),ex);
		if (vi != v.begin()){
			v.erase(vi-1);
		}
		v.push_back(a[i]);
	}
	LL res = 0;
	_for(i,0,v.size()){
		res += v[i];
	}
	cout << res;
	return 0;
}

