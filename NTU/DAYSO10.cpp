#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 3005;
int n,a[MAX];
LL f[MAX][MAX] = {0};

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n;
    _for(i,1,n+1) cin >> a[i];
    LL _max = LLONG_MIN;
    _for(i,1,n){
    	_for(j,n,i){
    		f[i][j] = 1LL*a[i]*a[j];
    		if (f[i-1][j+1]>0)
    			f[i][j] += f[i-1][j+1];
    		_max = max(_max,f[i][j]);
		}
	}
	cout << _max;
    return 0;
}

