#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const LL MOD = 1e9+7;

double a,b;
LL sum(int x){
	if (x<=1)
		return x;
	LL res = (1LL*x*(x+1)/2)%MOD;
	return ((res*(2*x+1))/3)%MOD;
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> a >> b;
    int u = ceil(a),v = floor(b);
    LL res = sum(v) - sum(u-1);
    if (res<0) res += MOD;
    cout << res;
    return 0;
}

