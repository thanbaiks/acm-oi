#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e5+5;
int n,a[MAX];

int main(){
//	freopen("input.txt","r",stdin);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n;
	_for(i,0,n) cin >> a[i];
	sort(a,a+n);
	LL res = 0;
	LL c = 1;
	_for(i,1,n+1){
		if (i==n || a[i] != a[i-1])
			res += c*(c-1)/2,c=1;
		else
			c++;
	}
	cout << res;
	return 0;
}

