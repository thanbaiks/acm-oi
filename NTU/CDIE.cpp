#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int a[55],n,m;
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	cin.sync_with_stdio(0);
	cin.tie(0);

	cin >> n >> m;
	_for(i,0,n)
		cin >> a[i];
	int cnt = 0, r = 1;
	sort(a,a+n,greater<int>());
	while (r < m && cnt < n){
		r = r+a[cnt++]-1;
	}
	if (cnt == n)
		cout << -1;
	else
		cout << cnt;
}


