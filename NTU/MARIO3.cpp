#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MOD = 1e9;

int n,a[1005],b[1005];

int getb(int i){
	if (i>=0)
		return b[i];
	else return 0;
}

int main(){
//	freopen("input.txt","r",stdin);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n;
	a[0]=0;
	for (int i=1;i<=n;i++)
		cin >> a[i];
	a[++n]=0;
	
	memset(b,0,sizeof b);
	b[0]=1;
	for (int i=1;i<=n;i++){
		if (a[i]==2)
			continue;
		if (a[i]==1){
			b[i]=b[i-1];
		}else{
			b[i]=b[i-1];
			if (i>1)
				b[i]+=b[i-2];
			b[i]%=MOD;
			if (i>2 && a[i-3]==0)
				b[i]+=b[i-3];
			b[i]%=MOD;
		}
	}
	cout << b[n];
	return 0;
}


