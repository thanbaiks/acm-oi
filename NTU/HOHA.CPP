#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,s;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n;s=1;
	int i=2;
	for (;i*i<n;i++){
        if (n%i==0) {
            s+=i+n/i;
        }
	}
	if (i*i==n)
        s+=i;
    cout << (s==n?"YES":"NO");
	return 0;
}
