#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 105;
int n,m,W[MAX],V[MAX];
int a[MAX][105];

int main(){
//	freopen("input.txt","r",stdin);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> m;
    _for(i,1,n+1) cin >> W[i] >> V[i];
    memset(a,0,sizeof a);
    _for(i,1,n+1){
    	_for(j,1,m+1){
    		if (W[i]>j){
    			a[i][j]=a[i-1][j];
			}else{
				a[i][j]=max(a[i-1][j],a[i-1][j-W[i]]+V[i]);
			}
		}
	}
	cout << a[n][m] << endl;
	while (n>0){
		if (a[n][m]==a[n-1][m]){
			n--;
		}else{
			cout << n << ' ';
			m -= W[n];
			n--;
		}
	}
    return 0;
}

