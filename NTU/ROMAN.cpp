#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

char s[1005];
int v[128];

void make(){
	memset(v,0,sizeof v);
	v['I'] = 1;
	v['V'] = 5;
	v['X'] = 10;
	v['L'] = 50;
	v['C'] = 100;
	v['D'] = 500;
	v['M'] = 1000;
}

int main(){
//	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);
	make();
	cin >> s;
	int len = strlen(s);
	int res = 0;
	_for(i,0,len){
		if (i < len-1 && v[s[i]] < v[s[i+1]])
			res -= v[s[i]];
		else
			res += v[s[i]];
	}
	cout << res << endl;
}


