#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 105;

int a[MAX],x[MAX],n,_min = INT_MAX,s;

int main(){
//	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);
	cin >> n;
	_for(i,0,n-1){
		cin >> a[i];
	}
	_for(i,0,n){
		cin >> x[i];
	}
	s = 0;
	_for(i,0,n-1){
		_min = min(_min,x[i]);
		s += _min * a[i];
	}
	cout << s << endl;
}


