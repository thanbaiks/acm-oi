#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

struct S {
	int t,d;
};

bool cmp(const S &a,const S &b){
	return a.t < b.t;
}


const int MAX = 1e5+5;
int n;
S a[MAX];

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n;
    _for(i,0,n){
    	cin >> a[i].t;
	}
    _for(i,0,n){
    	cin >> a[i].d;
	}
	sort(a,a+n,cmp);
	priority_queue<int,vector<int>,greater<int> > q;
	int res = 0;
	_for (i,0,n){
		while (!q.empty() && q.top() <= a[i].t) q.pop();
		q.push(a[i].t+a[i].d);
		res = max(res,(int)q.size());
	}
	cout << res;
}

