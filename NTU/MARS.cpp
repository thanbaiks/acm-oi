#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1005;
int dd[MAX][MAX],n;


int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n;
    memset(dd,0,sizeof dd);
    int t,res=0;
    _for (i,1,n+1){
    	_for(j,1,n+1){
    		cin >> t;
    		if (!t){
    			dd[i][j]=1+min(dd[i-1][j-1],min(dd[i][j-1],dd[i-1][j]));
    			res = max(res,dd[i][j]);
			}
		}
	}
	cout << res;
    return 0;
}

