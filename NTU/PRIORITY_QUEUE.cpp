#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
priority_queue<int,vector<int>, greater<int> > _q;

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	srand(1995);
	_for(i,0,100)
		_q.push(rand());
	while (!_q.empty()){
		cout << _q.top() << ' ';
		_q.pop();
	}
	return 0;
}


