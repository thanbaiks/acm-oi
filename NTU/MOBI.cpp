#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef pair<int,int> PII;

const int MAX = 1e6+1;

int n,k;
PII a[MAX];

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    
    cin >> n >> k;
    LL res = 0,tmp = 0;
    _for(i,0,n){
    	int x,y;
    	cin >> x >> y;
    	a[i] = make_pair(x,y);
    }
    sort(a,a+n);
    int j = 0;
    _for(i,0,n){
    	tmp += a[i].second;
    	while (a[i].first - 2*k > a[j].first) tmp-=a[j].second,j++;
    	res = max(res,tmp);
    }
    cout << res;
    return 0;
}

