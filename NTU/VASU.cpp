#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,a[105];

	
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> n;
	fori(i,n) cin >> a[i];
	sort(a,a+n,greater<int>());
	int i=0,cnt=0;
	while (i<n){
		if (a[i]-i<=0)
			break;
		cnt += a[i]-i;
		i++;
	}
	cout << cnt << endl;
	return 0;
}


