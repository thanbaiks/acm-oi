#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e3+3;

int n;
LL a[MAX],_max = LLONG_MIN;

int main(){
//	freopen("input.txt","r",stdin);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n;
	_for(i,0,n) cin >> a[i];
	_for(i,0,n){
        LL t = 0;
        _for(j,i,-1){
            t += a[j];
            if ((i-j+1)%3==0){
                _max = max(_max,t);
            }
        }
	}
	cout << _max << endl;
	return 0;
}
