#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 5e5+5;
int n,s[MAX],e[MAX];

int main(){
//	freopen("input.txt","r",stdin);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n;
    _for(i,0,n) cin >> s[i] >> e[i];
    sort(s,s+n);
	sort(e,e+n);
    LL res = 0, cnt = 0,i=0,j=0;
    while (j<n){
    	if (i>=n || e[j]<s[i]){
    		j++;cnt--;
		}else if (s[i]<e[j]){
			res += cnt;
			i++;cnt++;
		}else{
			i++;j++;
		}
	}
	cout << res;
    return 0;
}

