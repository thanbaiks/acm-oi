#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int a[100][100];
void fill(int x,int y,int &st,int size){
	if (size<=0)
		return;
	if (size==1){
		a[y][x] = st;
		return;
	}
	for (int t=size-1;t>0;t--){
		a[y][x]=st;st++;
		x++;
	}
	for (int t=size-1;t>0;t--){
		a[y][x]=st;st++;
		y++;
	}
	for (int t=size-1;t>0;t--){
		a[y][x]=st;st++;
		x--;
	}
	for (int t=size-2;t>0;t--){
		a[y][x]=st;st++;
		y--;
	}
	a[y][x]=st;st++;
	fill(x+1,y,st,size-2);
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	int n,st=1;
	cin >> n;
	fill(0,0,st,n);
	fori(i,n){
		fori(j,n) cout << a[i][j] << ' ';
		cout << endl;
	}
	return 0;
}


