#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,a[1010],s;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n;
	s = 0;
	a[0]=0;
	_for(i,0,n){
		cin >> a[i];
		s += a[i];
	}
	_for(i,n,1){
		if (s%i) continue;
		int t = s/i, k = 0;
		_for(j,0,n){
			if (k+a[j]>t){
				k = -1;
				break;
			} else {
				k = (k+a[j])%t;
			}
		}
		if (k==0){
			cout << i << endl;
			return 0;
		}
	}
	cout << 1 << endl;
	return 0;
}

