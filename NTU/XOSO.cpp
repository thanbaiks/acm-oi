#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e6 + 5;

int n;
int a[MAX],b[2];
LL sum,cnt;

int main(){
//	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);
	
	cin >> n;
	sum = 0;
	_for(i,0,n){
		cin >> a[i];
		sum += a[i];
	}
	sum %= 2;cnt=b[0]=b[1]=0;
	_for(i,0,n){
		if (sum){ // Le
			cnt += b[1-a[i]%2];
		}else{
			cnt += b[a[i]%2];
		}
		b[a[i]%2]++;
	}
	cout << cnt << endl;
}


