#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,_min,rs;
int main(){
//	freopen("input.txt","r",stdin);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n;
	int x;
	cin >> x;
	_min=x;n--;
	rs = -2e9-9;
	while (n--){
		cin >> x;
		if (x < _min){
			_min = x;
		} else if (x-_min > rs){
			rs = x - _min;
		}
	}
	cout << rs;
	return 0;
}


