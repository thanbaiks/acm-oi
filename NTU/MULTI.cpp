#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e5+5;
int n,m,am[MAX],khong[MAX];


int main(){
//	freopen("input.txt","r",stdin);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    memset(am,0,sizeof am);
    memset(khong,0,sizeof khong);
    int t,u;
    cin >> n >> m;
    _for(i,1,n+1){
    	cin >> t;
    	khong[i]=khong[i-1];am[i]=am[i-1];
    	if (!t)khong[i]++; else if (t<0) am[i]++;
	}
	_for(i,0,m){
		cin >> t >> u;
		if (khong[u]-khong[t-1])
			cout << '0' << endl;
		else if ((am[u]-am[t-1])%2)
			cout << '-' << endl;
		else
			cout << '+' << endl;
	}
    return 0;
}

