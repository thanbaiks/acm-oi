#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,m,W[22],V[22],_max;
bool mask[22],res[22];

void try_(const int &i,const int &w,const int &v){
	if (i==n){
		if (v > _max){
			_max = v;
			memcpy(res,mask,sizeof mask);
		}
	} else {
		try_(i+1,w,v);
		if (w+W[i]<=m){
			mask[i]=true;
			try_(i+1,w+W[i],v+V[i]);
			mask[i]=false;
		}
	}
}

int main(){
//	freopen("input.txt","r",stdin);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> m;
    _for(i,0,n) cin >> W[i] >> V[i];
    _max = 0;
    memset(mask,0,sizeof mask);
    memset(res,0,sizeof res);
    try_(0,0,0);
    cout << _max << endl;
    _for(i,0,n){
    	if (res[i])
    		cout << i+1 << ' ';
	}
    return 0;
}

