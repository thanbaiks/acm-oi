#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
map<int,int> _map;
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	cin.sync_with_stdio(0);
	cin.tie(0);

	_map[100] = 123;
	_map[-100] = -2135;
	_map[1000] = -100000;
	_map[0] = 1;
	
	_map.erase(_map.find(0));
	
	for (map<int,int>::iterator i = _map.begin();i != _map.end();++i){
		cout << (*i).first << ' ' << (*i).second << endl;
	}
}


