#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int INF = 1e9+5;

struct Node {
	int l,r,v;
};


int n,_max = 0;
Node t[100005];

int _(int i,int lo,int hi){
	if (t[i].v <= lo || t[i].v >= hi)
		return 0;
	int res = 1; // itself
	if (t[i].l && t[t[i].l].v < t[i].v) // has left && bigger than left
		res += _(t[i].l,lo,t[i].v);
	if (t[i].r && t[t[i].r].v > t[i].v) // ==
		res += _(t[i].r,t[i].v,hi);
	return res;
}

int main(){
//	freopen("input.txt","r",stdin);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n;
	for(int i=1;i<=n;i++)
		cin >> t[i].l >> t[i].r >> t[i].v;
	for(int i=1;i<=n&&_max<n;i++)
		_max = max(_(i,-INF,INF),_max);
	cout << _max << endl;
	return 0;
}
