#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e5+5;
LL n,q,a[MAX],b[MAX];
vector<int> v;

int main(){
	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);
	
	cin >> n;
	v.reserve(n+5);
	vector<int>::iterator it;
	_for(i,1,n+1){
		cin >> a[i];
		it = upper_bound(v.begin(),v.end(),a[i]);
		b[i] = b[i-1];
		if (it != v.end())
			b[i] += v.end()-it;
			
		v.insert(it,a[i]);
	}
	cout << b[n];
}


