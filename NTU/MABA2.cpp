#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 3e4+5;
int n,m,k;
short a[MAX],b[MAX];

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> m >> k;
    char c;int t;short *p;
	while (k--){
		cin >> c >> t;
		if (c=='H')
			p=a;
		else
			p=b;
		p[t]++;
		if (t)
			p[t-1]++;
	}
	int cnt[2][3] = {0};
	_for(i,0,n) cnt[0][a[i]]++;
	_for(i,0,m) cnt[1][b[i]]++;
	cout << cnt[0][0] * cnt[1][0] << ' '; // s0
	cout << cnt[0][1] * cnt[1][0] + cnt[0][0]*cnt[1][1] << ' '; // s1
	cout << cnt[0][1] * cnt[1][1] + cnt[0][0]*cnt[1][2] + cnt[0][2]*cnt[1][0]<< ' '; // s2
	cout << cnt[0][1] * cnt[1][2] + cnt[0][2]*cnt[1][1] << ' '; // s3
	cout << cnt[0][2] * cnt[1][2]<< ' '; // s4
    return 0;
}

