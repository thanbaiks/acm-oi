#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int BASE = 10102010;
const int MAX = 8000;

int n,res,a[MAX];
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	cin.sync_with_stdio(0);
	cin.tie(0);
	cin >> n;
	memset(a,0,sizeof a);
	int x;
	_for (i,0,n){
		cin >> x;
		a[x-BASE]++;
	}
	res = 0;
	_for(i,0,MAX){
		if (a[i])
			res += max(100,a[i]+95);
	}
	cout << res << endl;
}


