#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e5+5;

int a[MAX],st[MAX*4],n,m,L,R;

void make(const int& left,const int& right,const int& idx){
	if (left==right){
		st[idx] = a[left];
		return;
	}
	int m = (left+right)>>1;
	make(left,m,idx*2+1);
	make(m+1,right,idx*2+2);
	st[idx]=min(st[idx*2+1],st[idx*2+2]);
}

int rmq(const int& left,const int& right,const int& idx){
	if (right<L || left>R)
		return INT_MAX;
	if (left>=L && right <=R)
		return st[idx];
	int m = (left+right)>>1;
	return min(rmq(left,m,2*idx+1),rmq(m+1,right,2*idx+2));
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> n >> m;
	char *s = new char[n+1];
	cin >> s;
	a[0]=0;
	_for(i,0,n){
		a[i+1] = a[i]+(s[i]=='('?1:-1);
	}
	delete s;
	make(1,n,0);
	_for(i,0,m){
		cin >> L >> R;
		cout << (rmq(1,n,0)==a[R] && a[L-1]==a[R]?"YES":"NO") << endl;
	}
	return 0;
}


