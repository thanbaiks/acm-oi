#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <string>

#define fori(i,__z) for (int i=0;i<(__z);i++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int a[101][101];
int m,n,t;
int main(){
	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	memset(a,0,sizeof a);
	
	scanf("%d%d",&m,&n);
	fori(i,m) fori(j,n){
		scanf("%d",&a[i][j]);
		t = 0;
		if (i>0)
			t = a[i-1][j];
		if (j>0)
			t = max(t,a[i][j-1]);
		a[i][j]+=t;
	}
	printf("%d",a[m-1][n-1]);
	return 0;
}

