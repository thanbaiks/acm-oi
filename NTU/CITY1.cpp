#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e2+5;
int a[MAX][MAX],dd[MAX][MAX];
bool b[1000006];
int n,m,k;

int get(int i,int j){
	if (i>n) i = n;
	if (i<0) i = 0;
	if (j>m) j = m;
	if (j<0) j = 0;
	return dd[i][j];
}

int val(int i,int j){
	return get(i+1,j+1)-get(i+1,j-2)-get(i-2,j+1)+get(i-2,j-2)-a[i][j];
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> m >> k;
    memset(dd,0,sizeof dd);
    _for(i,1,n+1){
    	_for(j,1,m+1){
    		cin >> a[i][j];
    		dd[i][j]= dd[i][j-1]+dd[i-1][j]-dd[i-1][j-1]+a[i][j];		
		}
	}
	memset(b,0,sizeof b);
	_for(i,1,n+1){
		_for(j,1,m+1){
			b[val(i,j)]=true;
		}
	}
	_for(i,0,k){
		cin >> m;
		if (b[m])
			cout << 1 << endl;
		else
			cout << 0 << endl;;
	}
    return 0;
}

