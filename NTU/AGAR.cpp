#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
typedef pair<int,LL> PIL;

const int MAX = 1e5+5;
LL n,u,v;
vector<PIL> a;
queue<int> q;

bool cmp(PIL a, const int b){
	return a.first < b;
}

int main(){
	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> u >> v;
    if (u>v){
    	cout << 0;
    	return 0;
	}
    LL tmp;
    _for(i,0,n){
    	cin >> tmp;
    	a.push_back(make_pair(tmp,i+1));
	}
	sort(a.begin(),a.end());
	vector<PIL>::iterator it;
	while (!a.empty() && u <= v){
		it = lower_bound(a.begin(),a.end(),u,cmp);
		if (it == a.begin()){
			cout << -1;
			return 0;
		}
		it--;
		u += (*it).first;
		q.push((*it).second);
		a.erase(it);
	}
	if (u <= v){
		cout << -1;
	}else{
		cout << q.size() << endl;
		while (!q.empty()) {
			cout << q.front() << ' ';
			q.pop();
		}
	}
    return 0;
}

