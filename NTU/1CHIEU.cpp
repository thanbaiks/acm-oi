#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,m;
char s[2][105];

bool check(){
    if (s[0][0]==s[0][n-1])
    	return false;
    if (s[1][0]==s[1][m-1])
    	return false;
    if (s[0][0]=='R' && s[1][0] == 'D')
    	return false;
    if (s[0][0]=='L' && s[1][0] == 'U')
    	return false;
    return true;
}
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n >> m >> s[0] >> s[1];
    if (check())
    	cout << "YES";
    else
    	cout << "NO";
    return 0;
}

