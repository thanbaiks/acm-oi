#include <algorithm>
#include <bitset>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

#define BG begin()
#define ED end()
#define st first
#define nd second
#define PB push_back
#define PF push_front
#define FOR(i,a,b) for (long long i=a;i<b;i++)
#define FORE(i,a,b) for (long long i=a;i<=b;i++)
#define FORD(i,a,b) for (long long i=a;i>=b; i--)
#define TR(c, it) for(typeof((c).begin()) it=(c).begin(); it!=(c).end(); it++)
#define PI 2 * acos(0)

typedef long long ll;
typedef long double ld;
typedef unsigned long long ull;
typedef pair<int,int> II;
typedef pair<int,II> III;
typedef pair<ll,ll> LL;
const ll INF=1000000000+7;
const double esp=1e-13;

int test;
ull n;

ull mul(ull x, ull y, ull m){
    ull sum=0;
    while (y){
        if (y%2) sum=(sum+x)%m;
        x=x*2%m;
        y/=2;
    }
    return sum;
}

ull power(ull a, ull n, ull m){
    ull ans=1;
    while (n){
        if (n%2) ans=ans*a%m;
        a=a*a%m;
        n/=2;
    }
    return ans;
}

bool check(ull n){
    if (n==2) return true;
    if (n%2==0) return false;
    ull mx[7]={3,5,7,11, 13, 17,19};
    ull d = n-1;
    int s=0,r;
    while (d%2==0){
        d/=2;
        s++;
    }
    FORE(i,0,6){
        if (n==mx[i]) return true;
        if (n%mx[i]==0) return false;
        ull a=power(mx[i],d , n);
        if (a!=1){
            for(r=0; r<s && a!=n-1; r++) a=a*a%n;
            if (r==s) return false;
        }
    }
    return true;
}

int main(){
 //   freopen("1811.inp", "r", stdin);
 //   freopen("1811.out", "w", stdout);
    ios_base::sync_with_stdio (false);
    cin.tie();
    int l,r;
    int ans=0;
    cin >> l >> r;
    FORE(i,l,r) ans+=check(i);
    cout << ans;
}
    
