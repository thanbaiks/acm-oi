#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    int a[3],n;
    cin >> n;
	a[1]=a[2]=1;
	_for(i,1,n+1){
		if (i>2)
			a[i%3]=a[(i+1)%3]+a[(i+2)%3];
		cout << a[i%3] << ' ';
	}
    return 0;
}

