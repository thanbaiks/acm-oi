#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e6+10;

char s1[MAX],s2[MAX];

bool test(char *s,int len, int sz){
	for (int i=sz;i<len;i+=sz)
		if (memcmp(s,s+i,sz))
			return false;
	return true;
}

int main(){
//	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);

	cin >> s1 >> s2;
	int l1 = strlen(s1),l2 = strlen(s2),u = __gcd(l1,l2);
	if (memcmp(s1,s2,u) || !test(s1,l1,u) || !test(s2,l2,u)){
		cout << "NO";
	}else{
		s1[u]=0;
		cout << s1;
	}
}


