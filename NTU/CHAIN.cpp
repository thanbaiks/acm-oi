#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e5+5;
int n,a[MAX];

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cin >> n;
	_for(i,0,n)
		cin >> a[i];
	if (n==2){
		cout << 1;
		return 0;
	}
	sort(a,a+n);
	int k=n-1,i=0,j=0;
	while (j<k){
		int t = min(a[i],k-j);
		j += t; a[i] -= t;
		if (!a[i])
			k--;
		i++;
	}
	cout << j;
	return 0;
}

