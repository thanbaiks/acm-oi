#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e6+5;

char s[MAX];
int sum;
int main(){
//	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);
	cin >> s;
	sum = 0;
	_for(i,0,strlen(s))
		sum += s[i]-'0';
	int t = 0;
	while (sum >= 10){
		t = 0;
		while (sum)
			t += sum%10,sum/=10;
		sum = t;
	}
	cout << sum << endl;
}


