#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MAX = 1e5+5;
int a[MAX],b[MAX],n;

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	cin.sync_with_stdio(0);
	cin.tie(0);
	cin >> n;
	_for(i,0,n)
		cin >> a[i];
	_for(i,0,n)
		cin >> b[i];
	sort(a,a+n);sort(b,b+n);
	int i=0,j=0,c=0;
	while (i<n && j<n){
		if (a[i]<b[j])
			c++,i++,j++;
		else
			j++;
	}
	cout << c << endl;
	return 0;
}

