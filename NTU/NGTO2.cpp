#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

LL mul(LL a, LL b, LL mod)
{
    LL x = 0,y = a % mod;
    while (b > 0)
    {
        if (b % 2 == 1)
        {    
            x = (x + y) % mod;
        }
        y = (y * 2) % mod;
        b /= 2;
    }
    return x % mod;
}

LL pw(LL a, LL e, LL m){
	LL b = 1;
	while (e>1){
		if (e&1)
			b = mul(b,a,m);
		e>>=1;
		a=mul(a,a,m);
	}
	return mul(a,b,m);
}

const int RAB = 7;
const int rab[RAB] = {2,3,5,7,11,13,17};

bool test(LL n){
	if (n==2)
		return true;
	if (n<2||n%2==0)
		return false;
	LL m = n-1;
	int s = 0;
	while (m%2==0)
		s++, m>>=1;
	_for(i,0,RAB){
		int k = rab[i];
		if (n==k)
			return true;
		LL b = pw(k,m,n);
		if (b==1)
			continue;
		bool f = false;
		_for(i,0,s){
			if (b==n-1)
				f=true;
			b=mul(b,b,n);
		}
		if (!f)
			return false;
	}
	return true;
	
}

LL n,m;

int main(){
	cin.sync_with_stdio(0);
	cin.tie(0);
	cin >> n;
	m = 3;
	if (n==1){
		cout << 2;
		return 0;
	}
	n-=2;
	while (n--){
		m++;
		while (!test(m)) m++;
	}
	cout << m << endl;
}


