#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const double EPS = 1e-4;
double d[6][2],a[20];

double sqr(const double &a){
	return a*a;
}

double dis(double x1,double y1,double x2,double y2){
	return sqrt(sqr(x1-x2)+sqr(y1-y2));
}

bool check(){
	if (abs(a[0]-a[5]) > EPS)
		return false;
	if (abs(a[6]-a[11]) > EPS)
		return false;
	return abs(a[0]-a[5]) < EPS;
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    _for(i,0,6){
    	_for(j,0,2){
    		cin >> d[i][j];
		}
	}
	int t = 0;
    _for(i,0,5){
    	_for(j,i+1,6){
    		a[t++] = dis(d[i][0],d[i][1],d[j][0],d[j][1]);
		}
	}
	sort(a,a+t);
//	_for(i,0,t)
//		cout << a[i] << endl;
	if (check())
		cout << "YES";
	else
		cout << "NO";
    return 0;
}

