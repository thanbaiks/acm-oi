#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

LL k,n,m;
int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	cin >> k;
	k <<= 1;
	/**
	 * k	= n*m + (n-1)*(m-1)
	 * 		= 2*n*m-n-m+1
	 * 		= (2n-1)m-n+1
	 * m	= (k+n-1)/(2n-1)
	 */
	for (n=sqrt(k/2);n>1;n--){
		if ((k+n-1)%(2*n-1)==0){
			m = (k+n-1)/(2*n-1);
			cout << n-1 << ' ' << m-1 << endl;
			return 0;
		}
	}
	cout << -1 << endl;
	return 0;
}


