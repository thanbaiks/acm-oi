#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int VUONG = 1;
const int CHUNHAT = 2;
const int THOI = 3;
const int BINHHANH = 4;
const int KHAC = 5;

double sqr(const double &a){
	return a*a;
}

double dis(double x1,double y1,double x2,double y2){
	return sqrt(sqr(x1-x2)+sqr(y1-y2));
}
bool eq(const double &a,const double &b){
	return abs(a-b) < 1e-4;
}
double d[4][2],a[6];
int check(){
	if (eq(a[0],a[3])){
		if (eq(a[4],a[5])){
			return VUONG;
		} else {
			return THOI;
		}
	}else if (eq(a[0],a[1])&&eq(a[2],a[3])){
		if (eq(a[4],a[5])){
			return CHUNHAT;
		}else{
			return BINHHANH;
		}
	}
	if (eq(a[1],a[4]))
		return THOI;
	if (eq(a[3],a[4]) && (eq(a[0],a[1]) || eq(a[1],a[2])))
		return BINHHANH;
	return KHAC;
}


int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    _for(i,0,4){
    	cin >> d[i][0] >> d[i][1];
	}
	int n = 0;
	_for(i,0,3) _for(j,i+1,4){
		a[n++]=dis(d[i][0],d[i][1],d[j][0],d[j][1]);
	}
	sort(a,a+n);
//	_for(i,0,6)
//		cout << a[i] << endl;
	cout << check();
    return 0;
}

