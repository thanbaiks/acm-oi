#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

int n,a[100005];
int main(){
//	freopen("input.txt","r",stdin);
	cin.sync_with_stdio(0);
	cin.tie(0);

	cin >> n;
	_for(i,0,n) cin >> a[i];
	sort(a,a+n);
	int res = INT_MAX;
	_for(i,0,n-1)
		res = min(res,a[i+1]-a[i]);
	cout << res << endl;
}


