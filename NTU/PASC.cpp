#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

LL n,a[2][100];

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	cin.sync_with_stdio(0);
	cin.tie(0);
	
	cin >> n;
	memset(a,0,sizeof a);
	a[0][0]=a[1][0]=1;
	
	_for(i,0,n){
		int k = i%2;
		cout << 1 << ' ';
		_for(j,1,i+1){
			a[k][j]=a[1-k][j] + a[1-k][j-1];
			cout << a[k][j] << ' ';
		}
		cout << endl;
	}
}


