#include <bits/stdc++.h>

#define fori(i,__z) for (int i=0;i<(__z);i++)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int MOD = 1e4;

LL lt(LL x,LL n){
	if (!n)
		return 1;
	return (lt(x*x%MOD,n/2)*(n%2?x:1))%MOD;
}

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	LL x,n;
	cin >> x >> n;
	cout << lt(x,n);
	return 0;
}
