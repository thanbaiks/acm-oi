#include <iostream>
#define ui64 unsigned long long

using namespace std;

int n,k,m,test;
ui64 a[20];

int main(){
	cin >> test;
	for (int ti=1;ti<=test;ti++){
		// input
		cin >> n >> k >> m;
		n=m;
		for (int i=0;i<20;i++)
			a[i]=0;
		while (k--){
			a[0]++;
			a[1]++;
			ui64 tmp = a[0]+a[1];
			for (int i=2;i<n;i++){
				a[i]+=tmp;
				tmp+=a[i];
			}
		}
		// output
		cout << "Case #" << ti << ": " << a[m-1] << endl;
		//for (int i=0;i<20;i++)
		//	cout << "Slot " << i << " have " << a[i] << " rock(s)" << endl;
	}
	return 0;
}
