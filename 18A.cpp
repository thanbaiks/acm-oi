#include <iostream>
using namespace std;

int test,n,k,t;
char x,c;
int main(){
	cin >> test;
	for (int ti=1;ti<=test;ti++){
		// nhap n, k
		cin >> n >> k;
		// reset char x
		x = 0;
		while (n>0) {
			cin >> c >> t;
			if (k <= t && k > 0){
				x = c;
			}
			n-=t;k-=t;
		}
		cout << "Case #" << ti << ": " << x << endl;
	}
	return 0;
}
