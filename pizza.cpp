#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator

using namespace std;
typedef long long LL;
int main(){
	int r,w,h;
	int i = 1;
	while (cin >> r){
		
		cin >> w >> h;
		float f = sqrt(w*w+h*h);
		if (f <= r*2){
			cout << "Pizza " << i << " fits on the table." << endl;
		}else{
			cout << "Pizza " << i << " does not fits on the table." << endl;
		}
		i++;
	}
	return 0;
}

