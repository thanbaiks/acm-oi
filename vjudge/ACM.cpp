#include <iostream>
#define 	MAX		50000
using namespace std;
int main(){
	long n,q,i,j,h[MAX],max,min,st,en;
	cin >> n >> q;
	for (i=0;i<n;i++)
		cin >> h[i];
	for (i=0;i<q;i++){
		cin >> st >> en;
		max = 0;
		min = 1000001;
		for (j=st-1;j<en;j++){
			if (h[j]>max)max=h[j];
			if (h[j]<min)min=h[j];
		}
		cout << max-min << endl;
	}
	return 0;
}
