#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp fin
#define out fout
#define MAXN 1000000
using namespace std;
typedef unsigned long long ULL;
long long d[MAXN],n,m;

int main(){
	ifstream fin("HIGHWAY.INP");
	ofstream fout("HIGHWAY.OUT");
	inp >> n;
	for (ULL i=0;i<n;i++){
		inp >> d[i];
	}
	sort(d,d+n);
	m=10000000000;
	for (int i=0;i<n-1;i++){
		m = min(m,abs(d[i]-d[i+1]));
		if (!m){
			break;
		}
	}
	out << m;
	fin.close();
	fout.close();
	return 0;
}
