#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp fin
#define out fout
#define MAXM 1000000
using namespace std;
typedef unsigned long long ULL;

ULL m,n,k,c,p,d,q;
unsigned char v[MAXM];

void erase(ULL c,ULL p, ULL d, ULL q){
	unsigned char mask;
	for (ULL i=c;i<=d;i++){
		mask=0;
		if (i==c)
			mask |= (~0 << (p+1));
		if (i==d)
			mask |= ~(~0 << q);
		v[i]&=mask;
	}
}

bool erasable(ULL c,ULL p, ULL d, ULL q){
	unsigned char mask;
	for (ULL i=c;i<=d;i++){
		mask=0;
		if (i==c)
			mask |= (~0 << (p+1));
		if (i==d)
			mask |= ~(~0 << q);
		if (v[i]&~mask){
			return true;
		}
	}
	return false;
}
int main(){
	/* DECLARES */
	ifstream fin("STONE.INP");
	ofstream fout("STONE.OUT");
	
	/* INPUT */
	inp >> m >> n >> k;
	memset(v,0,sizeof(v));
	int tmp;
	for (ULL i=0;i<m;i++){
		inp >> tmp;
		v[i]=tmp;
	}
	/* first N people */
	ULL st,en;
	unsigned char mask;
	for (ULL i=0;i<n;i++){
		inp >> c >> p >> d >> q;
		erase(c,p,d,q);
	}
	/* next K people */
	for (ULL i=0;i<k;i++){
		inp >> c >> p >> d >> q;
		if (erasable(c,p,d,q)){
			out << "YES";
		}else{
			out << "PASS";
		}
		if (i<k-1)
			out << endl;
	}
	/* END ALL */
	fin.close();
	fout.close();
	return 0;
}
