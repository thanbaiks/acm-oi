#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;
typedef unsigned long long ULL;
long random(){
	long l = 0;
	for (int i=0;i<4;i++){
		l = l*256+rand()%256;
	}
	return abs(l);
}
void makeA(){
	ofstream out("HIGHWAY.INP");
	long n = 1000000;
	out << n << endl;
	for (long i=0;i<n;i++){
		out << random()%1000000000+1 << ' ';
	}
	out.close();
}
void makeB(){
	ofstream out("STONE.INP");
	long m = 1000000;
	long n = 100000;
	long k = 10;
	const long len = 100;
	out << m <<' '<<n<<' '<<k<< endl;
	for (long i=0;i<m;i++){
		out << rand()%256 << ' ';
	}
	out << endl;
	// n people
	long c,d,p,q;
	for (long i=0;i<n;i++){
		c = random()%(m-len);
		d = c+rand()%len;
		p = rand()%7;
		if (c==d){
			q = p+rand()%(7-p);
		}else{
			q = p+rand()%7;
		}
		out << c << ' ' << p << ' ' << d << ' ' << q << endl;
	}
	for (long i=0;i<k;i++){
		c = random()%(m-1001);
		d = c+rand()%1000;
		p = rand()%7;
		if (c==d){
			q = p+rand()%(7-p);
		}else{
			q = p+rand()%7;
		}
		out << c << ' ' << p << ' ' << d << ' ' << q << endl;
	}
	out.close();
}
void makeC(){
	ofstream out("SQ.INP");
	long n = 1000000;
	out << n << endl;
	for (long i=0;i<n;i++){
		out << random()%2000000-1000000 << ' ';
	}
	out.close();
}
int main(){
	srand (0xFAFAFA);
	makeA();
	makeB();
	makeC();
	return 0;
}
