#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp fin
#define out fout
#define MAXN 1000000

using namespace std;
typedef unsigned long long ULL;

struct SQ {
	long long val; // sq number
	ULL pre; // number of previus minus sq
	ULL count;// count bigger previous
	static SQ make(long long v,ULL p,ULL c){
		SQ s = {v,p,c};
		return s;
	}
};

long long a[MAXN];
vector<SQ> vt;
ULL n;

ULL solveA(){
	vt.clear();
	ULL count = 0;
	ULL sum = 0;
	for (ULL i=0;i<n;i++){
		if (a[i]<0)
			count++;
		else if (a[i]>0){
			if (vt.size()==0){
				// empty
				vt.push_back(SQ::make(a[i],count,0));
			}else{
				// find bigger pre  number
				long j;
				for (j=vt.size()-1;j>=0;j--){
					if (vt[j].val>a[i] && count-vt[j].count>=2){
						sum += vt[j].count+1;
						vt.push_back(SQ::make(a[i],count,vt[j].count+1));
						break;
					}
				}
				if (j<0){
					// not found
					vt.push_back(SQ::make(a[i],count,0));
				}
			}
		}
	}
	return sum;
}
ULL solveB(){
	vt.clear();
	ULL count = 0;
	ULL sum = 0;
	for (ULL i=0;i<n;i++){
		a[i]*=-1;
		if (a[i]<0)
			count++;
		else if (a[i]>0){
			if (vt.size()==0){
				// empty
				vt.push_back(SQ::make(a[i],count,0));
			}else{
				// find bigger pre  number
				long j;
				for (j=vt.size()-1;j>=0;j--){
					if (vt[j].val<a[i]){
						if (count-vt[j].count>=2)
							sum += vt[j].count+1;
						vt.push_back(SQ::make(a[i],count,vt[j].count+1));
						break;
					}
				}
				if (j<0){
					// not found
					vt.push_back(SQ::make(a[i],count,0));
				}
			}
		}
	}
	return sum;
	return 0;
}

int main(){
	ifstream fin("SQ.INP");
	ofstream fout("SQ.OUT");
	
	/* INPUT */
	inp >> n;
	for (ULL i=0;i<n;i++)
		inp >> a[i];
	ULL sa = solveA();
	ULL sb = solveB();
	out << sa << ' ' << sb;
	fin.close();
	fout.close();
	return 0;
}
