#include <iostream>
using namespace std;

int test,m,n,count[1000],picked[1000];
int main(){
	cin >> test;
	for (int ti=1;ti<=test;ti++){
		cin >> n >> m;
		int t,mx;
		for (int i =0;i<1000;i++)
			count[i]=picked[i]=0;
		mx=0;
		while (n--){
			cin >> t;
			for (int i=mx;i>0;i--)
				if (count[i]!=0){
					count[i+t]+=count[i];
					if (count[i+t]==1)
						picked[i+t]=picked[i]+1;
				}
			count[t]++;
			if (count[t]==1){
				picked[t]++;
			}
			mx += t;
			if (mx>m)mx=m;
		}
		// debug
		/*
		for (int i=0;i<1000;i++){
			if (count[i]!=0)
				cout << i << ' ' << count[i] << " " << picked[i] <<  endl;
		}
		*/
		// output
		cout << "Case #" << ti << ": ";
		if (count[m]==0){
			// ko tim thay
			cout << 0 << endl;
		}else if (count[m]==1){
			// tim thay
			cout << picked[m] << endl;
		}else{
			cout << -1 << endl;
		}
	}
	return 0;
}
