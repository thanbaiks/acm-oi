#include <iostream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define inp fin

using namespace std;
typedef unsigned int UI;
struct Tape{
	UI lv,rv;
	static Tape make(UI l,UI r){
		Tape tape = {l,r};
		return tape;
	}
};
vector<Tape> vleft,vright,vmid,vhybrid,vsingle;
UI n;
bool operator < (const Tape &a,const Tape &b){
	if (!a.lv){
		// not a hybrid
		return a.rv > b.rv;
	}else{
		
	}
}
int main(){
	ifstream fin("OLP_I.inp");
	inp >> n;
	char s[3];
	for (int i=0;i<n;i++){
		cin >> s;
		if (s[0]=='#' && s[1]== '#' && s[2]=='#'){
			// empty tape
			continue;
		}else if (s[0]=='#'&&s[2]=='#'){
			// single tape
			UI val = s[1]-'0';
			vsingle.push_back(Tape::make(0,val));
		}else if (s[0]=='#'){
			// left tape
			UI val = 0;
			if (s[1]!='#'){
				val = 10*(s[1]-'0');
			}
			val += s[2]-'0';
			vleft.push_back(Tape::make(0,val));
		}else if (s[2]=='#'){
			// right tape
			UI val = s[0]-'0';
			if (s[1]!='#')
				val = val*10+s[1]-'0';
			vright.push_back(Tape::make(0,val));
		}else if (s[1]=='#'){
			// hybrid
			UI v1 = s[0]-'0';
			UI v2 = s[2]-'0';
			vhybrid.push_back(Tape::make(v1,v2));
		}else{
			// normal tape
			UI val = (s[0]-'0')*100+(s[1]-'0')*10+s[2]-'0';
			vmid.push_back(Tape::make(0,val));
		}
		
		// end of input
	}
	return 0;
}
