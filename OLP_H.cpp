#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#define inp fin
#define ZERO 0.00001
typedef unsigned long long ULL;
using namespace std;
ULL n,s,tree;
long pos[3][2];
long gcd(long a, long b){
	return !b?a:gcd(b,a%b);
}
long sqr(long x){
	return x*x;
}
ULL area(){
	long long s = 0;
	for (int i=0;i<3;i++){
		int j = (i+1)%3;
		s += pos[i][0]*pos[j][1]-pos[i][1]*pos[j][0];
	}
	//cout << "S = " << abs(s)/2 << endl;
	return abs(s)/2;
}

ULL boundary(){
	ULL bnd = 0;
	for (int i=0;i<3;i++){
		int j = (i+1)%3;
		long dx = abs(pos[i][0]-pos[j][0]);
		long dy = abs(pos[i][1]-pos[j][1]);
		bnd+=gcd(dx,dy);
	}
	//cout << "Bnd = " << bnd << endl;
	return bnd;
}

bool triangle(){
	double d[3];
	for (int i=0;i<3;i++){
		int j=(i+1)%3;
		d[i] = sqrt(sqr(pos[i][0]-pos[j][0])+sqr(pos[i][1]-pos[j][1]));
		if (d[i]==0)
			return false;
	}
	sort(d,d+3);
	return d[0]+d[1]>d[2];
}

int main(){
	ifstream fin("OLP_H.inp");
	inp >> n;
	for (ULL ti=0;ti<n;ti++){
		for (int i=0;i<3;i++)
			inp >> pos[i][0] >> pos[i][1];
		// area
		// a = i + b/2 - 1
		if (triangle()){
			tree = area()-boundary()/2+1;
			cout << tree << endl;
		}else{
			cout << 0 << endl;
		}
	}
	return 0;
}
