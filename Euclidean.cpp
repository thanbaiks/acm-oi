#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

LL modInverse(LL a, LL m) {
	LL q, r, y0=0, y1=1, y=-1, m0 = m;
	while (a > 0) {
		r = m % a;
		if (!r) return (y%m0+m0)%m0;;
		q = m / a;
		y = y0 - q*y1;
		
		m = a;
		a = r;
		y0 = y1;
		y1 = y;
	}
}

LL xGCD(LL a, LL b, LL &x, LL &y) {
	if (!a) {
		x=0;y=1;return b;
	}
	LL xx, yy, t = xGCD(b%a, a, xx, yy);
	x = yy - b/a*xx;
	y = xx;
	return t;
}

LL modInverse2(LL a, LL m) {
	LL x,y;
	xGCD(a,m, x, y);
	return (x%m + m) % m;
}

int main(){
	cout << modInverse(3, 104729) << '\n';
	cout << modInverse2(3, 104729) << '\n';
}

