#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#define ZERO 0.000001
#define inp cin
using namespace std;
typedef long double ULD;
typedef unsigned long long ULL;

ULL n;
ULD block[3][3];

ULD cubeSize(){
	ULD sum=0;
	for (int i=0;i<3;i++){
		sum += block[i][0]*block[i][1]*block[i][2];
	}
	return pow(sum,(double)1/3);
}
bool eq(ULD a,ULD b){
	return abs(a-b)<ZERO;
}
bool checkSize(ULD sz){
	for (int i=0;i<3;i++){
		if (!eq(block[i][0],sz)){
			return false;
		}
	}
	if (eq(block[0][1],block[1][1])&&eq(block[1][1],block[2][1]) // 3 canh bang nhau
		&& eq(block[0][2]+block[1][2]+block[2][2],sz)){
			return true;
	}
	// check method 2 
	bool bigger[3]={false};
	bool f = false;
	ULD remain;
	for (int i=0;i<3;i++){
		if (eq(block[i][1],sz)){
			f=true;
			bigger[i]=true;
			remain = sz-block[i][2];
			break;
		}
	}
	if (!f)
		return false;
	for (int i=0;i<3;i++){
		if (bigger[i])
			continue;
		if (!eq(block[i][1],remain)&&!eq(block[i][2],remain))
			f=false;
	}
	if (!f)
		return false;
	return true;
}

int main(){
	ifstream fin("OLP_G.txt");
	inp >> n;
	for (int ti=0;ti<n;ti++){
		// input data
		for (int i=0;i<3;i++){
			inp >> block[i][0]>> block[i][1]>> block[i][2];
		}
		// check cube
		ULD sz = cubeSize();
		if (checkSize(sz)){
			// valid
			cout << "TRUE" << endl;
		}else{
			cout << "FALSE" << endl;
		}
	}
}
