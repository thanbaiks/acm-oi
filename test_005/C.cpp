#include <iostream>
using namespace std;
typedef long long LL;
LL k,n;

int main(){
	while (cin >> k){
		bool f = false;
		for (int i=1;i<=9;i++){
			n=0;
			for (int j=0;j<k;j++){
				n=(n*10+i)%k;
				if (n==0){
					f=true;
					cout << i << ' ' << j+1;
					break;
				}
			}
			if (f)break;
		}
		if (!f)
			cout << -1;
		cout << endl;
	}
	return 0;
}
