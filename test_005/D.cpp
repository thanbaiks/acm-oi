#include <iostream>
#include <cstring>
#include <fstream>
#define inp cin

using namespace std;
typedef unsigned long long ULL;
ULL n;
bool prime[10000000];
// sang nguyen to
void sangnt(){
	memset(prime,1,sizeof(prime));
	ULL i=2;
	while (i<10000000){
		if (prime[i]){
			for (int j=i*2;j<10000000;j+=i){
				prime[j]=false;
			}
		}
		i++;
	}
}
int main(){
	sangnt();
	while (cin >> n){
		if (n<8){
			cout << "Impossible." << endl;
		}else if (n%2==0){
			// even
			n-=4;
			// find 2 next prime number
			ULL i=2;
			while (i < n-1){
				if (prime[i] && prime[n-i])
					break;
				i++;
			}
			if (prime[i] && prime[n-i])
				cout << "2 2 " << i << " " << n-i << endl;
			else
				cout << "Impossible." << endl;
		}else{
			// odd
			n-=5;
			ULL i=2;
			while (i < n-1){
				if (prime[i] && prime[n-i])
					break;
				i++;
			}
			if (prime[i] && prime[n-i])
				cout << "2 3 " << i << " " << n-i << endl;
			else
				cout << "Impossible." << endl;\
		}
	}
}
