#include<stdio.h>
void solve(int k) {
    int i, j, res;
    for (i = 1; i < 10; i++) {
        for (j = 1, res = 0; j <= k; j++) {
            res = (res * 10 + i) % k;
            if (res == 0) {
                printf("%d %d\n", i, j);
                return;
            }
        }
    }
    puts("-1");
}
int main() {
#ifndef ONLINE_JUDGE
    freopen("data.in", "r", stdin);
#endif
    int k;
    while (scanf("%d", &k) != EOF) {
        solve(k);
    }
    return 0;
}
