#include <iostream>
#include <cstring>
#include <fstream>
#define inp cin

using namespace std;
typedef long long LL;

LL n;
bool f;

char readChar(istream &inp){
	char tmp=0;
	do {
		inp >> tmp;
	}while(tmp==' ' || tmp == '\t' || tmp == '\n');
	return tmp;
}

// return 0 for a empty tree
bool readTree(LL sum,bool opener,istream &inp){
	if (opener)
		readChar(inp);// skip char '('
	char c = readChar(inp);// after char (
	if (c >= '0' && c <= '9' || c == '-'){
		// not a empty node
		LL l = 0;
		bool minus = c == '-';
		if (minus)
			c = readChar(inp);
		do {
			l = l*10 + (c-'0');
			c=readChar(inp);
		}while (c>='0'&&c<='9');
		if (minus)l*=-1;
		//cout << "node = " << l << endl;
		// left tree
		bool b1 = readTree(sum+l,false,inp);
		// right tree
		// openning of right tree
		bool b2 = readTree(sum+l,true,inp);
		readChar(inp);// close ')'
		if (!b1 && !b2){
			// it's a leaf
			//cout << "Root-to-leaf => " << sum + l << endl;
			if (sum+l==n)
				f = true;
		}
		return true;
	}else{
		return false;
		// empty tree. character is )
	}
}
int main(){
	ifstream fin("B.inp");
	while (inp >> n){
		//cout << "Number is " << n << endl;
		f = false;// default not found
		readTree(0,true,inp);
		// check for f
		if (f)
			cout << "yes" << endl;
		else
			cout << "no" << endl;
	}
	return 0;
}
