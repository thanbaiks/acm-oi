#include <iostream>
#include <graphics.h>
#include <cmath>

using namespace std;

void wait(){
	while (!kbhit()){
		delay(100);
	}
}

void init(bool invert = true){
    initwindow(800,600);
    if (invert){
	    setcolor(BLACK);
	    setbkcolor(WHITE);
    }
    clearviewport();
}


void drawLineBresenham(int x1,int y1,int x2,int y2){
	if (x2==x1){
		// song song voi oy
		for (int y=y1;y<=y2;y++){
			putpixel(x1,y,getcolor());
		}
		return;
	}else if (x2 < x1){
		drawLineBresenham(x2,y2,x1,y1);
		return;
	}
	float m = (float)(y2-y1)/(x2-x1);
	m = abs(m);
	float err = 0;
	if (m <= .5){
		for (int x=x1;x<=x2;x++){
			putpixel(x,y1,getcolor());
			err+=m;
			if (err>.5){
				y1++;err-=1;
			}
		}
	}else{
		m = 1/m;
		for (int y=y1;y<=y2;y++){
			putpixel(x1,y,getcolor());
			err+=m;
			if (err>.5){
				x1++;err-=1;
			}
		}
	}
}

int main() {
	init();
	drawLineBresenham(70,10,10,300);
    wait();
    return 0 ;
}
