#include <iostream>
#include <cstdio>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <algorithm>
#define _i ::iterator
#define FOR(i,a,b) for (int (i)=(a);(i)<b;(i)++)
#define MOD 1000000000
/****************
	a       b
	c       d
****************/
using namespace std;
typedef long long LL;
struct Mat{
	LL a,b,c,d;
};
Mat mul(Mat a,Mat b){
	Mat m;
	m.a = (a.a*b.a + a.b * b.c)%MOD;
	m.b = (a.a*b.b + a.b * b.d)%MOD;
	m.c = (a.c*b.a + a.d * b.c)%MOD;
	m.d = (a.c*b.b + a.d * b.d)%MOD;
	return m;
}

LL fib(LL x){
	Mat rs = {1,1,1,0};
	Mat pow = {1,1,1,0};
	x--;
	while (x > 1){
		if (x%2!=0)
			rs = mul(rs,pow);
		pow = mul(pow,pow);
		x/=2;
	}
	rs = mul(pow,rs);
	return rs.b;
}
int main(){
	LL n;
	while (cin >> n)
		cout << fib(n) << endl;
	return 0;
}

