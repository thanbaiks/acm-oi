#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

void make(char *pat, int *lps) {
	int len = 0, i = 1, N = strlen(pat);
	lps[0] = 0;
	while (i < N) {
		if (pat[i] == pat[len]) 
			lps[i++] = ++len;
		else {
			// Not matching, fallback
			if (len) {
				len = lps[len-1];
			} else {
				lps[i++] = 0;
			}
		}
	}
	cout << "lps:\n";
	_for(i,0,N) cout << lps[i] << ' ';
	cout << endl;
}

int search(char s[], char pat[]) {
	int lps[1000];
	make(pat, lps);
	int i = 0, j = 0, N = strlen(s), M = strlen(pat);
	while (i < N) {
		if (s[i] == pat[j]) {
			i++;
			j++;
			if (j == M) return i-j;
		} else {
			if (j) {
				j = lps[j-1];
			} else {
				i++;
			}
		}
	}
	return -1;
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	char s[] = "AABAACAADAABAB324CABABABABABCABABCADEFAAABABCDEABABCD";
	char pat[] = "foobarfoo";
	cout << "Search result: " << search(s, pat);
	return 0;
}


