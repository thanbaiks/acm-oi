#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

LL moduloInverse(LL a, LL m){
	LL q,r,y0=0,y1=1,y,m0=m;
	while (a>0) {
		q = m/a;
		r = m%a;
		if (!r) return (y%m0 + m0) % m0;
		y = y0-y1*q;
		y0=y1;
		y1=y;
		m=a;
		a=r;
	}
}


LL xGCD(LL a, LL b, LL &x, LL &y) {
	if (!b) {
		x = 1; y = 0;
		return a;
	}
	LL xx,yy,t = xGCD(b,a%b,xx,yy);
	x = yy;
	y = xx - a/b*yy;;
	return t;
}



int main(){
	LL x,y,t = xGCD(101, 10008, x, y);
	x = (x+10008)%10008;
	cout << moduloInverse(101,10008) << ' ';
	cout << x;
}


