#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;

const int N = 1e7+7;
bool m[N];

void sieveEra() {
	memset(m, 0, sizeof(m));
	int i, lim = sqrt(N);
	for (i=3; i <= lim; i+=2) {
		if (!m[i]) {
			for (LL j=i*i, i2=2*i; j < N; j+=i2)
				if (!m[j]) m[j] = true;
		}
	}
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	sieveEra();
	
	int n;
	cin >> n;
	if (n==1) {
		cout << 2;
		return 0;
	}
	n-=2;
	for (int i=3;i<N;i+=2)
		if (!m[i]) {
			if (!n--) {
				cout << i;
				return 0;
			}
		}
}


