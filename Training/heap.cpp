#include <bits/stdc++.h>

#define _for(i,a,b) for (int i=(a),_b_=(b);i<_b_;i++)
#define _fod(i,a,b) for (int i=(a),_b_=(b);i>_b_;i--)
#define _it(i,v) for (typeof((v).begin()) i = (v).begin(); i != (v).end(); ++i)
#define _all(v) v.begin(), v.end()
#define __(v) memset(v,0,sizeof(v))

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;
template<typename T> vector<T> &operator += (vector<T> &v, T x) {v.push_back(x);return v;}

struct Heap {
    int *A,n;
    Heap(int maxSize) : n(0) {
        A = new int[maxSize+1];
    }
    ~Heap() {
        delete[] A;
    }
    int size() {
        return n;
    }
    
    void push(int t) {
        A[++n] = t;
        upHeap(n);
    }
    int top() {
        return A[1];
    }
    void pop() {
        swap(A[1],A[n--]);
        downHeap(1);
    }
    
    private:
        void upHeap(int i) {
            if (i==1) return;
            int p = i>>1;
            if (A[p]<A[i]) {
                swap(A[i],A[p]);
                upHeap(p);
            }
        }
        void downHeap(int i) {
            int lc = i<<1, rc = i<<1|1, j = i;
            if (lc<=n && A[j]<A[lc]) j = lc;
            if (rc<=n && A[j]<A[rc]) j = rc;
            if (j!=i) {
                swap(A[i], A[j]);
                downHeap(j);
            }
        }
};

void solve() {
    srand(12345);
    int n = 10000000;
    priority_queue<int, vector<int>, greater<int> > pq;
//    Heap heap(n);
    _for(i,0,n) {
        int r = rand();
//        heap.push(r);
        pq.push(r);
    }
    _for(i,0,n) {
//        cout << heap.top() << '\n';
        pq.pop();
//        heap.pop();
    }
}

int main(){
    #ifdef NGOBACH
//    freopen("input.txt","r",stdin);
//  freopen("output.txt","w",stdout);
    #endif
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
}


