#include <bits/stdc++.h>
#define BachNX
#define _for(i,a,b) for (int i=(a),_b_=(b),_d_=(a<b?1:-1);i!=_b_;i+=_d_)

using namespace std;
typedef long long LL;
typedef unsigned long long ULL;



const int RAB[] = {3,5,7,11,13,17}, R = sizeof(RAB)/sizeof(RAB[0]);

LL mm(LL a, LL b, LL m) {
	if (!a || !b || m==1) return 0;
	LL t = 1;
	while (b>1) {
		if (b&1) t = (t+a)%m;
		a = (a+a)%m;
		b >>= 1;
	}
	return (t+a)%m;
}

LL pm(LL a, LL e, LL m) {
	if (m==1) return 0;
	if (!e) return 1;
	LL t = 1;
	while (e > 1) {
		if (e&1) t = t*a%m;
		a = a*a%m;
		e >>= 1;
	}
	return t*a%m;
}

bool primeTest(LL n) {
	if (n==2) return true;
	if (n<2 || (n&1)==0) return false;
	
	LL m = n-1, s = 0;
	while ((m&1)==0) {
		m >>= 1;
		s++;
	}
	
	_for(i,0,R) {
		LL k = RAB[i], b = pm(k,m,n);
		if (n == k) return true;
		if (n%k == 0) return false;
		
		if (b == 1) continue;
		bool pass = false;
		_for (j,0,s) {
			if (b == n-1) {
				pass = true;
				break;
			}
			b = b*b%n;
		}
		if (!pass) return false;
	}
	return true;
}

int main(){
//	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	LL n = 0;
	_for(i,1,3000000+1) if (primeTest(i)) n++;
	cout << n;
	return 0;
}


