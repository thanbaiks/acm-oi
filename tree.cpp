#include <iostream>
#define MAX_SIZE 10000
#define NULL_NODE -1
using namespace std;

typedef int Node;
typedef char DataType;

struct Tree{
	DataType data[MAX_SIZE];
	Node parent[MAX_SIZE];
	Node size;
};
enum Mode{
	Truoc,
	Giua,
	Sau
};
void init(Tree *tree){
	tree->size = 0;
}

bool empty(Tree *tree){
	return tree->size == 0;
}

bool full(Tree *tree){
	return tree->size == MAX_SIZE - 1;
}

Node parent(Tree *tree,Node node){
	if (node > tree->size)
		return NULL_NODE;
	return tree->parent[node];
}

Node leftMostChild(Tree *tree,Node node){
	for (Node i=node+1;i < tree->size;i++)
		if (tree->parent[i] == node) return i;
	return NULL_NODE;
}

void inputTree(Tree *tree){
	int n;
	cout << "Nhap so luong node: " ;
	cin >> n;
	for (int i=0;i<n;i++){
		cout << "Nhap cha va nha~n cho node [" << i << "]: ";
		cin >> tree->parent[i] >> tree->data[i];
	}
	tree->size = n;
}

void printNode(Tree *tree,Node node){cout << node << ":{" << tree->data[node] << "} ";}

void duyet(Tree *tree,Node node, Mode mode){
	if (empty(tree))
		return;
	switch (mode){
		case Truoc:
			printNode(tree,node);
			for (Node i=node+1;i<tree->size;i++){
				if (tree->parent[i]==node)
					duyet(tree,i,mode);
			}
			break;
		case Sau:
			for (Node i=node+1;i<tree->size;i++){
				if (tree->parent[i]==node)
					duyet(tree,i,mode);
			}
			printNode(tree,node);
			break;
		case Giua:
			Node lmc = leftMostChild(tree,node);
			if (lmc != NULL_NODE)
				duyet(tree,lmc,mode);
			printNode(tree,node);
			if (lmc == NULL_NODE)
				break;
			for (Node i = lmc+1;i < tree->size;i++)
				if (tree->parent[i]==node)
					duyet(tree,i,mode);
			break;
	}
}


int main(){
	Tree *tree = new Tree();
	init(tree);
	/* Nhap vao */
	inputTree(tree);
	cout << "== Truoc ==" << endl;
	duyet(tree,0,Truoc);cout << endl << endl;
	cout << "== Giua ==" << endl;
	duyet(tree,0,Giua);cout << endl << endl;
	cout << "== Sau ==" << endl;
	duyet(tree,0,Sau);cout << endl << endl;
	return 0;
}

